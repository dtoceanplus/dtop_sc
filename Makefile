# Makefile

.PHONY: build run dev compose verify

build:
	docker build --tag sc .

run:
	docker run --detach -p 5000:5000 sc

dev:
	docker run --tty --interactive sc bash

compose:
	docker-compose up sc


verify:
	make -f ci.makefile site-deploy
	docker-compose --project-name sc -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name sc -f verifiable.docker-compose stop sc
	docker cp `docker-compose --project-name sc -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
