# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ...outputs import LocalisedTimeSeries
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class WavesTimeSeries():

    """Waves Time Series
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._hs=LocalisedTimeSeries.LocalisedTimeSeries()
        self._hs.description = 'Timeseries of significant wave height'
        self._tp=LocalisedTimeSeries.LocalisedTimeSeries()
        self._tp.description = 'Timeseries of wave peak period'
        self._dp=LocalisedTimeSeries.LocalisedTimeSeries()
        self._dp.description = 'Timeseries of wave peak direction'
        self._te=LocalisedTimeSeries.LocalisedTimeSeries()
        self._te.description = 'Timeseries of wave energy period'
        self._gamma=LocalisedTimeSeries.LocalisedTimeSeries()
        self._gamma.description = 'Timeseries of wave peak factor (JONSWAP)'
        self._spr=LocalisedTimeSeries.LocalisedTimeSeries()
        self._spr.description = 'Timeseries of wave directional spreading'
        self._CgE=LocalisedTimeSeries.LocalisedTimeSeries()
        self._CgE.description = 'Timeseries of wave energy flux'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def hs(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of significant wave height
        """
        return self._hs
    #------------
    @ property
    def tp(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of wave peak period
        """
        return self._tp
    #------------
    @ property
    def dp(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of wave peak direction
        """
        return self._dp
    #------------
    @ property
    def te(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of wave energy period
        """
        return self._te
    #------------
    @ property
    def gamma(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of wave peak factor (JONSWAP)
        """
        return self._gamma
    #------------
    @ property
    def spr(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of wave directional spreading
        """
        return self._spr
    #------------
    @ property
    def CgE(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of wave energy flux
        """
        return self._CgE
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ hs.setter
    def hs(self,val): # pragma: no cover
        self._hs=val
    #------------
    @ tp.setter
    def tp(self,val): # pragma: no cover
        self._tp=val
    #------------
    @ dp.setter
    def dp(self,val): # pragma: no cover
        self._dp=val
    #------------
    @ te.setter
    def te(self,val): # pragma: no cover
        self._te=val
    #------------
    @ gamma.setter
    def gamma(self,val): # pragma: no cover
        self._gamma=val
    #------------
    @ spr.setter
    def spr(self,val): # pragma: no cover
        self._spr=val
    #------------
    @ CgE.setter
    def CgE(self,val): # pragma: no cover
        self._CgE=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:point:outputs:WavesTimeSeries"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:point:outputs:WavesTimeSeries"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("hs"):
            if (short and not(deep)):
                rep["hs"] = self.hs.type_rep()
            else:
                rep["hs"] = self.hs.prop_rep(short, deep)
        if self.is_set("tp"):
            if (short and not(deep)):
                rep["tp"] = self.tp.type_rep()
            else:
                rep["tp"] = self.tp.prop_rep(short, deep)
        if self.is_set("dp"):
            if (short and not(deep)):
                rep["dp"] = self.dp.type_rep()
            else:
                rep["dp"] = self.dp.prop_rep(short, deep)
        if self.is_set("te"):
            if (short and not(deep)):
                rep["te"] = self.te.type_rep()
            else:
                rep["te"] = self.te.prop_rep(short, deep)
        if self.is_set("gamma"):
            if (short and not(deep)):
                rep["gamma"] = self.gamma.type_rep()
            else:
                rep["gamma"] = self.gamma.prop_rep(short, deep)
        if self.is_set("spr"):
            if (short and not(deep)):
                rep["spr"] = self.spr.type_rep()
            else:
                rep["spr"] = self.spr.prop_rep(short, deep)
        if self.is_set("CgE"):
            if (short and not(deep)):
                rep["CgE"] = self.CgE.type_rep()
            else:
                rep["CgE"] = self.CgE.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("hs")
        self.hs.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("tp")
        self.tp.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("dp")
        self.dp.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("te")
        self.te.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("gamma")
        self.gamma.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("spr")
        self.spr.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("CgE")
        self.CgE.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "hs"
        try :
            if data[varName] != None:
                self.hs=LocalisedTimeSeries.LocalisedTimeSeries()
                self.hs.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "tp"
        try :
            if data[varName] != None:
                self.tp=LocalisedTimeSeries.LocalisedTimeSeries()
                self.tp.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "dp"
        try :
            if data[varName] != None:
                self.dp=LocalisedTimeSeries.LocalisedTimeSeries()
                self.dp.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "te"
        try :
            if data[varName] != None:
                self.te=LocalisedTimeSeries.LocalisedTimeSeries()
                self.te.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "gamma"
        try :
            if data[varName] != None:
                self.gamma=LocalisedTimeSeries.LocalisedTimeSeries()
                self.gamma.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "spr"
        try :
            if data[varName] != None:
                self.spr=LocalisedTimeSeries.LocalisedTimeSeries()
                self.spr.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "CgE"
        try :
            if data[varName] != None:
                self.CgE=LocalisedTimeSeries.LocalisedTimeSeries()
                self.CgE.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("hs" in list(gr.keys())):
            subgroup = gr["hs"]
            self.hs.loadFromHDF5Handle(subgroup)
        if ("tp" in list(gr.keys())):
            subgroup = gr["tp"]
            self.tp.loadFromHDF5Handle(subgroup)
        if ("dp" in list(gr.keys())):
            subgroup = gr["dp"]
            self.dp.loadFromHDF5Handle(subgroup)
        if ("te" in list(gr.keys())):
            subgroup = gr["te"]
            self.te.loadFromHDF5Handle(subgroup)
        if ("gamma" in list(gr.keys())):
            subgroup = gr["gamma"]
            self.gamma.loadFromHDF5Handle(subgroup)
        if ("spr" in list(gr.keys())):
            subgroup = gr["spr"]
            self.spr.loadFromHDF5Handle(subgroup)
        if ("CgE" in list(gr.keys())):
            subgroup = gr["CgE"]
            self.CgE.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
