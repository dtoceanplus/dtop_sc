# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import BasicCurrents
from . import EPDCurrents
from . import EJPDCurrents
from . import EXTCurrents
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class StatsCurrents():

    """Currents Statistics
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._Basic=BasicCurrents.BasicCurrents()
        self._Basic.description = 'Basic statistics on Currents'
        self._EPD=EPDCurrents.EPDCurrents()
        self._EPD.description = 'Empirical Probability Distribution on 1 current variable'
        self._EJPD=EJPDCurrents.EJPDCurrents()
        self._EJPD.description = 'Empirical Join Probability Distribution on 2 current variables'
        self._EXT=EXTCurrents.EXTCurrents()
        self._EXT.description = 'Extreme Value Analysis on current variable'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Basic(self): # pragma: no cover
        """:obj:`~.BasicCurrents.BasicCurrents`: Basic statistics on Currents
        """
        return self._Basic
    #------------
    @ property
    def EPD(self): # pragma: no cover
        """:obj:`~.EPDCurrents.EPDCurrents`: Empirical Probability Distribution on 1 current variable
        """
        return self._EPD
    #------------
    @ property
    def EJPD(self): # pragma: no cover
        """:obj:`~.EJPDCurrents.EJPDCurrents`: Empirical Join Probability Distribution on 2 current variables
        """
        return self._EJPD
    #------------
    @ property
    def EXT(self): # pragma: no cover
        """:obj:`~.EXTCurrents.EXTCurrents`: Extreme Value Analysis on current variable
        """
        return self._EXT
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Basic.setter
    def Basic(self,val): # pragma: no cover
        self._Basic=val
    #------------
    @ EPD.setter
    def EPD(self,val): # pragma: no cover
        self._EPD=val
    #------------
    @ EJPD.setter
    def EJPD(self,val): # pragma: no cover
        self._EJPD=val
    #------------
    @ EXT.setter
    def EXT(self,val): # pragma: no cover
        self._EXT=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:point:outputs:StatsCurrents"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:point:outputs:StatsCurrents"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Basic"):
            if (short and not(deep)):
                rep["Basic"] = self.Basic.type_rep()
            else:
                rep["Basic"] = self.Basic.prop_rep(short, deep)
        if self.is_set("EPD"):
            if (short and not(deep)):
                rep["EPD"] = self.EPD.type_rep()
            else:
                rep["EPD"] = self.EPD.prop_rep(short, deep)
        if self.is_set("EJPD"):
            if (short and not(deep)):
                rep["EJPD"] = self.EJPD.type_rep()
            else:
                rep["EJPD"] = self.EJPD.prop_rep(short, deep)
        if self.is_set("EXT"):
            if (short and not(deep)):
                rep["EXT"] = self.EXT.type_rep()
            else:
                rep["EXT"] = self.EXT.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("Basic")
        self.Basic.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EPD")
        self.EPD.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EJPD")
        self.EJPD.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EXT")
        self.EXT.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "Basic"
        try :
            if data[varName] != None:
                self.Basic=BasicCurrents.BasicCurrents()
                self.Basic.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EPD"
        try :
            if data[varName] != None:
                self.EPD=EPDCurrents.EPDCurrents()
                self.EPD.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EJPD"
        try :
            if data[varName] != None:
                self.EJPD=EJPDCurrents.EJPDCurrents()
                self.EJPD.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT"
        try :
            if data[varName] != None:
                self.EXT=EXTCurrents.EXTCurrents()
                self.EXT.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("Basic" in list(gr.keys())):
            subgroup = gr["Basic"]
            self.Basic.loadFromHDF5Handle(subgroup)
        if ("EPD" in list(gr.keys())):
            subgroup = gr["EPD"]
            self.EPD.loadFromHDF5Handle(subgroup)
        if ("EJPD" in list(gr.keys())):
            subgroup = gr["EJPD"]
            self.EJPD.loadFromHDF5Handle(subgroup)
        if ("EXT" in list(gr.keys())):
            subgroup = gr["EXT"]
            self.EXT.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
