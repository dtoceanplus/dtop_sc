# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DatabasesPaths():

    """paths to the different physical databases or input files
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._LeaseArea=''
        self._Corridor=''
        self._Devices=''
        self._Bathymetry=''
        self._Seabed=''
        self._Roughness=''
        self._Species=''
        self._Waves=''
        self._Currents=''
        self._Winds=''
        self._Levels=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def LeaseArea(self): # pragma: no cover
        """str: path to the lease area shapefile (polygon)
        """
        return self._LeaseArea
    #------------
    @ property
    def Corridor(self): # pragma: no cover
        """str: path to the corridor shapefile (line)
        """
        return self._Corridor
    #------------
    @ property
    def Devices(self): # pragma: no cover
        """str: path to the devices shapefile (points)
        """
        return self._Devices
    #------------
    @ property
    def Bathymetry(self): # pragma: no cover
        """str: path to the bathymetry file
        """
        return self._Bathymetry
    #------------
    @ property
    def Seabed(self): # pragma: no cover
        """str: path to the seabed file
        """
        return self._Seabed
    #------------
    @ property
    def Roughness(self): # pragma: no cover
        """str: path to the roughness length file
        """
        return self._Roughness
    #------------
    @ property
    def Species(self): # pragma: no cover
        """str: path to the marine species file
        """
        return self._Species
    #------------
    @ property
    def Waves(self): # pragma: no cover
        """str: path to the waves file
        """
        return self._Waves
    #------------
    @ property
    def Currents(self): # pragma: no cover
        """str: path to the currents file
        """
        return self._Currents
    #------------
    @ property
    def Winds(self): # pragma: no cover
        """str: path to the winds file
        """
        return self._Winds
    #------------
    @ property
    def Levels(self): # pragma: no cover
        """str: path to the water levels file
        """
        return self._Levels
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ LeaseArea.setter
    def LeaseArea(self,val): # pragma: no cover
        self._LeaseArea=str(val)
    #------------
    @ Corridor.setter
    def Corridor(self,val): # pragma: no cover
        self._Corridor=str(val)
    #------------
    @ Devices.setter
    def Devices(self,val): # pragma: no cover
        self._Devices=str(val)
    #------------
    @ Bathymetry.setter
    def Bathymetry(self,val): # pragma: no cover
        self._Bathymetry=str(val)
    #------------
    @ Seabed.setter
    def Seabed(self,val): # pragma: no cover
        self._Seabed=str(val)
    #------------
    @ Roughness.setter
    def Roughness(self,val): # pragma: no cover
        self._Roughness=str(val)
    #------------
    @ Species.setter
    def Species(self,val): # pragma: no cover
        self._Species=str(val)
    #------------
    @ Waves.setter
    def Waves(self,val): # pragma: no cover
        self._Waves=str(val)
    #------------
    @ Currents.setter
    def Currents(self,val): # pragma: no cover
        self._Currents=str(val)
    #------------
    @ Winds.setter
    def Winds(self,val): # pragma: no cover
        self._Winds=str(val)
    #------------
    @ Levels.setter
    def Levels(self,val): # pragma: no cover
        self._Levels=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:databases:outputs:DatabasesPaths"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:databases:outputs:DatabasesPaths"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("LeaseArea"):
            rep["LeaseArea"] = self.LeaseArea
        if self.is_set("Corridor"):
            rep["Corridor"] = self.Corridor
        if self.is_set("Devices"):
            rep["Devices"] = self.Devices
        if self.is_set("Bathymetry"):
            rep["Bathymetry"] = self.Bathymetry
        if self.is_set("Seabed"):
            rep["Seabed"] = self.Seabed
        if self.is_set("Roughness"):
            rep["Roughness"] = self.Roughness
        if self.is_set("Species"):
            rep["Species"] = self.Species
        if self.is_set("Waves"):
            rep["Waves"] = self.Waves
        if self.is_set("Currents"):
            rep["Currents"] = self.Currents
        if self.is_set("Winds"):
            rep["Winds"] = self.Winds
        if self.is_set("Levels"):
            rep["Levels"] = self.Levels
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.LeaseArea.encode("ascii"))
        handle["LeaseArea"] = np.asarray(ar)
        ar = []
        ar.append(self.Corridor.encode("ascii"))
        handle["Corridor"] = np.asarray(ar)
        ar = []
        ar.append(self.Devices.encode("ascii"))
        handle["Devices"] = np.asarray(ar)
        ar = []
        ar.append(self.Bathymetry.encode("ascii"))
        handle["Bathymetry"] = np.asarray(ar)
        ar = []
        ar.append(self.Seabed.encode("ascii"))
        handle["Seabed"] = np.asarray(ar)
        ar = []
        ar.append(self.Roughness.encode("ascii"))
        handle["Roughness"] = np.asarray(ar)
        ar = []
        ar.append(self.Species.encode("ascii"))
        handle["Species"] = np.asarray(ar)
        ar = []
        ar.append(self.Waves.encode("ascii"))
        handle["Waves"] = np.asarray(ar)
        ar = []
        ar.append(self.Currents.encode("ascii"))
        handle["Currents"] = np.asarray(ar)
        ar = []
        ar.append(self.Winds.encode("ascii"))
        handle["Winds"] = np.asarray(ar)
        ar = []
        ar.append(self.Levels.encode("ascii"))
        handle["Levels"] = np.asarray(ar)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "LeaseArea"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Corridor"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Devices"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Bathymetry"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Roughness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Species"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Waves"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Currents"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Winds"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Levels"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("LeaseArea" in list(gr.keys())):
            self.LeaseArea = gr["LeaseArea"][0].decode("ascii")
        if ("Corridor" in list(gr.keys())):
            self.Corridor = gr["Corridor"][0].decode("ascii")
        if ("Devices" in list(gr.keys())):
            self.Devices = gr["Devices"][0].decode("ascii")
        if ("Bathymetry" in list(gr.keys())):
            self.Bathymetry = gr["Bathymetry"][0].decode("ascii")
        if ("Seabed" in list(gr.keys())):
            self.Seabed = gr["Seabed"][0].decode("ascii")
        if ("Roughness" in list(gr.keys())):
            self.Roughness = gr["Roughness"][0].decode("ascii")
        if ("Species" in list(gr.keys())):
            self.Species = gr["Species"][0].decode("ascii")
        if ("Waves" in list(gr.keys())):
            self.Waves = gr["Waves"][0].decode("ascii")
        if ("Currents" in list(gr.keys())):
            self.Currents = gr["Currents"][0].decode("ascii")
        if ("Winds" in list(gr.keys())):
            self.Winds = gr["Winds"][0].decode("ascii")
        if ("Levels" in list(gr.keys())):
            self.Levels = gr["Levels"][0].decode("ascii")
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
