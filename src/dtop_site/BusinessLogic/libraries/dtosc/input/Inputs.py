# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Inputs():

    """Inputs of the module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._complexity_level=0
        self._run_mode='standalone'
        self._lease=''
        self._corr=''
        self._wave=''
        self._current=''
        self._bathymetry=''
        self._seabed=''
        self._roughness=''
        self._species=''
        self._ts=''
        self._ts2=''
        self._selected_lease=''
        self._lease_file_name=np.zeros(shape=(1), dtype=str)
        self._selected_corridor=''
        self._corridor_file_name=np.zeros(shape=(1), dtype=str)
        self._selected_seabed=''
        self._seabed_file_name=''
        self._selected_roughness=''
        self._roughness_file_name=''
        self._selected_species=''
        self._species_file_name=''
        self._selected_ts=''
        self._selected_ts2=''
        self._ts_file_name=''
        self._ts_file_name2=''
        self._uniform_bathy=False
        self._selected_bathy=''
        self._bathy_file_name=''
        self._entity_id=''
        self._entity_status=0
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def complexity_level(self): # pragma: no cover
        """str: complexity level []
        """
        return self._complexity_level
    #------------
    @ property
    def run_mode(self): # pragma: no cover
        """str: run mode
        """
        return self._run_mode
    #------------
    @ property
    def lease(self): # pragma: no cover
        """str: full path to lease area file
        """
        return self._lease
    #------------
    @ property
    def corr(self): # pragma: no cover
        """str: full path to corridor file
        """
        return self._corr
    #------------
    @ property
    def wave(self): # pragma: no cover
        """str: wave level of energy (used only for complexity level 1 and 2)
        """
        return self._wave
    #------------
    @ property
    def current(self): # pragma: no cover
        """str: current level of energy (used only for complexity level 1 and 2)
        """
        return self._current
    #------------
    @ property
    def bathymetry(self): # pragma: no cover
        """str: full path to bathymetry file or value of the constant depth
        """
        return self._bathymetry
    #------------
    @ property
    def seabed(self): # pragma: no cover
        """str: full path to seabed file
        """
        return self._seabed
    #------------
    @ property
    def roughness(self): # pragma: no cover
        """str: full path to roughness file
        """
        return self._roughness
    #------------
    @ property
    def species(self): # pragma: no cover
        """str: full path to species file
        """
        return self._species
    #------------
    @ property
    def ts(self): # pragma: no cover
        """str: full path to time series file
        """
        return self._ts
    #------------
    @ property
    def ts2(self): # pragma: no cover
        """str: full path to 2D time series file
        """
        return self._ts2
    #------------
    @ property
    def selected_lease(self): # pragma: no cover
        """str: option selected in the lease area file selector (only used for complexity level 3)
        """
        return self._selected_lease
    #------------
    @ property
    def lease_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the lease area file selector in the GUI (only used for complexity level 3)
        """
        return self._lease_file_name
    #------------
    @ property
    def selected_corridor(self): # pragma: no cover
        """str: option selected in the corridor file selector (only used for complexity level 3)
        """
        return self._selected_corridor
    #------------
    @ property
    def corridor_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the corridor file selector in the GUI (only used for complexity level 3)
        """
        return self._corridor_file_name
    #------------
    @ property
    def selected_seabed(self): # pragma: no cover
        """str: option selected in the seabed file selector (only used for complexity level 3)
        """
        return self._selected_seabed
    #------------
    @ property
    def seabed_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the seabed file selector in the GUI (only used for complexity level 3)
        """
        return self._seabed_file_name
    #------------
    @ property
    def selected_roughness(self): # pragma: no cover
        """str: option selected in the roughness file selector (only used for complexity level 3)
        """
        return self._selected_roughness
    #------------
    @ property
    def roughness_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the roughness file selector in the GUI (only used for complexity level 3)
        """
        return self._roughness_file_name
    #------------
    @ property
    def selected_species(self): # pragma: no cover
        """str: option selected in the species file selector (only used for complexity level 3)
        """
        return self._selected_species
    #------------
    @ property
    def species_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the species file selector in the GUI (only used for complexity level 3)
        """
        return self._species_file_name
    #------------
    @ property
    def selected_ts(self): # pragma: no cover
        """str: option selected in the time series file selector (only used for complexity level 3)
        """
        return self._selected_ts
    #------------
    @ property
    def selected_ts2(self): # pragma: no cover
        """str: option selected in the time series file selector for 2D file (only used for complexity level 3)
        """
        return self._selected_ts2
    #------------
    @ property
    def ts_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the time series file selector in the GUI (only used for complexity level 3)
        """
        return self._ts_file_name
    #------------
    @ property
    def ts_file_name2(self): # pragma: no cover
        """str: file name corresponding to the selected option in the time series file for 2D file selector in the GUI (only used for complexity level 3)
        """
        return self._ts_file_name2
    #------------
    @ property
    def uniform_bathy(self): # pragma: no cover
        """bool: option to define if the bathymetry is uniform or not
        """
        return self._uniform_bathy
    #------------
    @ property
    def selected_bathy(self): # pragma: no cover
        """str: option selected in the bathymetry file selector (only used for complexity level 3)
        """
        return self._selected_bathy
    #------------
    @ property
    def bathy_file_name(self): # pragma: no cover
        """str: file name corresponding to the selected option in the bathymetry file selector in the GUI (only used for complexity level 3)
        """
        return self._bathy_file_name
    #------------
    @ property
    def entity_id(self): # pragma: no cover
        return self._entity_id
    #------------
    @ property
    def entity_status(self): # pragma: no cover
        return self._entity_status
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ complexity_level.setter
    def complexity_level(self,val): # pragma: no cover
        self._complexity_level=val
    #------------
    @ run_mode.setter
    def run_mode(self,val): # pragma: no cover
        self._run_mode=str(val)
    #------------
    @ lease.setter
    def lease(self,val): # pragma: no cover
        self._lease=val
    #------------
    @ corr.setter
    def corr(self,val): # pragma: no cover
        self._corr=val
    #------------
    @ wave.setter
    def wave(self,val): # pragma: no cover
        self._wave=val
    #------------
    @ current.setter
    def current(self,val): # pragma: no cover
        self._current=val
    #------------
    @ bathymetry.setter
    def bathymetry(self,val): # pragma: no cover
        self._bathymetry=val
    #------------
    @ seabed.setter
    def seabed(self,val): # pragma: no cover
        self._seabed=val
    #------------
    @ roughness.setter
    def roughness(self,val): # pragma: no cover
        self._roughness=val
    #------------
    @ species.setter
    def species(self,val): # pragma: no cover
        self._species=val
    #------------
    @ ts.setter
    def ts(self,val): # pragma: no cover
        self._ts=val
    #------------
    @ ts2.setter
    def ts2(self,val): # pragma: no cover
        self._ts2=val
    #------------
    @ selected_lease.setter
    def selected_lease(self,val): # pragma: no cover
        self._selected_lease=val
    #------------
    @ lease_file_name.setter
    def lease_file_name(self,val): # pragma: no cover
        self._lease_file_name=val
    #------------
    @ selected_corridor.setter
    def selected_corridor(self,val): # pragma: no cover
        self._selected_corridor=val
    #------------
    @ corridor_file_name.setter
    def corridor_file_name(self,val): # pragma: no cover
        self._corridor_file_name=val
    #------------
    @ selected_seabed.setter
    def selected_seabed(self,val): # pragma: no cover
        self._selected_seabed=val
    #------------
    @ seabed_file_name.setter
    def seabed_file_name(self,val): # pragma: no cover
        self._seabed_file_name=val
    #------------
    @ selected_roughness.setter
    def selected_roughness(self,val): # pragma: no cover
        self._selected_roughness=val
    #------------
    @ roughness_file_name.setter
    def roughness_file_name(self,val): # pragma: no cover
        self._roughness_file_name=val
    #------------
    @ selected_species.setter
    def selected_species(self,val): # pragma: no cover
        self._selected_species=val
    #------------
    @ species_file_name.setter
    def species_file_name(self,val): # pragma: no cover
        self._species_file_name=val
    #------------
    @ selected_ts.setter
    def selected_ts(self,val): # pragma: no cover
        self._selected_ts=val
    #------------
    @ selected_ts2.setter
    def selected_ts2(self,val): # pragma: no cover
        self._selected_ts2=val
    #------------
    @ ts_file_name.setter
    def ts_file_name(self,val): # pragma: no cover
        self._ts_file_name=val
    #------------
    @ ts_file_name2.setter
    def ts_file_name2(self,val): # pragma: no cover
        self._ts_file_name2=val
    #------------
    @ uniform_bathy.setter
    def uniform_bathy(self,val): # pragma: no cover
        self._uniform_bathy=val
    #------------
    @ selected_bathy.setter
    def selected_bathy(self,val): # pragma: no cover
        self._selected_bathy=val
    #------------
    @ bathy_file_name.setter
    def bathy_file_name(self,val): # pragma: no cover
        self._bathy_file_name=val
    #------------
    @ entity_id.setter
    def entity_id(self,val): # pragma: no cover
        self._entity_id=val
    #------------
    @ entity_status.setter
    def entity_status(self,val): # pragma: no cover
        self._entity_status=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:inputs:Inputs"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:inputs:Inputs"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("complexity_level"):
            rep["complexity_level"] = self.complexity_level
        if self.is_set("run_mode"):
            rep["run_mode"] = self.run_mode
        if self.is_set("lease"):
            rep["lease"] = self.lease
        if self.is_set("corr"):
            rep["corr"] = self.corr
        if self.is_set("wave"):
            rep["wave"] = self.wave
        if self.is_set("current"):
            rep["current"] = self.current
        if self.is_set("bathymetry"):
            rep["bathymetry"] = self.bathymetry
        if self.is_set("seabed"):
            rep["seabed"] = self.seabed
        if self.is_set("roughness"):
            rep["roughness"] = self.roughness
        if self.is_set("species"):
            rep["species"] = self.species
        if self.is_set("ts"):
            rep["ts"] = self.ts
        if self.is_set("ts2"):
            rep["ts2"] = self.ts2
        if self.is_set("selected_lease"):
            rep["selected_lease"] = self.selected_lease
        if self.is_set("lease_file_name"):
            if (short):
                rep["lease_file_name"] = str(self.lease_file_name.shape)
            else:
                try:
                    rep["lease_file_name"] = self.lease_file_name.tolist()
                except:
                    rep["lease_file_name"] = self.lease_file_name
        if self.is_set("selected_corridor"):
            rep["selected_corridor"] = self.selected_corridor
        if self.is_set("corridor_file_name"):
            if (short):
                rep["corridor_file_name"] = str(self.corridor_file_name.shape)
            else:
                try:
                    rep["corridor_file_name"] = self.corridor_file_name.tolist()
                except:
                    rep["corridor_file_name"] = self.corridor_file_name
        if self.is_set("selected_seabed"):
            rep["selected_seabed"] = self.selected_seabed
        if self.is_set("seabed_file_name"):
            rep["seabed_file_name"] = self.seabed_file_name
        if self.is_set("selected_roughness"):
            rep["selected_roughness"] = self.selected_roughness
        if self.is_set("roughness_file_name"):
            rep["roughness_file_name"] = self.roughness_file_name
        if self.is_set("selected_species"):
            rep["selected_species"] = self.selected_species
        if self.is_set("species_file_name"):
            rep["species_file_name"] = self.species_file_name
        if self.is_set("selected_ts"):
            rep["selected_ts"] = self.selected_ts
        if self.is_set("selected_ts2"):
            rep["selected_ts2"] = self.selected_ts2
        if self.is_set("ts_file_name"):
            rep["ts_file_name"] = self.ts_file_name
        if self.is_set("ts_file_name2"):
            rep["ts_file_name2"] = self.ts_file_name2
        if self.is_set("uniform_bathy"):
            rep["uniform_bathy"] = self.uniform_bathy
        if self.is_set("selected_bathy"):
            rep["selected_bathy"] = self.selected_bathy
        if self.is_set("bathy_file_name"):
            rep["bathy_file_name"] = self.bathy_file_name
        if self.is_set("entity_id"):
            rep["entity_id"] = self.entity_id
        if self.is_set("entity_status"):
            rep["entity_status"] = self.entity_status
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "complexity_level"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "run_mode"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "lease"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "corr"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "wave"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "current"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "bathymetry"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "roughness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "species"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ts"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ts2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "selected_lease"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "lease_file_name"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "selected_corridor"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "corridor_file_name"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "selected_seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "selected_roughness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "roughness_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "selected_species"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "species_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "selected_ts"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "selected_ts2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ts_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ts_file_name2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uniform_bathy"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "selected_bathy"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "bathy_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "entity_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "entity_status"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
