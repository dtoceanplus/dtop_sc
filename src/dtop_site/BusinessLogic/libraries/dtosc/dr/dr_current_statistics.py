# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..outputs import EPD
from ..outputs import EPDMonthly
from ..outputs import EJPD
from ..outputs import EJPDMonthly
from ..outputs import EXT
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_current_statistics():

    """Current statistics digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._mag_mean=0.0
        self._mag_min=0.0
        self._mag_max=0.0
        self._Flux_mean=0.0
        self._Flux_min=0.0
        self._Flux_max=0.0
        self._EPD_mag=EPD.EPD()
        self._EPD_mag.description = 'Empirical Probability Distribution on current magnitude'
        self._Monthly_EPD_mag=EPDMonthly.EPDMonthly()
        self._Monthly_EPD_mag.description = 'Monthly Empirical Probability Distribution on current magnitude'
        self._EPD_theta=EPD.EPD()
        self._EPD_theta.description = 'Empirical Probability Distribution on current direction'
        self._Monthly_EPD_theta=EPDMonthly.EPDMonthly()
        self._Monthly_EPD_theta.description = 'Monthly Empirical Probability Distribution on current direction'
        self._EJPD_mag_theta=EJPD.EJPD()
        self._EJPD_mag_theta.description = 'Empirical Join Probability Distribution on current magnitude and direction'
        self._Monthly_EJPD_mag_theta=EJPDMonthly.EJPDMonthly()
        self._Monthly_EJPD_mag_theta.description = 'Monthly Empirical Join Probability Distribution on current magnitude and direction'
        self._EXT_mag=EXT.EXT()
        self._EXT_mag.description = 'EXtreme value analysis on current magnitude'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def mag_mean(self): # pragma: no cover
        """float: mean value of current magnitude [m/s] []
        """
        return self._mag_mean
    #------------
    @ property
    def mag_min(self): # pragma: no cover
        """float: minimum value of current magnitude [m/s] []
        """
        return self._mag_min
    #------------
    @ property
    def mag_max(self): # pragma: no cover
        """float: maximum value of current magnitude [m/s] []
        """
        return self._mag_max
    #------------
    @ property
    def Flux_mean(self): # pragma: no cover
        """float: mean value of current energy flux [kW/m^2] []
        """
        return self._Flux_mean
    #------------
    @ property
    def Flux_min(self): # pragma: no cover
        """float: minimum value of current energy flux [kW/m^2] []
        """
        return self._Flux_min
    #------------
    @ property
    def Flux_max(self): # pragma: no cover
        """float: maximum value of current energy flux [kW/m^2] []
        """
        return self._Flux_max
    #------------
    @ property
    def EPD_mag(self): # pragma: no cover
        """:obj:`~.EPD.EPD`: Empirical Probability Distribution on current magnitude
        """
        return self._EPD_mag
    #------------
    @ property
    def Monthly_EPD_mag(self): # pragma: no cover
        """:obj:`~.EPDMonthly.EPDMonthly`: Monthly Empirical Probability Distribution on current magnitude
        """
        return self._Monthly_EPD_mag
    #------------
    @ property
    def EPD_theta(self): # pragma: no cover
        """:obj:`~.EPD.EPD`: Empirical Probability Distribution on current direction
        """
        return self._EPD_theta
    #------------
    @ property
    def Monthly_EPD_theta(self): # pragma: no cover
        """:obj:`~.EPDMonthly.EPDMonthly`: Monthly Empirical Probability Distribution on current direction
        """
        return self._Monthly_EPD_theta
    #------------
    @ property
    def EJPD_mag_theta(self): # pragma: no cover
        """:obj:`~.EJPD.EJPD`: Empirical Join Probability Distribution on current magnitude and direction
        """
        return self._EJPD_mag_theta
    #------------
    @ property
    def Monthly_EJPD_mag_theta(self): # pragma: no cover
        """:obj:`~.EJPDMonthly.EJPDMonthly`: Monthly Empirical Join Probability Distribution on current magnitude and direction
        """
        return self._Monthly_EJPD_mag_theta
    #------------
    @ property
    def EXT_mag(self): # pragma: no cover
        """:obj:`~.EXT.EXT`: EXtreme value analysis on current magnitude
        """
        return self._EXT_mag
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ mag_mean.setter
    def mag_mean(self,val): # pragma: no cover
        self._mag_mean=float(val)
    #------------
    @ mag_min.setter
    def mag_min(self,val): # pragma: no cover
        self._mag_min=float(val)
    #------------
    @ mag_max.setter
    def mag_max(self,val): # pragma: no cover
        self._mag_max=float(val)
    #------------
    @ Flux_mean.setter
    def Flux_mean(self,val): # pragma: no cover
        self._Flux_mean=float(val)
    #------------
    @ Flux_min.setter
    def Flux_min(self,val): # pragma: no cover
        self._Flux_min=float(val)
    #------------
    @ Flux_max.setter
    def Flux_max(self,val): # pragma: no cover
        self._Flux_max=float(val)
    #------------
    @ EPD_mag.setter
    def EPD_mag(self,val): # pragma: no cover
        self._EPD_mag=val
    #------------
    @ Monthly_EPD_mag.setter
    def Monthly_EPD_mag(self,val): # pragma: no cover
        self._Monthly_EPD_mag=val
    #------------
    @ EPD_theta.setter
    def EPD_theta(self,val): # pragma: no cover
        self._EPD_theta=val
    #------------
    @ Monthly_EPD_theta.setter
    def Monthly_EPD_theta(self,val): # pragma: no cover
        self._Monthly_EPD_theta=val
    #------------
    @ EJPD_mag_theta.setter
    def EJPD_mag_theta(self,val): # pragma: no cover
        self._EJPD_mag_theta=val
    #------------
    @ Monthly_EJPD_mag_theta.setter
    def Monthly_EJPD_mag_theta(self,val): # pragma: no cover
        self._Monthly_EJPD_mag_theta=val
    #------------
    @ EXT_mag.setter
    def EXT_mag(self,val): # pragma: no cover
        self._EXT_mag=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_current_statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_current_statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("mag_mean"):
            rep["mag_mean"] = self.mag_mean
        if self.is_set("mag_min"):
            rep["mag_min"] = self.mag_min
        if self.is_set("mag_max"):
            rep["mag_max"] = self.mag_max
        if self.is_set("Flux_mean"):
            rep["Flux_mean"] = self.Flux_mean
        if self.is_set("Flux_min"):
            rep["Flux_min"] = self.Flux_min
        if self.is_set("Flux_max"):
            rep["Flux_max"] = self.Flux_max
        if self.is_set("EPD_mag"):
            if (short and not(deep)):
                rep["EPD_mag"] = self.EPD_mag.type_rep()
            else:
                rep["EPD_mag"] = self.EPD_mag.prop_rep(short, deep)
        if self.is_set("Monthly_EPD_mag"):
            if (short and not(deep)):
                rep["Monthly_EPD_mag"] = self.Monthly_EPD_mag.type_rep()
            else:
                rep["Monthly_EPD_mag"] = self.Monthly_EPD_mag.prop_rep(short, deep)
        if self.is_set("EPD_theta"):
            if (short and not(deep)):
                rep["EPD_theta"] = self.EPD_theta.type_rep()
            else:
                rep["EPD_theta"] = self.EPD_theta.prop_rep(short, deep)
        if self.is_set("Monthly_EPD_theta"):
            if (short and not(deep)):
                rep["Monthly_EPD_theta"] = self.Monthly_EPD_theta.type_rep()
            else:
                rep["Monthly_EPD_theta"] = self.Monthly_EPD_theta.prop_rep(short, deep)
        if self.is_set("EJPD_mag_theta"):
            if (short and not(deep)):
                rep["EJPD_mag_theta"] = self.EJPD_mag_theta.type_rep()
            else:
                rep["EJPD_mag_theta"] = self.EJPD_mag_theta.prop_rep(short, deep)
        if self.is_set("Monthly_EJPD_mag_theta"):
            if (short and not(deep)):
                rep["Monthly_EJPD_mag_theta"] = self.Monthly_EJPD_mag_theta.type_rep()
            else:
                rep["Monthly_EJPD_mag_theta"] = self.Monthly_EJPD_mag_theta.prop_rep(short, deep)
        if self.is_set("EXT_mag"):
            if (short and not(deep)):
                rep["EXT_mag"] = self.EXT_mag.type_rep()
            else:
                rep["EXT_mag"] = self.EXT_mag.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["mag_mean"] = np.array([self.mag_mean],dtype=float)
        handle["mag_min"] = np.array([self.mag_min],dtype=float)
        handle["mag_max"] = np.array([self.mag_max],dtype=float)
        handle["Flux_mean"] = np.array([self.Flux_mean],dtype=float)
        handle["Flux_min"] = np.array([self.Flux_min],dtype=float)
        handle["Flux_max"] = np.array([self.Flux_max],dtype=float)
        subgroup = handle.create_group("EPD_mag")
        self.EPD_mag.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EPD_mag")
        self.Monthly_EPD_mag.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EPD_theta")
        self.EPD_theta.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EPD_theta")
        self.Monthly_EPD_theta.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EJPD_mag_theta")
        self.EJPD_mag_theta.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EJPD_mag_theta")
        self.Monthly_EJPD_mag_theta.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EXT_mag")
        self.EXT_mag.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "mag_mean"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mag_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mag_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Flux_mean"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Flux_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Flux_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "EPD_mag"
        try :
            if data[varName] != None:
                self.EPD_mag=EPD.EPD()
                self.EPD_mag.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EPD_mag"
        try :
            if data[varName] != None:
                self.Monthly_EPD_mag=EPDMonthly.EPDMonthly()
                self.Monthly_EPD_mag.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EPD_theta"
        try :
            if data[varName] != None:
                self.EPD_theta=EPD.EPD()
                self.EPD_theta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EPD_theta"
        try :
            if data[varName] != None:
                self.Monthly_EPD_theta=EPDMonthly.EPDMonthly()
                self.Monthly_EPD_theta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EJPD_mag_theta"
        try :
            if data[varName] != None:
                self.EJPD_mag_theta=EJPD.EJPD()
                self.EJPD_mag_theta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EJPD_mag_theta"
        try :
            if data[varName] != None:
                self.Monthly_EJPD_mag_theta=EJPDMonthly.EJPDMonthly()
                self.Monthly_EJPD_mag_theta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT_mag"
        try :
            if data[varName] != None:
                self.EXT_mag=EXT.EXT()
                self.EXT_mag.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("mag_mean" in list(gr.keys())):
            self.mag_mean = gr["mag_mean"][0]
        if ("mag_min" in list(gr.keys())):
            self.mag_min = gr["mag_min"][0]
        if ("mag_max" in list(gr.keys())):
            self.mag_max = gr["mag_max"][0]
        if ("Flux_mean" in list(gr.keys())):
            self.Flux_mean = gr["Flux_mean"][0]
        if ("Flux_min" in list(gr.keys())):
            self.Flux_min = gr["Flux_min"][0]
        if ("Flux_max" in list(gr.keys())):
            self.Flux_max = gr["Flux_max"][0]
        if ("EPD_mag" in list(gr.keys())):
            subgroup = gr["EPD_mag"]
            self.EPD_mag.loadFromHDF5Handle(subgroup)
        if ("Monthly_EPD_mag" in list(gr.keys())):
            subgroup = gr["Monthly_EPD_mag"]
            self.Monthly_EPD_mag.loadFromHDF5Handle(subgroup)
        if ("EPD_theta" in list(gr.keys())):
            subgroup = gr["EPD_theta"]
            self.EPD_theta.loadFromHDF5Handle(subgroup)
        if ("Monthly_EPD_theta" in list(gr.keys())):
            subgroup = gr["Monthly_EPD_theta"]
            self.Monthly_EPD_theta.loadFromHDF5Handle(subgroup)
        if ("EJPD_mag_theta" in list(gr.keys())):
            subgroup = gr["EJPD_mag_theta"]
            self.EJPD_mag_theta.loadFromHDF5Handle(subgroup)
        if ("Monthly_EJPD_mag_theta" in list(gr.keys())):
            subgroup = gr["Monthly_EJPD_mag_theta"]
            self.Monthly_EJPD_mag_theta.loadFromHDF5Handle(subgroup)
        if ("EXT_mag" in list(gr.keys())):
            subgroup = gr["EXT_mag"]
            self.EXT_mag.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
