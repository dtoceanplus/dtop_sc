# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..outputs import EPD
from ..outputs import EPDMonthly
from ..outputs import EXT
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_sea_level_statistics():

    """Sea level statistics digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._WLEV_mean=0.0
        self._WLEV_min=0.0
        self._WLEV_max=0.0
        self._EPD_WLEV=EPD.EPD()
        self._EPD_WLEV.description = 'Empirical Probability Distribution on water level height'
        self._Monthly_EPD_WLEV=EPDMonthly.EPDMonthly()
        self._Monthly_EPD_WLEV.description = 'Monthly Empirical Probability Distribution on water level height'
        self._EXT_WLEV_negative=EXT.EXT()
        self._EXT_WLEV_negative.description = 'EXtreme value analysis on the lower values of water height'
        self._EXT_WLEV_positive=EXT.EXT()
        self._EXT_WLEV_positive.description = 'EXtreme value analysis on the upper values of water height'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def WLEV_mean(self): # pragma: no cover
        """float: mean value of water level [m] []
        """
        return self._WLEV_mean
    #------------
    @ property
    def WLEV_min(self): # pragma: no cover
        """float: minimum value of water level [m] []
        """
        return self._WLEV_min
    #------------
    @ property
    def WLEV_max(self): # pragma: no cover
        """float: maximum value of water level [m] []
        """
        return self._WLEV_max
    #------------
    @ property
    def EPD_WLEV(self): # pragma: no cover
        """:obj:`~.EPD.EPD`: Empirical Probability Distribution on water level height
        """
        return self._EPD_WLEV
    #------------
    @ property
    def Monthly_EPD_WLEV(self): # pragma: no cover
        """:obj:`~.EPDMonthly.EPDMonthly`: Monthly Empirical Probability Distribution on water level height
        """
        return self._Monthly_EPD_WLEV
    #------------
    @ property
    def EXT_WLEV_negative(self): # pragma: no cover
        """:obj:`~.EXT.EXT`: EXtreme value analysis on the lower values of water height
        """
        return self._EXT_WLEV_negative
    #------------
    @ property
    def EXT_WLEV_positive(self): # pragma: no cover
        """:obj:`~.EXT.EXT`: EXtreme value analysis on the upper values of water height
        """
        return self._EXT_WLEV_positive
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ WLEV_mean.setter
    def WLEV_mean(self,val): # pragma: no cover
        self._WLEV_mean=float(val)
    #------------
    @ WLEV_min.setter
    def WLEV_min(self,val): # pragma: no cover
        self._WLEV_min=float(val)
    #------------
    @ WLEV_max.setter
    def WLEV_max(self,val): # pragma: no cover
        self._WLEV_max=float(val)
    #------------
    @ EPD_WLEV.setter
    def EPD_WLEV(self,val): # pragma: no cover
        self._EPD_WLEV=val
    #------------
    @ Monthly_EPD_WLEV.setter
    def Monthly_EPD_WLEV(self,val): # pragma: no cover
        self._Monthly_EPD_WLEV=val
    #------------
    @ EXT_WLEV_negative.setter
    def EXT_WLEV_negative(self,val): # pragma: no cover
        self._EXT_WLEV_negative=val
    #------------
    @ EXT_WLEV_positive.setter
    def EXT_WLEV_positive(self,val): # pragma: no cover
        self._EXT_WLEV_positive=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_sea_level_statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_sea_level_statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("WLEV_mean"):
            rep["WLEV_mean"] = self.WLEV_mean
        if self.is_set("WLEV_min"):
            rep["WLEV_min"] = self.WLEV_min
        if self.is_set("WLEV_max"):
            rep["WLEV_max"] = self.WLEV_max
        if self.is_set("EPD_WLEV"):
            if (short and not(deep)):
                rep["EPD_WLEV"] = self.EPD_WLEV.type_rep()
            else:
                rep["EPD_WLEV"] = self.EPD_WLEV.prop_rep(short, deep)
        if self.is_set("Monthly_EPD_WLEV"):
            if (short and not(deep)):
                rep["Monthly_EPD_WLEV"] = self.Monthly_EPD_WLEV.type_rep()
            else:
                rep["Monthly_EPD_WLEV"] = self.Monthly_EPD_WLEV.prop_rep(short, deep)
        if self.is_set("EXT_WLEV_negative"):
            if (short and not(deep)):
                rep["EXT_WLEV_negative"] = self.EXT_WLEV_negative.type_rep()
            else:
                rep["EXT_WLEV_negative"] = self.EXT_WLEV_negative.prop_rep(short, deep)
        if self.is_set("EXT_WLEV_positive"):
            if (short and not(deep)):
                rep["EXT_WLEV_positive"] = self.EXT_WLEV_positive.type_rep()
            else:
                rep["EXT_WLEV_positive"] = self.EXT_WLEV_positive.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["WLEV_mean"] = np.array([self.WLEV_mean],dtype=float)
        handle["WLEV_min"] = np.array([self.WLEV_min],dtype=float)
        handle["WLEV_max"] = np.array([self.WLEV_max],dtype=float)
        subgroup = handle.create_group("EPD_WLEV")
        self.EPD_WLEV.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EPD_WLEV")
        self.Monthly_EPD_WLEV.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EXT_WLEV_negative")
        self.EXT_WLEV_negative.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EXT_WLEV_positive")
        self.EXT_WLEV_positive.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "WLEV_mean"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "WLEV_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "WLEV_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "EPD_WLEV"
        try :
            if data[varName] != None:
                self.EPD_WLEV=EPD.EPD()
                self.EPD_WLEV.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EPD_WLEV"
        try :
            if data[varName] != None:
                self.Monthly_EPD_WLEV=EPDMonthly.EPDMonthly()
                self.Monthly_EPD_WLEV.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT_WLEV_negative"
        try :
            if data[varName] != None:
                self.EXT_WLEV_negative=EXT.EXT()
                self.EXT_WLEV_negative.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT_WLEV_positive"
        try :
            if data[varName] != None:
                self.EXT_WLEV_positive=EXT.EXT()
                self.EXT_WLEV_positive.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("WLEV_mean" in list(gr.keys())):
            self.WLEV_mean = gr["WLEV_mean"][0]
        if ("WLEV_min" in list(gr.keys())):
            self.WLEV_min = gr["WLEV_min"][0]
        if ("WLEV_max" in list(gr.keys())):
            self.WLEV_max = gr["WLEV_max"][0]
        if ("EPD_WLEV" in list(gr.keys())):
            subgroup = gr["EPD_WLEV"]
            self.EPD_WLEV.loadFromHDF5Handle(subgroup)
        if ("Monthly_EPD_WLEV" in list(gr.keys())):
            subgroup = gr["Monthly_EPD_WLEV"]
            self.Monthly_EPD_WLEV.loadFromHDF5Handle(subgroup)
        if ("EXT_WLEV_negative" in list(gr.keys())):
            subgroup = gr["EXT_WLEV_negative"]
            self.EXT_WLEV_negative.loadFromHDF5Handle(subgroup)
        if ("EXT_WLEV_positive" in list(gr.keys())):
            subgroup = gr["EXT_WLEV_positive"]
            self.EXT_WLEV_positive.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
