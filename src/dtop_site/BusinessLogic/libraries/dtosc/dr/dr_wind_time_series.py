# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..outputs import LocalisedTimeSeries
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_wind_time_series():

    """Winds time series digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._mag10=LocalisedTimeSeries.LocalisedTimeSeries()
        self._mag10.description = 'Timeseries of 10m-wind magnitude [m/s]'
        self._theta10=LocalisedTimeSeries.LocalisedTimeSeries()
        self._theta10.description = 'Timeseries of 10m-wind direction [deg]'
        self._U10=LocalisedTimeSeries.LocalisedTimeSeries()
        self._U10.description = 'Timeseries of 10m-wind zonal velocity [m/s]'
        self._V10=LocalisedTimeSeries.LocalisedTimeSeries()
        self._V10.description = 'Timeseries of 10m-wind meridional velocity [m/s]'
        self._gust10=LocalisedTimeSeries.LocalisedTimeSeries()
        self._gust10.description = 'Timeseries of 10m-wind gust [m/s]'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def mag10(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of 10m-wind magnitude [m/s]
        """
        return self._mag10
    #------------
    @ property
    def theta10(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of 10m-wind direction [deg]
        """
        return self._theta10
    #------------
    @ property
    def U10(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of 10m-wind zonal velocity [m/s]
        """
        return self._U10
    #------------
    @ property
    def V10(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of 10m-wind meridional velocity [m/s]
        """
        return self._V10
    #------------
    @ property
    def gust10(self): # pragma: no cover
        """:obj:`~.LocalisedTimeSeries.LocalisedTimeSeries`: Timeseries of 10m-wind gust [m/s]
        """
        return self._gust10
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ mag10.setter
    def mag10(self,val): # pragma: no cover
        self._mag10=val
    #------------
    @ theta10.setter
    def theta10(self,val): # pragma: no cover
        self._theta10=val
    #------------
    @ U10.setter
    def U10(self,val): # pragma: no cover
        self._U10=val
    #------------
    @ V10.setter
    def V10(self,val): # pragma: no cover
        self._V10=val
    #------------
    @ gust10.setter
    def gust10(self,val): # pragma: no cover
        self._gust10=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_wind_time_series"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_wind_time_series"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("mag10"):
            if (short and not(deep)):
                rep["mag10"] = self.mag10.type_rep()
            else:
                rep["mag10"] = self.mag10.prop_rep(short, deep)
        if self.is_set("theta10"):
            if (short and not(deep)):
                rep["theta10"] = self.theta10.type_rep()
            else:
                rep["theta10"] = self.theta10.prop_rep(short, deep)
        if self.is_set("U10"):
            if (short and not(deep)):
                rep["U10"] = self.U10.type_rep()
            else:
                rep["U10"] = self.U10.prop_rep(short, deep)
        if self.is_set("V10"):
            if (short and not(deep)):
                rep["V10"] = self.V10.type_rep()
            else:
                rep["V10"] = self.V10.prop_rep(short, deep)
        if self.is_set("gust10"):
            if (short and not(deep)):
                rep["gust10"] = self.gust10.type_rep()
            else:
                rep["gust10"] = self.gust10.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("mag10")
        self.mag10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("theta10")
        self.theta10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("U10")
        self.U10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("V10")
        self.V10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("gust10")
        self.gust10.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "mag10"
        try :
            if data[varName] != None:
                self.mag10=LocalisedTimeSeries.LocalisedTimeSeries()
                self.mag10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "theta10"
        try :
            if data[varName] != None:
                self.theta10=LocalisedTimeSeries.LocalisedTimeSeries()
                self.theta10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "U10"
        try :
            if data[varName] != None:
                self.U10=LocalisedTimeSeries.LocalisedTimeSeries()
                self.U10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "V10"
        try :
            if data[varName] != None:
                self.V10=LocalisedTimeSeries.LocalisedTimeSeries()
                self.V10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "gust10"
        try :
            if data[varName] != None:
                self.gust10=LocalisedTimeSeries.LocalisedTimeSeries()
                self.gust10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("mag10" in list(gr.keys())):
            subgroup = gr["mag10"]
            self.mag10.loadFromHDF5Handle(subgroup)
        if ("theta10" in list(gr.keys())):
            subgroup = gr["theta10"]
            self.theta10.loadFromHDF5Handle(subgroup)
        if ("U10" in list(gr.keys())):
            subgroup = gr["U10"]
            self.U10.loadFromHDF5Handle(subgroup)
        if ("V10" in list(gr.keys())):
            subgroup = gr["V10"]
            self.V10.loadFromHDF5Handle(subgroup)
        if ("gust10" in list(gr.keys())):
            subgroup = gr["gust10"]
            self.gust10.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
