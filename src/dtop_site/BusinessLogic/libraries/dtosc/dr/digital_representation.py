# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import dr_environment
#------------------------------------
# @ USER DEFINED IMPORTS START
from dtop_site.BusinessLogic.libraries.dtosc.outputs import Project
# @ USER DEFINED IMPORTS END
#------------------------------------

class digital_representation():

    """SC project results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._environment=dr_environment.dr_environment()
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self, Results):
        """Store the digital representation (storage).
        """
        ## Ressource group

        ### Sea level statistics
        self.environment.resource.statistics.sea_level.WLEV_mean = Results.point.statistics.water_levels.Basic.WLEV.mean
        self.environment.resource.statistics.sea_level.WLEV_min = Results.point.statistics.water_levels.Basic.WLEV.min
        self.environment.resource.statistics.sea_level.WLEV_max = Results.point.statistics.water_levels.Basic.WLEV.max
        self.environment.resource.statistics.sea_level.EPD_WLEV = Results.point.statistics.water_levels.EPD.WLEV
        self.environment.resource.statistics.sea_level.Monthly_EPD_WLEV = Results.point.statistics.water_levels.EPD.WLEV_monthly
        self.environment.resource.statistics.sea_level.EXT_WLEV_negative = Results.point.statistics.water_levels.EXT.WLEVnegative
        self.environment.resource.statistics.sea_level.EXT_WLEV_positive = Results.point.statistics.water_levels.EXT.WLEVpositive

        ### Waves statistics
        self.environment.resource.statistics.waves.hs_mean = Results.point.statistics.waves.Basic.hs.mean
        self.environment.resource.statistics.waves.hs_min = Results.point.statistics.waves.Basic.hs.min
        self.environment.resource.statistics.waves.hs_max = Results.point.statistics.waves.Basic.hs.max
        self.environment.resource.statistics.waves.tp_mean = Results.point.statistics.waves.Basic.tp.mean
        self.environment.resource.statistics.waves.tp_min = Results.point.statistics.waves.Basic.tp.min
        self.environment.resource.statistics.waves.tp_max = Results.point.statistics.waves.Basic.tp.max
        self.environment.resource.statistics.waves.CgE_mean = Results.point.statistics.waves.Basic.CgE.mean
        self.environment.resource.statistics.waves.CgE_min = Results.point.statistics.waves.Basic.CgE.min
        self.environment.resource.statistics.waves.CgE_max = Results.point.statistics.waves.Basic.CgE.max
        self.environment.resource.statistics.waves.gamma_mean = Results.point.statistics.waves.Basic.gamma.mean
        self.environment.resource.statistics.waves.gamma_min = Results.point.statistics.waves.Basic.gamma.min
        self.environment.resource.statistics.waves.gamma_max = Results.point.statistics.waves.Basic.gamma.max
        self.environment.resource.statistics.waves.EPD_hs = Results.point.statistics.waves.EPD.hs
        self.environment.resource.statistics.waves.Monthly_EPD_hs = Results.point.statistics.waves.EPD.hs_monthly
        self.environment.resource.statistics.waves.EJPD_hs_tp = Results.point.statistics.waves.EJPD.hs_tp
        self.environment.resource.statistics.waves.Monthly_EJPD_hs_tp = Results.point.statistics.waves.EJPD.hs_tp_monthly
        self.environment.resource.statistics.waves.EJPD_hs_dp = Results.point.statistics.waves.EJPD.hs_dp
        self.environment.resource.statistics.waves.Monthly_EJPD_hs_dp = Results.point.statistics.waves.EJPD.hs_dp_monthly
        self.environment.resource.statistics.waves.EJPD3v_hs_dp_tp = Results.point.statistics.waves.EJPD3v.hs_tp_dp
        self.environment.resource.statistics.waves.EXT_hs = Results.point.statistics.waves.EXT.hs
        self.environment.resource.statistics.waves.EXT_tp = Results.point.statistics.waves.EXT.tp
        self.environment.resource.statistics.waves.EXC_hs_tp = Results.point.statistics.waves.EXC.hs_tp

        ### Currents statistics
        self.environment.resource.statistics.current.mag_mean = Results.point.statistics.currents.Basic.mag.mean
        self.environment.resource.statistics.current.mag_min = Results.point.statistics.currents.Basic.mag.min
        self.environment.resource.statistics.current.mag_max = Results.point.statistics.currents.Basic.mag.max
        self.environment.resource.statistics.current.Flux_mean = Results.point.statistics.currents.Basic.Flux.mean
        self.environment.resource.statistics.current.Flux_min = Results.point.statistics.currents.Basic.Flux.min
        self.environment.resource.statistics.current.Flux_max = Results.point.statistics.currents.Basic.Flux.max
        self.environment.resource.statistics.current.EPD_mag = Results.point.statistics.currents.EPD.mag
        self.environment.resource.statistics.current.Monthly_EPD_mag = Results.point.statistics.currents.EPD.mag_monthly
        self.environment.resource.statistics.current.EPD_theta = Results.point.statistics.currents.EPD.theta
        self.environment.resource.statistics.current.Monthly_EPD_theta = Results.point.statistics.currents.EPD.theta_monthly
        self.environment.resource.statistics.current.EJPD_mag_theta = Results.point.statistics.currents.EJPD.mag_theta
        self.environment.resource.statistics.current.Monthly_EJPD_mag_theta = Results.point.statistics.currents.EJPD.mag_theta_monthly
        self.environment.resource.statistics.current.EXT_mag = Results.point.statistics.currents.EXT.mag

        ### Winds statistics
        self.environment.resource.statistics.wind.mag10_mean = Results.point.statistics.winds.Basic.mag10.mean
        self.environment.resource.statistics.wind.mag10_min = Results.point.statistics.winds.Basic.mag10.min
        self.environment.resource.statistics.wind.mag10_max = Results.point.statistics.winds.Basic.mag10.max
        self.environment.resource.statistics.wind.EPD_mag10 = Results.point.statistics.winds.EPD.mag10
        self.environment.resource.statistics.wind.Monthly_EPD_mag10 = Results.point.statistics.winds.EPD.mag10_monthly
        self.environment.resource.statistics.wind.EPD_theta10 = Results.point.statistics.winds.EPD.theta10
        self.environment.resource.statistics.wind.Monthly_EPD_theta10 = Results.point.statistics.winds.EPD.theta10_monthly
        self.environment.resource.statistics.wind.EJPD_mag10_theta10 = Results.point.statistics.winds.EJPD.mag10_theta10
        self.environment.resource.statistics.wind.Monthly_EJPD_mag10_theta10 = Results.point.statistics.winds.EJPD.mag10_theta10_monthly
        self.environment.resource.statistics.wind.EXT_mag10 = Results.point.statistics.winds.EXT.mag10
        self.environment.resource.statistics.wind.EXT_gust10 = Results.point.statistics.winds.EXT.gust10

        ### Sea level time series
        self.environment.resource.time_series.sea_level.XE = Results.point.time_series.water_levels.XE
        self.environment.resource.time_series.sea_level.WLEV = Results.point.time_series.water_levels.WLEV

        ### Waves time series
        self.environment.resource.time_series.waves.hs = Results.point.time_series.waves.hs
        self.environment.resource.time_series.waves.tp = Results.point.time_series.waves.tp
        self.environment.resource.time_series.waves.dp = Results.point.time_series.waves.dp
        self.environment.resource.time_series.waves.te = Results.point.time_series.waves.te
        self.environment.resource.time_series.waves.gamma = Results.point.time_series.waves.gamma
        self.environment.resource.time_series.waves.CgE = Results.point.time_series.waves.CgE

        ### Currents time series
        self.environment.resource.time_series.current.mag = Results.point.time_series.currents.mag
        self.environment.resource.time_series.current.theta = Results.point.time_series.currents.theta
        self.environment.resource.time_series.current.U = Results.point.time_series.currents.U
        self.environment.resource.time_series.current.V = Results.point.time_series.currents.V
        self.environment.resource.time_series.current.Flux = Results.point.time_series.currents.Flux

        ### Winds time series
        self.environment.resource.time_series.wind.mag10 = Results.point.time_series.winds.mag10
        self.environment.resource.time_series.wind.theta10 = Results.point.time_series.winds.theta10
        self.environment.resource.time_series.wind.U10 = Results.point.time_series.winds.U10
        self.environment.resource.time_series.wind.V10 = Results.point.time_series.winds.V10
        self.environment.resource.time_series.wind.gust10 = Results.point.time_series.winds.gust10

        ### Winds time series
        self.environment.resource.time_series.wind.mag10 = Results.point.time_series.winds.mag10
        self.environment.resource.time_series.wind.theta10 = Results.point.time_series.winds.theta10
        self.environment.resource.time_series.wind.U10 = Results.point.time_series.winds.U10
        self.environment.resource.time_series.wind.V10 = Results.point.time_series.winds.V10
        self.environment.resource.time_series.wind.gust10 = Results.point.time_series.winds.gust10

        ## Seabed properties group

        ### Bathymetry data
        self.environment.seabed_properties.bathymetry.lease_area.bathymetry_lease_area = Results.farm.direct_values.bathymetry
        self.environment.seabed_properties.bathymetry.corridor.bathymetry_corridor = Results.corridor.direct_values.bathymetry

        ### Soil properties data
        self.environment.seabed_properties.soil_properties.seabed_type = Results.farm.direct_values.seabed_type
        self.environment.seabed_properties.soil_properties.roughness_length = Results.farm.direct_values.roughness_length

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def environment(self): # pragma: no cover
        """:obj:`~.dr_environment.dr_environment`: none
        """
        return self._environment
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ environment.setter
    def environment(self,val): # pragma: no cover
        self._environment=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:digital_representation"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:digital_representation"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("environment"):
            if (short and not(deep)):
                rep["environment"] = self.environment.type_rep()
            else:
                rep["environment"] = self.environment.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("environment")
        self.environment.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "environment"
        try :
            if data[varName] != None:
                self.environment=dr_environment.dr_environment()
                self.environment.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("environment" in list(gr.keys())):
            subgroup = gr["environment"]
            self.environment.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
