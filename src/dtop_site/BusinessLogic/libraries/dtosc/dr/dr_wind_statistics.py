# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..outputs import EPD
from ..outputs import EPDMonthly
from ..outputs import EJPD
from ..outputs import EJPDMonthly
from ..outputs import EXT
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class dr_wind_statistics():

    """Winds statistics digital representation
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._mag10_mean=0.0
        self._mag10_min=0.0
        self._mag10_max=0.0
        self._EPD_mag10=EPD.EPD()
        self._EPD_mag10.description = 'Empirical Probability Distribution on 10m-wind magnitude'
        self._Monthly_EPD_mag10=EPDMonthly.EPDMonthly()
        self._Monthly_EPD_mag10.description = 'Monthly Empirical Probability Distribution on 10m-wind magnitude'
        self._EPD_theta10=EPD.EPD()
        self._EPD_theta10.description = 'Empirical Probability Distribution on 10m-wind direction'
        self._Monthly_EPD_theta10=EPDMonthly.EPDMonthly()
        self._Monthly_EPD_theta10.description = 'Monthly Empirical Probability Distribution on 10m-wind direction'
        self._EJPD_mag10_theta10=EJPD.EJPD()
        self._EJPD_mag10_theta10.description = 'Empirical Join Probability Distribution on 10m-wind magnitude and direction'
        self._Monthly_EJPD_mag10_theta10=EJPDMonthly.EJPDMonthly()
        self._Monthly_EJPD_mag10_theta10.description = 'Monthly Empirical Join Probability Distribution on 10m-wind magnitude and direction'
        self._EXT_mag10=EXT.EXT()
        self._EXT_mag10.description = 'EXtreme value analysis on 10m-wind magnitude'
        self._EXT_gust10=EXT.EXT()
        self._EXT_gust10.description = 'EXtreme value analysis on 10m-wind gust'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def mag10_mean(self): # pragma: no cover
        """float: mean value of 10m-wind magnitude [m/s] []
        """
        return self._mag10_mean
    #------------
    @ property
    def mag10_min(self): # pragma: no cover
        """float: minimum value of 10m-wind magnitude [m/s] []
        """
        return self._mag10_min
    #------------
    @ property
    def mag10_max(self): # pragma: no cover
        """float: maximum value of 10m-wind magnitude [m/s] []
        """
        return self._mag10_max
    #------------
    @ property
    def EPD_mag10(self): # pragma: no cover
        """:obj:`~.EPD.EPD`: Empirical Probability Distribution on 10m-wind magnitude
        """
        return self._EPD_mag10
    #------------
    @ property
    def Monthly_EPD_mag10(self): # pragma: no cover
        """:obj:`~.EPDMonthly.EPDMonthly`: Monthly Empirical Probability Distribution on 10m-wind magnitude
        """
        return self._Monthly_EPD_mag10
    #------------
    @ property
    def EPD_theta10(self): # pragma: no cover
        """:obj:`~.EPD.EPD`: Empirical Probability Distribution on 10m-wind direction
        """
        return self._EPD_theta10
    #------------
    @ property
    def Monthly_EPD_theta10(self): # pragma: no cover
        """:obj:`~.EPDMonthly.EPDMonthly`: Monthly Empirical Probability Distribution on 10m-wind direction
        """
        return self._Monthly_EPD_theta10
    #------------
    @ property
    def EJPD_mag10_theta10(self): # pragma: no cover
        """:obj:`~.EJPD.EJPD`: Empirical Join Probability Distribution on 10m-wind magnitude and direction
        """
        return self._EJPD_mag10_theta10
    #------------
    @ property
    def Monthly_EJPD_mag10_theta10(self): # pragma: no cover
        """:obj:`~.EJPDMonthly.EJPDMonthly`: Monthly Empirical Join Probability Distribution on 10m-wind magnitude and direction
        """
        return self._Monthly_EJPD_mag10_theta10
    #------------
    @ property
    def EXT_mag10(self): # pragma: no cover
        """:obj:`~.EXT.EXT`: EXtreme value analysis on 10m-wind magnitude
        """
        return self._EXT_mag10
    #------------
    @ property
    def EXT_gust10(self): # pragma: no cover
        """:obj:`~.EXT.EXT`: EXtreme value analysis on 10m-wind gust
        """
        return self._EXT_gust10
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ mag10_mean.setter
    def mag10_mean(self,val): # pragma: no cover
        self._mag10_mean=float(val)
    #------------
    @ mag10_min.setter
    def mag10_min(self,val): # pragma: no cover
        self._mag10_min=float(val)
    #------------
    @ mag10_max.setter
    def mag10_max(self,val): # pragma: no cover
        self._mag10_max=float(val)
    #------------
    @ EPD_mag10.setter
    def EPD_mag10(self,val): # pragma: no cover
        self._EPD_mag10=val
    #------------
    @ Monthly_EPD_mag10.setter
    def Monthly_EPD_mag10(self,val): # pragma: no cover
        self._Monthly_EPD_mag10=val
    #------------
    @ EPD_theta10.setter
    def EPD_theta10(self,val): # pragma: no cover
        self._EPD_theta10=val
    #------------
    @ Monthly_EPD_theta10.setter
    def Monthly_EPD_theta10(self,val): # pragma: no cover
        self._Monthly_EPD_theta10=val
    #------------
    @ EJPD_mag10_theta10.setter
    def EJPD_mag10_theta10(self,val): # pragma: no cover
        self._EJPD_mag10_theta10=val
    #------------
    @ Monthly_EJPD_mag10_theta10.setter
    def Monthly_EJPD_mag10_theta10(self,val): # pragma: no cover
        self._Monthly_EJPD_mag10_theta10=val
    #------------
    @ EXT_mag10.setter
    def EXT_mag10(self,val): # pragma: no cover
        self._EXT_mag10=val
    #------------
    @ EXT_gust10.setter
    def EXT_gust10(self,val): # pragma: no cover
        self._EXT_gust10=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_wind_statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:dr:dr_wind_statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("mag10_mean"):
            rep["mag10_mean"] = self.mag10_mean
        if self.is_set("mag10_min"):
            rep["mag10_min"] = self.mag10_min
        if self.is_set("mag10_max"):
            rep["mag10_max"] = self.mag10_max
        if self.is_set("EPD_mag10"):
            if (short and not(deep)):
                rep["EPD_mag10"] = self.EPD_mag10.type_rep()
            else:
                rep["EPD_mag10"] = self.EPD_mag10.prop_rep(short, deep)
        if self.is_set("Monthly_EPD_mag10"):
            if (short and not(deep)):
                rep["Monthly_EPD_mag10"] = self.Monthly_EPD_mag10.type_rep()
            else:
                rep["Monthly_EPD_mag10"] = self.Monthly_EPD_mag10.prop_rep(short, deep)
        if self.is_set("EPD_theta10"):
            if (short and not(deep)):
                rep["EPD_theta10"] = self.EPD_theta10.type_rep()
            else:
                rep["EPD_theta10"] = self.EPD_theta10.prop_rep(short, deep)
        if self.is_set("Monthly_EPD_theta10"):
            if (short and not(deep)):
                rep["Monthly_EPD_theta10"] = self.Monthly_EPD_theta10.type_rep()
            else:
                rep["Monthly_EPD_theta10"] = self.Monthly_EPD_theta10.prop_rep(short, deep)
        if self.is_set("EJPD_mag10_theta10"):
            if (short and not(deep)):
                rep["EJPD_mag10_theta10"] = self.EJPD_mag10_theta10.type_rep()
            else:
                rep["EJPD_mag10_theta10"] = self.EJPD_mag10_theta10.prop_rep(short, deep)
        if self.is_set("Monthly_EJPD_mag10_theta10"):
            if (short and not(deep)):
                rep["Monthly_EJPD_mag10_theta10"] = self.Monthly_EJPD_mag10_theta10.type_rep()
            else:
                rep["Monthly_EJPD_mag10_theta10"] = self.Monthly_EJPD_mag10_theta10.prop_rep(short, deep)
        if self.is_set("EXT_mag10"):
            if (short and not(deep)):
                rep["EXT_mag10"] = self.EXT_mag10.type_rep()
            else:
                rep["EXT_mag10"] = self.EXT_mag10.prop_rep(short, deep)
        if self.is_set("EXT_gust10"):
            if (short and not(deep)):
                rep["EXT_gust10"] = self.EXT_gust10.type_rep()
            else:
                rep["EXT_gust10"] = self.EXT_gust10.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["mag10_mean"] = np.array([self.mag10_mean],dtype=float)
        handle["mag10_min"] = np.array([self.mag10_min],dtype=float)
        handle["mag10_max"] = np.array([self.mag10_max],dtype=float)
        subgroup = handle.create_group("EPD_mag10")
        self.EPD_mag10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EPD_mag10")
        self.Monthly_EPD_mag10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EPD_theta10")
        self.EPD_theta10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EPD_theta10")
        self.Monthly_EPD_theta10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EJPD_mag10_theta10")
        self.EJPD_mag10_theta10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("Monthly_EJPD_mag10_theta10")
        self.Monthly_EJPD_mag10_theta10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EXT_mag10")
        self.EXT_mag10.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("EXT_gust10")
        self.EXT_gust10.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "mag10_mean"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mag10_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mag10_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "EPD_mag10"
        try :
            if data[varName] != None:
                self.EPD_mag10=EPD.EPD()
                self.EPD_mag10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EPD_mag10"
        try :
            if data[varName] != None:
                self.Monthly_EPD_mag10=EPDMonthly.EPDMonthly()
                self.Monthly_EPD_mag10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EPD_theta10"
        try :
            if data[varName] != None:
                self.EPD_theta10=EPD.EPD()
                self.EPD_theta10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EPD_theta10"
        try :
            if data[varName] != None:
                self.Monthly_EPD_theta10=EPDMonthly.EPDMonthly()
                self.Monthly_EPD_theta10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EJPD_mag10_theta10"
        try :
            if data[varName] != None:
                self.EJPD_mag10_theta10=EJPD.EJPD()
                self.EJPD_mag10_theta10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Monthly_EJPD_mag10_theta10"
        try :
            if data[varName] != None:
                self.Monthly_EJPD_mag10_theta10=EJPDMonthly.EJPDMonthly()
                self.Monthly_EJPD_mag10_theta10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT_mag10"
        try :
            if data[varName] != None:
                self.EXT_mag10=EXT.EXT()
                self.EXT_mag10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT_gust10"
        try :
            if data[varName] != None:
                self.EXT_gust10=EXT.EXT()
                self.EXT_gust10.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("mag10_mean" in list(gr.keys())):
            self.mag10_mean = gr["mag10_mean"][0]
        if ("mag10_min" in list(gr.keys())):
            self.mag10_min = gr["mag10_min"][0]
        if ("mag10_max" in list(gr.keys())):
            self.mag10_max = gr["mag10_max"][0]
        if ("EPD_mag10" in list(gr.keys())):
            subgroup = gr["EPD_mag10"]
            self.EPD_mag10.loadFromHDF5Handle(subgroup)
        if ("Monthly_EPD_mag10" in list(gr.keys())):
            subgroup = gr["Monthly_EPD_mag10"]
            self.Monthly_EPD_mag10.loadFromHDF5Handle(subgroup)
        if ("EPD_theta10" in list(gr.keys())):
            subgroup = gr["EPD_theta10"]
            self.EPD_theta10.loadFromHDF5Handle(subgroup)
        if ("Monthly_EPD_theta10" in list(gr.keys())):
            subgroup = gr["Monthly_EPD_theta10"]
            self.Monthly_EPD_theta10.loadFromHDF5Handle(subgroup)
        if ("EJPD_mag10_theta10" in list(gr.keys())):
            subgroup = gr["EJPD_mag10_theta10"]
            self.EJPD_mag10_theta10.loadFromHDF5Handle(subgroup)
        if ("Monthly_EJPD_mag10_theta10" in list(gr.keys())):
            subgroup = gr["Monthly_EJPD_mag10_theta10"]
            self.Monthly_EJPD_mag10_theta10.loadFromHDF5Handle(subgroup)
        if ("EXT_mag10" in list(gr.keys())):
            subgroup = gr["EXT_mag10"]
            self.EXT_mag10.loadFromHDF5Handle(subgroup)
        if ("EXT_gust10" in list(gr.keys())):
            subgroup = gr["EXT_gust10"]
            self.EXT_gust10.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
