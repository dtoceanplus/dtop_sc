# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class MonthResults():

    """Results per months
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._January=np.zeros(shape=(1), dtype=float)
        self._February=np.zeros(shape=(1), dtype=float)
        self._March=np.zeros(shape=(1), dtype=float)
        self._April=np.zeros(shape=(1), dtype=float)
        self._May=np.zeros(shape=(1), dtype=float)
        self._June=np.zeros(shape=(1), dtype=float)
        self._July=np.zeros(shape=(1), dtype=float)
        self._August=np.zeros(shape=(1), dtype=float)
        self._September=np.zeros(shape=(1), dtype=float)
        self._October=np.zeros(shape=(1), dtype=float)
        self._November=np.zeros(shape=(1), dtype=float)
        self._December=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def January(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for January dim(*) []
        """
        return self._January
    #------------
    @ property
    def February(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for February dim(*) []
        """
        return self._February
    #------------
    @ property
    def March(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for March dim(*) []
        """
        return self._March
    #------------
    @ property
    def April(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for April dim(*) []
        """
        return self._April
    #------------
    @ property
    def May(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for May dim(*) []
        """
        return self._May
    #------------
    @ property
    def June(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for June dim(*) []
        """
        return self._June
    #------------
    @ property
    def July(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for July dim(*) []
        """
        return self._July
    #------------
    @ property
    def August(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for August dim(*) []
        """
        return self._August
    #------------
    @ property
    def September(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for September dim(*) []
        """
        return self._September
    #------------
    @ property
    def October(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for October dim(*) []
        """
        return self._October
    #------------
    @ property
    def November(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for November dim(*) []
        """
        return self._November
    #------------
    @ property
    def December(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Results for December dim(*) []
        """
        return self._December
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ January.setter
    def January(self,val): # pragma: no cover
        self._January=val
    #------------
    @ February.setter
    def February(self,val): # pragma: no cover
        self._February=val
    #------------
    @ March.setter
    def March(self,val): # pragma: no cover
        self._March=val
    #------------
    @ April.setter
    def April(self,val): # pragma: no cover
        self._April=val
    #------------
    @ May.setter
    def May(self,val): # pragma: no cover
        self._May=val
    #------------
    @ June.setter
    def June(self,val): # pragma: no cover
        self._June=val
    #------------
    @ July.setter
    def July(self,val): # pragma: no cover
        self._July=val
    #------------
    @ August.setter
    def August(self,val): # pragma: no cover
        self._August=val
    #------------
    @ September.setter
    def September(self,val): # pragma: no cover
        self._September=val
    #------------
    @ October.setter
    def October(self,val): # pragma: no cover
        self._October=val
    #------------
    @ November.setter
    def November(self,val): # pragma: no cover
        self._November=val
    #------------
    @ December.setter
    def December(self,val): # pragma: no cover
        self._December=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:MonthResults"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:MonthResults"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("January"):
            if (short):
                rep["January"] = str(self.January.shape)
            else:
                try:
                    rep["January"] = self.January.tolist()
                except:
                    rep["January"] = self.January
        if self.is_set("February"):
            if (short):
                rep["February"] = str(self.February.shape)
            else:
                try:
                    rep["February"] = self.February.tolist()
                except:
                    rep["February"] = self.February
        if self.is_set("March"):
            if (short):
                rep["March"] = str(self.March.shape)
            else:
                try:
                    rep["March"] = self.March.tolist()
                except:
                    rep["March"] = self.March
        if self.is_set("April"):
            if (short):
                rep["April"] = str(self.April.shape)
            else:
                try:
                    rep["April"] = self.April.tolist()
                except:
                    rep["April"] = self.April
        if self.is_set("May"):
            if (short):
                rep["May"] = str(self.May.shape)
            else:
                try:
                    rep["May"] = self.May.tolist()
                except:
                    rep["May"] = self.May
        if self.is_set("June"):
            if (short):
                rep["June"] = str(self.June.shape)
            else:
                try:
                    rep["June"] = self.June.tolist()
                except:
                    rep["June"] = self.June
        if self.is_set("July"):
            if (short):
                rep["July"] = str(self.July.shape)
            else:
                try:
                    rep["July"] = self.July.tolist()
                except:
                    rep["July"] = self.July
        if self.is_set("August"):
            if (short):
                rep["August"] = str(self.August.shape)
            else:
                try:
                    rep["August"] = self.August.tolist()
                except:
                    rep["August"] = self.August
        if self.is_set("September"):
            if (short):
                rep["September"] = str(self.September.shape)
            else:
                try:
                    rep["September"] = self.September.tolist()
                except:
                    rep["September"] = self.September
        if self.is_set("October"):
            if (short):
                rep["October"] = str(self.October.shape)
            else:
                try:
                    rep["October"] = self.October.tolist()
                except:
                    rep["October"] = self.October
        if self.is_set("November"):
            if (short):
                rep["November"] = str(self.November.shape)
            else:
                try:
                    rep["November"] = self.November.tolist()
                except:
                    rep["November"] = self.November
        if self.is_set("December"):
            if (short):
                rep["December"] = str(self.December.shape)
            else:
                try:
                    rep["December"] = self.December.tolist()
                except:
                    rep["December"] = self.December
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["January"] = np.array(self.January,dtype=float)
        handle["February"] = np.array(self.February,dtype=float)
        handle["March"] = np.array(self.March,dtype=float)
        handle["April"] = np.array(self.April,dtype=float)
        handle["May"] = np.array(self.May,dtype=float)
        handle["June"] = np.array(self.June,dtype=float)
        handle["July"] = np.array(self.July,dtype=float)
        handle["August"] = np.array(self.August,dtype=float)
        handle["September"] = np.array(self.September,dtype=float)
        handle["October"] = np.array(self.October,dtype=float)
        handle["November"] = np.array(self.November,dtype=float)
        handle["December"] = np.array(self.December,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "January"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "February"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "March"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "April"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "May"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "June"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "July"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "August"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "September"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "October"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "November"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "December"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("January" in list(gr.keys())):
            self.January = gr["January"][:]
        if ("February" in list(gr.keys())):
            self.February = gr["February"][:]
        if ("March" in list(gr.keys())):
            self.March = gr["March"][:]
        if ("April" in list(gr.keys())):
            self.April = gr["April"][:]
        if ("May" in list(gr.keys())):
            self.May = gr["May"][:]
        if ("June" in list(gr.keys())):
            self.June = gr["June"][:]
        if ("July" in list(gr.keys())):
            self.July = gr["July"][:]
        if ("August" in list(gr.keys())):
            self.August = gr["August"][:]
        if ("September" in list(gr.keys())):
            self.September = gr["September"][:]
        if ("October" in list(gr.keys())):
            self.October = gr["October"][:]
        if ("November" in list(gr.keys())):
            self.November = gr["November"][:]
        if ("December" in list(gr.keys())):
            self.December = gr["December"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
