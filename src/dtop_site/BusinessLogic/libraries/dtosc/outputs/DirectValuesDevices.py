# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from . import BathymetryDevices
from . import SlopeDevices
from . import SeabedTypeDevices
from . import RoughnessLengthDevices
from . import MarineSpeciesDevices
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DirectValuesDevices():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._bathymetry=BathymetryDevices.BathymetryDevices()
        self._slope=SlopeDevices.SlopeDevices()
        self._seabed_type=SeabedTypeDevices.SeabedTypeDevices()
        self._roughness_length=RoughnessLengthDevices.RoughnessLengthDevices()
        self._marine_species=MarineSpeciesDevices.MarineSpeciesDevices()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def bathymetry(self): # pragma: no cover
        return self._bathymetry
    #------------
    @ property
    def slope(self): # pragma: no cover
        return self._slope
    #------------
    @ property
    def seabed_type(self): # pragma: no cover
        return self._seabed_type
    #------------
    @ property
    def roughness_length(self): # pragma: no cover
        return self._roughness_length
    #------------
    @ property
    def marine_species(self): # pragma: no cover
        return self._marine_species
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ bathymetry.setter
    def bathymetry(self,val): # pragma: no cover
        self._bathymetry=val
    #------------
    @ slope.setter
    def slope(self,val): # pragma: no cover
        self._slope=val
    #------------
    @ seabed_type.setter
    def seabed_type(self,val): # pragma: no cover
        self._seabed_type=val
    #------------
    @ roughness_length.setter
    def roughness_length(self,val): # pragma: no cover
        self._roughness_length=val
    #------------
    @ marine_species.setter
    def marine_species(self,val): # pragma: no cover
        self._marine_species=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:DirectValuesDevices"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:DirectValuesDevices"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("bathymetry"):
            if (short and not(deep)):
                rep["bathymetry"] = self.bathymetry.type_rep()
            else:
                rep["bathymetry"] = self.bathymetry.prop_rep(short, deep)
        if self.is_set("slope"):
            if (short and not(deep)):
                rep["slope"] = self.slope.type_rep()
            else:
                rep["slope"] = self.slope.prop_rep(short, deep)
        if self.is_set("seabed_type"):
            if (short and not(deep)):
                rep["seabed_type"] = self.seabed_type.type_rep()
            else:
                rep["seabed_type"] = self.seabed_type.prop_rep(short, deep)
        if self.is_set("roughness_length"):
            if (short and not(deep)):
                rep["roughness_length"] = self.roughness_length.type_rep()
            else:
                rep["roughness_length"] = self.roughness_length.prop_rep(short, deep)
        if self.is_set("marine_species"):
            if (short and not(deep)):
                rep["marine_species"] = self.marine_species.type_rep()
            else:
                rep["marine_species"] = self.marine_species.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "bathymetry"
        try :
            if data[varName] != None:
                self.bathymetry=BathymetryDevices.BathymetryDevices()
                self.bathymetry.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "slope"
        try :
            if data[varName] != None:
                self.slope=SlopeDevices.SlopeDevices()
                self.slope.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "seabed_type"
        try :
            if data[varName] != None:
                self.seabed_type=SeabedTypeDevices.SeabedTypeDevices()
                self.seabed_type.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "roughness_length"
        try :
            if data[varName] != None:
                self.roughness_length=RoughnessLengthDevices.RoughnessLengthDevices()
                self.roughness_length.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "marine_species"
        try :
            if data[varName] != None:
                self.marine_species=MarineSpeciesDevices.MarineSpeciesDevices()
                self.marine_species.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
