# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from ..farm.outputs import FarmResults
from ..corridor.outputs import CorridorResults
from ..databases.outputs import DatabasesPaths
from ..point.outputs import PointResults
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Project():

    """Site characterisation project results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._ProjectID=''
        self._farm=FarmResults.FarmResults()
        self._farm.description = 'farm results'
        self._corridor=CorridorResults.CorridorResults()
        self._corridor.description = 'corridor results'
        self._databases_paths=DatabasesPaths.DatabasesPaths()
        self._databases_paths.description = 'databases paths'
        self._point=PointResults.PointResults()
        self._point.description = 'results at a specific point (center of the farm by default)'
        self._path_to_digital_representation=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def ProjectID(self): # pragma: no cover
        """str: project id
        """
        return self._ProjectID
    #------------
    @ property
    def farm(self): # pragma: no cover
        """:obj:`~.FarmResults.FarmResults`: farm results
        """
        return self._farm
    #------------
    @ property
    def corridor(self): # pragma: no cover
        """:obj:`~.CorridorResults.CorridorResults`: corridor results
        """
        return self._corridor
    #------------
    @ property
    def databases_paths(self): # pragma: no cover
        """:obj:`~.DatabasesPaths.DatabasesPaths`: databases paths
        """
        return self._databases_paths
    #------------
    @ property
    def point(self): # pragma: no cover
        """:obj:`~.PointResults.PointResults`: results at a specific point (center of the farm by default)
        """
        return self._point
    #------------
    @ property
    def path_to_digital_representation(self): # pragma: no cover
        """str: path to digital representation file
        """
        return self._path_to_digital_representation
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ ProjectID.setter
    def ProjectID(self,val): # pragma: no cover
        self._ProjectID=str(val)
    #------------
    @ farm.setter
    def farm(self,val): # pragma: no cover
        self._farm=val
    #------------
    @ corridor.setter
    def corridor(self,val): # pragma: no cover
        self._corridor=val
    #------------
    @ databases_paths.setter
    def databases_paths(self,val): # pragma: no cover
        self._databases_paths=val
    #------------
    @ point.setter
    def point(self,val): # pragma: no cover
        self._point=val
    #------------
    @ path_to_digital_representation.setter
    def path_to_digital_representation(self,val): # pragma: no cover
        self._path_to_digital_representation=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:Project"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:Project"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("ProjectID"):
            rep["ProjectID"] = self.ProjectID
        if self.is_set("farm"):
            if (short and not(deep)):
                rep["farm"] = self.farm.type_rep()
            else:
                rep["farm"] = self.farm.prop_rep(short, deep)
        if self.is_set("corridor"):
            if (short and not(deep)):
                rep["corridor"] = self.corridor.type_rep()
            else:
                rep["corridor"] = self.corridor.prop_rep(short, deep)
        if self.is_set("databases_paths"):
            if (short and not(deep)):
                rep["databases_paths"] = self.databases_paths.type_rep()
            else:
                rep["databases_paths"] = self.databases_paths.prop_rep(short, deep)
        if self.is_set("point"):
            if (short and not(deep)):
                rep["point"] = self.point.type_rep()
            else:
                rep["point"] = self.point.prop_rep(short, deep)
        if self.is_set("path_to_digital_representation"):
            rep["path_to_digital_representation"] = self.path_to_digital_representation
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.ProjectID.encode("ascii"))
        handle["ProjectID"] = np.asarray(ar)
        subgroup = handle.create_group("farm")
        self.farm.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("corridor")
        self.corridor.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("databases_paths")
        self.databases_paths.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("point")
        self.point.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.path_to_digital_representation.encode("ascii"))
        handle["path_to_digital_representation"] = np.asarray(ar)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "ProjectID"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "farm"
        try :
            if data[varName] != None:
                self.farm=FarmResults.FarmResults()
                self.farm.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "corridor"
        try :
            if data[varName] != None:
                self.corridor=CorridorResults.CorridorResults()
                self.corridor.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "databases_paths"
        try :
            if data[varName] != None:
                self.databases_paths=DatabasesPaths.DatabasesPaths()
                self.databases_paths.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "point"
        try :
            if data[varName] != None:
                self.point=PointResults.PointResults()
                self.point.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "path_to_digital_representation"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("ProjectID" in list(gr.keys())):
            self.ProjectID = gr["ProjectID"][0].decode("ascii")
        if ("farm" in list(gr.keys())):
            subgroup = gr["farm"]
            self.farm.loadFromHDF5Handle(subgroup)
        if ("corridor" in list(gr.keys())):
            subgroup = gr["corridor"]
            self.corridor.loadFromHDF5Handle(subgroup)
        if ("databases_paths" in list(gr.keys())):
            subgroup = gr["databases_paths"]
            self.databases_paths.loadFromHDF5Handle(subgroup)
        if ("point" in list(gr.keys())):
            subgroup = gr["point"]
            self.point.loadFromHDF5Handle(subgroup)
        if ("path_to_digital_representation" in list(gr.keys())):
            self.path_to_digital_representation = gr["path_to_digital_representation"][0].decode("ascii")
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
