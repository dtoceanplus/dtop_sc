# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 14:20:15 2019

@author: ykervell

Functions to read files
"""
import geopandas as gpd
import numpy as np

# Global variables
INDENT = '\t'


def read_shapefile(file):
    '''
    file : shapefile (usually created using QGIS)
    '''
    data = gpd.read_file(file)
    g = [i for i in data.geometry]
    dico_points = {}
    if g[0].geom_type == "LineString":    # corridor
        dico_points['lons'] = g[0].coords.xy[0].tolist()
        dico_points['lats'] = g[0].coords.xy[1].tolist()
        dico_points['names'] = tuple(['PtC' + str(i) for i in range(len(dico_points['lons']))])
    elif g[0].geom_type == "Polygon": # farm
        dico_points['lons'] = (g[0].centroid.x,) + tuple(g[0].boundary.coords.xy[0].tolist())  # centroid + boundary points
        dico_points['lats'] = (g[0].centroid.y,) +  tuple(g[0].boundary.coords.xy[1].tolist())  # centroid + boundary points
        dico_points['names'] = tuple(['PtF' + str(i) for i in range(len(dico_points['lons']))])
    else:
        print("This type of shapefile is not implemented yet")
    
    return dico_points

def read_shapefile_and_return_polygon(file):
    '''
    file : shapefile (usually created using QGIS)
    '''
    data = gpd.read_file(file)
    g = [i for i in data.geometry]
    polyg = g[0]
    
    return polyg

def compute_shapefile_area(file):
    '''
    file : shapefile (usually created using QGIS)
    return the shapefile area in km^2
    '''
    data = gpd.read_file(file)
    S = data['geometry'].to_crs({'init': 'epsg:3857'}).map(lambda p: p.area / 10**6)
    
    return float(S)

def read_shapefile_and_create_grid(file, nbx, nby):
    '''
    file : shapefile (usually created using QGIS)
    create grid for extraction over the farm area
    '''
    data = gpd.read_file(file)
    g = [i for i in data.geometry]
    dico_points = {}
    if g[0].geom_type == "LineString":    # corridor
        dico_points['lons'] = g[0].coords.xy[0].tolist()
        dico_points['lats'] = g[0].coords.xy[1].tolist()
        dico_points['names'] = tuple(['Pt' + str(i) for i in range(len(dico_points['lons']))])
    elif g[0].geom_type == "Polygon": # farm or 2D corridor
        polygon = g[0]
        list_lons = []
        list_lats = []
        min_lon = min(g[0].boundary.coords.xy[0])-0.01
        max_lon = max(g[0].boundary.coords.xy[0])+0.01
        min_lat = min(g[0].boundary.coords.xy[1])-0.01
        max_lat = max(g[0].boundary.coords.xy[1])+0.01

        eps = 0.000001 # to keep the boundary points in the polygon
        dx = (max_lon - min_lon)/(nbx-1) - eps
        dy = (max_lat - min_lat)/(nby-1) - eps
        
        for i in range(nbx):
            for j in range(nby):
                lon_tmp = min_lon + eps  + i * dx
                lat_tmp = min_lat + eps  + j * dy
                # test if point is inside the polygon
                from shapely.geometry import Point
                pt = Point(lon_tmp,lat_tmp)
                if pt.within(polygon):
                    list_lons.append(lon_tmp)
                    list_lats.append(lat_tmp)
        if len(np.unique(list_lons)) <10:
            list_lons = []
            list_lats = []
            for i in range(nbx):
                for j in range(nby):
                    list_lons.append(min_lon + eps + i * dx)
                    list_lats.append(min_lat + eps + j * dy)

        # dx = (max_lon - min_lon)/(nbx-1)
        # dy = (max_lat - min_lat)/(nby-1)

        # for i in range(nbx):
        #     for j in range(nby):
        #         list_lons.append(min_lon + i * dx)
        #         list_lats.append(min_lat + j * dy)
    
        dico_points['lons'] = list_lons
        dico_points['lats'] = list_lats
        dico_points['names'] = tuple(['Pt' + str(i) for i in range(len(dico_points['lons']))])
   
    return dico_points