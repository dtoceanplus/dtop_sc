# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 10:33:03 2019

@author: ykervell

Set of useful functions
"""

import numpy as np
import collections
import os

#def copy_dict(dictionary):
#    '''Copies a dict instance.
#
#    The classical method doesn't account for the case where one of dictionary's values is also a dict itself...
#    '''
#    new_dictionary = collections.OrderedDict()
#    for key, values in dictionary.items():
#        if isinstance(values, dict):
#            new_dictionary[key] = copy_dict(values)
#        elif isinstance(values, list):
#            new_dictionary[key] = list(values)
#        elif isinstance(values, np.ndarray):
#            new_dictionary[key] = np.array(values)
#        else:
#            new_dictionary.update({key: values})
#    return new_dictionary
#
#def makedirs(thing):
#    '''Creates all the necessary directories in which the file named thing will be placed.'''
#    directoryname = os.path.dirname(thing)
#    if not os.path.isdir(directoryname):
#        try:
#            os.makedirs(directoryname)
#        except:
#            pass

def haversine(pt1, pt2, unit='degree'):
    '''Distance(s) in meters between points
    arguments :
    pt1 : (lon1,lat1) where lon1 and lat1 are floats or arrays
    pt2 : (lon2,lat2) where lon2 and lat2 are floats or arrays
    optional : unit ('radian' or 'degree', default: 'degree')
    dist = haversine(pt1,pt2)
    '''
    lon1 = np.array(pt1[0])
    lat1 = np.array(pt1[1])
    lon2 = pt2[0]
    lat2 = pt2[1]

    if(unit.lower()=='degree'):
        degtorad = np.pi/180.
        lon1 *= degtorad
        lat1 *= degtorad
        lon2 *= degtorad
        lat2 *= degtorad
    dlat = abs(lat1-lat2)
    dlon = abs(lon1-lon2)
    a = np.sin(dlat/2.)**2 + np.cos(lat1)*np.cos(lat2)*(np.sin(dlon/2.)**2)
    c = 2.*np.arctan2(np.sqrt(a),np.sqrt(1-a))
    R = 6371000.
    return R*c

def angle(U, V, **kwargs):
    """Return theta from U and V, going to convention
    """
    arctan = np.arctan2(U, V)
    arctan[arctan < 0] += 2 * np.pi
    A = 180. / np.pi * arctan
    return A

def angle_meteo(U, V, **kwargs):
    """Return theta from U and V, coming from convention
    """
    A = angle(U, V)
    A += 180
    A %= 360
    return A

def num2date(thing, units='days since 0001-01-02T00:00Z'):
    '''
    default units : pylab.num2date units

    netCDF4 num2date function : the less the better
    '''
    import netCDF4
    import numpy as np
    import datetime
    
    unit = units.split(' ')[0]
    if(len(np.shape(thing))==1):
        date_begin = netCDF4.num2date(thing[0],units)
        offset = thing - thing[0]
        timedeltas = []
        if unit != 'seconds':
            for i in range(len(offset)):
                timedeltas.append(datetime.timedelta(**{unit : offset[i]}))
        else:
            for i in range(len(offset)):
                timedeltas.append(datetime.timedelta(**{unit : int(offset[i])}))
        timedeltas = np.array(timedeltas)
        return np.array([date_begin]*len(thing)) + timedeltas
    else:
        res = []
        for things in thing:
            res.append(num2date(things,units))
        return res

def monthly_mask(TV):
    """Return a mask to cut the time vector in months
    """
    #TV = TV[~np.isnan(TV)] # remove nans
    MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    d = num2date(np.squeeze(TV))
    mask = []
    for i, m in enumerate(MONTHS):
        bool_date = [date.month == i + 1 for date in d]
        while(len(bool_date) < len(TV)):
            bool_date.append(False)
        bool_date = np.array(bool_date)
        mask.append(bool_date)
    return mask

def seasonly_mask(TV):
    """Return a mask to cut the time vector in seasons
    """
    #TV = TV[~np.isnan(TV)] # remove nans
    SEASONS = ['Winter', 'Spring', 'Summer', 'Autumn']
    d = num2date(np.squeeze(TV))
    mask = []
    for i, m in enumerate(SEASONS):
        bool_date = [date.month % 12 in range(i * 3, (i + 1) * 3) for date in d]
        while(len(bool_date) < len(TV)):
            bool_date.append(False)
        bool_date = np.array(bool_date)
        mask.append(bool_date)
    return mask

def remove_table_nans(TV, TS_table):
    """Remove nans from table of timeseries and time vector.
    Only if a nan is present everywhere
    """
    ind = ~np.isnan(TS_table[:,0])
    TV_new = TV[ind]
    TS_table_new = TS_table[ind, :]
    return TV_new, TS_table_new

#def find_minimum_number_of_timesteps(list_TV):
#    """ Find the time vector in the list which has the minimum number of
#    timesteps
#    """
#    minimum = len(list_TV[0])
#    ind_min = 0
#    for i, tv in enumerate(list_TV):
#        print("############ ind:", i, " / len = ", len(tv))
#        if len(tv) < minimum:
#            minimum = len(tv)
#            ind_min = i
#    print("ind_min:", ind_min)
#    return list_TV[ind_min]

def common_2series(TV1, TS1, TV2, TS2):
    """ combine 2 series to have the same number of timesteps
    """
    TV1_new = []; TS1_new = []
    TV2_new = []; TS2_new = []
    for i in range(len(TV1)):
        TV, TV1_ind, TV2_ind = np.intersect1d(TV1[i], TV2[i], return_indices=True)
        TV1bis = np.squeeze(TV1[i]); TS1bis = np.squeeze(TS1[i])
        TV2bis = np.squeeze(TV2[i]); TS2bis = np.squeeze(TS2[i])
        TV1_new.append(TV1bis[TV1_ind])
        TS1_new.append(TS1bis[TV1_ind])
        TV2_new.append(TV2bis[TV2_ind])
        TS2_new.append(TS2bis[TV2_ind])
    return TV1_new, TS1_new, TV2_new, TS2_new

def common_3series(TV1, TS1, TV2, TS2, TV3, TS3):
    """ combine 3 series to have the same number of timesteps
    """
    TV1b, TS1b, TV2b, TS2b = common_2series(TV1, TS1, TV2, TS2)
    TV1c, TS1c, TV3b, TS3b = common_2series(TV1b, TS1b, TV3, TS3)
    TV1c, TS1c, TV2c, TS2c = common_2series(TV1c, TS1c, TV2, TS2)
    TV1c, TS1c, TV3c, TS3c = common_2series(TV1c, TS1c, TV3, TS3)
    return TV1c, TS1c, TV2c, TS2c, TV3c, TS3c
    
    
def Jonswap_peak_factor(ti, hs, tp):
    """ Compute the Jonswap peak shape parameter, based on 3.5.5.5 of DNV RP-C205
    """
    hs = np.array(hs)
    tp = np.array(tp)
    ti = np.array(ti)
#    hs = hs[~np.isnan(tp)] # remove nans
#    ti = ti[~np.isnan(tp)] # remove nans
#    tp = tp[~np.isnan(tp)] # remove nans
    Gamma = np.exp(5.75 - 1.15 * tp / np.sqrt(hs))
    limiting_inputs = np.array(
            tp / np.sqrt(hs),
            dtype=np.float
            )
    Gamma[limiting_inputs <= 3.6] = 5.
    Gamma[limiting_inputs >= 5.] = 1.
    
    return ti.tolist(), Gamma.tolist()

def regrid_to_structured(lons, lats, TS, directional):
    """ Regrid unstructured data on a structured grid.
    """
    from scipy.interpolate import griddata
    WHITE = "\033[0m"
    # create structured grid
    lon_uniform = np.linspace(np.amin(lons),np.amax(lons),num=20)
    lat_uniform = np.linspace(np.amin(lats),np.amax(lats),num=20)
    [lon_grid,lat_grid] = np.meshgrid(lon_uniform,lat_uniform)
    matrix_structured = np.zeros((len(lat_uniform), len(lon_uniform), TS.shape[0]))
    # Interpolation (cubic and nearest for NaN points)
    time_length = TS.shape[0]
    lon_grid_vector = np.reshape(lon_grid,lon_grid.shape[0]*lon_grid.shape[1])
    lat_grid_vector = np.reshape(lat_grid,lat_grid.shape[0]*lat_grid.shape[1])
    for i in range(time_length):
        for pt in range(TS.shape[1]):
            if (TS[i,pt] > 100) or (TS[i,pt] < -100): # ça c'est moche, remplacer par filled_value
                TS[i,pt] = np.nan #TS[i-1,pt]
        vector_nearest = griddata((lats, lons), TS[i,:],(lat_grid_vector,lon_grid_vector), method='nearest')
        # print("")
        # print("vector_nearest", vector_nearest)
        if directional:
            #print('directional')
            vector_cubic = vector_nearest
        else:
            vector_cubic = griddata((lats, lons), TS[i,:],(lat_grid_vector,lon_grid_vector), method='cubic')
            ind_nan = np.argwhere(np.isnan(vector_cubic))
#            print("vector_cubic before", vector_cubic)
            vector_cubic[ind_nan] = vector_nearest[ind_nan]
#            print("vector_cubic after", vector_cubic)
        matrix_cubic = np.reshape(vector_cubic,(lat_grid.shape[0],lon_grid.shape[1]))
        # normalization
        initial_max = np.nanmax(abs(TS[i,:]))
        new_max = np.nanmax(abs(matrix_cubic))
        matrix_cubic = matrix_cubic / new_max * initial_max
        matrix_structured[:,:,i] = matrix_cubic
        progress_bar = ''.join((WHITE, " %0.2f" % min(np.round(i/TS.shape[0]*100., 2), 100.), '%'))
        # print(progress_bar, end='\r')
    
    return lon_uniform, lat_uniform, matrix_structured        