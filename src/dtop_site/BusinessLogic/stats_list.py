# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 11:03:39 2020

@author: youen

This files gives the list of statistics that will be computed
"""

stats_flags = {'Basic_hs':True,
           'Basic_tp':True,
           'Basic_CgE':True,
           'Basic_gamma':True,
           'Basic_spr':True,
           'Basic_mag':True,
           'Basic_Flux':True,
           'Basic_mag10':True,
           'Basic_wlev':True,
           'EPD_hs':True,
           'EPD_tp':True,
           'EPD_dp':True,
           'EPD_mag':True,
           'EPD_theta':True,
           'EPD_mag10':True,
           'EPD_theta10':True,
           'EPD_wlev':True,
           'EPD_hs_monthly':True,
           'EPD_tp_monthly': True,
           'EPD_dp_monthly':True,
           'EPD_mag_monthly':True,
           'EPD_theta_monthly':True,
           'EPD_mag10_monthly':False,
           'EPD_theta10_monthly':False,
           'EPD_wlev_monthly':False,
           'EJPD_hs_tp':True,
           'EJPD_hs_dp':True,
           'EJPD_mag_theta':True,
           'EJPD_mag10_theta10':True,
           'EJPD_hs_tp_monthly':True,
           'EJPD_hs_dp_monthly':True,
           'EJPD_mag_theta_monthly':True,
           'EJPD_mag10_theta10_monthly':False,
           'EJPD3v':True,
           'EJPD3v_monthly':True,
           'EXT_hs': True,
           'EXT_tp': False,
           'EXT_gamma': False,
           'EXT_mag': True,
           'EXT_mag10': True,
           'EXT_gust10': True,
           'EXT_wlev': True,
           'EXC': True,
           'ENVS': True,
           '2D_waves': False,
           '2D_currents': True
        }
