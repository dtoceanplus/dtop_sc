# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 15:55:22 2020

@author: ykervell

simple functions to make DTO+ plots
"""

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import os
from pathlib import Path
import scipy.io as sio

working= os.getcwd()
data_path = Path('databases/SiteCharacterisation_Main-Database/')
PATH_DATABASES = data_path
PATH_COASTLINE = os.path.join(PATH_DATABASES, Path('Shapefiles/Coastline/'))
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))

def make_basic_table(path_save, dic_wav, dic_cur, dic_win, dic_lev):
    """ make a table based on basic statistics
    """
    path_figure = os.path.join(path_save, "Overview_of_metocean_conditions.png")
    values = [['<b>Waves<b><br>Hs [m]', '<b>Currents<b><br>Mag [m/s]', '<b>Winds<b><br>Mag10 [m/s]', '<b>Water levels<b><br>WLEV [m from MSL]'],
              [np.round(dic_wav['min'],2), np.round(dic_cur['min'],2), np.round(dic_win['min'],2), np.round(dic_lev['min'],2)],
              [np.round(dic_wav['mean'],2), np.round(dic_cur['mean'],2), np.round(dic_win['mean'],2), np.round(dic_lev['mean'],2)],
              [np.round(dic_wav['max'],2), np.round(dic_cur['max'],2), np.round(dic_win['max'],2), np.round(dic_lev['max'],2)],
              [np.round(dic_wav['std'],2), np.round(dic_cur['std'],2), np.round(dic_win['std'],2), np.round(dic_lev['std'],2)]]
    fig = go.Figure(data=[go.Table(
        columnorder = [1,2,3,4,5],
        columnwidth = [150,90,90,90,90],
        header = dict(
            values = [['<b>Variable</b>'],
                      ['<b>Min</b>'],
                      ['<b>Mean</b>'],
                      ['<b>Max</b>'],
                      ['<b>Std</b>']],
            line_color='darkslategray',
            fill_color='royalblue',
            align=['left','center'],
            font=dict(color='white', size=12),
            height=40
        ),
        cells=dict(
            values=values,
            line_color='darkslategray',
            fill=dict(color=['paleturquoise', 'white']),
            align=['left', 'center'],
            font_size=12,
            height=30)
        )
    ])
    fig.update_layout(
        margin=dict(l=20, r=20, t=20, b=0),
    #    paper_bgcolor="LightSteelBlue",
    )
    fig.write_image(path_figure)
    return path_figure

def make_extreme_table(path_save, dic_wav, dic_cur, dic_win):
    """ make a table based on extreme statistics
    """
    path_figure = os.path.join(path_save, "Extreme_values_of_metocean_conditions.png")
    # values = [['<b>Waves<b><br>Hs [m]', '<b>Currents<b><br>Mag [m/s]', '<b>Winds<b><br>Mag10 [m/s]'],
    #           [np.round(dic_wav['return_values'][0],2), np.round(dic_cur['return_values'][0],2), np.round(dic_win['return_values'][0],2)],
    #           [np.round(dic_wav['return_values'][1],2), np.round(dic_cur['return_values'][1],2), np.round(dic_win['return_values'][1],2)],
    #           [np.round(dic_wav['return_values'][2],2), np.round(dic_cur['return_values'][2],2), np.round(dic_win['return_values'][2],2)],
    #           [np.round(dic_wav['return_values'][3],2), np.round(dic_cur['return_values'][3],2), np.round(dic_win['return_values'][3],2)]]
    values = [['<b>Waves<b><br>Hs [m]', '<b>Currents<b><br>Mag [m/s]', '<b>Winds<b><br>Mag10 [m/s]'],
              [dic_wav['return_values'][0], dic_cur['return_values'][0], dic_win['return_values'][0]],
              [dic_wav['return_values'][1], dic_cur['return_values'][1], dic_win['return_values'][1]],
              [dic_wav['return_values'][2], dic_cur['return_values'][2], dic_win['return_values'][2]],
              [dic_wav['return_values'][3], dic_cur['return_values'][3], dic_win['return_values'][3]]]
    fig = go.Figure(data=[go.Table(
        columnorder = [1,2,3,4,5],
        columnwidth = [150,90,90,90,90],
        header = dict(
            values = [['<b>Variable / RP</b>'],
                      ['<b>1y</b>'],
                      ['<b>5y</b>'],
                      ['<b>10y</b>'],
                      ['<b>50y</b>']],
            line_color='darkslategray',
            fill_color='royalblue',
            align=['left','center'],
            font=dict(color='white', size=12),
            height=40
        ),
        cells=dict(
            values=values,
            line_color='darkslategray',
            fill=dict(color=['paleturquoise', 'white']),
            align=['left', 'center'],
            font_size=12,
            height=30)
        )
    ])
    fig.update_layout(
        margin=dict(l=20, r=20, t=20, b=0),
    #    paper_bgcolor="LightSteelBlue",
    )
    fig.write_image(path_figure)
    return path_figure

def make_windrose_plot(path_save, name, var1, var2, label1, label2):
    """make a windrose plot
    """
    from windrose import WindroseAxes
    
    name_figure = "Windrose_" + name + ".png"
    path_figure = os.path.join(path_save, name_figure)
    fig = plt.figure(figsize=(9, 9), dpi=80, facecolor='w', edgecolor='w')
    rect = [0.1, 0.1, 0.8, 0.8]
    ax = WindroseAxes(fig, rect)
    fig.add_axes(ax)
    
    my_cmap = plt.get_cmap('RdYlBu_r')
    ax.bar(var2, var1, normed=True, opening=0.8, cmap=my_cmap, edgecolor='k', alpha=0.9)
    ax.set_title(name_figure[:-4], fontsize='20', position=(0.5, 1.0))
    
    ax.set_legend()
    ax.legend(title=label1, loc=(0.95, 0))
    fig.savefig(path_figure)
    plt.show()
    return path_figure

def make_EJPD_figure(path_save, name, ejpd, label1, label2):
    """ make a figure for an EJPD (Empirical Joint Probability Distribution)
    """
    name_figure = "EJPD_" + name + ".png"
    path_figure = os.path.join(path_save, name_figure)
    bins1 = ejpd['bins1']
    bins2 = ejpd['bins2']
    pdf = np.array(ejpd['pdf'])
    # different size for contour plot
    pdf2 = np.zeros((np.shape(pdf)[0]+1, np.shape(pdf)[1]+1))
    pdf2[1:,1:] = pdf
    if len(pdf) > 0:
        pdf2[0,1:] = pdf[0,:]
        pdf2[1:,0] = pdf[:,0]
        pdf2[0,0] = pdf[0,0]
    
    # go to percentages
    S = np.sum(pdf2)
    pdf2 = pdf2 / S * 100

    fig = plt.figure()
    ax = fig.gca()
    # axis limits for nicer figures
    start1 = 0; end1 = 1
    start2 = 0; end2 = 2
    MPO = 1  # Minimum Percentage of Occurences
    if bins1[-1] < 300.:    # to eliminate directional bins
        for i,b in enumerate(bins1):
            if np.sum(pdf2[i,:]) > MPO:
                if i == 0:
                    start1 = bins1[i]
                else:
                    start1 = bins1[i-1]
                break
        for i,b in reversed(list(enumerate(bins1))):
            if np.sum(pdf2[i,:]) > MPO:
                if i == 0:
                    end1 = bins1[i]
                else:
                    end1 = bins1[i-1]
                break
        ax.set_xlim(start1, end1)
    if bins2[-1] < 300.:    # to eliminate directional bins
        for j,b in enumerate(bins2):
            if np.sum(pdf2[:,j]) > MPO:
                if j == 0:
                    start2 = bins2[j]
                else:
                    start2 = bins2[j-1]
                break
        for j,b in reversed(list(enumerate(bins2))):
            if np.sum(pdf2[:,j]) > MPO:
                if j == 0:
                    end2 = bins2[j]
                else:
                    end2 = bins2[j-1]
                break
        ax.set_ylim(start2, end2)

    if len(pdf) > 0:
        cfset = ax.contourf(bins1, bins2, np.transpose(pdf2), cmap="Blues")
        plt.grid()
    #    cset = ax.contour(bins2, bins1, pdf2, colors="k")
    #    ax.clabel(cset, inline=1, fontsize=10, colors="k", fmt="%1.0f")
        cbar = fig.colorbar(cfset)
        cbar.ax.set_ylabel('Percentage of occurrences')
        ax.set_xlabel(label1)
        ax.set_ylabel(label2)
    #    plt.show()

    fig.savefig(path_figure)
    return path_figure

def make_2D_map(point_name, path_save, name, lon, lat, var, label1, FarmFile, CorridorFile, my_cmap):
    """plot 2D map of the variable"""
    
    import geopandas as gpd
    
    name_figure = "MAP_" + name + ".png"
    path_figure = os.path.join(path_save, name_figure)

    CoastFile = os.path.join(PATH_COASTLINE, 'coasts.mat')
    mat_contents = sio.loadmat(CoastFile)
    lon_coasts = mat_contents['lon']
    lat_coasts = mat_contents['lat']

    fig = plt.figure(figsize=(6, 6))
    axes = plt.gca()

    # 2D layer
    plt.contourf(lon, lat, var, cmap=my_cmap)
    # coastline
    plt.plot(lon_coasts,lat_coasts,'k',linewidth=1)
    # lease_area
    polyg = gpd.read_file(FarmFile)
    polyg.geometry.boundary.plot(facecolor="none",edgecolor='k',linewidth = 1,ax=axes)
    # corridor
    if not('RazBlanchard' in CorridorFile):
        lin = gpd.read_file(CorridorFile)
        lin.geometry.boundary.plot(edgecolor='k',linewidth = 1,ax=axes)
    
    plt.xlabel('Longitude',size=12)
    plt.ylabel('Latitude',size=12)
    axes.set_aspect('equal')
    axes.set_xlim(min(lon)-0.05,max(lon)+0.05)
    axes.set_ylim(min(lat)-0.05,max(lat)+0.05)
    # check if depth is uniform or not
    unif = np.all(var == var[0])
    if unif:
        plt.title("Uniform water depth (%sm)" %var[0,0])
    else:
        cbar = plt.colorbar(orientation='horizontal',aspect=20)
        cbar.ax.tick_params(labelsize=12)
        cbar.ax.set_xlabel(label1)
    plt.grid()
    
    fig.savefig(path_figure)
    return path_figure

def make_2D_map_sediments(point_name, path_save, name, lon, lat, var, label1, FarmFile, CorridorFile):
    """plot 2D map of the variable"""
    
    import geopandas as gpd
    from matplotlib.colors import ListedColormap
    import matplotlib
    
    name_figure = "MAP_" + name + ".png"
    path_figure = os.path.join(path_save, name_figure)

    CoastFile = os.path.join(PATH_COASTLINE, 'coasts.mat')
    mat_contents = sio.loadmat(CoastFile)
    lon_coasts = mat_contents['lon']
    lat_coasts = mat_contents['lat']

    # design special colormap for sediments
    col_dict={0:"black",
          1:"brown",
          2:"saddlebrown",
          3:"peru",
          4:"goldenrod",
          5:"darkorange",
          6:"orange",
          7:"gold",
          8:"yellow",
          9:"orangered",
          10:"red",
          11:"crimson",
          12:"hotpink"}
    my_cmap = ListedColormap([col_dict[x] for x in col_dict.keys()])
    # my_cmap = matplotlib.cm.get_cmap('PiYG', 13)
    sediment_labels = np.array(['No data', 'Rocks/Gravels/Peebles', 'Very dense sand', 'Dense sand', 'Medium dense sand', 'Loose sand', 'Very loose sand', 'Hard clay', 'Very stiff clay', 'Stiff clay', 'Firm clay', 'Soft clay', 'Very soft clay'])
    
    fig = plt.figure(figsize=(9, 6))
    axes = plt.gca()

    # Prepare bins for the normalizer
    norm_bins = np.sort([*col_dict.keys()]) + 0.5
    norm_bins = np.insert(norm_bins, 0, np.min(norm_bins) - 1.0)
    ## Make normalizer and formatter
    norm = matplotlib.colors.BoundaryNorm(norm_bins, len(sediment_labels), clip=True)
    fmt = matplotlib.ticker.FuncFormatter(lambda x, pos: sediment_labels[norm(x)])

    # 2D layer
    im = axes.pcolor(lon, lat, var, cmap=my_cmap, norm=norm)
    diff = norm_bins[1:] - norm_bins[:-1]
    tickz = norm_bins[:-1] + diff / 2
    cbar = fig.colorbar(im, format=fmt, ticks=tickz)
    cbar.ax.tick_params(labelsize=10)
    cbar.ax.set_xlabel(label1,size=12)

    # coastline
    plt.plot(lon_coasts,lat_coasts,'k',linewidth=1)
    # lease_area
    polyg = gpd.read_file(FarmFile)
    polyg.geometry.boundary.plot(facecolor="none",edgecolor='k',linewidth = 1,ax=axes)
    # corridor
    if not('RazBlanchard' in CorridorFile):
        lin = gpd.read_file(CorridorFile)
        lin.geometry.boundary.plot(edgecolor='k',linewidth = 1,ax=axes)
    
    plt.xlabel('Longitude',size=12)
    plt.ylabel('Latitude',size=12)
    axes.set_aspect('equal')
    axes.set_xlim(min(lon)-0.05,max(lon)+0.05)
    axes.set_ylim(min(lat)-0.05,max(lat)+0.05)
    # cbar = plt.colorbar(orientation='horizontal',aspect=20)
    # cbar.ax.tick_params(labelsize=12)
    # cbar.ax.set_xlabel(label1)
    plt.grid()
    
    fig.savefig(path_figure)
    return path_figure
