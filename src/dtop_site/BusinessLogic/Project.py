# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Fri Nov  8 15:26:33 2019

@author: ykervell

The Project module is the module that is used to launch all the process 
(use Site for the computation after reading the GUI via API).

Overview
========

This module uses the API results to initialize objects that will be used by Site.

"""

from dtop_site.BusinessLogic import useful_functions as uf
from dtop_site.BusinessLogic import plot_functions as pf
from dtop_site.BusinessLogic import Read
from dtop_site.BusinessLogic import Site
from dtop_site.BusinessLogic.stats_list import stats_flags as sf
import os
import numpy as np
from dtop_site.BusinessLogic.libraries.dtosc.outputs import Project
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs import PointResults
from pathlib import Path
import copy
import logging
from logging.handlers import RotatingFileHandler

working= os.getcwd()
data_path = Path('databases/SiteCharacterisation_Main-Database/')
PATH_DATABASES = data_path
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage/'))

MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', \
          'August', 'September', 'October', 'November', 'December']

def create_logger(log_to_file = True, log_to_stream = True):
    """Create a log file to be displayed in the GUI

    Args:
        log_to_file (:obj:`bool`, optional): Define if the log is saved in a file or not.
        log_to_stream (:obj:`bool`, optional): Define if the log is redirected to the console where backend is running.
    """

    # création de l'objet logger qui va nous servir à écrire dans les logs
    logger = logging.getLogger()
    # on met le niveau du logger à DEBUG, comme ça il écrit tout
    logger.setLevel(logging.INFO)
	
    # création d'un formateur qui va ajouter le temps, le niveau
    # de chaque message quand on écrira un message dans le log
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s', '%Y-%m-%d %H:%M:%S')

    if log_to_file:
        # effacement du fichier s'il existe
        if os.path.isfile('sc.log'):
            os.remove('sc.log')
        # création d'un handler qui va rediriger une écriture du log vers
        # un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
        file_handler = RotatingFileHandler('sc.log', 'w', 100000,1)
        # on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
        # créé précédement et on ajoute ce handler au logger
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
	
    if log_to_stream:
        # création d'un second handler qui va rediriger chaque écriture de log
        # sur la console
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        logger.addHandler(stream_handler)

class MyProject:

    '''Defines a Project (statistics on user locations of choosen databases).

    Overview
    --------

    The methods of a Project object will launch computation and storage.

    '''

    def __init__(self, project_name, project_level, my_databases, farm_file, corridor_file, storage, **kwargs):
        '''
        Arguments
        ---------
            :project_name:
                user info, provided by gui.
            :project_level:
                user info, provided by gui (complexity level).
            :my_databases:
                user info, provided by gui, databases respectively for waves,
                currents, wind, levels, bathy, seabed type and roughness.
            :farm_file:
                shapefile, provided by gui (user), with the lease area (farm)
                of the project (polygon)
            :corridor_file:
                shapefile, provided by gui (user), with the corridor of the
                project (line)
            :storage:
                path to a specific storage

        Keyword arguments
        -----------------
            :todo:
                todo
        '''

        create_logger()
        self.logger = logging.getLogger()
        self.logger.info('****************************************')
        self.logger.info('INITIALIZE SITE CHARACTERISATION PROJECT')

        self.project_name = project_name
        self.project_level = project_level
        self.my_databases = my_databases
        if self.my_databases['WAV'] == self.my_databases['CUR'] == self.my_databases['WIN'] == self.my_databases['LEV']:
            self.all_in_1_file = True
        self.storage = PATH_STORAGE #storage
        self.FarmFile = farm_file 
        self.CorridorFile = corridor_file 
#        self.devices_file = devices_file
        
    def __load_shapefiles__(self):
        """read shapefiles provided by user.
        """
        try:
            # create_logger()
            # logger = logging.getLogger()
            self.logger.info('****************************************')
            self.logger.info('            LOAD SHAPEFILES ')
            # print("\033[1;35m######### LOAD SHAPEFILES #########\033[1;0m")
            # Farm boundary
            self.farm_points = Read.read_shapefile(self.FarmFile)
            self.logger.info('1. Lease Area')
            # Center of the farm
            self.center_farm_point = {}
            self.center_farm_point['names'] = ('center_farm',)
            self.center_farm_point['lons'] = (self.farm_points["lons"][0],)
            self.center_farm_point['lats'] = (self.farm_points["lats"][0],)
            # surface
            self.farm_area = Read.compute_shapefile_area(self.FarmFile)
            # grid on the farm area
            self.farm_grid_points = Read.read_shapefile_and_create_grid(self.FarmFile, 10, 10)
            # return polygon of the farm
            self.polygon = Read.read_shapefile_and_return_polygon(self.FarmFile)
            
            # Corridor
            self.corridor_points = Read.read_shapefile_and_create_grid(self.CorridorFile, 10, 10)
            self.logger.info('2. Corridor')
            
            # UTM infos
            import utm
            x, y, self.zonenumber, self.zoneletter = utm.from_latlon(self.farm_points["lats"][0], self.farm_points["lons"][0])
            
            # distance from the center of the farm to landing point
            self.distanceC2L = uf.haversine((self.center_farm_point['lons'][0], self.center_farm_point['lats'][0]),(self.corridor_points['lons'][-1], self.corridor_points['lats'][-1]))
            return self.polygon

        except Exception as e:
            # Log the complete error stack trace message in the log
            self.logger.exception(e)
            self.logger.info('**************************')
            self.logger.info('End main module with error')
            self.logger.info('**************************')
            self.logger.handlers.clear()
            raise Exception('Main module failed. Check the log file.')

    def __launch_extractions__(self, user_bathymetry):
        """Launch databases extractions.
        """
        try:
            self.logger.info('****************************************')
            self.logger.info('            LAUNCH EXTRACTIONS ')
            # print("\033[1;35m######### LAUNCH EXTRACTIONS #########\033[1;0m")
            # Instance a MySite object
            self.TestSite = Site.MySite(self.my_databases, self.storage, mylevel=self.project_level)
            
            self.logger.info('1. Direct Values: bathymetry, seabed types, roughness length and endangered species')
            # print("\033[1;34mDirect Values\033[1;0m")
            # Direct Values
            self.F_bathy_extracted_values = self.TestSite.__extract_single_values_at_points__(self.farm_grid_points, "bathy")
            self.C_bathy_extracted_values = self.TestSite.__extract_single_values_at_points__(self.corridor_points, "bathy")
            self.P_bathy_extracted_values = self.TestSite.__extract_single_values_at_points__(self.center_farm_point, "bathy")
            self.lon_bathy, self.lat_bathy, self.bathy_grid = self.TestSite.__extract_2D__(self.farm_grid_points, "bathy")
            if isinstance(user_bathymetry, float):  # convert water depth to an uniform value given by user
                self.F_bathy_extracted_values = np.full(len(self.F_bathy_extracted_values), user_bathymetry)
                self.C_bathy_extracted_values = np.full(len(self.C_bathy_extracted_values), user_bathymetry)
                self.P_bathy_extracted_values = np.full(len(self.P_bathy_extracted_values), user_bathymetry)
                self.bathy_grid = np.full(np.shape(self.bathy_grid), user_bathymetry)
            # Maximum slope at the center of the farm
            self.slope_angle = self.TestSite.__compute_slope__(self.center_farm_point)
            self.F_seabed_extracted_values = self.TestSite.__extract_single_values_at_points__(self.farm_grid_points, "seabed")
            self.C_seabed_extracted_values = self.TestSite.__extract_single_values_at_points__(self.corridor_points, "seabed")
            self.P_seabed_extracted_values = self.TestSite.__extract_single_values_at_points__(self.center_farm_point, "seabed")
            self.lon_seabed, self.lat_seabed, self.seabed_grid = self.TestSite.__extract_2D__(self.farm_grid_points, "seabed")
            self.F_roughness_extracted_values = self.TestSite.__extract_single_values_at_points__(self.farm_grid_points, "roughness")
            self.C_roughness_extracted_values = self.TestSite.__extract_single_values_at_points__(self.corridor_points, "roughness")
            self.P_roughness_extracted_values = self.TestSite.__extract_single_values_at_points__(self.center_farm_point, "roughness")
            self.F_species_extracted_values = self.TestSite.__extract_single_values_at_points__(self.farm_grid_points, "species")
            self.C_species_extracted_values = self.TestSite.__extract_single_values_at_points__(self.corridor_points, "species")
            self.P_species_extracted_values = self.TestSite.__extract_single_values_at_points__(self.center_farm_point, "species")
            ## Marine Species case
            self.F_species_extracted_values_in_list = []
            for i in range(len(self.farm_points['names'])):
                self.F_species_extracted_values_in_list.append(self.F_species_extracted_values[i])
            self.C_species_extracted_values_in_list = []
            for i in range(len(self.corridor_points['names'])):
                self.C_species_extracted_values_in_list.append(self.C_species_extracted_values[i])
            self.P_species_extracted_values_in_list = []
            for i in range(len(self.center_farm_point['names'])):
                self.P_species_extracted_values_in_list.append(self.P_species_extracted_values[i])
            self.logger.info('2. Time Series: waves, currents, winds, water levels')
            # print("\033[1;34mTime Series\033[1;0m")
            # TimeSeries
            if self.all_in_1_file:
                TV, TS_table = self.TestSite.__extract_several_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, ['hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd'])
                self.TV_hs = self.TV_tp = self.TV_dp = self.TV_te = self.TV_cge = \
                self.TV_spr = self.TV_U = self.TV_V = self.TV_U10 = self.TV_V10 = \
                self.TV_g10 = self.TV_XE = TV
                TS_table_array = np.array(TS_table[0])
                self.TS_hs = []; self.TS_hs.append(TS_table_array[:,0])
                self.TS_te = []; self.TS_te.append(TS_table_array[:,1])
                self.TS_spr = []; self.TS_spr.append(TS_table_array[:,2])
                self.TS_tp = []; self.TS_tp.append(1. / np.array(TS_table_array[:,3]))
                self.TS_dp = []; self.TS_dp.append(TS_table_array[:,4])
                self.TS_cge = []; self.TS_cge.append(TS_table_array[:,5])
                self.TS_XE = []; self.TS_XE.append(TS_table_array[:,6])
                if "all_1D" in self.my_databases['WAV']:
                    # HOMERE not masked
                    TS_XE_temp = self.TS_XE[0]
                    TS_XE_temp[TS_XE_temp < 0.] = 0.
                    self.TS_XE[0] = TS_XE_temp
                #
                self.TS_U = []; self.TS_U.append(TS_table_array[:,7])
                self.TS_V = []; self.TS_V.append(TS_table_array[:,8])
                self.TS_U10 = []; self.TS_U10.append(TS_table_array[:,9])
                self.TS_V10 = []; self.TS_V10.append(TS_table_array[:,10])
            else:
                self.TV_hs, self.TS_hs = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, "hs")
                self.TV_tp, self.TS_tp = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, "tp")
                self.TV_dp, self.TS_dp = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, "dp")
                self.TV_te, self.TS_te = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, "tm0m1")
                self.TV_cge, self.TS_cge = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, "CgE")
                self.TV_spr, self.TS_spr = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wave, self.center_farm_point, "spr")
                self.TV_U, self.TS_U = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_current, self.center_farm_point, "U")
                self.TV_V, self.TS_V = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_current, self.center_farm_point, "V")
                self.TV_U10, self.TS_U10 = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wind, self.center_farm_point, "u10")
                self.TV_V10, self.TS_V10 = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wind, self.center_farm_point, "v10")
                self.TV_g10, self.TS_g10 = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_wind, self.center_farm_point, "i10fg")
                self.TV_XE, self.TS_XE = self.TestSite.__extract_time_series_at_points__(self.TestSite.dat_level, self.center_farm_point, "XE")
            # Water levels
            self.TV_WLEV = self.TV_XE
            self.TS_WLEV = copy.deepcopy(self.TS_XE)
            for i in range(len(self.P_bathy_extracted_values)):
                bathy = self.P_bathy_extracted_values[i]
                self.TS_WLEV[i] = [elem + bathy for elem in self.TS_WLEV[i]]
            # Current velocity, direction and power
            RHO = 1026.5
            self.mag = np.sqrt(np.power(self.TS_U,2) + np.power(self.TS_V,2))
            self.theta = uf.angle(self.TS_U,self.TS_V)
            self.CFlux = 0.5 * RHO * np.power(self.mag,3)
            # Wind velocity and direction
            self.mag10 = np.sqrt(np.power(self.TS_U10,2) + np.power(self.TS_V10,2))
            self.theta10 = uf.angle_meteo(self.TS_U10,self.TS_V10)
            if self.all_in_1_file:
                # wind gust conversion (https://www.wmo.int/pages/prog/www/tcp/Meetings/HC31/documents/Doc.3.part2.pdf)
                self.TS_g10 = 1.30 * self.mag10  # conversion from 1h wind to 3s gust wind
            # Jonswap peak factor Gamma, from 3.5.5.5 of DNV RP-C205
            TV_hs2, TS_hs2, TV_tp2, TS_tp2 = uf.common_2series(self.TV_hs, self.TS_hs, self.TV_tp, self.TS_tp)
            self.TS_Gamma = copy.deepcopy(TS_hs2)
            self.TV_Gamma = copy.deepcopy(TV_hs2)
            for ind in range(len(self.center_farm_point['names'])):
                self.TV_Gamma[ind], self.TS_Gamma[ind] = uf.Jonswap_peak_factor(TV_hs2[ind], TS_hs2[ind], TS_tp2[ind])

            # Create dico of TS for statistic computations
            dic_compute = {}
            dic_compute['WAV'] = {}
            dic_compute['BATHY'] = self.P_bathy_extracted_values
            dic_compute['WAV']['hs'] = {}
            dic_compute['WAV']['hs']['times'] = self.TV_hs
            dic_compute['WAV']['hs']['values'] = self.TS_hs
            dic_compute['WAV']['tp'] = {}
            dic_compute['WAV']['tp']['times'] = self.TV_tp
            dic_compute['WAV']['tp']['values'] = self.TS_tp
            dic_compute['WAV']['dp'] = {}
            dic_compute['WAV']['dp']['times'] = self.TV_dp
            dic_compute['WAV']['dp']['values'] = self.TS_dp
            dic_compute['WAV']['cge'] = {}
            dic_compute['WAV']['cge']['times'] = self.TV_cge
            dic_compute['WAV']['cge']['values'] = self.TS_cge
            dic_compute['WAV']['gamma'] = {}
            dic_compute['WAV']['gamma']['times'] = self.TV_Gamma
            dic_compute['WAV']['gamma']['values'] = self.TS_Gamma
            dic_compute['WAV']['spr'] = {}
            dic_compute['WAV']['spr']['times'] = self.TV_spr
            dic_compute['WAV']['spr']['values'] = self.TS_spr
            dic_compute['CUR'] = {}
            dic_compute['CUR']['U'] = {}
            dic_compute['CUR']['U']['times'] = self.TV_U
            dic_compute['CUR']['U']['values'] = self.TS_U
            dic_compute['CUR']['V'] = {}
            dic_compute['CUR']['V']['times'] = self.TV_V
            dic_compute['CUR']['V']['values'] = self.TS_V
            dic_compute['CUR']['cflux'] = {}
            dic_compute['CUR']['cflux']['times'] = self.TV_U
            dic_compute['CUR']['cflux']['values'] = self.CFlux
            dic_compute['WIN'] = {}
            dic_compute['WIN']['U10'] = {}
            dic_compute['WIN']['U10']['times'] = self.TV_U10
            dic_compute['WIN']['U10']['values'] = self.TS_U10
            dic_compute['WIN']['V10'] = {}
            dic_compute['WIN']['V10']['times'] = self.TV_V10
            dic_compute['WIN']['V10']['values'] = self.TS_V10
            dic_compute['WIN']['G10'] = {}
            dic_compute['WIN']['G10']['times'] = self.TV_g10
            dic_compute['WIN']['G10']['values'] = self.TS_g10
            dic_compute['LEV'] = {}
            dic_compute['LEV']['XE'] = {}
            dic_compute['LEV']['XE']['times'] = self.TV_XE
            dic_compute['LEV']['XE']['values'] = self.TS_XE
            dic_compute['LEV']['WLEV'] = {}
            dic_compute['LEV']['WLEV']['times'] = self.TV_WLEV
            dic_compute['LEV']['WLEV']['values'] = self.TS_WLEV
            
            self.dico_for_statistics = dic_compute

        except Exception as e:
            # Log the complete error stack trace message in the log
            self.logger.exception(e)
            self.logger.info('**************************')
            self.logger.info('End main module with error')
            self.logger.info('**************************')
            self.logger.handlers.clear()
            raise Exception('Main module failed. Check the log file.')
        
    def __extract_2D_values__(self, my_polygon):
        """Extract some statistics computed in preprocessing (for faster execution).
        """
        try:
            import dtop_site.BusinessLogic.catalogs.data_catalog as daca
            # ns = 10 # number of scenarii for EC module
            xc = float(self.center_farm_point['lons'][0])
            yc = float(self.center_farm_point['lats'][0])
            if 'WAV2D' not in self.my_databases.keys():
                if sf['2D_currents']:
                    x = np.array([xc])
                    y = np.array([yc])
                    ##
                    t = np.array(np.squeeze(self.TV_U))
                    u = np.array(self.TS_U)
                    ug = u.reshape(1,1,len(t))
                    v = np.array(self.TS_V)
                    vg = v.reshape(1,1,len(t))
                    TI = ug * 0.
                    xe = np.array(self.TS_XE)
                    XE = xe.reshape(1,1,len(t))
                    # self.dico_2D_currents = { 'U':ug, 'V':vg, 'TI':TI, 'SSH':XE, 't':t, 'xc':xc, 'yc':yc, 'x':x, 'y':y, 'ns':ns}
                    self.dico_2D_currents = { 'U':ug, 'V':vg, 'TI':TI, 'SSH':XE, 't':t, 'xc':xc, 'yc':yc, 'x':x, 'y':y}
                ##
                if sf['2D_waves']:
                    TV_hs2, TS_hs2, TV_tp2, TS_tp2, TV_dp2, TS_dp2 = uf.common_3series(self.TV_hs, self.TS_hs, self.TV_tp, self.TS_tp, self.TV_dp, self.TS_dp)
                    t = np.array(np.squeeze(TV_hs2))
                    hs = np.array(TS_hs2)
                    hsg = hs.reshape(1,1,len(t))
                    tp = np.array(TS_tp2)
                    tpg = tp.reshape(1,1,len(t))
                    dp = np.array(TS_dp2)
                    dpg = dp.reshape(1,1,len(t))
                    self.dico_2D_waves = { 'HS':hsg, 'TP':tpg, 'DP':dpg, 't':t, 'xc':xc, 'yc':yc, 'x':x, 'y':y}
            else:
                print("")
                if sf['2D_currents'] or sf['2D_waves']:
                    self.logger.info('3. 2D values')
                    # print("\033[1;35m######### EXTRACT 2D values #########\033[1;0m")
                    # points_names, lons, lats = self.TestSite.dat_wave2D.intersection_with_polygon(my_polygon)
                    points_names, lons, lats = self.TestSite.dat_wave2D.intersection_with_buffer_rectangle_around_polygon(my_polygon)
                    try:
                        list_of_variables = ['hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur', 'ti']
                        time_vector, time_series_table = self.TestSite.__extract_several_2D_variables__(my_polygon, self.TestSite.dat_wave2D, list_of_variables)
                        ti_is_available = True
                    except:
                        list_of_variables = ['hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur']
                        time_vector, time_series_table = self.TestSite.__extract_several_2D_variables__(my_polygon, self.TestSite.dat_wave2D, list_of_variables)
                        ti_is_available = False
                    for ind,key in enumerate(list_of_variables):
                        my_time_series = time_series_table[ind,:,:]
                        my_time_series[my_time_series<daca.Limiting_values[key]['min']] = np.nan
                        my_time_series[my_time_series>daca.Limiting_values[key]['max']] = np.nan
                        time_series_table[ind,:,:] = my_time_series
                if sf['2D_waves']:
                    self.logger.info('     Regridding Hs')
                    # print('\033[1;34mRegrid Hs\033[1;0m')
                    self.x, self.y, self.hs = uf.regrid_to_structured(lons, lats, time_series_table[0,:,:], False)
                    self.logger.info('     Regridding Tp')
                    # print('\033[1;34mRegrid Tp\033[1;0m')
                    x, y, tp = uf.regrid_to_structured(lons, lats, 1./time_series_table[1,:,:], False)
                    self.logger.info('     Regridding Dp')
                    # print('\033[1;34mRegrid Dp\033[1;0m')
                    x, y, dp = uf.regrid_to_structured(lons, lats, time_series_table[2,:,:], True)
                    self.dico_2D_waves = { 'HS':self.hs, 'TP':tp, 'DP':dp, 't':time_vector, 'xc':xc, 'yc':yc, 'x':x, 'y':y}
                else:
                    # for 2D maps
                    self.logger.info('     Regridding Hs')
                    # print('\033[1;34mRegrid Hs\033[1;0m')
                    self.x, self.y, self.hs = uf.regrid_to_structured(lons, lats, time_series_table[0,:,:], False)
                if sf['2D_currents']:
                    self.logger.info('     Regridding XE')
                    # print('\033[1;34mRegrid XE\033[1;0m')
                    x, y, XE = uf.regrid_to_structured(lons, lats, time_series_table[3,:,:], False)
                    self.logger.info('     Regridding U')
                    # print('\033[1;34mRegrid U\033[1;0m')
                    x, y, U = uf.regrid_to_structured(lons, lats, time_series_table[4,:,:], False)
                    self.logger.info('     Regridding V')
                    # print('\033[1;34mRegrid V\033[1;0m')
                    x, y, V = uf.regrid_to_structured(lons, lats, time_series_table[5,:,:], False)
                    if ti_is_available:
                        self.logger.info('     Regridding TI')
                        # print('\033[1;34mRegrid V\033[1;0m')
                        x, y, TI = uf.regrid_to_structured(lons, lats, time_series_table[6,:,:], False)
                    else:
                        TI = U * 0.
                    self.dico_2D_currents = { 'U':U, 'V':V, 'TI':TI, 'SSH':XE, 't':time_vector, 'xc':xc, 'yc':yc, 'x':x, 'y':y}

        except Exception as e:
            # Log the complete error stack trace message in the log
            self.logger.exception(e)
            self.logger.info('**************************')
            self.logger.info('End main module with error')
            self.logger.info('**************************')
            self.logger.handlers.clear()
            raise Exception('Main module failed. Check the log file.')

    def __launch_statistics__(self):
        """Launch statistics on the extractions.
        """
        try:
            self.logger.info('****************************************')
            self.logger.info('            LAUNCH STATISTICS ')
            # print("\033[1;35m######### LAUNCH STATISTICS #########\033[1;0m")
            self.dico_stats = self.TestSite.__launch_stats__(self.dico_for_statistics, self.center_farm_point)
            self.logger.info('****************************************')
            self.logger.info('            LAUNCH 2D STATISTICS ')
            # print("\033[1;35m######### LAUNCH 2D STATISTICS #########\033[1;0m")
            if sf['2D_currents']:
                self.logger.info('1. Currents')
                self.dico_stats2D_currents, ltabu, ltabv, tabu, tabv, du = self.TestSite.__launch_2D_stats__(self.dico_2D_currents)
                number_of_scenarii = len(self.dico_stats2D_currents['p'])

                # monthly 2D currents
                self.dico_stats2D_currents_monthly = []
                ti_all = self.dico_2D_currents['t']
                # test on duration
                # total_duration = (ti_all[-1] - ti_all[0]) / 365.25
                # if total_duration < 1:
                #     print("timeseries too short to compute monthly stats")
                #     dico_output = {'V': V, 'U':U, 'p':p, 'TI':TI, 'x':x, 'y':y, 'SSH':SSH, 'id':idp}
                # else:
                mask_times_CUR = uf.monthly_mask(ti_all)
                for i in range(len(mask_times_CUR)):
                    if len(ti_all[mask_times_CUR[i]]) == 0:
                        self.dico_stats2D_currents_monthly.append({'V':[], 'U':[], 'p':[], 'TI':[], 'x':[], 'y':[], 'SSH':[], 'id':[]})
                    else:
                        self.dico_2D_currents_temp = { 'U':self.dico_2D_currents['U'][:,:,mask_times_CUR[i]], 'V':self.dico_2D_currents['V'][:,:,mask_times_CUR[i]], 'TI':self.dico_2D_currents['TI'][:,:,mask_times_CUR[i]], 'SSH':self.dico_2D_currents['SSH'][:,:,mask_times_CUR[i]], 't':self.dico_2D_currents['t'][i], 'xc':self.dico_2D_currents['xc'], 'yc':self.dico_2D_currents['yc'], 'x':self.dico_2D_currents['x'], 'y':self.dico_2D_currents['y']}
                        dico_stats2D_currents_month, ltabu, ltabv, tabu, tabv, du = self.TestSite.__launch_2D_stats__(self.dico_2D_currents_temp, forced_scenarii=number_of_scenarii, forced_ltabu=ltabu, forced_ltabv=ltabv, forced_tabu=tabu, forced_tabv=tabv, forced_du=du)
                        self.dico_stats2D_currents_monthly.append(dico_stats2D_currents_month)

            if sf['2D_waves']:
                self.logger.info('2. Waves')
                self.dico_stats2D_waves = self.TestSite.__launch_2D_stats__(self.dico_2D_waves)

        except Exception as e:
            # Log the complete error stack trace message in the log
            self.logger.exception(e)
            self.logger.info('**************************')
            self.logger.info('End main module with error')
            self.logger.info('**************************')
            self.logger.handlers.clear()
            raise Exception('Main module failed. Check the log file.')

    def __plot_results__(self, point_name):
        """plots some results of the project (in storage/figures).
        """
        try:
            self.logger.info('****************************************')
            self.logger.info('            PLOTTING RESULTS ')
            # print("\033[1;35m######### PLOTTING RESULTS #########\033[1;0m")
            path_save = os.path.join(PATH_STORAGE, "figures", self.project_name)
            os.makedirs(path_save, exist_ok=True)
            # if sf['Basic_hs'] and sf['Basic_mag'] and sf['Basic_mag10'] and sf['Basic_wlev']:
            #     pf.make_basic_table(path_save, self.dico_stats['WAV']['Basic']['hs'], self.dico_stats['CUR']['Basic']['mag'], self.dico_stats['WIN']['Basic']['mag10'], self.dico_stats['LEV']['Basic']['WLEV'])
            # if sf['EXT_hs'] and sf['EXT_mag'] and sf['EXT_mag10']:
            #     pf.make_extreme_table(path_save, self.dico_stats['WAV']['EXT']['hs'], self.dico_stats['CUR']['EXT']['mag'], self.dico_stats['WIN']['EXT']['mag10'])
            if sf['EJPD_hs_tp']:
                pf.make_EJPD_figure(path_save, "hs-tp", self.dico_stats['WAV']['EJPD']['hs_tp'], "Hs [m]", "Tp [s]")
            if sf['EJPD_hs_dp']:
                pf.make_EJPD_figure(path_save, "hs-dp", self.dico_stats['WAV']['EJPD']['hs_dp'], "Hs [m]", "Dp [°]")
                pf.make_windrose_plot(path_save, "hs-dp", np.array(np.squeeze(self.TS_hs)), np.array(np.squeeze(self.TS_dp)), "Hs [m]", "Dp [°]")
            if sf['EJPD_mag_theta']:
                pf.make_EJPD_figure(path_save, "mag-theta", self.dico_stats['CUR']['EJPD']['mag_theta'], "mag [m/s]", "theta [°]")
                pf.make_windrose_plot(path_save, "mag-theta", np.array(np.squeeze(self.mag)), np.array(np.squeeze(self.theta)), "mag [m/s]", "theta [°]")
            if 'WAV2D' in self.my_databases.keys():
                # bathymetry
                pf.make_2D_map(point_name, path_save, "bathymetry", self.lon_bathy, self.lat_bathy, self.bathy_grid, "Bathymetry [m]", self.FarmFile, self.CorridorFile, 'Blues')
                # seabed types
                pf.make_2D_map_sediments(point_name, path_save, "seabed", self.lon_seabed, self.lat_seabed, self.seabed_grid, "Seabed type", self.FarmFile, self.CorridorFile)
                # waves
                hs_mean = np.nanmean(self.hs,2)
                pf.make_2D_map(point_name, path_save, "hs-mean", self.x, self.y, hs_mean, "Mean Hs [m]", self.FarmFile, self.CorridorFile, 'RdYlBu_r')
                hs_max = np.nanmax(self.hs,2)
                pf.make_2D_map(point_name, path_save, "hs-max", self.x, self.y, hs_max, "Max Hs [m]", self.FarmFile, self.CorridorFile, 'RdYlBu_r')
                # currents
                mag = np.sqrt(self.dico_2D_currents['U']**2 + self.dico_2D_currents['V']**2)
                mag_mean = np.nanmean(mag,2)
                pf.make_2D_map(point_name, path_save, "mag-mean", self.dico_2D_currents['x'], self.dico_2D_currents['y'], mag_mean, "Mean mag [m/s]", self.FarmFile, self.CorridorFile, 'RdYlBu_r')
                mag_max = np.nanmax(mag,2)
                pf.make_2D_map(point_name, path_save, "mag-max", self.dico_2D_currents['x'], self.dico_2D_currents['y'], mag_max, "Max mag [m/s]", self.FarmFile, self.CorridorFile, 'RdYlBu_r')

        except Exception as e:
            # Log the complete error stack trace message in the log
            self.logger.exception(e)
            self.logger.info('**************************')
            self.logger.info('End main module with error')
            self.logger.info('**************************')
            self.logger.handlers.clear()
            raise Exception('Main module failed. Check the log file.')

    def __store_project__(self):
        """Store a complete project (storage).
        """
        try:
            self.logger.info('****************************************')
            self.logger.info('            STORING PROJECT ')
            # print("\033[1;35m######### STORING PROJECT #########\033[1;0m")
            MyProj = Project.Project(self.project_name)
            #######################################################################
            ## DATABASES PATHS
            #######################################################################
            MyProj.databases_paths.description = "Paths to databases or input files"
            MyProj.databases_paths.LeaseArea = str(Path(self.FarmFile))
            MyProj.databases_paths.Corridor = str(Path(self.CorridorFile))
            #MyProj.databases_paths.Devices = str(Path(self.DevicesFile))
            MyProj.databases_paths.Bathymetry = str(Path(self.TestSite.dat_bathy.driver_file))
            MyProj.databases_paths.Seabed = str(Path(self.TestSite.dat_seabed.driver_file))
            MyProj.databases_paths.Roughness = str(Path(self.TestSite.dat_roughness.driver_file))
            MyProj.databases_paths.Species = str(Path(self.TestSite.dat_species.driver_file))
            MyProj.databases_paths.Waves = str(Path(self.TestSite.dat_wave.driver_file))
            MyProj.databases_paths.Currents = str(Path(self.TestSite.dat_current.driver_file))
            MyProj.databases_paths.Winds = str(Path(self.TestSite.dat_wind.driver_file))
            MyProj.databases_paths.Levels = str(Path(self.TestSite.dat_level.driver_file))
            #######################################################################
            ## FARM
            #######################################################################
            MyProj.farm.description = 'Farm or lease area'
            MyProj.farm.name = self.FarmFile
            # Information
            MyProj.farm.info.description = "Information on farm"
            MyProj.farm.info.longitude = [float(x) for x in self.farm_grid_points['lons']]
            MyProj.farm.info.latitude = [float(x) for x in self.farm_grid_points['lats']]
            MyProj.farm.info.boundary.longitude = [float(x) for x in self.farm_points['lons']]
            MyProj.farm.info.boundary.longitude = MyProj.farm.info.boundary.longitude[1:]
            MyProj.farm.info.boundary.latitude = [float(x) for x in self.farm_points['lats']]
            MyProj.farm.info.boundary.latitude = MyProj.farm.info.boundary.latitude[1:]
            MyProj.farm.info.surface = self.farm_area
            MyProj.farm.info.distance = self.distanceC2L
            MyProj.farm.info.zonenumber = self.zonenumber
            MyProj.farm.info.zoneletter = self.zoneletter
            # Bathymetry
            MyProj.farm.direct_values.bathymetry.description = "Water depth"
            MyProj.farm.direct_values.bathymetry.longitude = [float(x) for x in self.farm_grid_points['lons']]
            MyProj.farm.direct_values.bathymetry.latitude = [float(x) for x in self.farm_grid_points['lats']]
            MyProj.farm.direct_values.bathymetry.value = self.F_bathy_extracted_values
            # Seabed type
            MyProj.farm.direct_values.seabed_type.description = "Bottom sediment type"
            MyProj.farm.direct_values.seabed_type.longitude = [float(x) for x in self.farm_grid_points['lons']]
            MyProj.farm.direct_values.seabed_type.latitude = [float(x) for x in self.farm_grid_points['lats']]
            MyProj.farm.direct_values.seabed_type.value = self.F_seabed_extracted_values
            # Roughness length
            MyProj.farm.direct_values.roughness_length.description = "Rougness length calculated using seabed type"
            MyProj.farm.direct_values.roughness_length.longitude = [float(x) for x in self.farm_grid_points['lons']]
            MyProj.farm.direct_values.roughness_length.latitude = [float(x) for x in self.farm_grid_points['lats']]
            MyProj.farm.direct_values.roughness_length.value = self.F_roughness_extracted_values
            # Marine species
            MyProj.farm.direct_values.marine_species.description = "Probability of endangered marine species"
            MyProj.farm.direct_values.marine_species.longitude = [float(x) for x in self.farm_grid_points['lons']]
            MyProj.farm.direct_values.marine_species.latitude = [float(x) for x in self.farm_grid_points['lats']]
            MyProj.farm.direct_values.marine_species.species = self.TestSite.dat_species.species_names
            MyProj.farm.direct_values.marine_species.probability = np.array(self.F_species_extracted_values)
            # Scenarii matrices
            MyProj.farm.scenarii_matrices.currents.description = "Matrix of tidal current probabilities"
            MyProj.farm.scenarii_matrices.currentsmonthly.description = "Matrix of tidal current monthly probabilities"
            if sf['2D_currents']:
                if ((self.project_level == 1) or (len(self.dico_stats2D_currents['x'])==1)):
                    MyProj.farm.scenarii_matrices.currents.x = np.unique([float(x) for x in self.farm_grid_points['lons']])
                    MyProj.farm.scenarii_matrices.currents.y = np.unique([float(x) for x in self.farm_grid_points['lats']])
                    MyProj.farm.scenarii_matrices.currents.p = self.dico_stats2D_currents['p']

                    MyProj.farm.scenarii_matrices.currents.U = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)
                    MyProj.farm.scenarii_matrices.currents.V = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)
                    MyProj.farm.scenarii_matrices.currents.TI = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)
                    MyProj.farm.scenarii_matrices.currents.SSH = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)

                    for i in range(MyProj.farm.scenarii_matrices.currents.U.shape[0]):
                        MyProj.farm.scenarii_matrices.currents.U[i][:][:] = self.dico_stats2D_currents['U'][0][0][i]
                        MyProj.farm.scenarii_matrices.currents.V[i][:][:] = self.dico_stats2D_currents['V'][0][0][i]
                        MyProj.farm.scenarii_matrices.currents.TI[i][:][:] = self.dico_stats2D_currents['TI'][0][0][i]
                        MyProj.farm.scenarii_matrices.currents.SSH[i][:][:] = self.dico_stats2D_currents['SSH'][0][0][i]
                    MyProj.farm.scenarii_matrices.currents.id = self.dico_stats2D_currents['id']
                else:
                    MyProj.farm.scenarii_matrices.currents.x = self.dico_stats2D_currents['x']
                    MyProj.farm.scenarii_matrices.currents.y = self.dico_stats2D_currents['y']
                    MyProj.farm.scenarii_matrices.currents.p = self.dico_stats2D_currents['p']
                    MyProj.farm.scenarii_matrices.currents.id = self.dico_stats2D_currents['id']

                    temp1 = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)
                    temp2 = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)
                    temp3 = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)
                    temp4 = np.zeros((len(MyProj.farm.scenarii_matrices.currents.p),len(MyProj.farm.scenarii_matrices.currents.x),len(MyProj.farm.scenarii_matrices.currents.y)),dtype=float)

                    for i in range(len(MyProj.farm.scenarii_matrices.currents.x)):
                        for j in range(len(MyProj.farm.scenarii_matrices.currents.y)):
                            for k in range(len(MyProj.farm.scenarii_matrices.currents.p)):
                                temp1[k][i][j] = self.dico_stats2D_currents['U'][i][j][k]
                                temp2[k][i][j] = self.dico_stats2D_currents['V'][i][j][k]
                                temp3[k][i][j] = self.dico_stats2D_currents['TI'][i][j][k]
                                temp4[k][i][j] = self.dico_stats2D_currents['SSH'][i][j][k]

                    MyProj.farm.scenarii_matrices.currents.U = temp1
                    MyProj.farm.scenarii_matrices.currents.V = temp2
                    MyProj.farm.scenarii_matrices.currents.TI = temp3
                    MyProj.farm.scenarii_matrices.currents.SSH = temp4
                # Monthly scenarri matrices
                
                from dtop_site.BusinessLogic.libraries.dtosc.outputs import SMCurrentsMonthly
                from dtop_site.BusinessLogic.libraries.dtosc.outputs import MonthResults
                MyProj.farm.scenarii_matrices.currentsmonthly = SMCurrentsMonthly.SMCurrentsMonthly()

                MyProj.farm.scenarii_matrices.currentsmonthly.x = self.dico_stats2D_currents_monthly[0]['x']
                MyProj.farm.scenarii_matrices.currentsmonthly.y = self.dico_stats2D_currents_monthly[0]['y']

                # Manually include results for U

                month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                variables = ['U', 'V', 'TI', 'SSH', 'p', 'id']
                for j in range(6):
                    for i in range(12):
                        my_command1 = "MyProj.farm.scenarii_matrices.currentsmonthly." + variables[j] + "." + month[i] + " = self.dico_stats2D_currents_monthly[" + str(i) + "]['" + variables[j] + "'].tolist()"
                        my_command2 = "MyProj.farm.scenarii_matrices.currentsmonthly." + variables[j] + "." + month[i] + " = self.dico_stats2D_currents_monthly[" + str(i) + "]['" + variables[j] + "']"
                        try:
                            exec(my_command1)
                        except:
                            exec(my_command2)
                
            MyProj.farm.scenarii_matrices.waves.description = "Matrix of wave probabilities"
            if sf['2D_waves']:
                MyProj.farm.scenarii_matrices.waves.x = self.dico_stats2D_waves['x']
                MyProj.farm.scenarii_matrices.waves.y = self.dico_stats2D_waves['y']
                MyProj.farm.scenarii_matrices.waves.HS = self.dico_stats2D_waves['HS']
                MyProj.farm.scenarii_matrices.waves.TP = self.dico_stats2D_waves['TP']
                MyProj.farm.scenarii_matrices.waves.DP = self.dico_stats2D_waves['DP']
                MyProj.farm.scenarii_matrices.waves.p = self.dico_stats2D_waves['p']
            #######################################################################
            ## CORRIDOR
            #######################################################################
            MyProj.corridor.description = 'Corridor'
            MyProj.corridor.name = self.CorridorFile
            # Bathymetry
            MyProj.corridor.direct_values.bathymetry.description = "Water depth"
            MyProj.corridor.direct_values.bathymetry.longitude = [float(x) for x in self.corridor_points['lons']]
            MyProj.corridor.direct_values.bathymetry.latitude = [float(x) for x in self.corridor_points['lats']]
            MyProj.corridor.direct_values.bathymetry.value = self.C_bathy_extracted_values
            # Seabed type
            MyProj.corridor.direct_values.seabed_type.description = "Bottom sediment type"
            MyProj.corridor.direct_values.seabed_type.longitude = [float(x) for x in self.corridor_points['lons']]
            MyProj.corridor.direct_values.seabed_type.latitude = [float(x) for x in self.corridor_points['lats']]
            MyProj.corridor.direct_values.seabed_type.value = self.C_seabed_extracted_values
            # Roughness length
            MyProj.corridor.direct_values.roughness_length.description = "Rougness length calculated using seabed type"
            MyProj.corridor.direct_values.roughness_length.longitude = [float(x) for x in self.corridor_points['lons']]
            MyProj.corridor.direct_values.roughness_length.latitude = [float(x) for x in self.corridor_points['lats']]
            MyProj.corridor.direct_values.roughness_length.value = self.C_roughness_extracted_values
            # Marine species
            MyProj.corridor.direct_values.marine_species.description = "Probability of endangered marine species"
            MyProj.corridor.direct_values.marine_species.longitude = [float(x) for x in self.corridor_points['lons']]
            MyProj.corridor.direct_values.marine_species.latitude = [float(x) for x in self.corridor_points['lats']]
            MyProj.corridor.direct_values.marine_species.species = self.TestSite.dat_species.species_names
            MyProj.corridor.direct_values.marine_species.probability = np.array(self.C_species_extracted_values)
            #######################################################################
            ## POINT of interest (equivalent to center of the farm in this version)
            #######################################################################
            # MyProj.point = PointResults.PointResults()
            # for i in range(len(self.center_farm_point['names'])):
            MyProj.point.name = self.center_farm_point['names'][0]
            # ************ Direct Values **************************************
            # Bathymetry
            MyProj.point.direct_values.bathymetry.description = "Water depth"
            MyProj.point.direct_values.bathymetry.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.direct_values.bathymetry.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.direct_values.bathymetry.value = self.P_bathy_extracted_values[0]
            # Slope angle
            MyProj.point.direct_values.slope.description = "Local slope angle (in degrees)"
            MyProj.point.direct_values.slope.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.direct_values.slope.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.direct_values.slope.value = self.slope_angle[0]
            # Seabed type
            MyProj.point.direct_values.seabed_type.description = "Bottom sediment type"
            MyProj.point.direct_values.seabed_type.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.direct_values.seabed_type.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.direct_values.seabed_type.value = self.P_seabed_extracted_values[0]
            # Roughness length
            MyProj.point.direct_values.roughness_length.description = "Rougness length calculated using seabed type"
            MyProj.point.direct_values.roughness_length.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.direct_values.roughness_length.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.direct_values.roughness_length.value = self.P_roughness_extracted_values[0]
            # Marine species
            MyProj.point.direct_values.marine_species.description = "Probability of endangered marine species"
            MyProj.point.direct_values.marine_species.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.direct_values.marine_species.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.direct_values.marine_species.species = self.TestSite.dat_species.species_names
            MyProj.point.direct_values.marine_species.probability = self.P_species_extracted_values[0]
            # ************ TimeSeries *****************************************
            # Waves
            MyProj.point.time_series.waves.hs.description = "Hs Timeseries"
            MyProj.point.time_series.waves.hs.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.hs.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.hs.times = np.array(np.squeeze(self.TV_hs))
            MyProj.point.time_series.waves.hs.values =  np.array(np.squeeze(self.TS_hs))
            MyProj.point.time_series.waves.tp.description = "Tp Timeseries"
            MyProj.point.time_series.waves.tp.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.tp.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.tp.times =  np.array(np.squeeze(self.TV_tp))
            MyProj.point.time_series.waves.tp.values =  np.array(np.squeeze(self.TS_tp))
            MyProj.point.time_series.waves.dp.description = "Dp Timeseries"
            MyProj.point.time_series.waves.dp.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.dp.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.dp.times =  np.array(np.squeeze(self.TV_dp))
            MyProj.point.time_series.waves.dp.values =  np.array(np.squeeze(self.TS_dp))
            MyProj.point.time_series.waves.CgE.description = "CgE Timeseries"
            MyProj.point.time_series.waves.CgE.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.CgE.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.CgE.times =  np.array(np.squeeze(self.TV_cge))
            MyProj.point.time_series.waves.CgE.values =  np.array(np.squeeze(self.TS_cge))
            MyProj.point.time_series.waves.te.description = "Te Timeseries"
            MyProj.point.time_series.waves.te.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.te.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.te.times =  np.array(np.squeeze(self.TV_te))
            MyProj.point.time_series.waves.te.values =  np.array(np.squeeze(self.TS_te))
            MyProj.point.time_series.waves.gamma.description = "Gamma Timeseries"
            MyProj.point.time_series.waves.gamma.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.gamma.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.gamma.times =  np.array(np.squeeze(self.TV_Gamma))
            MyProj.point.time_series.waves.gamma.values =  np.array(np.squeeze(self.TS_Gamma))
            MyProj.point.time_series.waves.spr.description = "Spr Timeseries"
            MyProj.point.time_series.waves.spr.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.waves.spr.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.waves.spr.times =  np.array(np.squeeze(self.TV_spr))
            MyProj.point.time_series.waves.spr.values =  np.array(np.squeeze(self.TS_spr))
            # Currents
            MyProj.point.time_series.currents.U.description = "U Timeseries"
            MyProj.point.time_series.currents.U.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.currents.U.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.currents.U.times =  np.array(np.squeeze(self.TV_U))
            MyProj.point.time_series.currents.U.values =  np.array(np.squeeze(self.TS_U))
            MyProj.point.time_series.currents.V.description = "V Timeseries"
            MyProj.point.time_series.currents.V.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.currents.V.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.currents.V.times =  np.array(np.squeeze(self.TV_V))
            MyProj.point.time_series.currents.V.values =  np.array(np.squeeze(self.TS_V))
            MyProj.point.time_series.currents.mag.description = "mag Timeseries"
            MyProj.point.time_series.currents.mag.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.currents.mag.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.currents.mag.times =  np.array(np.squeeze(self.TV_U))
            MyProj.point.time_series.currents.mag.values =  np.array(np.squeeze(self.mag))
            MyProj.point.time_series.currents.theta.description = "theta Timeseries"
            MyProj.point.time_series.currents.theta.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.currents.theta.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.currents.theta.times =  np.array(np.squeeze(self.TV_U))
            MyProj.point.time_series.currents.theta.values =  np.array(np.squeeze(self.theta))
            MyProj.point.time_series.currents.Flux.description = "Current flux Timeseries"
            MyProj.point.time_series.currents.Flux.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.currents.Flux.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.currents.Flux.times =  np.array(np.squeeze(self.TV_U))
            MyProj.point.time_series.currents.Flux.values =  np.array(np.squeeze(self.CFlux))
            # Winds
            MyProj.point.time_series.winds.U10.description = "U10 Timeseries"
            MyProj.point.time_series.winds.U10.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.winds.U10.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.winds.U10.times =  np.array(np.squeeze(self.TV_U10))
            MyProj.point.time_series.winds.U10.values =  np.array(np.squeeze(self.TS_U10))
            MyProj.point.time_series.winds.V10.description = "V10 Timeseries"
            MyProj.point.time_series.winds.V10.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.winds.V10.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.winds.V10.times =  np.array(np.squeeze(self.TV_V10))
            MyProj.point.time_series.winds.V10.values =  np.array(np.squeeze(self.TS_V10))
            MyProj.point.time_series.winds.mag10.description = "mag10 Timeseries"
            MyProj.point.time_series.winds.mag10.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.winds.mag10.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.winds.mag10.times =  np.array(np.squeeze(self.TV_U10))
            MyProj.point.time_series.winds.mag10.values =  np.array(np.squeeze(self.mag10))
            MyProj.point.time_series.winds.theta10.description = "theta10 Timeseries"
            MyProj.point.time_series.winds.theta10.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.winds.theta10.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.winds.theta10.times =  np.array(np.squeeze(self.TV_U10))
            MyProj.point.time_series.winds.theta10.values =  np.array(np.squeeze(self.theta10))
            MyProj.point.time_series.winds.gust10.description = "gust10 Timeseries"
            MyProj.point.time_series.winds.gust10.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.winds.gust10.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.winds.gust10.times =  np.array(np.squeeze(self.TV_U10))
            MyProj.point.time_series.winds.gust10.values =  np.array(np.squeeze(self.TS_g10))
            # Water Levels
            MyProj.point.time_series.water_levels.XE.description = "XE Timeseries"
            MyProj.point.time_series.water_levels.XE.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.water_levels.XE.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.water_levels.XE.times =  np.array(np.squeeze(self.TV_XE))
            MyProj.point.time_series.water_levels.XE.values =  np.array(np.squeeze(self.TS_XE))
            MyProj.point.time_series.water_levels.WLEV.description = "WLEV Timeseries"
            MyProj.point.time_series.water_levels.WLEV.longitude = float(self.center_farm_point['lons'][0])
            MyProj.point.time_series.water_levels.WLEV.latitude = float(self.center_farm_point['lats'][0])
            MyProj.point.time_series.water_levels.WLEV.times =  np.array(np.squeeze(self.TV_WLEV))
            MyProj.point.time_series.water_levels.WLEV.values =  np.array(np.squeeze(self.TS_WLEV))
            # ************ Statistics *****************************************
            # Waves ---------
            # Waves-Basic
            names = ['hs', 'tp', 'CgE', 'gamma', 'spr']
            for name in names:
                if sf['Basic_' + name]:
                    exec("MyProj.point.statistics.waves.Basic." + name + ".description = '" + name + " basic statistics'")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".mean = self.dico_stats['WAV']['Basic']['" + name + "']['mean']")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".min = self.dico_stats['WAV']['Basic']['" + name + "']['min']")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".max = self.dico_stats['WAV']['Basic']['" + name + "']['max']")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".median = self.dico_stats['WAV']['Basic']['" + name + "']['median']")
                    exec("MyProj.point.statistics.waves.Basic." + name + ".std = self.dico_stats['WAV']['Basic']['" + name + "']['std']")
                # Waves-EPD
            names = ['hs', 'tp', 'dp']
            for name in names:
                if sf['EPD_' + name]:
                    exec("MyProj.point.statistics.waves.EPD." + name + ".description = '" + name + " EPD'")
                    exec("MyProj.point.statistics.waves.EPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EPD." + name + ".bins = self.dico_stats['WAV']['EPD']['" + name + "']['bins']")
                    exec("MyProj.point.statistics.waves.EPD." + name + ".pdf = self.dico_stats['WAV']['EPD']['" + name + "']['pdf']")
                if sf['EPD_' + name + '_monthly']:
                    exec("MyProj.point.statistics.waves.EPD." + name + "_monthly.description = '" + name + " monthly EPD'")
                    exec("MyProj.point.statistics.waves.EPD." + name + "_monthly.longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EPD." + name + "_monthly.latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EPD." + name + "_monthly.bins = self.dico_stats['WAV']['EPD']['" + name + "_monthly']['bins'][:]")
                    exec("MyProj.point.statistics.waves.EPD." + name + "_monthly.pdf = self.dico_stats['WAV']['EPD']['" + name + "_monthly']['pdf'][:]")
            # Waves-EJPD
            names = ['hs_tp', 'hs_dp']
            for name in names:
                if sf['EJPD_' + name]:
                    exec("MyProj.point.statistics.waves.EJPD." + name + ".description = '" + name + " EJPD'")
                    exec("MyProj.point.statistics.waves.EJPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EJPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EJPD." + name + ".bins1 = self.dico_stats['WAV']['EJPD']['" + name + "']['bins1']")
                    exec("MyProj.point.statistics.waves.EJPD." + name + ".bins2 = self.dico_stats['WAV']['EJPD']['" + name + "']['bins2']")
                    exec("MyProj.point.statistics.waves.EJPD." + name + ".pdf = self.dico_stats['WAV']['EJPD']['" + name + "']['pdf']")
                if sf['EJPD_' + name + '_monthly']:
                    exec("MyProj.point.statistics.waves.EJPD." + name + "_monthly.description = '" + name + " monthly EJPD'")
                    exec("MyProj.point.statistics.waves.EJPD." + name + "_monthly.longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EJPD." + name + "_monthly.latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EJPD." + name + "_monthly.bins1 = self.dico_stats['WAV']['EJPD']['" + name + "_monthly']['bins1'][:]")
                    exec("MyProj.point.statistics.waves.EJPD." + name + "_monthly.bins2 = self.dico_stats['WAV']['EJPD']['" + name + "_monthly']['bins2'][:]")
                    exec("MyProj.point.statistics.waves.EJPD." + name + "_monthly.pdf = self.dico_stats['WAV']['EJPD']['" + name + "_monthly']['pdf'][:]")
            # Waves-EJPD3v
            names = ['hs_tp_dp',]
            for name in names:
                if sf['EJPD3v']:
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".description = '" + name + " EJPD3v'")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".bins1 = self.dico_stats['WAV']['EJPD3v']['" + name + "']['bins1']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".bins2 = self.dico_stats['WAV']['EJPD3v']['" + name + "']['bins2']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".bins3 = self.dico_stats['WAV']['EJPD3v']['" + name + "']['bins3']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".pdf = self.dico_stats['WAV']['EJPD3v']['" + name + "']['pdf']")
                    # flattened
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.description = '" + name + " EJPD3v_flattened'")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.bins1 = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "']['bins1']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.bins2 = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "']['bins2']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.bins3 = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "']['bins3']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.pdf = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "']['pdf']")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_flattened.id = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "']['id']")
                    
                if sf['EJPD3v_monthly']:
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".description = '" + name + " EJPD3v_monthly'")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly.bins1 = self.dico_stats['WAV']['EJPD3v']['" + name + "_monthly']['bins1'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly.bins2 = self.dico_stats['WAV']['EJPD3v']['" + name + "_monthly']['bins2'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly.bins3 = self.dico_stats['WAV']['EJPD3v']['" + name + "_monthly']['bins3'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly.pdf = self.dico_stats['WAV']['EJPD3v']['" + name + "_monthly']['pdf'][:]")
                    # flattened
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.description = '" + name + " EJPD3v_flattened_monthly'")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.bins1 = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "_monthly']['bins1'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.bins2 = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "_monthly']['bins2'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.bins3 = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "_monthly']['bins3'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.pdf = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "_monthly']['pdf'][:]")
                    exec("MyProj.point.statistics.waves.EJPD3v." + name + "_monthly_flattened.id = self.dico_stats['WAV']['EJPD3v_flattened']['" + name + "_monthly']['id'][:]")
            # Waves-EXT
            names = ['hs', 'tp', 'gamma']
            for name in names:
                if sf['EXT_' + name]:
                    exec("MyProj.point.statistics.waves.EXT." + name + ".description = '" + name + " EXT'")
                    exec("MyProj.point.statistics.waves.EXT." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EXT." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EXT." + name + ".return_periods = self.dico_stats['WAV']['EXT']['" + name + "']['return_periods']")
                    exec("MyProj.point.statistics.waves.EXT." + name + ".return_values = self.dico_stats['WAV']['EXT']['" + name + "']['return_values']")
            # Waves-EXC
            names = ['hs_tp']
            for name in names:
                if sf['EXC']:
                    exec("MyProj.point.statistics.waves.EXC." + name + ".description = '" + name + " EXC'")
                    exec("MyProj.point.statistics.waves.EXC." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.waves.EXC." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.waves.EXC." + name + ".return_periods = self.dico_stats['WAV']['EXC']['" + name + "']['return_periods']")
                    exec("MyProj.point.statistics.waves.EXC." + name + ".return_values1 = np.array(self.dico_stats['WAV']['EXC']['" + name + "']['return_values1'])")
                    exec("MyProj.point.statistics.waves.EXC." + name + ".return_values2 = np.array(self.dico_stats['WAV']['EXC']['" + name + "']['return_values2'])")
            for i in range(MyProj.point.statistics.waves.EXC.hs_tp.return_values2.shape[0]):
                for j in range(MyProj.point.statistics.waves.EXC.hs_tp.return_values2.shape[1]):
                    if (MyProj.point.statistics.waves.EXC.hs_tp.return_values1[i][j]>50):
                        MyProj.point.statistics.waves.EXC.hs_tp.return_values1[i,j] = 0
                        MyProj.point.statistics.waves.EXC.hs_tp.return_values2[i,j] = 10
            # Waves-Environments (SK)
            if sf['ENVS']:
                MyProj.point.statistics.waves.ENVS.description = "Environments for fatigue analysis"
                MyProj.point.statistics.waves.ENVS.longitude = float(self.center_farm_point['lons'][0])
                MyProj.point.statistics.waves.ENVS.latitude = float(self.center_farm_point['lats'][0])
                MyProj.point.statistics.waves.ENVS.hs_dp_proba = self.dico_stats['WAV']['ENVS']['hs_dp_proba']
                MyProj.point.statistics.waves.ENVS.intervals_hs_min = self.dico_stats['WAV']['ENVS']['intervals_hs_min']
                MyProj.point.statistics.waves.ENVS.intervals_hs_max = self.dico_stats['WAV']['ENVS']['intervals_hs_max']
                MyProj.point.statistics.waves.ENVS.intervals_dp_min = self.dico_stats['WAV']['ENVS']['intervals_dp_min']
                MyProj.point.statistics.waves.ENVS.intervals_dp_max = self.dico_stats['WAV']['ENVS']['intervals_dp_max']
                MyProj.point.statistics.waves.ENVS.associated_tp = self.dico_stats['WAV']['ENVS']['associated_tp']
                MyProj.point.statistics.waves.ENVS.associated_cur = self.dico_stats['WAV']['ENVS']['associated_cur']
                MyProj.point.statistics.waves.ENVS.associated_wind = self.dico_stats['WAV']['ENVS']['associated_wind']
                MyProj.point.statistics.waves.ENVS.associated_dircur = self.dico_stats['WAV']['ENVS']['associated_dircur']
                MyProj.point.statistics.waves.ENVS.associated_dirwind = self.dico_stats['WAV']['ENVS']['associated_dirwind']
            # Currents ---------
            # Currents-Basic
            names = ['mag', 'Flux']
            for name in names:
                if sf['Basic_' + name]:
                    exec("MyProj.point.statistics.currents.Basic." + name + ".description = '" + name + " basic statistics'")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".mean = self.dico_stats['CUR']['Basic']['" + name + "']['mean']")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".min = self.dico_stats['CUR']['Basic']['" + name + "']['min']")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".max = self.dico_stats['CUR']['Basic']['" + name + "']['max']")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".median = self.dico_stats['CUR']['Basic']['" + name + "']['median']")
                    exec("MyProj.point.statistics.currents.Basic." + name + ".std = self.dico_stats['CUR']['Basic']['" + name + "']['std']")
            # Currents-EPD
            names = ['mag', 'theta']
            for name in names:
                if sf['EPD_' + name]:
                    exec("MyProj.point.statistics.currents.EPD." + name + ".description = '" + name + " EPD'")
                    exec("MyProj.point.statistics.currents.EPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.currents.EPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.currents.EPD." + name + ".bins = self.dico_stats['CUR']['EPD']['" + name + "']['bins']")
                    exec("MyProj.point.statistics.currents.EPD." + name + ".pdf = self.dico_stats['CUR']['EPD']['" + name + "']['pdf']")
                if sf['EPD_' + name + '_monthly']:
                    exec("MyProj.point.statistics.currents.EPD." + name + "_monthly.bins = self.dico_stats['CUR']['EPD']['" + name + "_monthly']['bins']")
                    exec("MyProj.point.statistics.currents.EPD." + name + "_monthly.pdf = self.dico_stats['CUR']['EPD']['" + name + "_monthly']['pdf']")
            # Currents-EJPD
            names = ['mag_theta',]
            for name in names:
                if sf['EJPD_' + name]:
                    exec("MyProj.point.statistics.currents.EJPD." + name + ".description = '" + name + " EJPD'")
                    exec("MyProj.point.statistics.currents.EJPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.currents.EJPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.currents.EJPD." + name + ".bins1 = self.dico_stats['CUR']['EJPD']['" + name + "']['bins1']")
                    exec("MyProj.point.statistics.currents.EJPD." + name + ".bins2 = self.dico_stats['CUR']['EJPD']['" + name + "']['bins2']")
                    exec("MyProj.point.statistics.currents.EJPD." + name + ".pdf = self.dico_stats['CUR']['EJPD']['" + name + "']['pdf']")
                if sf['EJPD_' + name + '_monthly']:
                    exec("MyProj.point.statistics.currents.EJPD." + name + "_monthly.bins1 = self.dico_stats['CUR']['EJPD']['" + name + "_monthly']['bins1']")
                    exec("MyProj.point.statistics.currents.EJPD." + name + "_monthly.bins2 = self.dico_stats['CUR']['EJPD']['" + name + "_monthly']['bins2']")
                    exec("MyProj.point.statistics.currents.EJPD." + name + "_monthly.pdf = self.dico_stats['CUR']['EJPD']['" + name + "_monthly']['pdf']")
            # Currents-EXT
            names = ['mag',]
            for name in names:
                if sf['EXT_' + name]:
                    exec("MyProj.point.statistics.currents.EXT." + name + ".description = '" + name + " EXT'")
                    exec("MyProj.point.statistics.currents.EXT." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.currents.EXT." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.currents.EXT." + name + ".return_periods = self.dico_stats['CUR']['EXT']['" + name + "']['return_periods']")
                    exec("MyProj.point.statistics.currents.EXT." + name + ".return_values = self.dico_stats['CUR']['EXT']['" + name + "']['return_values']")
            # Winds ---------
            # Winds-Basic
            names = ['mag10',]
            for name in names:
                if sf['Basic_' + name]:
                    exec("MyProj.point.statistics.winds.Basic." + name + ".description = '" + name + " basic statistics'")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".mean = self.dico_stats['WIN']['Basic']['" + name + "']['mean']")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".min = self.dico_stats['WIN']['Basic']['" + name + "']['min']")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".max = self.dico_stats['WIN']['Basic']['" + name + "']['max']")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".median = self.dico_stats['WIN']['Basic']['" + name + "']['median']")
                    exec("MyProj.point.statistics.winds.Basic." + name + ".std = self.dico_stats['WIN']['Basic']['" + name + "']['std']")
            # Winds-EPD
            names = ['mag10', 'theta10']
            for name in names:
                if sf['EPD_' + name]:
                    exec("MyProj.point.statistics.winds.EPD." + name + ".description = '" + name + " EPD'")
                    exec("MyProj.point.statistics.winds.EPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.winds.EPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.winds.EPD." + name + ".bins = self.dico_stats['WIN']['EPD']['" + name + "']['bins']")
                    exec("MyProj.point.statistics.winds.EPD." + name + ".pdf = self.dico_stats['WIN']['EPD']['" + name + "']['pdf']")
                if sf['EPD_' + name + '_monthly']:
                    exec("MyProj.point.statistics.winds.EPD." + name + "_monthly.bins = self.dico_stats['WIN']['EPD']['" + name + "_monthly']['bins']")
                    exec("MyProj.point.statistics.winds.EPD." + name + "_monthly.pdf = self.dico_stats['WIN']['EPD']['" + name + "_monthly']['pdf']")
            # Winds-EJPD
            names = ['mag10_theta10',]
            for name in names:
                if sf['EJPD_' + name]:
                    exec("MyProj.point.statistics.winds.EJPD." + name + ".description = '" + name + " EJPD'")
                    exec("MyProj.point.statistics.winds.EJPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.winds.EJPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.winds.EJPD." + name + ".bins1 = self.dico_stats['WIN']['EJPD']['" + name + "']['bins1']")
                    exec("MyProj.point.statistics.winds.EJPD." + name + ".bins2 = self.dico_stats['WIN']['EJPD']['" + name + "']['bins2']")
                    exec("MyProj.point.statistics.winds.EJPD." + name + ".pdf = self.dico_stats['WIN']['EJPD']['" + name + "']['pdf']")
                if sf['EJPD_' + name + '_monthly']:
                    exec("MyProj.point.statistics.winds.EJPD." + name + "_monthly.bins1 = self.dico_stats['WIN']['EJPD']['" + name + "_monthly']['bins1']")
                    exec("MyProj.point.statistics.winds.EJPD." + name + "_monthly.bins2 = self.dico_stats['WIN']['EJPD']['" + name + "_monthly']['bins2']")
                    exec("MyProj.point.statistics.winds.EJPD." + name + "_monthly.pdf = self.dico_stats['WIN']['EJPD']['" + name + "_monthly']['pdf']")
            # Winds-EXT
            names = ['mag10', 'gust10']
            for name in names:
                if sf['EXT_' + name]:
                    exec("MyProj.point.statistics.winds.EXT." + name + ".description = '" + name + " EXT'")
                    exec("MyProj.point.statistics.winds.EXT." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.winds.EXT." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.winds.EXT." + name + ".return_periods = self.dico_stats['WIN']['EXT']['" + name + "']['return_periods']")
                    exec("MyProj.point.statistics.winds.EXT." + name + ".return_values = self.dico_stats['WIN']['EXT']['" + name + "']['return_values']")
            # WaterLevels ---------
            # WaterLevels-Basic
            names = ['WLEV',]
            for name in names:
                if sf['Basic_wlev']:
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".description = '" + name + " basic statistics'")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".mean = self.dico_stats['LEV']['Basic']['" + name + "']['mean']")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".min = self.dico_stats['LEV']['Basic']['" + name + "']['min']")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".max = self.dico_stats['LEV']['Basic']['" + name + "']['max']")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".median = self.dico_stats['LEV']['Basic']['" + name + "']['median']")
                    exec("MyProj.point.statistics.water_levels.Basic." + name + ".std = self.dico_stats['LEV']['Basic']['" + name + "']['std']")
            # WaterLevels-EPD
            names = ['WLEV',]
            for name in names:
                if sf['EPD_wlev']:
                    exec("MyProj.point.statistics.water_levels.EPD." + name + ".description = '" + name + " EPD'")
                    exec("MyProj.point.statistics.water_levels.EPD." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.water_levels.EPD." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.water_levels.EPD." + name + ".bins = self.dico_stats['LEV']['EPD']['" + name + "']['bins']")
                    exec("MyProj.point.statistics.water_levels.EPD." + name + ".pdf = self.dico_stats['LEV']['EPD']['" + name + "']['pdf']")
                if sf['EPD_wlev_monthly']:
                    exec("MyProj.point.statistics.water_levels.EPD." + name + "_monthly.bins = self.dico_stats['LEV']['EPD']['" + name + "_monthly']['bins']")
                    exec("MyProj.point.statistics.water_levels.EPD." + name + "_monthly.pdf = self.dico_stats['LEV']['EPD']['" + name + "_monthly']['pdf']")
            # WaterLevels-EXT
            names = ['WLEVnegative', 'WLEVpositive']
            for name in names:
                if sf['EXT_wlev']:
                    exec("MyProj.point.statistics.water_levels.EXT." + name + ".description = '" + name + " EXT'")
                    exec("MyProj.point.statistics.water_levels.EXT." + name + ".longitude = float(self.center_farm_point['lons'][0])")
                    exec("MyProj.point.statistics.water_levels.EXT." + name + ".latitude = float(self.center_farm_point['lats'][0])")
                    exec("MyProj.point.statistics.water_levels.EXT." + name + ".return_periods = self.dico_stats['LEV']['EXT']['" + name + "']['return_periods']")
                    exec("MyProj.point.statistics.water_levels.EXT." + name + ".return_values = self.dico_stats['LEV']['EXT']['" + name + "']['return_values']")

            # Digital Representation path
            MyProj.path_to_digital_representation =  './' + self.project_name[:-7] + 'dr.json'


            ## SAVE
            final_file = os.path.join(self.storage, self.project_name + '_outputs.h5')
            MyProj.saveHDF5(final_file)
            self.logger.info('****************************************')
            self.logger.info('   PROJECT SUCCESSFULLY CREATED ! ')
            self.logger.info('****************************************')
            self.logger.handlers.clear()
            del self.logger

        except Exception as e:
            # Log the complete error stack trace message in the log
            self.logger.exception(e)
            self.logger.info('**************************')
            self.logger.info('End main module with error')
            self.logger.info('**************************')
            self.logger.handlers.clear()
            raise Exception('Main module failed. Check the log file.')
