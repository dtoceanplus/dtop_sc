# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 14:35:40 2019

@author: ykervell

This file gives the correspondance between energy levels (for waves and currents) and databases
"""

Corresp = {'Waves-low_Currents-low':{'database_name': 'Kattegat', 'longitude': 11.11 , 'latitude': 56.77},
           'Waves-low_Currents-medium':{'database_name': 'Belgium', 'longitude': 2.75 , 'latitude': 51.59},
           'Waves-low_Currents-high':{'database_name': 'SaintGeorges', 'longitude': -6.0 , 'latitude': 55.26},
           'Waves-medium_Currents-low':{'database_name': 'Poland', 'longitude': 14.48 , 'latitude': 54.82},
           'Waves-medium_Currents-medium':{'database_name': 'DanemarkNorway', 'longitude': 8.41 , 'latitude': 57.45},
           'Waves-medium_Currents-high':{'database_name': 'Alderney', 'longitude': -2.0 , 'latitude': 49.76},
           'Waves-high_Currents-low':{'database_name': 'WestIreland', 'longitude': -10.8 , 'latitude': 54.78},
           'Waves-high_Currents-medium':{'database_name': 'Feroe', 'longitude': -7.75 , 'latitude': 62.1},
           'Waves-high_Currents-high':{'database_name': 'Fromveur', 'longitude': -5.03 , 'latitude': 48.44}
        }
Corresp_HOMERE = {'Waves-low_Currents-low':{'database_name': 'SaintBrieuc', 'longitude': -2.6793 , 'latitude': 48.6029},
           'Waves-low_Currents-medium':{'database_name': 'SaintNazaire', 'longitude': -2.3483 , 'latitude': 47.1019},
           'Waves-low_Currents-high':{'database_name': 'Calais', 'longitude': 1.2469 , 'latitude': 50.7955},
           'Waves-medium_Currents-low':{'database_name': 'WestGroix', 'longitude': -3.7484 , 'latitude': 47.7666},
           'Waves-medium_Currents-medium':{'database_name': 'IleDeRe', 'longitude': -1.4145 , 'latitude': 46.1037},
           'Waves-medium_Currents-high':{'database_name': 'RazBlanchard', 'longitude': -2.0 , 'latitude': 49.76},
           'Waves-high_Currents-low':{'database_name': 'Hossegor', 'longitude': -1.5793 , 'latitude': 43.6957},
           'Waves-high_Currents-medium':{'database_name': 'Arcachon', 'longitude': -1.3417 , 'latitude': 44.5289},
           'Waves-high_Currents-high':{'database_name': 'Fromveur', 'longitude': -5.03 , 'latitude': 48.44}
        }