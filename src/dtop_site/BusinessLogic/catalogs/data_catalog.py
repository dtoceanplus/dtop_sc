# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 15:20:42 2019

@author: ykervell

This file is the default input file for databases info (will come with DTOceanPlus software).
"""
import os
from pathlib import Path

working= os.getcwd()
data_path = Path('databases/SiteCharacterisation_Main-Database')
PATH_DATABASES_ALL = os.path.join(data_path, 'All')
PATH_DATABASES_WAVES = os.path.join(data_path, 'Waves')
PATH_DATABASES_WINDS = os.path.join(data_path, 'Winds')
PATH_DATABASES_CURRENTS = os.path.join(data_path, 'Currents')
PATH_DATABASES_BATHYMETRY = os.path.join(data_path, 'Bathymetry')
PATH_DATABASES_SEABED = os.path.join(data_path, 'Seabed')
PATH_DATABASES_SPECIES = os.path.join(data_path, 'Species')
      
SingleValueDatabases = {
            'GEBCO':{
            'driver': {'path': 'Y:\\01 P1\\DATA\\GEBCO\\GEBCO_2019', 'file_ref': 'GEBCO_2019.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 180., 'lat_max': 90},
            'variables': {'name': ('elevation',), 'units': ('m from MSL',)},
                    },
            'GEBCO_Europa_downgrade20':{
            'driver': {'path': PATH_DATABASES_BATHYMETRY, 'file_ref': PATH_DATABASES_BATHYMETRY+'/Europa-TEST2_GEBCO2019_downgrade20.nc', 'type': 'nc'},
            'extent': {'lon_min': -20., 'lat_min': 35., 'lon_max': 20., 'lat_max': 70},
            'variables': {'name': ('elevation',), 'units': ('m from MSL',)},
                    },
            'World_BATHYMETRY_GEBCO2019_9km':{
            'driver': {'path': PATH_DATABASES_BATHYMETRY, 'file_ref': PATH_DATABASES_BATHYMETRY+'/World_BATHYMETRY_GEBCO2019_9km.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 180., 'lat_max': 90},
            'variables': {'name': ('elevation',), 'units': ('m from MSL',)},
                    },
            'France_BATHYMETRY_GEBCO2019_450m':{
            'driver': {'path': PATH_DATABASES_BATHYMETRY, 'file_ref': PATH_DATABASES_BATHYMETRY+'/France_BATHYMETRY_GEBCO2019_450m.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.2, 'lat_min': 43.3, 'lon_max': 2.68, 'lat_max': 51.4},
            'variables': {'name': ('elevation',), 'units': ('m from MSL',)},
                    },
            'France_SEABED-TYPE_SHOM_450m':{
            'driver': {'path': PATH_DATABASES_SEABED, 'file_ref': PATH_DATABASES_SEABED+'/France_SEABED-TYPE_SHOM_450m.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.2, 'lat_min': 43.3, 'lon_max': 2.68, 'lat_max': 51.4},
            'variables': {'name': ('sediment_type',), 'units': (None,)},
            'conversion': {'0': 'No data', '1': 'Rocks_Gravels_Pebbles', '2': 'Very dense sand', '3': 'Dense sand', '4': 'Medium dense sand', '5': 'Loose sand', '6': 'Very loose sand', '7': 'Hard clay', '8': 'Very stiff clay', '9': 'Stiff clay', '10': 'Firm clay', '11': 'Soft clay', '12': 'Very soft clay'}
                    },
            'France_SEABED-ROUGHNESS-LENGTH_SHOM_450m':{
            'driver': {'path': PATH_DATABASES_SEABED, 'file_ref': PATH_DATABASES_SEABED+'/France_SEABED-ROUGHNESS-LENGTH_SHOM_450m.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.2, 'lat_min': 43.3, 'lon_max': 2.68, 'lat_max': 51.4},
            'variables': {'name': ('roughness_length',), 'units': ('m',)}
                    },
            'Seabed_Types':{
            'driver': {'path': PATH_DATABASES_SEABED, 'file_ref': PATH_DATABASES_SEABED+'/World_SEABED-TYPE_SHOM_9km.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 180., 'lat_max': 90},
            'variables': {'name': ('sediment_type',), 'units': (None,)},
            'conversion': {'0': 'No data', '1': 'Rocks_Gravels_Pebbles', '2': 'Very dense sand', '3': 'Dense sand', '4': 'Medium dense sand', '5': 'Loose sand', '6': 'Very loose sand', '7': 'Hard clay', '8': 'Very stiff clay', '9': 'Stiff clay', '10': 'Firm clay', '11': 'Soft clay', '12': 'Very soft clay'}
            # 'conversion': {'0': 'No data', '1': 'Rock', '2': 'Peeble', '3': 'Gravel', '4': 'Sand', '5': 'Fine sand', '6': 'Mud'}
                    },
            'Seabed_Roughness_Length':{
            'driver': {'path': PATH_DATABASES_SEABED, 'file_ref': PATH_DATABASES_SEABED+'/World_SEABED-ROUGHNESS-LENGTH_SHOM_9km.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 180., 'lat_max': 90},
            'variables': {'name': ('roughness_length',), 'units': ('m',)}
                    },
            'Species_default':{
            'driver': {'path': PATH_DATABASES_SPECIES, 'file_ref': PATH_DATABASES_SPECIES+'/species.nc', 'type': 'nc'},
            'extent': {'lon_min': -180., 'lat_min': -90., 'lon_max': 180., 'lat_max': 90},
            'variables': {'name': ('probability',), 'units': (None)},
            'species': {'names': ('Acipenser_sturio', 'Anguilla_anguilla', 'Balaenoptera_borealis', 'Balaenoptera_musculus', 'Carcharodon_carcharias', 'Caretta_caretta', 'Cetorhinus_maximus', 'Chelonia_mydas', 'Dermochelys_coriacea', 'Dipturus_batis', 'Eretmochelys_imbricata', 'Eubalaena_glacialis', 'Lamna_nasus', 'Lepidochelys_kempii', 'Monachus_monachus', 'Phocoena_phocoena', 'Physeter_macrocephalus', 'Rostroraja_alba', 'Squatina_squatina', 'Thynnus_thunnus', 'Tursiops_truncatus')}
                    }
        }

TimeSeriesDatabases = {
            'WestGroix_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/WestGroix_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/WestGroix_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -3.7835, 'lat_min': 47.7329, 'lon_max': -3.7145, 'lat_max': 47.7957},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'WestGroix_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/WestGroix_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/WestGroix_1point/WestGroix_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/WestGroix_1point/WestGroix_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'IleDeRe_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/IleDeRe_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/IleDeRe_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -1.4548, 'lat_min': 46.0625, 'lon_max': -1.3739, 'lat_max': 46.1438},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'IleDeRe_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/IleDeRe_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/IleDeRe_1point/IleDeRe_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/IleDeRe_1point/IleDeRe_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'Hossegor_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/Hossegor_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/Hossegor_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -1.6821, 'lat_min': 43.5907, 'lon_max': -1.4780, 'lat_max': 43.8007},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'Hossegor_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/Hossegor_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/Hossegor_1point/Hossegor_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/Hossegor_1point/Hossegor_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'Calais_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/Calais_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/Calais_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': 1.1, 'lat_min': 50.6459, 'lon_max': 1.3973, 'lat_max': 50.9476},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'Calais_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/Calais_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/Calais_1point/Calais_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/Calais_1point/Calais_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'Arcachon_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/Arcachon_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/Arcachon_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.3798, 'lat_min': 44.47, 'lon_max': -2.3137, 'lat_max': 44.5869},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'Arcachon_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/Arcachon_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/Arcachon_1point/Arcachon_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/Arcachon_1point/Arcachon_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'SaintNazaire_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/SaintNazaire_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/SaintNazaire_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.3798, 'lat_min': 47.0672, 'lon_max': -2.3137, 'lat_max': 47.1364},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'SaintNazaire_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/SaintNazaire_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/SaintNazaire_1point/SaintNazaire_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/SaintNazaire_1point/SaintNazaire_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.3798, 'lat_min': 47.0672, 'lon_max': -2.3137, 'lat_max': 47.1364},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'SaintBrieuc_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/SaintBrieuc_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/SaintBrieuc_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.7131, 'lat_min': 48.5712, 'lon_max': -2.6468, 'lat_max': 48.6350},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'SaintBrieuc_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/SaintBrieuc_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/SaintBrieuc_1point/SaintBrieuc_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/SaintBrieuc_1point/SaintBrieuc_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'Fromveur_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/Fromveur_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/Fromveur_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.0695, 'lat_min': 48.3982, 'lon_max': -4.9886, 'lat_max': 48.4799},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'Fromveur_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/Fromveur_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/Fromveur_1point/Fromveur_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/Fromveur_1point/Fromveur_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'RazBlanchard_all_2D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE_2D/', 'template': PATH_DATABASES_ALL+'/HOMERE_2D/RazBlanchard_${yyyy}_2D.nc', 'year_start': '2014', 'year_end': '2016', 'file_ref': PATH_DATABASES_ALL+'/HOMERE_2D/RazBlanchard_2014_2D.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.0414, 'lat_min': 49.7082, 'lon_max': -1.9598, 'lat_max': 49.8057},
            'variables': {'name': ('hs', 'fp', 'dp', 'wlv', 'ucur', 'vcur'), 'units': ('m', 's-1', '°', 'm', 'm/s', 'm/s')}
                    },
            'RazBlanchard_all_1D':{
            'driver': {'path': PATH_DATABASES_ALL+'/HOMERE/RazBlanchard_1point/', 'template': PATH_DATABASES_ALL+'/HOMERE/RazBlanchard_1point/RazBlanchard_${yyyy}_1point.nc', 'year_start': '1994', 'year_end': '2016', 'file_ref': PATH_DATABASES_WAVES+'/HOMERE/RazBlanchard_1point/RazBlanchard_1994_1point.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd' ), 'units': ('m', 's', '°', 's-1', '°', 'kW.m-1', 'm', 'm/s', 'm/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_Fromveur_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/Fromveur_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Fromveur_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp' ), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_Fromveur_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/Fromveur_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Fromveur_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_Fromveur_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/Fromveur_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/Fromveur_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -5.1, 'lat_min': 48.4, 'lon_max': -5.0, 'lat_max': 48.5},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_Alderney_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/Alderney_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.05, 'lat_min': 49.7, 'lon_max': -1.95, 'lat_max': 49.8},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp' ), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_Alderney_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/Alderney_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.05, 'lat_min': 49.7, 'lon_max': -1.95, 'lat_max': 49.8},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_Alderney_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/Alderney_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/Alderney_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -2.05, 'lat_min': 49.7, 'lon_max': -1.95, 'lat_max': 49.8},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_Feroe_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/Feroe_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -7.8, 'lat_min': 62.05, 'lon_max': -7.7, 'lat_max': 62.15},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp' ), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_Feroe_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/Feroe_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': -7.8, 'lat_min': 62.05, 'lon_max': -7.7, 'lat_max': 62.15},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_Feroe_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/Feroe_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/Feroe_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -7.8, 'lat_min': 62.05, 'lon_max': -7.7, 'lat_max': 62.15},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_WestIreland_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/WestIreland_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -10.85, 'lat_min': 54.73, 'lon_max': -10.75, 'lat_max': 54.83},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp'), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_WestIreland_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/WestIreland_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': -10.85, 'lat_min': 54.73, 'lon_max': -10.75, 'lat_max': 54.83},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_WestIreland_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/WestIreland_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/WestIreland_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -10.85, 'lat_min': 54.73, 'lon_max': -10.75, 'lat_max': 54.83},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_DanemarkNorway_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/DanemarkNorway_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 8.36, 'lat_min': 57.4, 'lon_max': 8.46, 'lat_max': 57.5},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp'), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_DanemarkNorway_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/DanemarkNorway_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': 8.36, 'lat_min': 57.4, 'lon_max': 8.46, 'lat_max': 57.5},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_DanemarkNorway_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/DanemarkNorway_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/DanemarkNorway_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 14.9, 'lat_min': 54.8, 'lon_max': 15.00, 'lat_max': 54.9},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_Poland_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/Poland_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 14.4, 'lat_min': 54.8, 'lon_max': 14.5, 'lat_max': 54.9},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp'), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_Poland_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/Poland_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': 14.4, 'lat_min': 54.8, 'lon_max': 14.5, 'lat_max': 54.9},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_Poland_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/Poland_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/Poland_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 14.4, 'lat_min': 54.8, 'lon_max': 14.5, 'lat_max': 54.9},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_SaintGeorges_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/SaintGeorges_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -6.05, 'lat_min': 55.21, 'lon_max': -5.95, 'lat_max': 55.31},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp'), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_SaintGeorges_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/SaintGeorges_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': -6.05, 'lat_min': 55.21, 'lon_max': -5.95, 'lat_max': 55.31},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_SaintGeorges_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/SaintGeorges_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/SaintGeorges_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': -6.05, 'lat_min': 55.21, 'lon_max': -5.95, 'lat_max': 55.31},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_Belgium_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/Belgium_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 2.7, 'lat_min': 51.54, 'lon_max': 2.8, 'lat_max': 51.64},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp'), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_Belgium_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/Belgium_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': 2.7, 'lat_min': 51.54, 'lon_max': 2.8, 'lat_max': 51.64},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_Belgium_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/Belgium_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/Belgium_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 2.7, 'lat_min': 51.54, 'lon_max': 2.8, 'lat_max': 51.64},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    },
            'EarlyStage_Kattegat_waves':{
            'driver': {'path': PATH_DATABASES_WAVES+'/CAWCR/', 'template': PATH_DATABASES_WAVES+'/CAWCR/${yyyy}/Kattegat_GLOB-24M_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2010', 'file_ref': PATH_DATABASES_WAVES+'/CAWCR/1979/Alderney_GLOB-24M_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 11.06, 'lat_min': 56.72, 'lon_max': 11.16, 'lat_max': 56.82},
            'variables': {'name': ('hs', 't', 't02', 'tp', 'dp'), 'units': ('m', 's', 's', 's', '°', 'm/s', 'm/s')}
                    },
            'EarlyStage_Kattegat_currents':{
            'driver': {'path': PATH_DATABASES_CURRENTS+'/MARS2D', 'template': PATH_DATABASES_CURRENTS+'/MARS2D/${yyyy}/Kattegat_ATLNE2000_${yyyy}${mm}.nc', 'year_start': '2007', 'year_end': '2019', 'file_ref': PATH_DATABASES_CURRENTS+'/MARS2D/DTOceanPlus/ATLNE2000/2007/Alderney_ATLNE2000_200701.nc', 'type': 'nc'},
            'extent': {'lon_min': 11.06, 'lat_min': 56.72, 'lon_max': 11.16, 'lat_max': 56.82},
            'variables': {'name': ('U', 'V', 'XE'), 'units': ('m/s', 'm/s', 'm from MSL')}
                    },
            'EarlyStage_Kattegat_winds':{
            'driver': {'path': PATH_DATABASES_WINDS+'/ERA5/', 'template': PATH_DATABASES_WINDS+'/ERA5/${yyyy}/Kattegat_ERA5_${yyyy}${mm}.nc', 'year_start': '1979', 'year_end': '2019', 'file_ref': PATH_DATABASES_WINDS+'/ERA5/1979/Kattegat_ERA5_197901.nc', 'type': 'nc'},
            'extent': {'lon_min': 11.06, 'lat_min': 56.72, 'lon_max': 11.16, 'lat_max': 56.82},
            'variables': {'name': ('u10', 'v10', 'i10fg' ), 'units': ('m/s', 'm/s', 'm/s')}
                    }
        }

Limiting_values = {
            'hs':{'min': 0., 'max': 100.},
            'tp':{'min': 0., 'max': 100.},
            'fp':{'min': 0., 'max': 10.},
            'dp':{'min': 0., 'max': 360.},
            'ucur':{'min': -10., 'max': 10.},
            'vcur':{'min': -10., 'max': 10.},
            'uwnd':{'min': -100., 'max': 100.},
            'vwnd':{'min': -100., 'max': 100.},
            'ti':{'min': 0., 'max': 1.},
            'wlv':{'min': -100., 'max': 100.}
            }