# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 10:07:24 2019

@author: ykervell

This module defines the class Statistics that is used to build the specific instances.
"""

import numpy as np
from dtop_site.BusinessLogic.packages.lmoments3 import distr

def basic_stats(TS):
    """ compute basic statistics: min, mean, median, max, std
    """
    min_TS = np.nanmin(TS)
    mean_TS = np.nanmean(TS)
    median_TS = np.nanmedian(TS)
    max_TS = np.nanmax(TS)
    std_TS = np.nanstd(TS)
    
    return min_TS, mean_TS, median_TS, max_TS, std_TS

def EPD(TS, bin_value, angular, **kwargs):
    """ compute empirical probability distribution
    """
    bins = kwargs.get("forced_bins1", None)
    if bins is None:
        if angular:
            bins = np.arange(0, 360 + bin_value, bin_value)
        else:
            max_TS = np.ceil(np.nanmax(TS))
            min_TS = np.floor(np.nanmin(TS))
            bins = np.arange(min_TS, max_TS + bin_value, bin_value)
    hist,bins = np.histogram(TS, bins)
    
    return hist, bins

def EJPD(TS1, bin1_value, angular1, TS2, bin2_value, angular2, **kwargs):
    """ compute empirical join probability distribution
    """
    bins1 = kwargs.get("forced_bins1", None)
    if bins1 is None:
        if angular1:
            bins1 = np.arange(0, 360 + bin1_value, bin1_value)
        else:
            max_TS1 = np.ceil(np.nanmax(TS1))
            min_TS1 = np.floor(np.nanmin(TS1))
            bins1 = np.arange(min_TS1, max_TS1 + bin1_value, bin1_value)
    bins2 = kwargs.get("forced_bins2", None)
    if bins2 is None:
        if angular2:
            bins2 = np.arange(0, 360 + bin2_value, bin2_value)
        else:
            max_TS2 = np.ceil(np.nanmax(TS2))
            min_TS2 = np.floor(np.nanmin(TS2))
            bins2 = np.arange(min_TS2, max_TS2 + bin2_value, bin2_value)
    hist, [b1, b2] = np.histogramdd((TS1,TS2),bins=(bins1,bins2))
    
    return hist, b1, b2

def largest_indices(ary, n):
    """Returns the n largest indices from a numpy array."""
    flat = ary.flatten()
    indices = np.argpartition(flat, -n)[-n:]
    indices = indices[np.argsort(-flat[indices])]
    return np.unravel_index(indices, ary.shape)

def Environments_SK(hs, dp, tp, cur, dircur, wind, dirwind):
    """ compute environments for fatigue analysis (Station Keeping)
    """
    # EJPD hs-dp
    hist, b1, b2 = EJPD(hs, 1.0, False, dp, 30.0, True)
    # sort found environments
    nb_env = (len(b1)-1) * (len(b2)-1)
    ind = largest_indices(hist, nb_env)
    # associate cur and wind
    ind_hs = ind[0]
    ind_dp = ind[1]
    proba_envs = []
    intervals_hs_min = []
    intervals_hs_max = []
    intervals_dp_min = []
    intervals_dp_max = []
    tp_envs = []
    cur_envs = []
    dircur_envs = []
    wind_envs = []
    dirwind_envs = []
    for i in range(len(ind_hs)):
        proba = hist[ind_hs[i],ind_dp[i]]/len(hs)*100.
        if proba > 0.00001:
            proba_envs.append(proba)
            hs_lower = b1[ind_hs[i]]
            intervals_hs_min.append(hs_lower)
            hs_upper = b1[ind_hs[i]+1]
            intervals_hs_max.append(hs_upper)
            dp_lower = b2[ind_dp[i]]
            intervals_dp_min.append(dp_lower)
            dp_upper = b2[ind_dp[i]+1]
            intervals_dp_max.append(dp_upper)
            index_env = np.nonzero((hs>=hs_lower) & (hs<=hs_upper) & (dp>=dp_lower) & (dp<=dp_upper))
            tp_envs.append(np.nanmean(tp[index_env]))
            cur_max = np.argmax(cur[index_env])
            cur_envs.append(np.nanmean(cur[index_env]))
            wind_max = np.argmax(wind[index_env])
            wind_envs.append(np.nanmean(wind[index_env]))
            dircur_envs.append(dircur[cur_max])
            dirwind_envs.append(dirwind[wind_max])
    
    return proba_envs, intervals_hs_min, intervals_hs_max, intervals_dp_min, intervals_dp_max, tp_envs, cur_envs, dircur_envs, wind_envs, dirwind_envs

def EJPD3v(TS1, bin1_value, angular1, TS2, bin2_value, angular2, TS3, bin3_value, angular3, **kwargs):
    """ compute empirical join probability distribution within 3 variables
    """
    bins1 = kwargs.get("forced_bins1", None)
    if bins1 is None:
        if angular1:
            bins1 = np.arange(0, 360 + bin1_value, bin1_value)
        else:
            max_TS1 = np.ceil(np.nanmax(TS1))
            min_TS1 = np.floor(np.nanmin(TS1))
            bins1 = np.arange(min_TS1, max_TS1 + bin1_value, bin1_value)
    bins2 = kwargs.get("forced_bins2", None)
    if bins2 is None:
        if angular2:
            bins2 = np.arange(0, 360 + bin2_value, bin2_value)
        else:
            max_TS2 = np.ceil(np.nanmax(TS2))
            min_TS2 = np.floor(np.nanmin(TS2))
            bins2 = np.arange(min_TS2, max_TS2 + bin2_value, bin2_value)
    bins3 = kwargs.get("forced_bins3", None)
    if bins3 is None:
        if angular3:
            bins3 = np.arange(0, 360 + bin3_value, bin3_value)
        else:
            max_TS3 = np.ceil(np.nanmax(TS3))
            min_TS3 = np.floor(np.nanmin(TS3))
            bins3 = np.arange(min_TS3, max_TS3 + bin3_value, bin3_value)
    hist, [b1, b2, b3] = np.histogramdd((TS1,TS2,TS3),bins=(bins1,bins2,bins3))
    
    return hist, b1, b2, b3

###############################################################################
### EXTREMES ##################################################################
def EXT(TS, law, RP):
    """ compute extreme value analysis using a specified law: GPA, GEV, Exp or Weibull
    """
    # from lmoments3 import distr
#    TS = TS[~np.isnan(TS)] # remove nans
    TS_sort = np.sort(TS)
    
    if law == "GPA":
        my_fit = distr.gpa.lmom_fit(TS_sort)
        analysis = distr.gpa(**my_fit)
    elif law == "GEV":
        my_fit = distr.gev.lmom_fit(TS_sort)
        analysis = distr.gev(**my_fit)
    elif law == "Weibull":
        my_fit = distr.wei.lmom_fit(TS_sort)
        analysis = distr.wei(**my_fit)
    elif law == "Exp":
        my_fit = distr.exp.lmom_fit(TS_sort)
        analysis = distr.exp(**my_fit)
    else:
        print("This law is not computed yet")
        
    RV = []
    for i,rp in enumerate(RP):
        rv = round(analysis.ppf(1-(1/(rp))),2)
        if rv < 0:
            eps = 0.1
            rv = round(analysis.ppf(1-(1/(rp+eps))),2)
            if rv < 0:
                RV.append('NotAvailable')
            else:
                RV.append(rv)
        else:
            RV.append(rv)
   
    return RP, RV

def find_peaks(TV, TS, law, RP, **kwargs):
    """ compute extreme value analysis on the sample (empirical values)
    """
    import peakutils
    
#    TV = TV[~np.isnan(TS)] # remove nans
#    TS = TS[~np.isnan(TS)] # remove nans
    total_duration = (TV[-1] - TV[0]) / 365.25
    if total_duration < 1.0:
        total_duration = (np.nanmax(TV[:]) - np.nanmin(TV[:])) / 365.25
    independence_duration = 48. # duration between 2 storms in hours
    dt = (TV[1] - TV[0]) * 24. # timestep in hours
    ndt = independence_duration / dt

    ## THRESHOLD
    # thresholds interval
    # mean to max works well but is long..
#    start_interval = np.mean(TS)
#    end_interval = np.max(TS)
    # test with percentile(90) to the Nth greatest element, where N is the number of years.
    start_interval = np.percentile(TS, 90)
    TS_list = list(TS); TS_list.sort()
    end_interval = TS_list[-int(total_duration)]
    step_threshold = (end_interval - start_interval) / 100.
    thresholds_list = []
    rmse_list = []
    ## interpolation to enhance the resolution of the peak detection (too long at the moment)
#    interpolate = kwargs.get("interpolate", False)
    # interpolate = False
    # if interpolate:
    #     from scipy.interpolate import UnivariateSpline
    #     f_spline = UnivariateSpline(TV, TS, k=3, s=40)
    #     TS_interpolated_spline =  f_spline(TV)
    # else:
    TS_interpolated_spline = TS
    ## determine optimal threshold
    for t in np.arange(start_interval, end_interval + step_threshold, step_threshold):
#        from datetime import datetime
        ind_peaks = peakutils.indexes(TS_interpolated_spline, (t - np.min(TS_interpolated_spline)) / (np.max(TS_interpolated_spline) - np.min(TS_interpolated_spline)), min_dist=ndt)
        # less than 1 storm per year or less than 5 storms (for short durations)
        if len(ind_peaks) < total_duration or len(ind_peaks) < 5:
            continue
        ## EMPIRICAL RV
        empirical_return_periods = []
        empirical_return_values = []
        sorted_sample = np.sort(TS[ind_peaks])
        for value in sorted_sample:
            empirical_return_periods.append(total_duration / np.sum(sorted_sample >= value))
        empirical_return_periods = np.array(empirical_return_periods)
        empirical_return_values = sorted_sample
        try:
            ## THEORETICAL RV
            theoretical_return_periods, theoretical_return_values = EXT(TS[ind_peaks], law, empirical_return_periods)
            ## PERFORMANCE 
            inds = np.isfinite(theoretical_return_values)
            rmse = np.sqrt(np.ma.average((theoretical_return_values - empirical_return_values)[inds]**2, weights=empirical_return_periods[inds]))
    
            thresholds_list.append(t)
            rmse_list.append(rmse)
        except:
            continue

    if rmse_list:
        min_rmse = rmse_list[np.argmin(rmse_list)]
        optimal_threshold = thresholds_list[np.argmin(rmse_list)]
    else:
        min_rmse = 1
        optimal_threshold = start_interval
        

    ## PEAKS (STORMS)
    peaks_indexes = peakutils.indexes(TS, (optimal_threshold - np.min(TS)) / (np.max(TS) - np.min(TS)), min_dist=ndt)
    
    return min_rmse, optimal_threshold, peaks_indexes


def EXC(TS1, TS2, RP, nb_points):
    """ compute extreme contour for Hs/Tp using IFORM
    """
    from viroconcom.fitting import Fit
    from viroconcom.contours import IFormContour

    # Fitting
    dist_description_0 = {'name': 'Weibull_3p', 'dependency': (None, None, None), 'number_of_intervals': nb_points}
    try:
        dist_description_1 = {'name': 'Lognormal', 'dependency': (None, None, 0), 'functions': (None, None, 'exp3')}
        my_fit = Fit((TS1, TS2), (dist_description_0, dist_description_1))
    except:
        dist_description_1 = {'name': 'Normal', 'dependency': (None, None, 0), 'functions': (None, None, 'exp3')}
        my_fit = Fit((TS1, TS2), (dist_description_0, dist_description_1))

    # IFORM
    iform_contours = []
    iform_contours_x_coordinates = []
    iform_contours_y_coordinates = []
    for rp in RP:
        iform_contours.append(IFormContour(my_fit.mul_var_dist, rp, 1, nb_points))
        iform_contours_x_coordinates.append(IFormContour(my_fit.mul_var_dist, rp, 1, nb_points).coordinates[0][0].tolist())
        iform_contours_y_coordinates.append(IFormContour(my_fit.mul_var_dist, rp, 1, nb_points).coordinates[0][1].tolist())

    return iform_contours_x_coordinates, iform_contours_y_coordinates