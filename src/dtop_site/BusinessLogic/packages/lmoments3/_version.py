import json
import sys

version_json = '''
{
 "dirty": false,
 "error": null,
 "full-revisionid": "435a7c5430708a71c26daa3509ec986fb0ffb167",
 "version": "1.0.4"
}
'''  # END VERSION_JSON


def get_versions():
    return json.loads(version_json)
