# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 15:51:55 2019

@author: ykervell

Overview
========
This module defines the Data class that is used to extract Physical Databases.

Usage
=====
The main use of this class is to extract data at a location.

    - Extract at a given location:
        import Data.MyData as MD
        outs = MD.extract_time_series(*args, **kwargs) # refer to extract_time_series
    - Extract the bathymetry:
        outs = MD.extract_bathymetry(*args, **kwargs) # refer to extract_bathymetry

Notes
=====
The extraction can take a long time (particularly on a large number of non-pivoted files).
"""
import os
import netCDF4

possible_lon_names = ['longitude', 'lon', 'x', 'X']
possible_lat_names = ['latitude', 'lat', 'y', 'Y']
possible_bathy_names = ['H0', 'Band1', 'elevation', 'Bathymetry', 'DEPTH', 'depth']
possible_seabed_names = ['roughness_length', 'seabed_type', 'sediment_type']
possible_species_names = ['probability',]

class MyData:
    """ Creation of data from netCDF files to statistics"""

    def __init__(self, data_name, **kw):
        '''Initializes the MyData instance.

        Arguments
        ---------
            - data_name: name of the data (in the file data_catalog or user
            input)

        Keyword arguments
        -----------------
            - data_catalog: a user-defined input file from which the object
            MyData can be initialized.

        '''

        data_catalog = kw.get("user_data_catalog", None)
        self._loaded = False
        self.name = data_name
        self._catalog = data_catalog
        
        if self._catalog == "user_input":
            self.driver_file = data_name
            self.timeseries = kw.get("timeseries", False)
            self.id = kw.get("id", None)
            if not self.timeseries:
                nc = netCDF4.Dataset(data_name)
                self.variables = []
                for ind, var_name in enumerate(nc.variables.keys()):
                    if var_name not in (possible_bathy_names + possible_seabed_names + possible_species_names):
                        if var_name in (possible_lon_names + possible_lat_names):
                            continue
                        if var_name in (['species',]):
                            self.species_names = ('Acipenser_sturio', 'Anguilla_anguilla', 'Balaenoptera_borealis', 'Balaenoptera_musculus', 'Carcharodon_carcharias', 'Caretta_caretta', 'Cetorhinus_maximus', 'Chelonia_mydas', 'Dermochelys_coriacea', 'Dipturus_batis', 'Eretmochelys_imbricata', 'Eubalaena_glacialis', 'Lamna_nasus', 'Lepidochelys_kempii', 'Monachus_monachus', 'Phocoena_phocoena', 'Physeter_macrocephalus', 'Rostroraja_alba', 'Squatina_squatina', 'Thynnus_thunnus', 'Tursiops_truncatus')
                            continue
                        print("%s is not a standard name for a variable" %var_name)
                        raise Exception('NotVariableStandardName')
                        pass
                    else:
                        self.variables.append(var_name)
                        if var_name == 'sediment_type':
                            self.conversion = {'0': 'No data', '1': 'Rocks_Gravels_Pebbles', '2': 'Very dense sand', '3': 'Dense sand', '4': 'Medium dense sand', '5': 'Loose sand', '6': 'Very loose sand', '7': 'Hard clay', '8': 'Very stiff clay', '9': 'Stiff clay', '10': 'Firm clay', '11': 'Soft clay', '12': 'Very soft clay'}
        else:
            data_info = self._find_data_in_catalog(data_catalog)
            self.driver_type = data_info['driver']['type']
            self.driver_path = data_info['driver']['path']
            self.driver_file_ref = data_info['driver']['file_ref']
            if 'species' in data_info.keys():
                self.species_names = data_info['species']['names']
            self.driver_file = self.driver_file_ref
            try:
                self.driver_template = data_info['driver']['template']
                self.driver_year_start = data_info['driver']['year_start']
                self.driver_year_end = data_info['driver']['year_end']
            except:
                print("Extract %s" %data_name)
    
            #       Default Dataset method is the netCDF4 one.
            self.Dataset = netCDF4.Dataset
            self.data_lon_min = data_info['extent']['lon_min']
            self.data_lat_min = data_info['extent']['lat_min']
            self.data_lon_max = data_info['extent']['lon_max']
            self.data_lat_max = data_info['extent']['lat_max']
            self.variables = data_info['variables']['name']
            self.variables_units = data_info['variables']['units']
            # special case with a conversion dictionary (for seabed type for example)
            try:
                self.conversion = data_info['conversion']
            except:
                pass
        
          
    def _find_data_in_catalog(self, data_catalog=None):
        '''Tries to find the database in either:
            - the file given as a parameter by the user (data_catalog)
            - the default catalog file (/BusinessLogic/catalogs/data_catalog.py)
        '''
        # Look for the database in the user input file
        if isinstance(data_catalog, str):
            if not os.path.isfile(data_catalog):
                print('\033[1;41mI cannot find the catalog %s\033[1;0m' %data_catalog)
                raise Exception('NotExistingCatalog')

        if data_catalog is None:
            # No user input file was given, look for our database in the default catalog
            data_info = None
            import dtop_site.BusinessLogic.catalogs.data_catalog as daca
            try:
                data_info = daca.TimeSeriesDatabases[self.name]
            except:
                try:
                    data_info = daca.SingleValueDatabases[self.name]
                except:
                    print('\033[1;41m%s is not in catalogs/data_catalog.py\033[1;0m' %self.name)
                    raise Exception('NotInCatalog')

        else:   # user catalog
            print("TODO: read user catalog")

        self._catalog = data_catalog
        return data_info

    def extract_single_value(self, point_name, lon, lat):#**indices):
        '''Extract a single value (no temporal dimension) from self.driver_file'''
        from dtop_site.BusinessLogic.Location import MyPoint
        point = MyPoint(point_name, lon, lat)
        return point.get_value(data_file=self.driver_file, data_var=self.variables[0])

    def compute_my_slope(self, point_name, lon, lat):#**indices):
        '''Compute slope from self.driver_file'''
        from dtop_site.BusinessLogic.Location import MyPoint
        point = MyPoint(point_name, lon, lat)
        return point.get_local_slope(data_file=self.driver_file, data_var=self.variables[0])


    def extract_time_series(self, var_name, point_name, lat, lon, **kwargs):
        '''
        Loops on the files of the dataset and extracts the variable var_name at the given point in pickle format.
        Date_begin and date_end are defined in catalogs/data_catalog.py.

        Keyword arguments
        -----------------

        Outputs
        -------
        create a pickle (.pyt) file with:
            - time_vector: array of time values in pylab units
            - time_series: array of values of the variable
            
        '''
        from dtop_site.BusinessLogic.Location import MyPoint
#        import pickle
#        import pylab
        import numpy as np
        
        point = MyPoint(point_name, lon, lat)
        time_vector, time_series = point.get_time_series(data_name=self.name, data_template=self.driver_template, year_start=self.driver_year_start, year_end=self.driver_year_end, data_var=var_name)
        time_series = np.ma.filled(time_series, np.nan)

#        # save in pickle format
#        output_pickle_file = 'turlututu.pyt'
#        data_dic = {
#                'time_vector': time_vector,
#                'TS': time_series,
#                   }
#        chan = open(output_pickle_file, 'wb')
#        pickle.dump(data_dic, chan)
#        chan.close()
#
#        # save in csv format
#        output_csv_file = 'turlututu.csv'
#        chan = open(output_csv_file, 'w')
#        for time_value, variable_value in zip(time_vector, time_series):
#            print("####################### time_value:", time_value)
#            print("####################### variable_value:", variable_value)
#            chan.writelines(
#                (pylab.num2date(time_value).strftime("%d/%m/%Y %H:%M"), ' ', str(variable_value), '\n'))
#        chan.close()

        return time_vector, time_series


    def extract_several_time_series(self, var_names, point_name, lat, lon, **kwargs):
        '''
        Loops on the files of the dataset and extracts the variables var_names at the given point.
        Date_begin and date_end are defined in catalogs/data_catalog.py.

        Keyword arguments
        -----------------

        Outputs
        -------
        create arrays or a pickle (.pyt) file with:
            - time_vector: array of time values in pylab units
            - time_series: array of values of the variable
            
        '''
        from dtop_site.BusinessLogic.Location import MyPoint
        import numpy as np
        
        point = MyPoint(point_name, lon, lat)
        level = kwargs.get("mylevel", None)
        if int(level) == 3:
            time_vector, time_series_table = point.get_time_series_several_variables_in_1_file(data_name=self.name, data_vars_list=var_names)
        else:
            time_vector, time_series_table = point.get_time_series_several_variables(data_name=self.name, data_template=self.driver_template, year_start=self.driver_year_start, year_end=self.driver_year_end, data_vars_list=var_names)
        time_series_table = np.ma.filled(time_series_table, np.nan)

        return time_vector, time_series_table

    def intersection_with_polygon(self, polygon, **kwargs):
        '''
        compute the list of the database points that are in the polygon area
        '''
        import pandas as pd
        import numpy as np
        import logging
        self.logger = logging.getLogger()
        
        if self.driver_file[-3:] == '.nc':
            nc = netCDF4.Dataset(self.driver_file)
            lon_nodes = nc.variables['longitude'][:]
            lat_nodes = nc.variables['latitude'][:]
        else:
            df = pd.read_csv(self.driver_file, sep=';')
            len_times = len(np.unique(df['times']))
            len_nodes = int(len(df['longitude'])/len_times)
            lon_nodes = df['longitude'][:len_nodes]
            lat_nodes = df['latitude'][:len_nodes]
        # dico_points['names'] = tuple(['PtC' + str(i) for i in range(len(dico_points['lons']))])
        
        from shapely.geometry import Point
        lons = []
        lats = []
        list_names = []
        for ind in range(len(lon_nodes)):
            pt = Point(lon_nodes[ind],lat_nodes[ind])
            if self.driver_file[-3:] == '.nc':
                if pt.within(polygon):
                    list_names.append("pt" + str(ind))
                    lons.append(lon_nodes[ind])
                    lats.append(lat_nodes[ind])
            else:
                list_names.append("pt" + str(ind))
                lons.append(lon_nodes[ind])
                lats.append(lat_nodes[ind])
        return list_names, lons, lats

    def intersection_with_buffer_rectangle_around_polygon(self, polygon, **kwargs):
        '''
        compute the list of the database points that are in a "buffer" rectangle zone around the polygon area
        '''
        import pandas as pd
        import numpy as np
        import logging
        # import geopandas as gpd
        self.logger = logging.getLogger()
        
        if self.driver_file[-3:] == '.nc':
            nc = netCDF4.Dataset(self.driver_file)
            lon_nodes = nc.variables['longitude'][:]
            lat_nodes = nc.variables['latitude'][:]
        else:
            df = pd.read_csv(self.driver_file, sep=';')
            len_times = len(np.unique(df['times']))
            len_nodes = int(len(df['longitude'])/len_times)
            lon_nodes = df['longitude'][:len_nodes]
            lat_nodes = df['latitude'][:len_nodes]
        
        # data = gpd.read_file(polygon)
        # g = [i for i in data.geometry]
        lons_polygon = polygon.boundary.coords.xy[0]
        lats_polygon = polygon.boundary.coords.xy[1]
        min_lon_polygon = np.nanmin(lons_polygon)
        max_lon_polygon = np.nanmax(lons_polygon)
        min_lat_polygon = np.nanmin(lats_polygon)
        max_lat_polygon = np.nanmax(lats_polygon)
        
        buffer_length = 0.01 # in degrees (WGS84)
        min_lon_buffer = min_lon_polygon - buffer_length
        max_lon_buffer = max_lon_polygon + buffer_length
        min_lat_buffer = min_lat_polygon - buffer_length
        max_lat_buffer = max_lat_polygon + buffer_length

        lons = []
        lats = []
        list_names = []
        for ind in range(len(lon_nodes)):
            if self.driver_file[-3:] == '.nc':
                if ((lon_nodes[ind]>min_lon_buffer) and (lon_nodes[ind]<max_lon_buffer)
                and (lat_nodes[ind]>min_lat_buffer) and (lat_nodes[ind]<max_lat_buffer)):
                    list_names.append("pt" + str(ind))
                    lons.append(lon_nodes[ind])
                    lats.append(lat_nodes[ind])
            else:
                list_names.append("pt" + str(ind))
                lons.append(lon_nodes[ind])
                lats.append(lat_nodes[ind])
        return list_names, lons, lats
        
    def extract_2D_variables(self, polygon, var_names, **kwargs):
        '''
        Loops on the files of the dataset and extracts the variables var_names
        at points which are in the polygon.
        Date_begin and date_end are defined in catalogs/data_catalog.py.

        Keyword arguments
        -----------------

        Outputs
        -------
        create arrays or a pickle (.pyt) file with:
            - time_vector: array of time values in pylab units
            - time_series: array of values of the variable
            
        '''
        #TODO
        from dtop_site.BusinessLogic.Location import MyPoints
        import numpy as np
                
        # points_names, lons, lats = self.intersection_with_polygon(polygon)
        points_names, lons, lats = self.intersection_with_buffer_rectangle_around_polygon(polygon)
        points = MyPoints(points_names, lons, lats)
        
        level = kwargs.get("mylevel", None)
        if int(level) == 3:
            time_vector, time_series_table = points.get_time_series_several_variables_in_1_file(data_name=self.name, data_vars_list=var_names)
        else:
            time_vector, time_series_table = points.get_time_series_several_variables(data_name=self.name, data_template=self.driver_template, year_start=self.driver_year_start, year_end=self.driver_year_end, data_vars_list=var_names)
        time_series_table = np.ma.filled(time_series_table, np.nan)

        return time_vector, time_series_table


