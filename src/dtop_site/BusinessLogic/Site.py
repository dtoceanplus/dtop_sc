# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 09:23:31 2019

@author: ykervell

The Site module is the module that is used to launch all the computations.

Overview
========

This module uses several configuration files (stats_to_launch, databases_catalog, ..) to initialize objects
that will be used by other modules to compute statistics, visualisations and exports.

"""

from dtop_site.BusinessLogic import Data
from dtop_site.BusinessLogic import statistics
from dtop_site.BusinessLogic import useful_functions as uf
from dtop_site.BusinessLogic.stats_list import stats_flags as sf
MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', \
          'August', 'September', 'October', 'November', 'December']


class MySite:

    '''Defines a Site (statistics on user locations of choosen databases).

    Overview
    --------

    The methods of a Site object will launch statistics computation,
    visualisation and export.

    '''

    def __init__(self, my_databases, storage, **kwargs):
        '''
        Arguments
        ---------
            :my_databases:
                user info, provided by gui, databases respectively for waves,
                currents, wind, levels, bathy, seabed type and roughness.
            :storage:
                path to a specific storage

        Keyword arguments
        -----------------
            :todo:
                todo
        '''

        self.wave_database = my_databases['WAV']
        self.current_database = my_databases['CUR']
        try:
            self.wave_2D_database = my_databases['WAV2D']
            self.current_2D_database = my_databases['CUR2D']
        except:
            print("only 1D databases")
        self.wind_database = my_databases['WIN']
        self.level_database = my_databases['LEV']
        self.bathy_database = my_databases['BATHY']
        self.seabed_database = my_databases['SEABED']
        self.roughness_database = my_databases['ROUGHNESS']
        self.species_database = my_databases['SPECIES']
        
        # instance objects MyData
        self.project_level = kwargs.get("mylevel", None)
        if int(self.project_level) == 3:
            self.dat_wave = Data.MyData(self.wave_database, user_data_catalog="user_input", timeseries=True, id="1D")
            self.dat_current = Data.MyData(self.current_database, user_data_catalog="user_input", timeseries=True, id="1D")
            try:
                self.dat_wave2D = Data.MyData(self.wave_2D_database, user_data_catalog="user_input", timeseries=True, id="2D")
                self.dat_current2D = Data.MyData(self.current_2D_database, user_data_catalog="user_input", timeseries=True, id="2D")
            except:
                print("only 1D databases")
            self.dat_wind = Data.MyData(self.wind_database, user_data_catalog="user_input", timeseries=True, id="1D")
            self.dat_level = Data.MyData(self.level_database, user_data_catalog="user_input", timeseries=True, id="1D")
            self.dat_bathy = Data.MyData(self.bathy_database, user_data_catalog="user_input", id="bathy")
            self.dat_seabed = Data.MyData(self.seabed_database, user_data_catalog="user_input", id="seabed")
            self.dat_roughness = Data.MyData(self.roughness_database, user_data_catalog="user_input", id="roughness")
            self.dat_species = Data.MyData(self.species_database, user_data_catalog="user_input", id="species")
        else:
            self.dat_wave = Data.MyData(self.wave_database)
            self.dat_current = Data.MyData(self.current_database)
            try:
                self.dat_wave2D = Data.MyData(self.wave_2D_database)
                self.dat_current2D = Data.MyData(self.current_2D_database)
            except:
                print("only 1D databases")
            self.dat_wind = Data.MyData(self.wind_database)
            self.dat_level = Data.MyData(self.level_database)
            self.dat_bathy = Data.MyData(self.bathy_database)
            self.dat_seabed = Data.MyData(self.seabed_database)
            self.dat_roughness = Data.MyData(self.roughness_database)
            self.dat_species = Data.MyData(self.species_database)
            
    def __extract_2D__(self, points, data_type):
        '''Extract data from databases at farm, corridor or farm center points.
        points: dictionary of geographical coordinates
        data_type: bathy, seabed or roughness
        '''
        
        import netCDF4
        import numpy as np
        lon_min = float(points['lons'][0]) - 0.1
        lon_max = float(points['lons'][-1]) + 0.1
        lat_min = float(points['lats'][0]) - 0.1
        lat_max = float(points['lats'][-1]) + 0.1

        if data_type == "bathy":
            nc_file = self.dat_bathy.driver_file
            var = 'elevation'
        elif data_type == "seabed":
            nc_file = self.dat_seabed.driver_file
            var = 'sediment_type'
        else:
            print("not implemented yet")
            
        with netCDF4.Dataset(nc_file) as nc:
            lat = nc.variables['latitude'][:]
            lon = nc.variables['longitude'][:]
            mask_cut_lon = (lon >= lon_min) & (lon <= lon_max)
            mask_cut_lat = (lat >= lat_min) & (lat <= lat_max)
            lon_new = lon[mask_cut_lon]
            lat_new = lat[mask_cut_lat]
            valid_values = nc.variables[var][mask_cut_lat, mask_cut_lon]
            valid_values = np.ma.array(valid_values, mask=(valid_values < 0))
                
        return lon_new, lat_new, valid_values

        
    def __extract_single_values_at_points__(self, points, data_type):
        '''Extract data from databases at farm, corridor or farm center points.
        points: dictionary of geographical coordinates
        data_type: bathy, seabed or roughness
        '''
        
        import numpy as np
        extracted_values = []
        lons = points['lons']
        lats = points['lats']

        for ind, name in enumerate(points['names']):
            if data_type == "bathy":
                extracted_values.append(np.round(self.dat_bathy.extract_single_value(name, lons[ind], lats[ind]),2))
            elif data_type == "seabed":
                seabed_type_number = self.dat_seabed.extract_single_value(name, lons[ind], lats[ind])
                try:
                    extracted_values.append(self.dat_seabed.conversion[str(int(float(seabed_type_number)))])
                except:
                    extracted_values.append('Warning: on land')
            elif data_type == "roughness":
                roughness_value = np.round(float(self.dat_roughness.extract_single_value(name, lons[ind], lats[ind])),8)
                if np.isnan(roughness_value):
                    roughness_value = 0.0
                extracted_values.append(roughness_value)
            elif data_type == "species":
                species_proba = np.ma.filled(self.dat_species.extract_single_value(name, lons[ind], lats[ind]), fill_value = 9999.)
                extracted_values.append(species_proba)
            else:
                print("not implemented yet")

        return extracted_values

    def __compute_slope__(self, points):
        '''Compute slope from bathymetry database.
        points: dictionary of geographical coordinates
        '''
        
        import numpy as np
        extracted_values = []
        lons = points['lons']
        lats = points['lats']

        for ind, name in enumerate(points['names']):
            extracted_values.append(np.round(self.dat_bathy.compute_my_slope(name, lons[ind], lats[ind]),2))

        return extracted_values

    
    def __extract_time_series_at_points__(self, database, points, var):
        '''Extract time series from databases at the center of the farm.
        points: dictionary of geographical coordinates
        var: variable to extract
        '''

        lons = points['lons']
        lats = points['lats']

        List_TV = []    # TV for Time Vector
        List_TS = []    # TS for TimeSeries
        for ind, name in enumerate(points['names']):
            try:
                TV, TS = database.extract_time_series(var, name, lons[ind], lats[ind])
                TV, TS = uf.remove_nans(TV, TS)
            except:
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                print("Check your input file please; strange characters are present.")
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            List_TV.append(TV.tolist()); List_TS.append(TS.tolist())

        return  List_TV, List_TS        

    def __extract_several_time_series_at_points__(self, database, points, variables):
        '''Extract several time series from database at the center of the farm.
        points: dictionary of geographical coordinates
        variables: list of variables to extract
        '''

        lons = points['lons']
        lats = points['lats']

        List_TV = []    # TV for Time Vector
        List_TS_table = []    # TS for TimeSeries
#        import numpy as np
        for ind, name in enumerate(points['names']):
            try:
                TV, TS_table = database.extract_several_time_series(variables, name, lons[ind], lats[ind], mylevel=self.project_level)
                TV, TS_table = uf.remove_table_nans(TV, TS_table)
            except:
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                print("Check your input file please; strange characters are present.")
                print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            List_TV.append(TV.tolist()); List_TS_table.append(TS_table.tolist())

        return  List_TV, List_TS_table       

    def __extract_several_2D_variables__(self, polygon, database, variables):
        '''Extract several variables from 2D database.
        polygon: farm area, from the input shapefile
        variables: list of variables to extract
        '''

        time_vector, time_series_table = database.extract_2D_variables(polygon, variables, mylevel=self.project_level)

        return  time_vector, time_series_table      
        

    def __launch_stats__(self, dico_computation, points):
        '''launch statistics at the center of the farm.
        dico_computation: dictionary of time vectors and time values
        '''
        import numpy as np
        import logging
        self.logger = logging.getLogger()

        
        # Time masks for monthly periodicity
        self.logger.info('1. Computing monthly masks')
        # print("MONTHLY MASKS")
        if sf['EPD_hs_monthly'] or sf['EPD_tp_monthly'] or sf['EPD_dp_monthly']\
             or sf['EJPD_hs_dp_monthly'] or sf['EJPD_hs_tp_monthly'] or \
                     sf['EJPD3v_monthly']:
            mask_times_WAV_hs = uf.monthly_mask(np.array(dico_computation['WAV']['hs']['times']))
            mask_times_WAV_tp = mask_times_WAV_hs
            mask_times_WAV_dp = mask_times_WAV_hs
            # mask_times_WAV_tp = uf.monthly_mask(np.array(dico_computation['WAV']['tp']['times']))
            # mask_times_WAV_dp = uf.monthly_mask(np.array(dico_computation['WAV']['dp']['times']))
        if sf['EPD_mag_monthly'] or sf['EPD_theta_monthly'] or sf['EJPD_mag_theta_monthly']:
            times_CUR = np.array(dico_computation['CUR']['U']['times'])
            mask_times_CUR = uf.monthly_mask(times_CUR)
        if sf['EPD_mag10_monthly'] or sf['EPD_theta10_monthly'] or sf['EJPD_mag10_theta10_monthly']:
            times_WIN = np.array(dico_computation['WIN']['U10']['times'])
            mask_times_WIN = uf.monthly_mask(times_WIN)
        if sf['EPD_wlev_monthly']:
            times_LEV = np.array(dico_computation['LEV']['WLEV']['times'])
            mask_times_LEV = uf.monthly_mask(times_LEV)
        
        # # Initialise lists (lists are now dedicated to months)
        # hs_min_list = []; hs_mean_list = []; hs_median_list = []; hs_max_list = []; hs_std_list = []
        # cge_min_list = []; cge_mean_list = []; cge_median_list = []; cge_max_list = []; cge_std_list = []
        # tp_min_list = []; tp_mean_list = []; tp_median_list = []; tp_max_list = []; tp_std_list = []
        # gamma_min_list = []; gamma_mean_list = []; gamma_median_list = []; gamma_max_list = []; gamma_std_list = []
        # spr_min_list = []; spr_mean_list = []; spr_median_list = []; spr_max_list = []; spr_std_list = []
        # mag_min_list = []; mag_mean_list = []; mag_median_list = []; mag_max_list = []; mag_std_list = []
        # cp_min_list = []; cp_mean_list = []; cp_median_list = []; cp_max_list = []; cp_std_list = []
        # mag10_min_list = []; mag10_mean_list = []; mag10_median_list = []; mag10_max_list = []; mag10_std_list = []
        # wlev_min_list = []; wlev_mean_list = []; wlev_median_list = []; wlev_max_list = []; wlev_std_list = []
        # hist_hs_list = []; bins_hs_list = []
        # hist_tp_list = []; bins_tp_list = []
        # hist_dp_list = []; bins_dp_list = []
        # hist_mag_list = []; bins_mag_list = []
        # hist_theta_list = []; bins_theta_list = []
        # hist_mag10_list = []; bins_mag10_list = []
        # hist_theta10_list = []; bins_theta10_list = []
        # hist_wlev_list = []; bins_wlev_list = []
        # hist_hs_tp_list = []; hist_hs_dp_list = []
        # bins_hs_hist_hs_tp_list = []; bins_tp_hist_hs_tp_list = []
        # bins_hs_hist_hs_dp_list = []; bins_dp_hist_hs_dp_list = []
        # hist_hs_tp_dp_list = []
        # bins_hs_hist_hs_tp_dp_list = []; bins_tp_hist_hs_tp_dp_list = []; bins_dp_hist_hs_tp_dp_list = []
        # hist_hs_tp_dp_flattened_list = []; id_flattened_list = []
        # bins_hs_hist_hs_tp_dp_flattened_list = []; bins_tp_hist_hs_tp_dp_flattened_list = []; bins_dp_hist_hs_tp_dp_flattened_list = []
        # hist_mag_theta_list = []; bins_mag_hist_mag_theta_list = []; bins_theta_hist_mag_theta_list = []
        # hist_mag10_theta10_list = []; bins_mag10_hist_mag10_theta10_list = []; bins_theta10_hist_mag10_theta10_list = []
        hist_hs_month_list = []; bins_hs_month_list = []
        hist_tp_month_list = []; bins_tp_month_list = []
        hist_dp_month_list = []; bins_dp_month_list = []
        hist_mag_month_list = []; bins_mag_month_list = []
        hist_theta_month_list = []; bins_theta_month_list = []
        hist_mag10_month_list = []; bins_mag10_month_list = []
        hist_theta10_month_list = []; bins_theta10_month_list = []
        hist_wlev_month_list = []; bins_wlev_month_list = []
        hist_hs_tp_month_list = []; hist_hs_dp_month_list = []
        bins_hs_hist_hs_tp_month_list = []; bins_tp_hist_hs_tp_month_list = []
        bins_hs_hist_hs_dp_month_list = []; bins_dp_hist_hs_dp_month_list = []
        hist_hs_tp_dp_month_list = []
        bins_hs_hist_hs_tp_dp_month_list = []; bins_tp_hist_hs_tp_dp_month_list = []; bins_dp_hist_hs_tp_dp_month_list = []
        hist_hs_tp_dp_month_flattened_list = []; id_month_flattened_list = []
        bins_hs_hist_hs_tp_dp_month_flattened_list = []; bins_tp_hist_hs_tp_dp_month_flattened_list = []; bins_dp_hist_hs_tp_dp_month_flattened_list = []
        hist_mag_theta_month_list= []; bins_mag_hist_mag_theta_month_list= []; bins_theta_hist_mag_theta_month_list= []
        hist_mag10_theta10_month_list= []; bins_mag10_hist_mag10_theta10_month_list= []; bins_theta10_hist_mag10_theta10_month_list= []
        # RV_hs_list = []; RV_tp_list = []; RV_mag_list = []; RV_mag10_list = []; RV_G10_list = []      
        # RV_xe_positive_list = []; RV_xe_negative_list = []
        # RV_wlev_positive_list = []; RV_wlev_negative_list = []
        # X_IFORM_hs_tp_list = []; Y_IFORM_hs_tp_list = []
        # proba_envs_list = []; intervals_hs_min_list = []
        # intervals_hs_max_list = []; intervals_dp_min_list = []
        # intervals_dp_max_list = []; tp_envs_list = []
        # cur_envs_list = []; dircur_envs_list = []
        # wind_envs_list = []; dirwind_envs_list = []

        # for ind, name in enumerate(points['names']):
        ind = 0 # only 1 point, the center of the farm
        ## BASIC STATISTICS
        self.logger.info('2. Computing basic statistics')
        # print("BASIC STATISTICS")
        # Waves
        if sf['Basic_hs']:
            hs_min, hs_mean, hs_median, hs_max, hs_std = statistics.basic_stats(dico_computation['WAV']['hs']['values'][ind])
            # hs_min_list.append(hs_min.tolist()); hs_mean_list.append(hs_mean.tolist()); hs_median_list.append(hs_median.tolist())
            # hs_max_list.append(hs_max.tolist()); hs_std_list.append(hs_std.tolist())
        if sf['Basic_CgE']:
            cge_min, cge_mean, cge_median, cge_max, cge_std = statistics.basic_stats(dico_computation['WAV']['cge']['values'][ind])
            # cge_min_list.append(cge_min.tolist()); cge_mean_list.append(cge_mean.tolist()); cge_median_list.append(cge_median.tolist())
            # cge_max_list.append(cge_max.tolist()); cge_std_list.append(cge_std.tolist())
        if sf['Basic_tp']:
            tp_min, tp_mean, tp_median, tp_max, tp_std = statistics.basic_stats(dico_computation['WAV']['tp']['values'][ind])
            # tp_min_list.append(tp_min.tolist()); tp_mean_list.append(tp_mean.tolist()); tp_median_list.append(tp_median.tolist())
            # tp_max_list.append(tp_max.tolist()); tp_std_list.append(tp_std.tolist())
        if sf['Basic_gamma']:
            gamma_min, gamma_mean, gamma_median, gamma_max, gamma_std = statistics.basic_stats(dico_computation['WAV']['gamma']['values'][ind])
            # gamma_min_list.append(gamma_min.tolist()); gamma_mean_list.append(gamma_mean.tolist()); gamma_median_list.append(gamma_median.tolist())
            # gamma_max_list.append(gamma_max.tolist()); gamma_std_list.append(gamma_std.tolist())
        if sf['Basic_spr']:
            spr_min, spr_mean, spr_median, spr_max, spr_std = statistics.basic_stats(dico_computation['WAV']['spr']['values'][ind])
            # spr_min_list.append(spr_min.tolist()); spr_mean_list.append(spr_mean.tolist()); spr_median_list.append(spr_median.tolist())
            # spr_max_list.append(spr_max.tolist()); spr_std_list.append(spr_std.tolist())
        # Currents
        u = np.squeeze(np.array(dico_computation['CUR']['U']['values'][ind]))
        v = np.squeeze(np.array(dico_computation['CUR']['V']['values'][ind]))
        mag = np.sqrt(np.power(u,2) + np.power(v,2))
        theta = uf.angle(u,v)
        if sf['Basic_mag']:
            mag_min, mag_mean, mag_median, mag_max, mag_std = statistics.basic_stats(mag)
            # mag_min_list.append(mag_min.tolist()); mag_mean_list.append(mag_mean.tolist()); mag_median_list.append(mag_median.tolist())
            # mag_max_list.append(mag_max.tolist()); mag_std_list.append(mag_std.tolist())
        if sf['Basic_Flux']:
            cp_min, cp_mean, cp_median, cp_max, cp_std = statistics.basic_stats(dico_computation['CUR']['cflux']['values'][ind])
            # cp_min_list.append(cp_min.tolist()); cp_mean_list.append(cp_mean.tolist()); cp_median_list.append(cp_median.tolist())
            # cp_max_list.append(cp_max.tolist()); cp_std_list.append(cp_std.tolist())
        # Winds
        u10 = np.squeeze(np.array(dico_computation['WIN']['U10']['values'][ind]))
        v10 = np.squeeze(np.array(dico_computation['WIN']['V10']['values'][ind]))
        mag10 = np.sqrt(np.power(u10,2) + np.power(v10,2))
        theta10 = uf.angle_meteo(u10,v10)
        if sf['Basic_mag10']:
            mag10_min, mag10_mean, mag10_median, mag10_max, mag10_std = statistics.basic_stats(mag10)
            # mag10_min_list.append(mag10_min.tolist()); mag10_mean_list.append(mag10_mean.tolist()); mag10_median_list.append(mag10_median.tolist())
            # mag10_max_list.append(mag10_max.tolist()); mag10_std_list.append(mag10_std.tolist())
        # Water levels
        if sf['Basic_wlev']:
            wlev_min, wlev_mean, wlev_median, wlev_max, wlev_std = statistics.basic_stats(dico_computation['LEV']['WLEV']['values'][ind])
            # wlev_min_list.append(wlev_min.tolist()); wlev_mean_list.append(wlev_mean.tolist()); wlev_median_list.append(wlev_median.tolist())
            # wlev_max_list.append(wlev_max.tolist()); wlev_std_list.append(wlev_std.tolist())
        
        ## EPD
        self.logger.info('3. Computing EPD')
        # print("EPD")
        # Waves
        if sf['EPD_hs'] or sf['EPD_hs_monthly']:
             hist_hsY, bins_hsY = statistics.EPD(dico_computation['WAV']['hs']['values'][ind], 0.5, False)
            # hist_hs_list.append(hist_hs.tolist()); bins_hs_list.append(bins_hs.tolist())
        if sf['EPD_tp'] or sf['EPD_tp_monthly']:
            hist_tpY, bins_tpY = statistics.EPD(dico_computation['WAV']['tp']['values'][ind], 0.5, False)
            # hist_tp_list.append(hist_tp.tolist()); bins_tp_list.append(bins_tp.tolist())
        if sf['EPD_dp'] or sf['EPD_dp_monthly']:
            hist_dpY, bins_dpY = statistics.EPD(dico_computation['WAV']['dp']['values'][ind], 10., True)
            # hist_dp_list.append(hist_dp.tolist()); bins_dp_list.append(bins_dp.tolist())
        # Currents
        if sf['EPD_mag'] or sf['EPD_mag_monthly']:
            hist_magY, bins_magY = statistics.EPD(mag, 0.1, False)
            # hist_mag_list.append(hist_mag.tolist()); bins_mag_list.append(bins_mag.tolist())
        if sf['EPD_theta'] or sf['EPD_theta_monthly']:
            hist_thetaY, bins_thetaY = statistics.EPD(theta, 10., True)
            # hist_theta_list.append(hist_theta.tolist()); bins_theta_list.append(bins_theta.tolist())
        # Winds
        if sf['EPD_mag10'] or sf['EPD_mag10_monthly']:
            hist_mag10Y, bins_mag10Y = statistics.EPD(mag10, 0.5, False)
            # hist_mag10_list.append(hist_mag10.tolist()); bins_mag10_list.append(bins_mag10.tolist())
        if sf['EPD_theta10'] or sf['EPD_theta10_monthly']:
            hist_theta10Y, bins_theta10Y = statistics.EPD(theta10, 10., True)
            # hist_theta10_list.append(hist_theta10.tolist()); bins_theta10_list.append(bins_theta10.tolist())
        # Water levels
        if sf['EPD_wlev'] or sf['EPD_wlev_monthly']:
            hist_wlevY, bins_wlevY = statistics.EPD(dico_computation['LEV']['WLEV']['values'][ind], 0.1, False)
            # hist_wlev_list.append(hist_wlev.tolist()); bins_wlev_list.append(bins_wlev.tolist())

        ## Monthly EPD
        self.logger.info('4. Computing monthly EPD')
        # print("Monthly EPD")
        # Waves
        if sf['EPD_hs_monthly'] or sf['EPD_tp_monthly'] or sf['EPD_dp_monthly']:
            for i in range(len(mask_times_WAV_hs)):
                if sf['EPD_hs_monthly']:
                    BINS_HS = bins_hsY
                    hs = np.array(dico_computation['WAV']['hs']['values'][ind])
                    hs_tmp = hs[mask_times_WAV_hs[i]]
                    if len(hs_tmp) == 0:
                        continue
                    hist_hs, bins_hs = statistics.EPD(hs_tmp, 0.5, False, forced_bins1=BINS_HS)
                    hist_hs_month_list.append(hist_hs.tolist()); bins_hs_month_list.append(bins_hs.tolist())
                if sf['EPD_tp_monthly']:
                    BINS_TP = bins_tpY
                    tp = np.array(dico_computation['WAV']['tp']['values'][ind])
                    tp_tmp = tp[mask_times_WAV_tp[i]]
                    hist_tp, bins_tp = statistics.EPD(tp_tmp, 0.5, False, forced_bins1=BINS_TP)
                    hist_tp_month_list.append(hist_tp.tolist()); bins_tp_month_list.append(bins_tp.tolist())
                if sf['EPD_dp_monthly']:
                    BINS_DP = bins_dpY
                    dp = np.array(dico_computation['WAV']['dp']['values'][ind])
                    dp_tmp = dp[mask_times_WAV_dp[i]]
                    hist_dp, bins_dp = statistics.EPD(dp_tmp, 10., True, forced_bins1=BINS_DP)
                    hist_dp_month_list.append(hist_dp.tolist()); bins_dp_month_list.append(bins_dp.tolist())
        # Currents
        if sf['EPD_mag_monthly'] or sf['EPD_theta_monthly']:
            for i in range(len(mask_times_CUR)):
                if sf['EPD_mag_monthly']:
                    BINS_MAG = bins_magY
                    mag_tmp = mag[mask_times_CUR[i]]
                    if len(mag_tmp) == 0:
                        continue
                    hist_mag, bins_mag = statistics.EPD(mag_tmp, 0.1, False, forced_bins1=BINS_MAG)
                    hist_mag_month_list.append(hist_mag.tolist()); bins_mag_month_list.append(bins_mag.tolist())
                if sf['EPD_theta_monthly']:
                    BINS_THETA = bins_thetaY
                    theta_tmp = theta[mask_times_CUR[i]]
                    hist_theta, bins_theta = statistics.EPD(theta_tmp, 10., True, forced_bins1=BINS_THETA)
                    hist_theta_month_list.append(hist_theta.tolist()); bins_theta_month_list.append(bins_theta.tolist())
        # Winds
        if sf['EPD_mag10_monthly'] or sf['EPD_theta10_monthly']:
            for i in range(len(mask_times_WIN)):
                if sf['EPD_mag10_monthly']:
                    BINS_MAG10 = bins_mag10Y
                    mag10_tmp = mag10[mask_times_WIN[i]]
                    if len(mag10_tmp) == 0:
                        continue
                    hist_mag10, bins_mag10 = statistics.EPD(mag10_tmp, 0.5, False, forced_bins1=BINS_MAG10)
                    hist_mag10_month_list.append(hist_mag10.tolist()); bins_mag10_month_list.append(bins_mag10.tolist())
                if sf['EPD_theta10_monthly']:
                    BINS_THETA10 = bins_theta10Y
                    theta10_tmp = theta10[mask_times_WIN[i]]
                    hist_theta10, bins_theta10 = statistics.EPD(theta10_tmp, 10., True, forced_bins1=BINS_THETA10)
                    hist_theta10_month_list.append(hist_theta10.tolist()); bins_theta10_month_list.append(bins_theta10.tolist())
        # Water levels
        if sf['EPD_wlev_monthly']:
            BINS_WLEV = bins_wlevY
            for i in range(len(mask_times_LEV)):
                wlev = np.array(dico_computation['LEV']['WLEV']['values'][ind])
                wlev_tmp = wlev[mask_times_LEV[i]]
                if len(wlev_tmp) == 0:
                    continue
                hist_wlev, bins_wlev = statistics.EPD(wlev_tmp, 0.1, False, forced_bins1=BINS_WLEV)
                hist_wlev_month_list.append(hist_wlev.tolist()); bins_wlev_month_list.append(bins_wlev.tolist())

        ## EJPD
        self.logger.info('5. Computing EJPD')
        # print("EJPD")
        # Waves
        TS_hs = dico_computation['WAV']['hs']['values']
        TV_hs = dico_computation['WAV']['hs']['times']
        TS_tp = dico_computation['WAV']['tp']['values']
        TV_tp = dico_computation['WAV']['tp']['times']
        TS_dp = dico_computation['WAV']['dp']['values']
        TV_dp = dico_computation['WAV']['dp']['times']
        TV_hs2, TS_hs2, TV_tp2, TS_tp2 = uf.common_2series(TV_hs, TS_hs, TV_tp, TS_tp)
        TV_hs3, TS_hs3, TV_dp3, TS_dp3 = uf.common_2series(TV_hs, TS_hs, TV_dp, TS_dp)
        TV_hs4, TS_hs4, TV_tp4, TS_tp4, TV_dp4, TS_dp4 = uf.common_3series(TV_hs, TS_hs, TV_tp, TS_tp, TV_dp, TS_dp)
        
        if sf['EJPD_hs_tp']:
            hist_hs_tpY, bins_hs_hist_hs_tpY, bins_tp_hist_hs_tpY = statistics.EJPD(TS_hs2[ind], 0.5, False, TS_tp2[ind], 1.0, False)
            # hist_hs_tp_list.append(hist_hs_tp.tolist()); bins_hs_hist_hs_tp_list.append(bins_hs_hist_hs_tp.tolist()); bins_tp_hist_hs_tp_list.append(bins_tp_hist_hs_tp.tolist())
        if sf['EJPD_hs_dp']:
            hist_hs_dpY, bins_hs_hist_hs_dpY, bins_dp_hist_hs_dpY = statistics.EJPD(TS_hs3[ind], 0.5, False, TS_dp3[ind], 10, True)
            # hist_hs_dp_list.append(hist_hs_dp.tolist()); bins_hs_hist_hs_dp_list.append(bins_hs_hist_hs_dp.tolist()); bins_dp_hist_hs_dp_list.append(bins_dp_hist_hs_dp.tolist())
        # Currents
        if sf['EJPD_mag_theta']:
            hist_mag_thetaY, bins_mag_hist_mag_thetaY, bins_theta_hist_mag_thetaY = statistics.EJPD(mag, 0.1, False, theta, 10., True)
            # hist_mag_theta_list.append(hist_mag_theta.tolist()); bins_mag_hist_mag_theta_list.append(bins_mag_hist_mag_theta.tolist()); bins_theta_hist_mag_theta_list.append(bins_theta_hist_mag_theta.tolist())
        # Winds
        if sf['EJPD_mag10_theta10']:
            hist_mag10_theta10Y, bins_mag10_hist_mag10_theta10Y, bins_theta10_hist_mag10_theta10Y = statistics.EJPD(mag10, 1.0, False, theta10, 10., True)
            # hist_mag10_theta10_list.append(hist_mag10_theta10.tolist()); bins_mag10_hist_mag10_theta10_list.append(bins_mag10_hist_mag10_theta10.tolist()); bins_theta10_hist_mag10_theta10_list.append(bins_theta10_hist_mag10_theta10.tolist())

        ## SK environments
        if sf['ENVS']:
            self.logger.info('6. Computing environments for SK module')
            # print("SK ENVS")
            # Waves
            proba_envs, intervals_hs_min, intervals_hs_max, intervals_dp_min, intervals_dp_max, tp_envs, cur_envs, dircur_envs, wind_envs, dirwind_envs = \
                statistics.Environments_SK(dico_computation['WAV']['hs']['values'][ind], \
                                           dico_computation['WAV']['dp']['values'][ind], \
                                           dico_computation['WAV']['tp']['values'][ind], \
                                               mag, theta, mag10, theta10)
            # proba_envs_list.append(proba_envs); intervals_hs_min_list.append(intervals_hs_min)
            # intervals_hs_max_list.append(intervals_hs_max);intervals_dp_min_list.append(intervals_dp_min)
            # intervals_dp_max_list.append(intervals_dp_max);tp_envs_list.append(tp_envs)
            # cur_envs_list.append(cur_envs); dircur_envs_list.append(dircur_envs)
            # wind_envs_list.append(wind_envs); dirwind_envs_list.append(dirwind_envs)

        ## Monthly EJPD
        self.logger.info('7. Computing monthly EJPD')
        # print("Monthly EJPD")
        # Waves
        hs2 = TS_hs2[ind]
        tp2 = TS_tp2[ind]
        hs3 = TS_hs3[ind]
        dp3 = TS_dp3[ind]
        if sf['EJPD_hs_tp_monthly'] or sf['EJPD_hs_dp_monthly']  or sf['EJPD3v_monthly']:
            # New common mask
            mask_times_WAV2 = uf.monthly_mask(TV_hs2)
            mask_times_WAV3 = uf.monthly_mask(TV_hs3)
            mask_times_WAV4 = uf.monthly_mask(TV_hs4)
            for i in range(len(mask_times_WAV2)):
                hs2_tmp = hs2[mask_times_WAV2[i]]
                tp2_tmp = tp2[mask_times_WAV2[i]]
                hs3_tmp = hs3[mask_times_WAV3[i]]
                dp3_tmp = dp3[mask_times_WAV3[i]]
                if sf['EJPD_hs_tp_monthly']:
                    if len(hs) == 0:
                        continue
                    hist_hs_tp, bins_hs, bins_tp = statistics.EJPD(hs2_tmp, 0.5, False, tp2_tmp, 1.0, False, forced_bins1=bins_hs_hist_hs_tpY, forced_bins2=bins_tp_hist_hs_tpY)
                    hist_hs_tp_month_list.append(hist_hs_tp.tolist()); bins_hs_hist_hs_tp_month_list.append(bins_hs.tolist()); bins_tp_hist_hs_tp_month_list.append(bins_tp.tolist())
                if sf['EJPD_hs_dp_monthly']:
                    hist_hs_dp, bins_hs, bins_dp = statistics.EJPD(hs3_tmp, 0.5, False, dp3_tmp, 10, True, forced_bins1=bins_hs_hist_hs_dpY, forced_bins2=bins_dp_hist_hs_dpY)
                    hist_hs_dp_month_list.append(hist_hs_dp.tolist()); bins_hs_hist_hs_dp_month_list.append(bins_hs.tolist()); bins_dp_hist_hs_dp_month_list.append(bins_dp.tolist())
        # Currents
        if sf['EJPD_mag_theta_monthly']:
            for i in range(len(mask_times_CUR)):
                mag_tmp = mag[mask_times_CUR[i]]
                theta_tmp = theta[mask_times_CUR[i]]
                if len(mag_tmp) == 0:
                    continue
                hist_mag_theta, bins_mag, bins_theta = statistics.EJPD(mag_tmp, 0.1, False, theta_tmp, 10., True, forced_bins1=bins_mag_hist_mag_thetaY, forced_bins2=bins_theta_hist_mag_thetaY)
                hist_mag_theta_month_list.append(hist_mag_theta.tolist()); bins_mag_hist_mag_theta_month_list.append(bins_mag.tolist()); bins_theta_hist_mag_theta_month_list.append(bins_theta.tolist())
        # Winds
        if sf['EJPD_mag10_theta10_monthly']:
            for i in range(len(mask_times_WIN)):
                mag10_tmp = mag10[mask_times_WIN[i]]
                theta10_tmp = theta10[mask_times_WIN[i]]
                if len(mag10_tmp) == 0:
                    continue
                hist_mag10_theta10, bins_mag10, bins_theta10 = statistics.EJPD(mag10_tmp, 1.0, False, theta10_tmp, 10., True, forced_bins1=bins_mag10_hist_mag10_theta10Y, forced_bins2=bins_theta10_hist_mag10_theta10Y)
                hist_mag10_theta10_month_list.append(hist_mag10_theta10.tolist()); bins_mag10_hist_mag10_theta10_month_list.append(bins_mag10.tolist()); bins_theta10_hist_mag10_theta10_month_list.append(bins_theta10.tolist())

        ## EJPD3v
        if sf['EJPD3v']:
            self.logger.info('8. Computing EJPD3v')
            # print("EJPD3v")
            # Waves
            hist_hs_tp_dpY, bins_hs_hist_hs_tp_dpY, bins_tp_hist_hs_tp_dpY, bins_dp_hist_hs_tp_dpY = statistics.EJPD3v(TS_hs4[ind], 0.5, False, TS_tp4[ind], 1.0, False, TS_dp4[ind], 45, True)
            # hist_hs_tp_dp_list.append(hist_hs_tp_dp.tolist()); bins_hs_hist_hs_tp_dp_list.append(bins_hs_hist_hs_tp_dp.tolist()); bins_tp_hist_hs_tp_dp_list.append(bins_tp_hist_hs_tp_dp.tolist()); bins_dp_hist_hs_tp_dp_list.append(bins_dp_hist_hs_tp_dp.tolist())
            # flatten
            bins_hs_hist_hs_tp_dp_flattenedY = []
            bins_tp_hist_hs_tp_dp_flattenedY = []
            bins_dp_hist_hs_tp_dp_flattenedY = []
            hist_hs_tp_dp_flattenedY = []
            id_flattened = []
            counter = 0
            for ihs, hs in enumerate(bins_hs_hist_hs_tp_dpY[1:]):
                for itp, tp in enumerate(bins_tp_hist_hs_tp_dpY[1:]):
                    for idp, dp in enumerate(bins_dp_hist_hs_tp_dpY[1:]):
                        bins_hs_hist_hs_tp_dp_flattenedY.append(str(bins_hs_hist_hs_tp_dpY[ihs])+'-'+str(hs))
                        bins_tp_hist_hs_tp_dp_flattenedY.append(str(bins_tp_hist_hs_tp_dpY[itp])+'-'+str(tp))
                        bins_dp_hist_hs_tp_dp_flattenedY.append(str(bins_dp_hist_hs_tp_dpY[idp])+'-'+str(dp))
                        hist_hs_tp_dp_flattenedY.append(hist_hs_tp_dpY[ihs,itp,idp])
                        id_flattened.append(counter)
                        counter += 1
            # id_flattened_list.append(id_flattened); hist_hs_tp_dp_flattened_list.append(hist_hs_tp_dp_flattened); bins_hs_hist_hs_tp_dp_flattened_list.append(bins_hs_hist_hs_tp_dp_flattened); bins_tp_hist_hs_tp_dp_flattened_list.append(bins_tp_hist_hs_tp_dp_flattened); bins_dp_hist_hs_tp_dp_flattened_list.append(bins_dp_hist_hs_tp_dp_flattened)
        ## Monthly EJPD3v
        if sf['EJPD3v_monthly']:
            self.logger.info('9. Computing monthly EJPD3v')
            # print("Monthly EJPD3v")
            # Waves
            for i in range(len(mask_times_WAV4)):
                # hs2_tmp = hs2[mask_times_WAV2[i]]
                # tp2_tmp = tp2[mask_times_WAV2[i]]
                # hs3_tmp = hs3[mask_times_WAV3[i]]
                # dp3_tmp = dp3[mask_times_WAV3[i]]
                hs4_tmp = TS_hs4[0][mask_times_WAV4[i]]
                tp4_tmp = TS_tp4[0][mask_times_WAV4[i]]
                dp4_tmp = TS_dp4[0][mask_times_WAV4[i]]

                if len(hs2_tmp) == 0:
                    continue
                hist_hs_tp_dp, bins_hs, bins_tp, bins_dp = statistics.EJPD3v(hs4_tmp, 0.5, False, tp4_tmp, 1.0, False, dp4_tmp, 45, True, forced_bins1=bins_hs_hist_hs_tp_dpY, forced_bins2=bins_tp_hist_hs_tp_dpY, forced_bins3=bins_dp_hist_hs_tp_dpY)
                hist_hs_tp_dp_month_list.append(hist_hs_tp_dp.tolist()); bins_hs_hist_hs_tp_dp_month_list.append(bins_hs.tolist()); bins_tp_hist_hs_tp_dp_month_list.append(bins_tp.tolist()); bins_dp_hist_hs_tp_dp_month_list.append(bins_dp.tolist())
                # flatten
                bins_hs_hist_hs_tp_dp_flattened = []
                bins_tp_hist_hs_tp_dp_flattened = []
                bins_dp_hist_hs_tp_dp_flattened = []
                hist_hs_tp_dp_flattened = []
                id_flattened = []
                counter = 0
                for ihs, hs in enumerate(bins_hs[1:]):
                    for itp, tp in enumerate(bins_tp[1:]):
                        for idp, dp in enumerate(bins_dp[1:]):
                            bins_hs_hist_hs_tp_dp_flattened.append(str(bins_hs[ihs])+'-'+str(hs))
                            bins_tp_hist_hs_tp_dp_flattened.append(str(bins_tp[itp])+'-'+str(tp))
                            bins_dp_hist_hs_tp_dp_flattened.append(str(bins_dp[idp])+'-'+str(dp))
                            hist_hs_tp_dp_flattened.append(hist_hs_tp_dp[ihs,itp,idp])
                            id_flattened.append(counter)
                            counter += 1
                id_month_flattened_list.append(id_flattened); hist_hs_tp_dp_month_flattened_list.append(hist_hs_tp_dp_flattened); bins_hs_hist_hs_tp_dp_month_flattened_list.append(bins_hs_hist_hs_tp_dp_flattened); bins_tp_hist_hs_tp_dp_month_flattened_list.append(bins_tp_hist_hs_tp_dp_flattened); bins_dp_hist_hs_tp_dp_month_flattened_list.append(bins_dp_hist_hs_tp_dp_flattened)

        ## Extreme Value Analysis
        self.logger.info('10. Computing Extreme Value Analyses')
        # print("EXTREMES")
        # Waves
        if sf['EXT_hs'] or sf['EXT_tp']:
            from datetime import datetime
            t0s = datetime.now()
            RP = [1., 10., 50., 100.]
            hs = np.array(dico_computation['WAV']['hs']['values'][ind])
            tp = np.array(dico_computation['WAV']['tp']['values'][ind])
            ti = np.array(dico_computation['WAV']['hs']['times'][ind])
            ti2 = np.array(dico_computation['WAV']['tp']['times'][ind])
            # test on duration
            total_duration = (ti[-1] - ti[0]) / 365.25
            if sf['EXT_hs']:
                if total_duration < 1:
                    # RV_hs = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                    # return values set to zero because the FEM api generator can't understand 2 types of results
                    RV_hs = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
                else:
                    try:
                        rmse, optimal_threshold, peaks = statistics.find_peaks(ti, hs, "GPA", RP)  # Generalised Pareto Analysis for Waves
                        RP, RV_hs = statistics.EXT(hs[peaks], "GPA", RP)
                        if RV_hs[-1] == RV_hs[-2]:
                            # Use exponential distribution if the RV are too convergent
                            rmse, optimal_threshold, peaks = statistics.find_peaks(ti, hs, "Exp", RP)
                            RP, RV_hs = statistics.EXT(hs[peaks], "Exp", RP)                        
                    except:
                        RV_hs = [x*0. for x in RP]    
            if sf['EXT_tp']:
                if total_duration < 1:
                    # RV_tp = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                    # return values set to zero because the FEM api generator can't understand 2 types of results
                    RV_tp = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
                else:
                    try:
                        rmse2, optimal_threshold2, peaks2 = statistics.find_peaks(ti2, tp, "GPA", RP, interpolate = True)  # Generalised Pareto Analysis for Waves
                        RP, RV_tp = statistics.EXT(tp[peaks2], "GPA", RP)
                        if RV_tp[-1] == RV_tp[-2]:
                            # Use exponential distribution if the RV are too convergent
                            rmse, optimal_threshold2, peaks2 = statistics.find_peaks(ti, tp, "Exp", RP)
                            RP, RV_tp = statistics.EXT(tp[peaks2], "Exp", RP)                        
                    except:
                        RV_tp = [x*0. for x in RP]    

            t1s = datetime.now()
            diff1s = (t1s-t0s).total_seconds()
            # print("***********************************************************")
            # print(" Extreme on waves computed in %s seconds" %diff1s)
            # print("***********************************************************")
        # Currents
        if sf['EXT_mag']:
            ti = np.array(dico_computation['CUR']['U']['times'][ind])
            # test on duration
            total_duration = (ti[-1] - ti[0]) / 365.25
            if total_duration < 1:
                # RV_mag = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                # return values set to zero because the FEM api generator can't understand 2 types of results
                RV_mag = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
            else:
                try:
                    rmse, optimal_threshold, peaks = statistics.find_peaks(ti, mag, "GEV", RP)  # Generalised Extreme Value for Currents
                    RP, RV_mag = statistics.EXT(mag[peaks], "GEV", RP)
                except:
                    RV_mag = [x*0. for x in RP]    
            # RV_mag_list.append(RV_mag)
            t2s = datetime.now()
            diff2s = (t2s-t1s).total_seconds()
            # print("***********************************************************")
            # print(" Extreme on currents computed in %s seconds" %diff2s)
            # print("***********************************************************")
            # Winds
        if sf['EXT_mag10'] or sf['EXT_gust10']:
            ti = np.array(dico_computation['WIN']['U10']['times'][ind])
            # test on duration
            total_duration = (ti[-1] - ti[0]) / 365.25
            if sf['EXT_mag10']:
                if total_duration < 1:
                    # RV_mag10 = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                    # return values set to zero because the FEM api generator can't understand 2 types of results
                    RV_mag10 = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
                else:
                    try:
                        rmse, optimal_threshold, peaks = statistics.find_peaks(ti, mag10, "Weibull", RP)  # Weibull for Winds
                        RP, RV_mag10 = statistics.EXT(mag10[peaks], "Weibull", RP)
                    except:
                        RV_mag10 = [x*0. for x in RP]    
                # RV_mag10_list.append(RV_mag10)
            if sf['EXT_gust10']:
                if total_duration < 1:
                    # RV_G10 = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                    # return values set to zero because the FEM api generator can't understand 2 types of results
                    RV_G10 = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
                else:
                    try:
                        rmse, optimal_threshold, peaks = statistics.find_peaks(np.array(dico_computation['WIN']['G10']['times'][ind]), np.array(dico_computation['WIN']['G10']['values'][ind]), "Weibull", RP)  # Weibull for Winds
                        RP, RV_G10 = statistics.EXT(np.array(dico_computation['WIN']['G10']['values'][ind])[peaks], "Weibull", RP)
                    except:
                        RV_G10 = [x*0. for x in RP]    
                # RV_G10_list.append(RV_G10)
            t3s = datetime.now()
            diff3s = (t3s-t2s).total_seconds()
            # print("***********************************************************")
            # print(" Extreme on winds computed in %s seconds" %diff3s)
            # print("***********************************************************")
        # Water levels
        if sf['EXT_wlev']:
            xe = np.squeeze(np.array(dico_computation['LEV']['XE']['values'][ind]))
            # distinction on positive/negative values for extreme calculations
            xe_positive = xe[xe > 0]
            xe_negative = xe[xe < 0]
            ti = np.array(dico_computation['LEV']['XE']['times'][ind])
            ti_positive = ti[xe > 0]
            ti_negative = ti[xe < 0]
            # test on duration
            total_duration = (ti[-1] - ti[0]) / 365.25
            if total_duration < 1:
                # RV_xe_positive = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                # RV_xe_negative = ["timeseries is too short for extreme computation" for x in RP] #np.zeros(len(RP)).tolist()
                # RV_wlev_positive = ["timeseries is too short for extreme computation" for x in RP]
                # RV_wlev_negative = ["timeseries is too short for extreme computation" for x in RP]
                # return values set to zero because the FEM api generator can't understand 2 types of results
                RV_xe_positive = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
                RV_xe_negative = [x*0. for x in RP] #np.zeros(len(RP)).tolist()
                RV_wlev_positive = [x*0. for x in RP]
                RV_wlev_negative = [x*0. for x in RP]
            else:
                try:
                    rmse_positive, optimal_threshold_positive, peaks_positive = statistics.find_peaks(ti_positive, xe_positive, "GEV", RP)  # Generalised Extreme Value for Water levels
                    RP, RV_xe_positive = statistics.EXT(xe_positive[peaks_positive], "GEV", RP)
                except:
                    RV_xe_positive = [x*0. for x in RP]    
                try:
                    rmse_negative, optimal_threshold_negative, peaks_negative = statistics.find_peaks(ti_negative, -1.*xe_negative, "GEV", RP)  # Generalised Extreme Value for Water levels
                    RP, RV_xe_negative = statistics.EXT(-1*xe_negative[peaks_negative], "GEV", RP)
                except:
                    RV_xe_negative = [x*0. for x in RP]    # for HOMERE WLV with no negative values..
                try:
                    RV_wlev_positive = [elt + float(dico_computation['BATHY'][ind]) for elt in RV_xe_positive]
                    RV_wlev_negative = [-1. * elt + float(dico_computation['BATHY'][ind]) for elt in RV_xe_negative]
                except: # not available case
                    RV_wlev_positive = [elt for elt in RV_xe_positive]
                    RV_wlev_negative = [elt for elt in RV_xe_negative]
                        
            # RV_xe_positive_list.append(RV_xe_positive); RV_xe_negative_list.append(RV_xe_negative)
            # RV_wlev_positive_list.append(RV_wlev_positive); RV_wlev_negative_list.append(RV_wlev_negative)
            t4s = datetime.now()
            diff4s = (t4s-t3s).total_seconds()
            # print("***********************************************************")
            # print(" Extreme on water levels computed in %s seconds" %diff4s)
            # print("***********************************************************")

        ## Extreme contour IFORM
        if sf['EXC']:
            self.logger.info('11. Computing environmental contours (IFORM)')
            # print("IFORM")
            RP = [1., 10., 50., 100.]
            hs = TS_hs2[ind]
            tp = TS_tp2[ind]
            ti = TV_hs2[ind]
            # test on duration
            total_duration = (ti[-1] - ti[0]) / 365.25
            if total_duration < 1:
                # X_IFORM_hs_tp = ["timeseries is too short for extreme computation" for x in RP]
                # Y_IFORM_hs_tp = ["timeseries is too short for extreme computation" for x in RP]
                # return values set to zero because the FEM api generator can't understand 2 types of results
                X_IFORM_hs_tp = [x*np.zeros(10) for x in RP]
                Y_IFORM_hs_tp = [x*np.zeros(10) for x in RP]
            else:
#                rmse, optimal_threshold, peaks = statistics.find_peaks(ti, hs, "GPA", RP)  # Generalised Pareto Analysis for Waves
#                X_IFORM_hs_tp, Y_IFORM_hs_tp = statistics.EXC(hs[peaks], tp[peaks], RP, 100)
                tp_new = tp[hs>np.percentile(hs, 90)]
                hs_new = hs[hs>np.percentile(hs, 90)]
                X_IFORM_hs_tp, Y_IFORM_hs_tp = statistics.EXC(hs_new, tp_new, RP, 10)
            # X_IFORM_hs_tp_list.append(X_IFORM_hs_tp); Y_IFORM_hs_tp_list.append(Y_IFORM_hs_tp)
            t5s = datetime.now()
            diff5s = (t5s-t4s).total_seconds()
            # print("***********************************************************")
            # print(" Extreme contours IFORM computed in %s seconds" %diff5s)
            # print("***********************************************************")

        # build dictionary
        dico_stats = {}
        dico_stats['WAV'] = {}
        dico_stats['WAV']['Basic'] = {}
        if sf['Basic_hs']:
            dico_stats['WAV']['Basic']['hs'] = {'min': hs_min, 'mean': hs_mean, 'median': hs_median, 'max': hs_max, 'std': hs_std}
        if sf['Basic_tp']:
            dico_stats['WAV']['Basic']['tp'] = {'min': tp_min, 'mean': tp_mean, 'median': tp_median, 'max': tp_max, 'std': tp_std}
        if sf['Basic_CgE']:
            dico_stats['WAV']['Basic']['CgE'] = {'min': cge_min, 'mean': cge_mean, 'median': cge_median, 'max': cge_max, 'std': cge_std}
        if sf['Basic_gamma']:
            dico_stats['WAV']['Basic']['gamma'] = {'min': gamma_min, 'mean': gamma_mean, 'median': gamma_median, 'max': gamma_max, 'std': gamma_std}
        if sf['Basic_spr']:
            dico_stats['WAV']['Basic']['spr'] = {'min': spr_min, 'mean': spr_mean, 'median': spr_median, 'max': spr_max, 'std': spr_std}
        dico_stats['WAV']['EPD'] = {}
        if sf['EPD_hs']:
            dico_stats['WAV']['EPD']['hs'] = {'bins': bins_hsY, 'pdf': hist_hsY}
        if sf['EPD_tp']:
            dico_stats['WAV']['EPD']['tp'] = {'bins': bins_tpY, 'pdf': hist_tpY}
        if sf['EPD_dp']:
            dico_stats['WAV']['EPD']['dp'] = {'bins': bins_dpY, 'pdf': hist_dpY}
        if sf['EPD_hs_monthly']:
            dico_stats['WAV']['EPD']['hs_monthly'] = {'bins': bins_hs_month_list[0], 'pdf': hist_hs_month_list}
        if sf['EPD_tp_monthly']:
            dico_stats['WAV']['EPD']['tp_monthly'] = {'bins': bins_tp_month_list[0], 'pdf': hist_tp_month_list}
        if sf['EPD_dp_monthly']:
            dico_stats['WAV']['EPD']['dp_monthly'] = {'bins': bins_dp_month_list[0], 'pdf': hist_dp_month_list}
        dico_stats['WAV']['EJPD'] = {}
        if sf['EJPD_hs_tp']:
            dico_stats['WAV']['EJPD']['hs_tp'] = {'bins1': bins_hs_hist_hs_tpY, 'bins2': bins_tp_hist_hs_tpY, 'pdf': hist_hs_tpY}
        if sf['EJPD_hs_dp']:
            dico_stats['WAV']['EJPD']['hs_dp'] = {'bins1': bins_hs_hist_hs_dpY, 'bins2': bins_dp_hist_hs_dpY, 'pdf': hist_hs_dpY}
        if sf['EJPD_hs_tp_monthly']:
            dico_stats['WAV']['EJPD']['hs_tp_monthly'] = {'bins1': bins_hs_hist_hs_tp_month_list[0], 'bins2': bins_tp_hist_hs_tp_month_list[0], 'pdf': hist_hs_tp_month_list}
        if sf['EJPD_hs_dp_monthly']:
            dico_stats['WAV']['EJPD']['hs_dp_monthly'] = {'bins1': bins_hs_hist_hs_dp_month_list[0], 'bins2': bins_dp_hist_hs_dp_month_list[0], 'pdf': hist_hs_dp_month_list}
        dico_stats['WAV']['EJPD3v'] = {}
        dico_stats['WAV']['EJPD3v_flattened'] = {}
        if sf['EJPD3v']:
            dico_stats['WAV']['EJPD3v']['hs_tp_dp'] = {'bins1': bins_hs_hist_hs_tp_dpY, 'bins2': bins_tp_hist_hs_tp_dpY, 'bins3': bins_dp_hist_hs_tp_dpY, 'pdf': hist_hs_tp_dpY}
            dico_stats['WAV']['EJPD3v_flattened']['hs_tp_dp'] = {'bins1': bins_hs_hist_hs_tp_dp_flattenedY, 'bins2': bins_tp_hist_hs_tp_dp_flattenedY, 'bins3': bins_dp_hist_hs_tp_dp_flattenedY, 'pdf': hist_hs_tp_dp_flattenedY, 'id': id_flattened}
        if sf['EJPD3v_monthly']:
            # dico_stats['WAV']['EJPD3v']['hs_tp_dp_monthly'] = {}
            # dico_stats['WAV']['EJPD3v_flattened']['hs_tp_dp_monthly'] = {}
            # for ind, month in enumerate(MONTHS):
            #     dico_stats['WAV']['EJPD3v']['hs_tp_dp_monthly'][month] = {'bins1': bins_hs_hist_hs_tp_dp_month_list[ind], 'bins2': bins_tp_hist_hs_tp_dp_month_list[ind], 'bins3': bins_dp_hist_hs_tp_dp_month_list[ind], 'pdf': hist_hs_tp_dp_month_list[ind]}
            #     dico_stats['WAV']['EJPD3v_flattened']['hs_tp_dp_monthly'][month] = {'bins1': bins_hs_hist_hs_tp_dp_month_flattened_list[ind], 'bins2': bins_tp_hist_hs_tp_dp_month_flattened_list[ind], 'bins3': bins_dp_hist_hs_tp_dp_month_flattened_list[ind], 'pdf': hist_hs_tp_dp_month_flattened_list[ind], 'id': id_flattened[ind]}
            dico_stats['WAV']['EJPD3v']['hs_tp_dp_monthly'] = {'bins1': bins_hs_hist_hs_tp_dp_month_list[0], 'bins2': bins_tp_hist_hs_tp_dp_month_list[0], 'bins3': bins_dp_hist_hs_tp_dp_month_list[0], 'pdf': hist_hs_tp_dp_month_list}
            dico_stats['WAV']['EJPD3v_flattened']['hs_tp_dp_monthly'] = {'bins1': bins_hs_hist_hs_tp_dp_month_flattened_list[0], 'bins2': bins_tp_hist_hs_tp_dp_month_flattened_list[0], 'bins3': bins_dp_hist_hs_tp_dp_month_flattened_list[0], 'pdf': hist_hs_tp_dp_month_flattened_list, 'id': id_month_flattened_list}
        dico_stats['WAV']['EXT'] = {}
        if sf['EXT_hs']:
            dico_stats['WAV']['EXT']['hs'] = {'return_periods': RP, 'return_values':RV_hs}
        if sf['EXT_tp']:
            dico_stats['WAV']['EXT']['tp'] = {'return_periods': RP, 'return_values':RV_tp}
        # Gamma return values (for Station Keeping)
        if sf['EXT_gamma']:
            try:
                RP_Gamma, RV_Gamma = uf.Jonswap_peak_factor(dico_stats['WAV']['EXT']['hs']['return_periods'], dico_stats['WAV']['EXT']['hs']['return_values'], dico_stats['WAV']['EXT']['tp']['return_values'])
            except: # if timeseries is too short for extreme computation
                RP_Gamma = dico_stats['WAV']['EXT']['hs']['return_periods']
                RV_Gamma = dico_stats['WAV']['EXT']['hs']['return_values']
            dico_stats['WAV']['EXT']['gamma'] = {'return_periods': RP_Gamma, 'return_values':RV_Gamma}
        # Environments (for Station Keeping)
        if sf['ENVS']:
            dico_stats['WAV']['ENVS'] = {}
            dico_stats['WAV']['ENVS']['hs_dp_proba'] = proba_envs
            dico_stats['WAV']['ENVS']['intervals_hs_min'] = intervals_hs_min
            dico_stats['WAV']['ENVS']['intervals_hs_max'] = intervals_hs_max
            dico_stats['WAV']['ENVS']['intervals_dp_min'] = intervals_dp_min
            dico_stats['WAV']['ENVS']['intervals_dp_max'] = intervals_dp_max
            dico_stats['WAV']['ENVS']['associated_tp'] = tp_envs
            dico_stats['WAV']['ENVS']['associated_cur'] = cur_envs
            dico_stats['WAV']['ENVS']['associated_wind'] = wind_envs
            dico_stats['WAV']['ENVS']['associated_dircur'] = dircur_envs
            dico_stats['WAV']['ENVS']['associated_dirwind'] = dirwind_envs
        dico_stats['WAV']['EXC'] = {}
        if sf['EXC']:
            dico_stats['WAV']['EXC']['hs_tp'] = {'return_periods': RP, 'return_values1':X_IFORM_hs_tp, 'return_values2':Y_IFORM_hs_tp}
#################################################################################
        dico_stats['CUR'] = {}
        dico_stats['CUR']['Basic'] = {}
        if sf['Basic_mag']:
            dico_stats['CUR']['Basic']['mag'] = {'min': mag_min, 'mean': mag_mean, 'median': mag_median, 'max': mag_max, 'std': mag_std}
        if sf['Basic_Flux']:
            dico_stats['CUR']['Basic']['Flux'] = {'min': cp_min, 'mean': cp_mean, 'median': cp_median, 'max': cp_max, 'std': cp_std}
        dico_stats['CUR']['EPD'] = {}
        if sf['EPD_mag']:
            dico_stats['CUR']['EPD']['mag'] = {'bins': bins_mag, 'pdf': hist_mag}
        if sf['EPD_theta']:
            dico_stats['CUR']['EPD']['theta'] = {'bins': bins_theta, 'pdf': hist_theta}
        if sf['EPD_mag_monthly']:
            dico_stats['CUR']['EPD']['mag_monthly'] = {'bins': bins_mag_month_list[0], 'pdf': hist_mag_month_list}
        if sf['EPD_theta_monthly']:
            dico_stats['CUR']['EPD']['theta_monthly'] = {'bins': bins_theta_month_list[0], 'pdf': hist_theta_month_list}
        dico_stats['CUR']['EJPD'] = {}
        if sf['EJPD_mag_theta']:
            dico_stats['CUR']['EJPD']['mag_theta'] = {'bins1': bins_mag_hist_mag_thetaY, 'bins2': bins_theta_hist_mag_thetaY, 'pdf': hist_mag_thetaY}
        if sf['EJPD_mag_theta_monthly']:
            dico_stats['CUR']['EJPD']['mag_theta_monthly'] = {'bins1': bins_mag_hist_mag_theta_month_list[0], 'bins2': bins_theta_hist_mag_theta_month_list[0], 'pdf': hist_mag_theta_month_list}
        dico_stats['CUR']['EXT'] = {}
        if sf['EXT_mag']:
            dico_stats['CUR']['EXT']['mag'] = {'return_periods': RP, 'return_values':RV_mag}
#################################################################################
        dico_stats['WIN'] = {}
        dico_stats['WIN']['Basic'] = {}
        if sf['Basic_mag10']:
            dico_stats['WIN']['Basic']['mag10'] = {'min': mag10_min, 'mean': mag10_mean, 'median': mag10_median, 'max': mag10_max, 'std': mag10_std}
        dico_stats['WIN']['EPD'] = {}
        if sf['EPD_mag10']:
            dico_stats['WIN']['EPD']['mag10'] = {'bins': bins_mag10Y, 'pdf': hist_mag10Y}
        if sf['EPD_theta10']:
            dico_stats['WIN']['EPD']['theta10'] = {'bins': bins_theta10Y, 'pdf': hist_theta10Y}
        if sf['EPD_mag10_monthly']:
            dico_stats['WIN']['EPD']['mag10_monthly'] = {'bins': bins_mag10_month_list[0], 'pdf': hist_mag10_month_list}
        if sf['EPD_theta10_monthly']:
            dico_stats['WIN']['EPD']['theta10_monthly'] = {'bins': bins_theta10_month_list[0], 'pdf': hist_theta10_month_list}
        dico_stats['WIN']['EJPD'] = {}
        if sf['EJPD_mag10_theta10']:
            dico_stats['WIN']['EJPD']['mag10_theta10'] = {'bins1': bins_mag10_hist_mag10_theta10Y, 'bins2': bins_theta10_hist_mag10_theta10Y, 'pdf': hist_mag10_theta10Y}
        if sf['EJPD_mag10_theta10_monthly']:
            dico_stats['WIN']['EJPD']['mag10_theta10_monthly'] = {'bins1': bins_mag10_hist_mag10_theta10_month_list[0], 'bins2': bins_theta10_hist_mag10_theta10_month_list[0], 'pdf': hist_mag10_theta10_month_list}
        dico_stats['WIN']['EXT'] = {}
        if sf['EXT_mag10']:
            dico_stats['WIN']['EXT']['mag10'] = {'return_periods': RP, 'return_values':RV_mag10}
        if sf['EXT_gust10']:
            dico_stats['WIN']['EXT']['gust10'] = {'return_periods': RP, 'return_values':RV_G10}
#################################################################################
        dico_stats['LEV'] = {}
        dico_stats['LEV']['Basic'] = {}
        if sf['Basic_wlev']:
            dico_stats['LEV']['Basic']['WLEV'] = {'min': wlev_min, 'mean': wlev_mean, 'median': wlev_median, 'max': wlev_max, 'std': wlev_std}
        dico_stats['LEV']['EPD'] = {}
        if sf['EPD_wlev']:
            dico_stats['LEV']['EPD']['WLEV'] = {'bins': bins_wlevY, 'pdf': hist_wlevY}
        if sf['EPD_wlev_monthly']:
            dico_stats['LEV']['EPD']['WLEV_monthly'] = {'bins': bins_wlev_month_list[0], 'pdf': hist_wlev_month_list}
        dico_stats['LEV']['EXT'] = {}
        if sf['EXT_wlev']:
            dico_stats['LEV']['EXT']['WLEVpositive'] = {'return_periods': RP, 'return_values':RV_wlev_positive}
            dico_stats['LEV']['EXT']['WLEVnegative'] = {'return_periods': RP, 'return_values':RV_wlev_negative}
        
        return dico_stats

    def __launch_2D_stats__(self, dico_input, forced_scenarii=None, forced_ltabu=None, forced_ltabv=None, forced_tabu=None, forced_tabv=None, forced_du=None):
        '''launch 2D statistics (matrices for EC module) in the farm.
        Script from DTOcean 1.0.
        dico_input: dictionary of time vectors and time values for waves or currents
        '''
        import numpy as np
        
        try:
            dico_input['U']
            dico_currents = True
        except:
            dico_currents = False
        
        if dico_currents:
            # extract data from dictionary
            uf = dico_input['U']
            vf = dico_input['V']
            TI = dico_input['TI']
            SSH = dico_input['SSH']
            t = dico_input['t']
            x = dico_input['x']
            y = dico_input['y']
            xc = dico_input['xc']
            yc = dico_input['yc']
            # ns = dico_input['ns']
            
            # case with all values equal to zero
            if (np.nanmax(uf)-np.nanmin(uf)) == 0:
                dico_output = {'V': vf, 'U':uf, 'p': np.zeros(1), 'TI':TI, 'x':x, 'y':y, 'SSH':SSH, 'id':np.arange(1)}
                return dico_output, 0, 0, 0, 0, 0

        
            # get the nearest point of the point of interest    
            u = uf[np.argmin(abs(y-yc)), np.argmin(abs(x-xc)), :]
            v = vf[np.argmin(abs(y-yc)), np.argmin(abs(x-xc)), :]
        
            # define increment and PDF axis
            if forced_scenarii:
                ns = forced_scenarii
                du = dv = forced_du
                ltabu = forced_ltabu
                ltabv = forced_ltabv
                tabu = forced_tabu
                tabv = forced_tabv
            else:
                du = dv = 0.1
                ns = int(np.ceil((np.nanmax(u)-np.nanmin(u))/du))
                if ns < 2:
                        du = dv = 0.01
                        ns = int(np.ceil((np.nanmax(u)-np.nanmin(u))/du))
                if ns > 15:
                    du = dv = 0.25
                    ns = int(np.ceil((np.nanmax(u)-np.nanmin(u))/du))
                    if ns > 15:
                        du = dv = 0.5
                        ns = int(np.ceil((np.nanmax(u)-np.nanmin(u))/du))
                ltabu = int(np.ceil(np.array([1.01*np.nanmax(u)-1.01*np.nanmin(u)])/du))
                ltabv = int(np.ceil(np.array([1.01*np.nanmax(v)-1.01*np.nanmin(v)])/dv))
                # then define each axis and center the bins
                tabu = np.linspace(1.01*np.nanmin(u), 1.01*np.nanmax(u), ltabu)
                tabv = np.linspace(1.01*np.nanmin(v), 1.01*np.nanmax(v), ltabv)

            sPDF = (len(tabu), len(tabv))
            # if (len(tabu) == 0 or len(tabv) == 0): # case for currents all zeros
            #     sPDF = (1,1)
            #     ns = 2
            PDF = np.zeros(sPDF)
            ub = np.zeros(sPDF)
            vb = np.zeros(sPDF)
        
            for iu in range(0,len(tabu)-1):
                for iv in range(0,len(tabv)-1):
                    booluv = np.array([(u > tabu[iu]) & (u <= tabu[iu+1]) & (v > tabv[iv]) & (v <= tabv[iv+1])])
                    NPDF = booluv.astype(int)
                    PDF[iu,iv] = np.nansum((NPDF==1)*1)
                    ub[iu,iv] = (tabu[iu]+tabu[iu+1])/2
                    vb[iu,iv] = (tabv[iv]+tabv[iv+1])/2
        
            # PDF normalization
            PDF = PDF/len(u)/du/dv
        
            # Now find the direction with maximum variance
            um = np.zeros(ns)
            vm = np.zeros(ns)
            Pb = np.zeros(ns)
        
            if (np.nanmax(ub) - np.nanmin(ub)) > (np.nanmax(vb) - np.nanmin(vb)):
               tabc = np.linspace(np.nanmin(ub), np.nanmax(ub), ns)
            # Now get the bounds for the probability estimation (largest variance u)
               tabc1 = np.zeros(ns)
               tabc2 = np.zeros(ns)
               dc = tabc[1]-tabc[0]
               tabc1 = tabc-dc/2
               tabc2 = tabc+dc/2
               for ic in range(0,ns):
                   imu = np.argmin(abs(tabc[ic]-tabu))
                   vm[ic] = np.nansum(PDF[imu,:]*tabv*dv)/np.nansum(PDF[imu,:]*dv)
                   um[ic] = tabu[imu]
                   imu1 = np.argmin(abs(tabc1[ic]-tabu))
                   imu2 = np.argmin(abs(tabc2[ic]-tabu))
                   Pb[ic] = np.nansum(np.nansum(PDF[np.nanmax((0,imu1)):np.nanmin((ltabu,imu2)),:]*du)*dv)
            else:
               tabc = np.linspace(np.nanmin(vb), np.nanmax(vb), ns)
               # Now get the bounds for the probability estimation (largest variance v)
               tabc1 = np.zeros(ns)
               tabc2 = np.zeros(ns)
               dc = tabc[1]-tabc[0]
               tabc1 = tabc-dc/2
               tabc2 = tabc+dc/2
               for ic in range(0,ns):
                   imv = np.argmin(abs(tabc[ic]-tabv))
                   um[ic] = np.nansum(PDF[:,imv]*tabu*du)/np.nansum(PDF[:,imv]*du)
                   vm[ic] = tabv[imv]
                   imv1 = np.argmin(abs(tabc1[ic]-tabv))
                   imv2 = np.argmin(abs(tabc2[ic]-tabv))
                   Pb[ic] = np.nansum(np.nansum(PDF[:,np.nanmax((0,imv1)):np.nanmin((ltabv,imv2))]*du)*dv)
        
            # Now get the time step with the closest characteristic
            # try to minimize the difference (um - u)+(vm-v)
            itmin=np.zeros(ns)
            for ic in range(0,ns):
                itmin[ic]=np.argmin(abs(um[ic]-u)+abs(vm[ic]-v))
            # convert itmin to integer
            ind=itmin.astype(int)   
            inds=ind.tolist()
            V=vf[:,:,inds]
            U=uf[:,:,inds]
            p=Pb
            TI=TI[:,:,inds]
            SSH=SSH[:,:,inds]
            idp = np.arange(len(p))
            
            dico_output = {'V': V, 'U':U, 'p':p, 'TI':TI, 'x':x, 'y':y, 'SSH':SSH, 'id':idp}
            return dico_output, ltabu, ltabv, tabu, tabv, du
        else:   # waves
            # extract data from dictionary
            hsg = dico_input['HS']
            tpg = dico_input['TP']
            dpg = dico_input['DP']
            t = dico_input['t']
            x = dico_input['x']
            y = dico_input['y']
            xc = dico_input['xc']
            yc = dico_input['yc']
            # ns = dico_input['ns']
        
            # get the nearest point of the point of interest    
            hs = hsg[np.argmin(abs(y-yc)), np.argmin(abs(x-xc)), :]
            tp = tpg[np.argmin(abs(y-yc)), np.argmin(abs(x-xc)), :]
        
            # define increment and PDF axis
            dhs = 0.1
            dtp = 0.5
            ns = int(np.ceil((np.nanmax(hs)-np.nanmin(hs))/dhs))
            # dtp = (np.nanmax(tp)-np.nanmin(tp))/25.
            # dhs = (np.nanmax(hs)-np.nanmin(hs))/25.
            ltabtp = int(np.ceil(np.array([1.01*np.nanmax(tp)-1.01*np.nanmin(tp)])/dtp))
            ltabhs = int(np.ceil(np.array([1.01*np.nanmax(hs)-1.01*np.nanmin(hs)])/dhs))
            
            # then define each axis and center the bins
            tabtp = np.linspace(1.01*np.nanmin(tp), 1.01*np.nanmax(tp), ltabtp)
            tabhs = np.linspace(1.01*np.nanmin(hs), 1.01*np.nanmax(hs), ltabhs)
            sPDF = (len(tabtp), len(tabhs))
            PDF = np.zeros(sPDF)
            tpb = np.zeros(sPDF)
            hsb = np.zeros(sPDF)
        
            for itp in range(0,len(tabtp)-1):
                for ihs in range(0,len(tabhs)-1):
                    booltphs = np.array([(tp > tabtp[itp]) & (tp <= tabtp[itp+1]) & (hs > tabhs[ihs]) & (hs <= tabhs[ihs+1])])
                    NPDF = booltphs.astype(int)
                    PDF[itp,ihs] = np.nansum((NPDF==1)*1)
                    tpb[itp,ihs] = (tabtp[itp]+tabtp[itp+1])/2
                    hsb[itp,ihs] = (tabhs[ihs]+tabhs[ihs+1])/2
        
            # PDF normalization
            PDF = PDF/len(tp)/dtp/dhs
        
            # Now find the direction with maximum variance
            tpm = np.zeros(ns)
            hsm = np.zeros(ns)
            Pb = np.zeros(ns)
        
            if (np.nanmax(tpb) - np.nanmin(tpb)) > (np.nanmax(hsb) - np.nanmin(hsb)):
               tabc = np.linspace(np.nanmin(tpb), np.nanmax(tpb), ns)
            # Now get the bounds for the probability estimation (largest variance tp)
               tabc1 = np.zeros(ns)
               tabc2 = np.zeros(ns)
               dc = tabc[1]-tabc[0]
               tabc1 = tabc-dc/2
               tabc2 = tabc+dc/2
               for ic in range(0,ns):
                   imtp = np.argmin(abs(tabc[ic]-tabtp))
                   hsm[ic] = np.nansum(PDF[imtp,:]*tabhs*dhs)/np.nansum(PDF[imtp,:]*dhs)
                   tpm[ic] = tabtp[imtp]
                   imtp1 = np.argmin(abs(tabc1[ic]-tabtp))
                   imtp2 = np.argmin(abs(tabc2[ic]-tabtp))
                   Pb[ic] = np.nansum(np.nansum(PDF[np.nanmax((0,imtp1)):np.nanmin((ltabtp,imtp2)),:]*dtp)*dhs)
        
            # Now get the time step with the closest characteristic
            # try to minimize the difference (tpm-tp)+(hsm-hs)
            itmin=np.zeros(ns)
            for ic in range(0,ns):
                itmin[ic]=np.argmin(abs(tpm[ic]-tp)+abs(hsm[ic]-hs))
            # convert itmin to integer
            ind=itmin.astype(int)   
            inds=ind.tolist()
            HS=hsg[:,:,inds]
            TP=tpg[:,:,inds]
            DP=dpg[:,:,inds]
            p=Pb
            
            dico_output = {'HS': HS, 'TP': TP, 'DP': DP, 'p': p, 'x':x, 'y':y}
            return dico_output
