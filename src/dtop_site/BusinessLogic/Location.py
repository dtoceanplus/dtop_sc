# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 15:51:55 2019

@author: ykervell

Overview
========
This module provides objects that handle the locations of the Site.

Usage
=====
The main use of this module is to extract data at a punctual location or at several locations.

    - Extract at a punctual location (direct value):
        from dtop_site.BusinessLogic.Location import MyPoint
        point = MyPoint(point_name, lon, lat)
        my_value = point.get_value(my_data_file, my_data_var)
    - Extract at a punctual location (timeseries):
        from dtop_site.BusinessLogic.Location import MyPoint
        point = MyPoint(point_name, lon, lat)
        time_vector, time_series = point.get_time_series(my_data_name, my_data_template, my_year_start, my_year_end, my_data_var)
    - Extract at several locations (timeseries):
        from dtop_site.BusinessLogic.Location import MyPoints
        points = MyPoints(points_names, lons, lats)
        time_vector, time_series = points.get_time_series_several_variables(my_data_name, my_data_template, my_year_start, my_year_end, my_data_vars)

Notes
=====
The extraction can take a long time.
"""
WHITE = "\033[0m"

class MyPoint:
    """
    Ponctual location.
    
    This class is dedicated to uniq location, 
    i.e. a point with one coordinates (longitude, latitude)
    
    Attributes:
        point_name: a string defining the name of the point
        lon: the longitude of the point
        lat: the latitude of the point
    """

    def __init__(self, point_name, lon, lat):
        self._myvalue = None
        self.name = str(point_name)
        self.lon = float(lon)
        self.lat = float(lat)

    def get_value(self, **kwargs):
        '''Extracts a value from a netcdf file.

        This function extract a value from a structured grid, within a netCDF format.
        
        kwargs:
            data_file: the netCDF file (default: Y:\\01 P1\\DATA\\GEBCO\\GEBCO_2019\\GEBCO_2019.nc)
            data_var: the name of the requested variable of the netCDF file (string, default: 'depth')
            lon_var: the name of the dimension corresponding to the longitude (string, default: 'longitude')
            lat_var: the name of the dimension corresponding to the longitude (string, default: 'longitude')
        
        Returns:
            A value of the extracted variable at requested point, within a float format.
         '''
        import netCDF4
        import numpy as np
        from dtop_site.BusinessLogic.useful_functions import haversine

        data_file = kwargs.get('data_file', None)

        possible_lon_names = ['longitude', 'lon', 'x', 'X']
        possible_lat_names = ['latitude', 'lat', 'y', 'Y']
        possible_bathy_names = ['H0', 'Band1', 'elevation', 'Bathymetry', 'DEPTH', 'depth']
        possible_seabed_names = ['roughness_length', 'seabed_type', 'sediment_type']
        possible_species_names = ['probability',]

        with netCDF4.Dataset(data_file) as nc:
            # fetch the data var
            data_var = kwargs.get('data_var', 'depth')
            if data_var not in (possible_bathy_names + possible_seabed_names + possible_species_names):
                print("%s is not a standard name for a variable" %data_var)
                raise Exception('NotVariableStandardName')
                pass
            bvar = nc.variables[data_var]

            # Coordinates of the nodes and elements (for structured grid)
            lon_var = kwargs.get('lon_var', 'longitude')
#            lon_var = kwargs.get('lon_var', 'lon')
            if lon_var not in possible_lon_names:
                print("%s is not a standard name for longitude" %lon_var)
                raise Exception('NotLongitudeStandardName')
                pass
            lat_var = kwargs.get('lat_var', 'latitude')
#            lat_var = kwargs.get('lat_var', 'lat')
            if lat_var not in possible_lat_names:
                print("%s is not a standard name for latitude" %lat_var)
                raise Exception('NotLatitudeStandardName')
                pass

            # fetch lon / lat values
            lon_nodes = nc.variables[lon_var][:]
            lat_nodes = nc.variables[lat_var][:]

            # interpolation for bathymetry
            if data_var in possible_bathy_names:
                ## Get the mesh that contains the Point
                # get the 2 closest longitude indices
                ilon = np.argmin(np.abs(lon_nodes - self.lon))
                upper_lon_idx = min(len(lon_nodes) - 1, ilon + 1)
                lower_lon_idx = max(0, ilon - 1)
                second_closest_lon_idx = upper_lon_idx
                if abs(lon_nodes[upper_lon_idx] - self.lon) > abs(lon_nodes[lower_lon_idx] - self.lon):
                    second_closest_lon_idx = lower_lon_idx
    
                # get the 2 closest latitude indices
                ilat = np.argmin(np.abs(lat_nodes - self.lat))
                upper_lat_idx = min(len(lat_nodes) - 1, ilat + 1)
                lower_lat_idx = max(0, ilat - 1)
                second_closest_lat_idx = upper_lat_idx
                if abs(lat_nodes[upper_lat_idx] - self.lat) > abs(lat_nodes[lower_lat_idx] - self.lat):
                    second_closest_lat_idx = lower_lat_idx
    
                # Extract the bathymetry at each node of the element
                # and their distance to the Point
                ilons = [ilon, ilon, second_closest_lon_idx, second_closest_lon_idx]
                ilats = [ilat, second_closest_lat_idx, second_closest_lat_idx, ilat]
    
                values = np.array([])
                distances = np.array([])
                for i, j in zip(ilons, ilats):
                    key_dict = {lat_var: j, lon_var: i, 'node': i}
                    key = []
                    for dimension in bvar.dimensions:
                        key.append(key_dict.get(dimension, slice(None)))
                    values = np.append(values, bvar[tuple(key)])
                    distances = np.append(distances,
                                          haversine((lon_nodes[i], lat_nodes[j]),
                                                    (self.lon, self.lat)))
    
                # And interpolate, weights = inverse of distance
                self._myvalue = abs(np.average(values, weights=1./distances))
            else:
                ilon = np.argmin(np.abs(lon_nodes - self.lon))
                ilat = np.argmin(np.abs(lat_nodes - self.lat))
                if data_var == 'probability':   # Marine Species case
                    self._myvalue = bvar[:, ilat, ilon]
                else:
                    self._myvalue = bvar[ilat, ilon]

        # Compute the slope if needed
#        return np.round(self._myvalue, 5)
        return self._myvalue

    def get_local_slope(self, **kwargs):
        '''Compute slope using a bathymetry netcdf file.
        Bathymetry must be given on a structured grid, within a netCDF format.
        Depth is positive in water.

        kwargs:
            - data_file (default: Y:\\01 P1\\DATA\\GEBCO\\GEBCO_2019\\GEBCO_2019.nc)
            - data_var (default: depth)
            - lon_var (default: longitude)
            - lat_var (default: latitude)
        '''
        import netCDF4
        import numpy as np
        from dtop_site.BusinessLogic.useful_functions import haversine

        data_file = kwargs.get('data_file', None)
        if data_file is None:
            print("NO DATA FILE")
            raise Exception('NoDataFile')
            pass

        possible_lon_names = ['longitude', 'lon', 'x', 'X']
        possible_lat_names = ['latitude', 'lat', 'y', 'Y']
        possible_bathy_names = ['H0', 'Band1', 'elevation', 'Bathymetry', 'DEPTH', 'depth']
        possible_seabed_names = ['roughness_length', 'seabed_type', 'sediment_type']
        possible_species_names = ['probability',]

        with netCDF4.Dataset(data_file) as nc:
            # fetch the data var
            data_var = kwargs.get('data_var', 'depth')
            if data_var not in (possible_bathy_names + possible_seabed_names + possible_species_names):
                print("%s is not a standard name for a variable" %data_var)
                raise Exception('NotVariableStandardName')
                pass
            bvar = nc.variables[data_var]

            # Coordinates of the nodes and elements (for structured grid)
            lon_var = kwargs.get('lon_var', 'longitude')
#            lon_var = kwargs.get('lon_var', 'lon')
            if lon_var not in possible_lon_names:
                print("%s is not a standard name for longitude" %lon_var)
                raise Exception('NotLongitudeStandardName')
                pass
            lat_var = kwargs.get('lat_var', 'latitude')
#            lat_var = kwargs.get('lat_var', 'lat')
            if lat_var not in possible_lat_names:
                print("%s is not a standard name for latitude" %lat_var)
                raise Exception('NotLatitudeStandardName')
                pass

            # fetch lon / lat values
            lon_nodes = nc.variables[lon_var][:]
            lat_nodes = nc.variables[lat_var][:]

            # Nearest elements
            ## Get the mesh that contains the Point
            # get the 2 closest longitude indices
            ilon = np.argmin(np.abs(lon_nodes - self.lon))
            upper_lon_idx = min(len(lon_nodes) - 1, ilon + 1)
            lower_lon_idx = max(0, ilon - 1)
            second_closest_lon_idx = upper_lon_idx
            if abs(lon_nodes[upper_lon_idx] - self.lon) > abs(lon_nodes[lower_lon_idx] - self.lon):
                second_closest_lon_idx = lower_lon_idx
    
            # get the 2 closest latitude indices
            ilat = np.argmin(np.abs(lat_nodes - self.lat))
            upper_lat_idx = min(len(lat_nodes) - 1, ilat + 1)
            lower_lat_idx = max(0, ilat - 1)
            second_closest_lat_idx = upper_lat_idx
            if abs(lat_nodes[upper_lat_idx] - self.lat) > abs(lat_nodes[lower_lat_idx] - self.lat):
                second_closest_lat_idx = lower_lat_idx
    
            # Extract the bathymetry at each node of the element
            # and their distance to the Point
            ilons = [ilon, ilon, second_closest_lon_idx, second_closest_lon_idx]
            ilats = [ilat, second_closest_lat_idx, second_closest_lat_idx, ilat]
    
            values = np.array([])
            new_lons = np.array([])
            new_lats = np.array([])
            distances = np.array([])
            for i, j in zip(ilons, ilats):
                key_dict = {lat_var: j, lon_var: i, 'node': i}
                key = []
                for dimension in bvar.dimensions:
                    key.append(key_dict.get(dimension, slice(None)))
                values = np.append(values, bvar[tuple(key)])
                new_lons = np.append(new_lons, lon_nodes[ilons])
                new_lats = np.append(new_lats, lat_nodes[ilats])
                distances = np.append(distances,
                                      haversine((lon_nodes[i], lat_nodes[j]),
                                                (self.lon, self.lat)))
    
            # compute slope between nearest elements
            ind_max = np.argmax(values)
            ind_min = np.argmin(values)
            height_difference = values[ind_max] - values[ind_min]
            distance_for_slope = haversine((new_lons[ind_max], new_lats[ind_max]), (lon_nodes[ind_min], lat_nodes[ind_min]))
            slope = height_difference / distance_for_slope
            slope_angle = np.arctan(slope) * 180. / np.pi

        return slope_angle
    

    def find_files(self, template, start, end, **kwargs):
        '''Iterates over the files of the database
        '''
        import numpy as np
        import os
        
        file_template = template
        from string import Template
        t = Template(file_template)
        list_of_files = []
        for year in np.arange(int(start), int(end)+1):
            if "mm" in file_template:
                for month in np.arange(1,13):
                    if month < 10:
                        month = '0' + str(month)
                    d = dict(yyyy=str(year), mm=str(month))
                    file_to_find = t.safe_substitute(d)
                    if os.path.isfile(file_to_find):
                        list_of_files.append(file_to_find)
            else:
                d = dict(yyyy=str(year))
                file_to_find = t.safe_substitute(d)
                if os.path.isfile(file_to_find):
                    list_of_files.append(file_to_find)
                
        return list_of_files

    def _get_time_series_length(self, list_of_files):
        '''Calculates the length of the 'time' dimension
        '''
        import netCDF4
        
        try:
            nc = netCDF4.Dataset(list_of_files[0])                
            data_start = nc.variables['time'][0]
    #        data_units = nc.variables['time'].units
            data_timestep = nc.variables['time'][1] - nc.variables['time'][0]
            nc.close()
            nc = netCDF4.Dataset(list_of_files[-1])
            data_end = nc.variables['time'][-1]
        except:
            nc = netCDF4.Dataset(list_of_files)
            data_start = nc.variables['time'][0]
            data_timestep = nc.variables['time'][1] - nc.variables['time'][0]
            data_end = nc.variables['time'][-1]

        data_length = (data_end - data_start) / data_timestep
        
        return int(round(data_length) + 1)


    def get_time_series(self, **kwargs):
        '''Loops on the files of the database and extracts the variable
        var_name at the given point

        kwargs:
            - data_name: name of the database (see in the folder catalogs)
            - data_var: name of the variable you want to extract
        '''
        import numpy as np
        import netCDF4
        import pylab
        
        data_name = kwargs.get('data_name', None)
        data_var = kwargs.get('data_var', None)
        data_template = kwargs.get('data_template', None)
        data_start = kwargs.get('year_start', None)
        data_end = kwargs.get('year_end', None)

        print("")
        print('\033[1;34m Extraction of the %s timeseries at point %s\033[1;0m' %(data_var, self.name))

        dataset_files = self.find_files(data_template, data_start, data_end) 
        print('Gonna loop on these files: \n\t%s' % '\n\t'.join(dataset_files))

        timesteps = self._get_time_series_length(dataset_files) 

        print('Detected %s timesteps in %s' % (timesteps, data_name))

        time_vector = np.ma.empty((timesteps,))
        time_series = np.ma.empty((timesteps,))
        time_vector.fill(np.nan)
        time_series.fill(np.nan)
        
        # find the nearest point in the dataset
        nc = netCDF4.Dataset(dataset_files[0])
        lon_nodes = nc.variables['longitude'][:]
        lat_nodes = nc.variables['latitude'][:]
        # if len(np.shape(lon_nodes)) == 2:
        #     ilon = np.argmin(np.abs(lon_nodes[0,:] - self.lon))
        #     ilat = np.argmin(np.abs(lat_nodes[:,0] - self.lat))
        # else:
        ilon = np.argmin(np.abs(lon_nodes - self.lon))
        ilat = np.argmin(np.abs(lat_nodes - self.lat))
            
        
        position = 0

        for i, data_file in enumerate(dataset_files):

            dat = netCDF4.Dataset(data_file)
            try:
                # temporary problem with library pylab and date2num compatibility
                # extracted_time = pylab.date2num(
                #         netCDF4.num2date(
                #                 dat.variables['time'][:],
                #                 dat.variables['time'].units
                #                 )
                #         )
                time_temp = netCDF4.num2date(
                                dat.variables['time'][:],
                                dat.variables['time'].units
                                )
                extracted_time = []
                import datetime
                for ind, ti in enumerate(time_temp):
                    extracted_time.append(pylab.date2num(
                        datetime.datetime(ti.year,ti.month,ti.day,ti.hour,ti.minute)
                        )
                    )
                extracted_time = np.array(extracted_time)
            except: # to avoid NaN in times vector
                ti = dat.variables['time'][:]
                dt = ti[1] - ti[0]
                for i, t in enumerate(ti):
                    if np.isnan(t):
                        ti[i] = ti[i-1] + dt
                extracted_time = pylab.date2num(
                        netCDF4.num2date(
                                ti,
                                dat.variables['time'].units
                                )
                        )
            if data_var == 'tp':
                extracted_variable = dat.variables['fp'][:, ilat, ilon]
            else:
                extracted_variable = dat.variables[data_var][:, ilat, ilon]
            #fill_value = getattr(dat.variables[data_var], '_FillValue', 9999)

            # Replace fill values by nan (if we extracted a vector)
            #extracted_variable[extracted_variable >= fill_value] = np.nan

            # fill the vectors (offset is the length of each file
            offset = np.size(extracted_time)
            time_vector[position: position + offset] = extracted_time[:offset]
            if data_var == 'tp':
                time_series[position: position + offset] = 1. / extracted_variable[:offset]
            else:
                time_series[position: position + offset] = extracted_variable[:offset]
            position += offset
            progress_bar = ''.join((WHITE,
                                        " %0.2f" % min(np.round(100 * (i + 1) / (len(dataset_files)), 2), 100.),
                                        '%'))
            #print(progress_bar, end=' ')
            print(progress_bar, end='\r')
            dat.close()
        print('\033[1;33m Extraction OK !!!\033[1;0m')

        return time_vector, time_series

    def get_time_series_several_variables(self, **kwargs):
        '''Loops on the files of the database and extracts several variables
        at the given point.
        Dedicated to a database which contains all or several variables.

        kwargs:
            - data_name: name of the database (see in the folder catalogs)
            - data_vars_list: list of variable names you want to extract
        '''
        import numpy as np
        import netCDF4
        import pylab
        import xarray as xr
        
        data_name = kwargs.get('data_name', None)
        data_vars_list = kwargs.get('data_vars_list', None)
        data_template = kwargs.get('data_template', None)
        data_start = kwargs.get('year_start', None)
        data_end = kwargs.get('year_end', None)

        print('\033[1;34mExtraction of the %s timeseries at point %s\033[1;0m' %(data_vars_list, self.name))

        dataset_files = self.find_files(data_template, data_start, data_end) 
        print('Gonna loop on these files: \n\t%s' % '\n\t'.join(dataset_files))

        timesteps = self._get_time_series_length(dataset_files) 

        print('Detected %s timesteps in %s' % (timesteps, data_name))

        time_vector = np.ma.empty((timesteps,))
        time_series_table = np.ma.empty((timesteps,len(data_vars_list)))
        time_vector.fill(np.nan)
        time_series_table.fill(np.nan)
        
        # find the nearest point in the dataset
        nc = netCDF4.Dataset(dataset_files[0])
        lon_nodes = nc.variables['longitude'][:]
        lat_nodes = nc.variables['latitude'][:]
        # if len(np.shape(lon_nodes)) == 2:
        #     ilon = np.argmin(np.abs(lon_nodes[0,:] - self.lon))
        #     ilat = np.argmin(np.abs(lat_nodes[:,0] - self.lat))
        # else:
        ilon = np.argmin(np.abs(lon_nodes - self.lon))
        ilat = np.argmin(np.abs(lat_nodes - self.lat))
            
        
        position = 0

        for i, data_file in enumerate(dataset_files):

            dat = netCDF4.Dataset(data_file)
            ds = xr.open_dataset(data_file)
            try:
                # temporary problem with library pylab and date2num compatibility
                # extracted_time = pylab.date2num(
                #         netCDF4.num2date(
                #                 dat.variables['time'][:],
                #                 dat.variables['time'].units
                #                 )
                #         )
                time_temp = netCDF4.num2date(
                                dat.variables['time'][:],
                                dat.variables['time'].units
                                )
                extracted_time = []
                import datetime
                for ind, ti in enumerate(time_temp):
                    extracted_time.append(pylab.date2num(
                        datetime.datetime(ti.year,ti.month,ti.day,ti.hour,ti.minute)
                        )
                    )
                extracted_time = np.array(extracted_time)
            except: # to avoid NaN in times vector
                ti = dat.variables['time'][:]
                dt = ti[1] - ti[0]
                for i, t in enumerate(ti):
                    if np.isnan(t):
                        ti[i] = ti[i-1] + dt
                extracted_time = pylab.date2num(
                        netCDF4.num2date(
                                ti,
                                dat.variables['time'].units
                                )
                        )
#            if data_var == 'tp':
#                extracted_variable = dat.variables['fp'][:, ilat, ilon]
#            else:
#                extracted_variable = dat.variables[data_var][:, ilat, ilon]

            offset = np.size(extracted_time)
            time_vector[position: position + offset] = extracted_time[:offset]
#            if data_var == 'tp':
#                time_series[position: position + offset] = 1. / extracted_variable[:offset]
#            else:
#                time_series[position: position + offset] = extracted_variable[:offset]
            extracted_variables = ds[data_vars_list].to_dataframe().values
            time_series_table[position: position + offset,:] = extracted_variables[:,:offset]
            position += offset
            progress_bar = ''.join((WHITE,
                                        " %0.2f" % min(np.round(100 * (i + 1) / (len(dataset_files)), 2), 100.),
                                        '%'))
            #print(progress_bar, end=' ')
            print(progress_bar, end='\r')
            dat.close()
        print('\033[1;33mExtraction OK !!!\033[1;0m')

        return time_vector, time_series_table

    def get_time_series_several_variables_in_1_file(self, **kwargs):
        '''Open the uniq file of the database and extracts several variables
        at the given point.
        Dedicated to a database which contains all or several variables.

        kwargs:
            - data_name: name of the database (user input)
            - data_vars_list: list of variable names you want to extract
        '''
        import numpy as np
        import netCDF4
        import pylab
        import xarray as xr
        import pandas as pd
        import datetime
        
        data_name = kwargs.get('data_name', None)
        data_vars_list = kwargs.get('data_vars_list', None)

        print('\033[1;34mExtraction of the %s timeseries at point %s\033[1;0m' %(data_vars_list, self.name))
        
        if data_name[-3:] == '.nc':
            timesteps = self._get_time_series_length(data_name) 
        else:
            df = pd.read_csv(data_name, sep=';')
            timesteps = len(df['times'])

        print('Detected %s timesteps in %s' % (timesteps, data_name))

        time_vector = np.ma.empty((timesteps,))
        time_series_table = np.ma.empty((timesteps,len(data_vars_list)))
        time_vector.fill(np.nan)
        time_series_table.fill(np.nan)
                    
        if data_name[-3:] == '.nc':
            nc = netCDF4.Dataset(data_name)
            ds = xr.open_dataset(data_name)
            time_series_table = ds[data_vars_list].to_dataframe().values
            try:
                # temporary problem with library pylab and date2num compatibility
                # extracted_time = pylab.date2num(
                #         netCDF4.num2date(
                #                 dat.variables['time'][:],
                #                 dat.variables['time'].units
                #                 )
                #         )
                time_temp = netCDF4.num2date(
                                   nc.variables['time'][:],
                                   nc.variables['time'].units
                                   )
                extracted_time = []
                for ind, ti in enumerate(time_temp):
                    extracted_time.append(pylab.date2num(
                        datetime.datetime(ti.year,ti.month,ti.day,ti.hour,ti.minute)
                        )
                    )
                extracted_time = np.array(extracted_time)
            except: # to avoid NaN in times vector
                ti = nc.variables['time'][:]
                dt = ti[1] - ti[0]
                for i, t in enumerate(ti):
                    if np.isnan(t):
                        ti[i] = ti[i-1] + dt
                extracted_time = pylab.date2num(
                    netCDF4.num2date(
                        ti,
                        nc.variables['time'].units
                        )
                    )
            time_vector = extracted_time
            nc.close()
        else:
            dateparse = lambda col: pd.datetime.strptime(col, '%Y-%m-%d %H:%M:%S')

            df = pd.read_csv(data_name, sep=';', parse_dates=['times'], date_parser=dateparse)
            time_vector = pylab.date2num(df['times'])
            time_series_table = df[data_vars_list].values
            
        print('\033[1;33mExtraction OK !!!\033[1;0m')

        return time_vector, time_series_table

class MyPoints:
    """ Ponctual location"""

    def __init__(self, point_names, lons, lats):
        self._myvalues = None
        self.names = point_names
        self.lons = [float(lon) for lon in lons]
        self.lats = [float(lat) for lat in lats]
        if len(self.names) == 0:
            print('\033[1;31mNo point to extract !!!\033[1;0m')
            raise Exception('NoPoints')
            pass

    def find_files(self, template, start, end, **kwargs):
        '''Iterates over the files of the database
        '''
        import numpy as np
        import os
        
        file_template = template
        from string import Template
        t = Template(file_template)
        list_of_files = []
        for year in np.arange(int(start), int(end)+1):
            d = dict(yyyy=str(year))
            file_to_find = t.safe_substitute(d)
            if os.path.isfile(file_to_find):
                list_of_files.append(file_to_find)
                
        return list_of_files

    def _get_time_series_length(self, list_of_files):
        '''Calculates the length of the 'time' dimension
        '''
        import netCDF4
        
        try:
            nc = netCDF4.Dataset(list_of_files[0])                
            data_start = nc.variables['time'][0]
    #        data_units = nc.variables['time'].units
            data_timestep = nc.variables['time'][1] - nc.variables['time'][0]
            nc.close()
            nc = netCDF4.Dataset(list_of_files[-1])
            data_end = nc.variables['time'][-1]
        except:
            nc = netCDF4.Dataset(list_of_files)
            data_start = nc.variables['time'][0]
            data_timestep = nc.variables['time'][1] - nc.variables['time'][0]
            data_end = nc.variables['time'][-1]

        data_length = (data_end - data_start) / data_timestep
        
        return int(round(data_length) + 1)

    def get_time_series_several_variables(self, **kwargs):
        '''Loops on the files of the database and extracts several variables
        at the given points.
        Dedicated to a database which contains all or several variables.

        kwargs:
            - data_name: name of the database (see in the folder catalogs)
            - data_vars_list: list of variable names you want to extract
        '''
        import numpy as np
        import netCDF4
        import pylab
        import xarray as xr
        from dtop_site.BusinessLogic.useful_functions import haversine
        
        data_name = kwargs.get('data_name', None)
        data_vars_list = kwargs.get('data_vars_list', None)
        data_template = kwargs.get('data_template', None)
        data_start = kwargs.get('year_start', None)
        data_end = kwargs.get('year_end', None)

        print('\033[1;34mExtraction of the %s timeseries at %s points\033[1;0m' %(data_vars_list, len(self.names)))

        dataset_files = self.find_files(data_template, data_start, data_end) 
        print('Gonna loop on these files: \n\t%s' % '\n\t'.join(dataset_files))

        timesteps = self._get_time_series_length(dataset_files) 
        # timesteps = 8784 
        # offset = 8784

        print('Detected %s timesteps in %s' % (timesteps, data_name))

        time_vector = np.ma.empty((timesteps,))
        time_series_table = np.ma.empty((len(data_vars_list), timesteps, len(self.names)))
        time_vector.fill(np.nan)
        time_series_table.fill(np.nan)
        
        # find the nearest point in the dataset
        nc = netCDF4.Dataset(dataset_files[0])
        lon_nodes = nc.variables['longitude'][:]
        lat_nodes = nc.variables['latitude'][:]
        index_nodes = []
        for lon, lat in zip(self.lons, self.lats):
            index_nodes.append(np.argmin(haversine((lon_nodes, lat_nodes), (lon, lat))))
        position = 0

        for i, data_file in enumerate(dataset_files):

            dat = netCDF4.Dataset(data_file)
            ds = xr.open_dataset(data_file)
            try:
                # temporary problem with library pylab and date2num compatibility
                # extracted_time = pylab.date2num(
                #         netCDF4.num2date(
                #                 dat.variables['time'][:],
                #                 dat.variables['time'].units
                #                 )
                #         )
                time_temp = netCDF4.num2date(
                                dat.variables['time'][:],
                                dat.variables['time'].units
                                )
                extracted_time = []
                import datetime
                for ind, ti in enumerate(time_temp):
                    extracted_time.append(pylab.date2num(
                        datetime.datetime(ti.year,ti.month,ti.day,ti.hour,ti.minute)
                        )
                    )
                extracted_time = np.array(extracted_time)
            except: # to avoid NaN in times vector
                ti = dat.variables['time'][:]
                dt = ti[1] - ti[0]
                for i, t in enumerate(ti):
                    if np.isnan(t):
                        ti[i] = ti[i-1] + dt
                extracted_time = pylab.date2num(
                        netCDF4.num2date(
                                ti,
                                dat.variables['time'].units
                                )
                        )
            offset = np.size(extracted_time)
            time_vector[position: position + offset] = extracted_time[:offset]

            extracted_variables_tmp = ds[data_vars_list].to_array().values
            extracted_variables = extracted_variables_tmp[:,:,index_nodes]
            time_series_table[:,position: position + offset,:] = extracted_variables[:,:offset,:]
            position += offset
            progress_bar = ''.join((WHITE,
                                        " %0.2f" % min(np.round(100 * (i + 1) / (len(dataset_files)), 2), 100.),
                                        '%'))
            #print(progress_bar, end=' ')
            print(progress_bar, end='\r')
            dat.close()
        print('\033[1;33mExtraction OK !!!\033[1;0m')

        return time_vector, time_series_table
    
    def get_time_series_several_variables_in_1_file(self, **kwargs):
        '''Read the file of the database and extracts several variables
        at the given points.
        Dedicated to a database which contains all or several variables.

        kwargs:
            - data_name: name of the database (user input)
            - data_vars_list: list of variable names you want to extract
        '''
        import numpy as np
        import netCDF4
        import pylab
        import xarray as xr
        from dtop_site.BusinessLogic.useful_functions import haversine
        import pandas as pd
        
        data_name = kwargs.get('data_name', None)
        data_vars_list = kwargs.get('data_vars_list', None)

        print('\033[1;34mExtraction of the %s timeseries at %s points\033[1;0m' %(data_vars_list, len(self.names)))

        if data_name[-3:] == '.nc':
            timesteps = self._get_time_series_length(data_name) 
            time_series_table = np.ma.empty((len(data_vars_list), timesteps, len(self.names)))
        else:
            df = pd.read_csv(data_name, sep=';')
            timesteps = len(np.unique(df['times']))
            nb_nodes = round(len(df['times'])/timesteps)
            time_series_table = np.ma.empty((len(data_vars_list), timesteps, nb_nodes))
        print('Detected %s timesteps in %s' % (timesteps, data_name))

        time_vector = np.ma.empty((timesteps,))
        time_vector.fill(np.nan)
        time_series_table.fill(np.nan)
        
        if data_name[-3:] == '.nc':
            # find the nearest nodes in the dataset
            nc = netCDF4.Dataset(data_name)
            lon_nodes = nc.variables['longitude'][:]
            lat_nodes = nc.variables['latitude'][:]
            index_nodes = []
            for lon, lat in zip(self.lons, self.lats):
                index_nodes.append(np.argmin(haversine((lon_nodes, lat_nodes), (lon, lat))))
    
            ds = xr.open_dataset(data_name)
            try:
                # temporary problem with library pylab and date2num compatibility
                # extracted_time = pylab.date2num(
                #         netCDF4.num2date(
                #                 dat.variables['time'][:],
                #                 dat.variables['time'].units
                #                 )
                #         )
                time_temp = netCDF4.num2date(
                    nc.variables['time'][:],
                    nc.variables['time'].units
                    )
                extracted_time = []
                import datetime
                for ind, ti in enumerate(time_temp):
                    extracted_time.append(pylab.date2num(
                        datetime.datetime(ti.year,ti.month,ti.day,ti.hour,ti.minute)
                        )
                    )
                extracted_time = np.array(extracted_time)
            except: # to avoid NaN in times vector
                ti = nc.variables['time'][:]
                dt = ti[1] - ti[0]
                for i, t in enumerate(ti):
                    if np.isnan(t):
                        ti[i] = ti[i-1] + dt
                extracted_time = pylab.date2num(
                    netCDF4.num2date(
                        ti,
                        nc.variables['time'].units
                        )
                    )
            time_vector = extracted_time
    
            extracted_variables_tmp = ds[data_vars_list].to_array().values
            extracted_variables = extracted_variables_tmp[:,:,index_nodes]
            time_series_table = extracted_variables
            nc.close()
        else:
            try:
                dateparse = lambda col: pd.datetime.strptime(col, '%Y-%m-%d %H:%M:%S')
                df = pd.read_csv(data_name, sep=';', parse_dates=['times'],date_parser=dateparse)
            except:
                dateparse = lambda col: pd.datetime.strptime(col, '%d-%m-%Y %H:%M:%S')
                df = pd.read_csv(data_name, sep=';', parse_dates=['times'],date_parser=dateparse)
            #df = pd.read_csv(data_name, sep=';', parse_dates=['times'],date_parser=lambda col: pd.to_datetime(col, utc=True))
            time_vector = pylab.date2num(np.unique(df['times']))
            time_series_table_flat = df[data_vars_list].values
            for t in range(timesteps):
                time_series_table[:,t,:] = np.transpose(time_series_table_flat[t*nb_nodes:(t+1)*nb_nodes,:])
            
        print('\033[1;33mExtraction OK !!!\033[1;0m')

        return time_vector, time_series_table