module.exports = {
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest'
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  snapshotSerializers: ['jest-serializer-vue'],
  testMatch: [
    '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  ],
  coverageThreshold: {
    global: {
      statements: 50, 
      branches: 50, 
      functions: 50, 
      lines: 50,
    }
  },
  collectCoverage: true,
  collectCoverageFrom: ['src/utils/**/*.{js,vue}', '!src/utils/auth.js', '!src/utils/request.js', 'src/components/**/*.{js,vue}', 'src/views/**/*.{js,vue}', '!src/views/404.vue', 'src/components/Breadcrumb/*', '!src/views/pdf/*', 'src/views/NewProject/*', '!src/views/outputs_page/2D_Maps/*'],
  coverageDirectory: '<rootDir>/tests/unit/coverage',
  coverageReporters: [
    'html',
    'text-summary',
    'clover'
  ],
  reporters: [
    'default',
    '<rootDir>/custom-reporter.js'
  ],
  testURL: 'http://localhost/',
  setupFiles: [
    "core-js",
    '<rootDir>/jest.stub.js'
  ]
}
