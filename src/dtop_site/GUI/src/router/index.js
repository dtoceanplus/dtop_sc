// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  // {
  //   path: '/404',
  //   component: () => import('@/views/404'),
  //   hidden: true
  // },

//   {
//     path: '/',
//     component: Layout,
//     redirect: '/dashboard',
//     children: [{
//       path: 'dashboard',
//       name: 'Dashboard',
//       component: () => import('@/views/dashboard/index'),
//       meta: { title: 'Dashboard', icon: 'dashboard' }
//     }]
//   },
// 
//   {
//     path: '/',
//     component: Layout,
//     redirect: '/schome',
//     children: [{
//       path: 'schome',
//       name: 'SCHome',
//       component: () => import('@/views/schome/index'),
//       meta: { title: 'SCHome', icon:'home' }
//     }]
//   },

  {
    path: '/',
    component: Layout,
    redirect: '/inputs',
    children: [{
      path: 'inputs',
      name: 'Inputs',
      component: () => import('@/views/Inputs/index'),
      meta: { title: 'Inputs', icon: 'create' }
    }]
  },

  {
    path: '/scoutputs',
    component: Layout,
    redirect: '/scoutputs',
    name: 'SKOutputs',
    meta: { title: 'Outputs', icon: 'nested' },
    children: [
      {
        path: 'overview',
        name: 'Overview',
        component: () => import('@/views/outputs_page/Overview/index'),
        meta: { title: 'Overview' }
      },
      {
        path: 'waves',
        name: 'Waves',
        component: () => import('@/views/outputs_page/Waves/index'),
        meta: { title: 'Waves' }
      },
      {
        path: 'currents',
        name: 'Currents',
        component: () => import('@/views/outputs_page/Currents/index'),
        meta: { title: 'Currents' }
      },
      {
        path: '2D_maps',
        name: '2D_Maps',
        component: () => import('@/views/outputs_page/2D_Maps/index'),
        meta: { title: '2D Maps' }
      }
    ]
  },

  {
    path: '/pdf',
    name: 'PDF',
    component: () => import('@/views/pdf/download'),
    hidden: true
  },

  {
    path: 'external-link',
    component: Layout,
    meta: { title: 'Links', icon: 'link' },
    children: [
      {
        path: 'https://dtoceanplus.eu',
        meta: { title: 'DTOcean+ website'}
      },
      {
        path: 'https://dtoceanplus.gitlab.io/documentation/index.html',
        meta: { title: 'DTOcean+ documentation'}
      },
      {
        path: 'https://dtoceanplus.gitlab.io/documentation/deployment/sc/docs/index.html',
        meta: { title: 'Site Characterisation documentation'}
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
