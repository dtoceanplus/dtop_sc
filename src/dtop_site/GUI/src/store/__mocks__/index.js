import Vue from 'vue';
import Vuex from 'vuex';

import ProjectInputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
//import ProjectStatusInputs from '@/../tests/unit/json/Project_status.json'

Vue.use(Vuex);

export const getters = {};

export const mutations = {
    set_Project: jest.fn(),
    changeRunMode: jest.fn(),
    changeComplexity: jest.fn(),
    //set_ProjectStatus: jest.fn(),
    set_TestBegin: jest.fn(),
    set_SwitchFarmInfoPage: jest.fn(),
    set_um_DI: jest.fn(),
    set_umf_DI: jest.fn(),
    set_um_EI: jest.fn()
  };
  
  export const actions = {
    set_ProjectAction: jest.fn(),
    //set_ProjectStatusAction: jest.fn(),
    set_testbeginAction: jest.fn(),
    set_UMDI: jest.fn(),
    set_UMFDI: jest.fn(),
    set_UMEI: jest.fn(),
  };

  export const state = {
    Project: ProjectInputs,
    //ProjectStatus: ProjectStatusInputs,
    test_begin: false,
    switch_farm_info_page: [false, false, false, false, false, false, false, false, false, false, false, false, false, false],
    used_materials_DI: [],
    used_materials_foundation_DI: [],
    used_materials_EI: []
  };

  export function __createMocks(custom = { getters: {}, mutations: {}, actions: {}, state: {} }) {
    const mockGetters = Object.assign({}, getters, custom.getters);
    const mockMutations = Object.assign({}, mutations, custom.mutations);
    const mockActions = Object.assign({}, actions, custom.actions);
    const mockState = Object.assign({}, state, custom.state);
  
    return {
      getters: mockGetters,
      mutations: mockMutations,
      actions: mockActions,
      state: mockState,
      store: new Vuex.Store({
        getters: mockGetters,
        mutations: mockMutations,
        actions: mockActions,
        state: mockState,
      }),
    };
  }
  
  export const store = __createMocks().store;