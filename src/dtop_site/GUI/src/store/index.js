// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import axios from 'axios'
import { DTOP_DOMAIN, DTOP_PROTOCOL } from "@/../dtop/dtopConsts"

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    Project: [],
    RunStatus: '',
    waves_basics: false,
    waves_ext: false,
    currents_basics: false,
    currents_ext: false,
    winds_basics: false,
    winds_ext: false,
    waterlevels_basics: false,
    waterlevels_ext: false,
    project_running: false,
    is_saved: false,
    switch_current: false,
    switch_wave: false,
    DR_present: 'true',
    Name: '',
    test_begin: false,
    TS_switch: false
  },
  modules: {
    app,
    settings,
    user
  },
  mutations: {
    set_Project: (state, Project) => {
      if (Project === 1) {
        Initiate_Project_ProjectStatus()
      } else {
        state.Project = Project
      }
    },
    set_RunStatus: (state, RunStatus) => {
      state.RunStatus = RunStatus
    },
    set_waves_basics: (state, waves_basics) => {
      state.waves_basics = waves_basics
    },
    set_waves_ext: (state, waves_ext) => {
      state.waves_ext = waves_ext
    },
    set_currents_basics: (state, currents_basics) => {
      state.currents_basics = currents_basics
    },
    set_currents_ext: (state, currents_ext) => {
      state.currents_ext = currents_ext
    },
    set_winds_basics: (state, winds_basics) => {
      state.winds_basics = winds_basics
    },
    set_winds_ext: (state, winds_ext) => {
      state.winds_ext = winds_ext
    },
    set_waterlevels_basics: (state, waterlevels_basics) => {
      state.waterlevels_basics = waterlevels_basics
    },
    set_waterlevels_ext: (state, waterlevels_ext) => {
      state.waterlevels_ext = waterlevels_ext
    },
    set_project_running: (state, project_running) => {
      state.project_running = project_running
    },
    set_is_saved: (state, is_saved) => {
      state.is_saved = is_saved
    },
    set_switch_current: (state, switch_current) => {
      state.switch_current = switch_current
    },
    set_switch_wave: (state, switch_wave) => {
      state.switch_wave = switch_wave
    },
    set_DR_present: (state, DR_present) => {
      state.DR_present = DR_present
    },
    set_Name: (state, Name) => {
      state.Name = Name
    },
    set_test_begin: (state, test_begin) => {
      state.test_begin = test_begin
    },
    set_TS_switch: (state, TS_switch) => {
      state.TS_switch = TS_switch
    }
  },
  actions: {
    set_ProjectAction: ({commit, state}, newValue) => {
      commit('set_Project', newValue)
      return state.Project
    },
    set_RunStatusAction: ({commit, state}, newValue) => {
      commit('set_RunStatus', newValue)
      return state.RunStatus
    },
    set_waves_basicsAction: ({commit, state}, newValue) => {
      commit('set_waves_basics', newValue)
      return state.waves_basics
    },
    set_waves_extAction: ({commit, state}, newValue) => {
      commit('set_waves_ext', newValue)
      return state.waves_ext
    },
    set_currents_basicsAction: ({commit, state}, newValue) => {
      commit('set_currents_basics', newValue)
      return state.currents_basics
    },
    set_currents_extAction: ({commit, state}, newValue) => {
      commit('set_currents_ext', newValue)
      return state.currents_ext
    },
    set_winds_basicsAction: ({commit, state}, newValue) => {
      commit('set_winds_basics', newValue)
      return state.winds_basics
    },
    set_winds_extAction: ({commit, state}, newValue) => {
      commit('set_winds_ext', newValue)
      return state.winds_ext
    },
    set_waterlevels_basicsAction: ({commit, state}, newValue) => {
      commit('set_waterlevels_basics', newValue)
      return state.waterlevels_basics
    },
    set_waterlevels_extAction: ({commit, state}, newValue) => {
      commit('set_waterlevels_ext', newValue)
      return state.waterlevels_ext
    },
    set_project_runningAction: ({commit, state}, newValue) => {
      commit('set_project_running', newValue)
      return state.project_running
    },
    set_is_savedAction: ({commit, state}, newValue) => {
      commit('set_is_saved', newValue)
      return state.is_saved
    },
    set_switch_currentAction: ({commit, state}, newValue) => {
      commit('set_switch_current', newValue)
      return state.switch_current
    },
    set_switch_waveAction: ({commit, state}, newValue) => {
      commit('set_switch_wave', newValue)
      return state.switch_wave
    },
    set_DR_presentAction: ({commit, state}, newValue) => {
      commit('set_DR_present', newValue)
      return state.DR_present
    },
    set_NameAction: ({commit, state}, newValue) => {
      commit('set_Name', newValue)
      return state.Name
    },
    set_test_beginAction: ({commit, state}, newValue) => {
      commit('set_test_begin', newValue)
      return state.test_begin
    },
    set_TS_switchAction: ({commit, state}, newValue) => {
      commit('set_TS_switch', newValue)
      return state.TS_switch
    }
  },
  getters
})

const Initiate_Project_ProjectStatus = () => {
  const path = `${DTOP_PROTOCOL}//sc.${DTOP_DOMAIN}/sc/initialise_Project`
  axios
    .get(path)
    .then((res) => {
      store.commit('set_Project', res.data);
    })
}

Initiate_Project_ProjectStatus();

export default store
