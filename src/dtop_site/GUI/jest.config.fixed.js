module.exports = {
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest'
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  snapshotSerializers: ['jest-serializer-vue'],
  testMatch: [
    '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  ],
  coverageThreshold: {
    global: {
      statements: 90.0,
      branches: 90.0,
      functions: 90.0,
      lines: 90.0
    }
  },
  collectCoverageFrom: [
    'src/views/schome/*'
  ],
  coverageDirectory: '<rootDir>/tests/unit/coverage',
  'collectCoverage': true,
  'coverageReporters': [
    'html',
    'text-summary',
    'clover'
  ],

  reporters: [
    'default'
  ],
  testURL: 'http://localhost/',
  setupFiles: [
    "core-js",
    '<rootDir>/jest.stub.js'
  ]
}
