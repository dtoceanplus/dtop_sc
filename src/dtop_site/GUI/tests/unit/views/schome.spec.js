// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'

import schome from '@/views/schome/index'
import ElementUI from 'element-ui'

import Project_inputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
import Project_outputs from '@/../tests/unit/json/RM1_SC3_outputs.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('schome', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(ElementUI)
  let schomeId = 1;

  const $router = {
      push: jest.fn(),
    }

  /*
  const $store = {
        commit: jest.fn(),
        dispatch: jest.fn()
    }
  */

  let store = new Vuex.Store({
      state: {
          Project: Project_inputs,
          Name: 'RM1_SC3'
        }
    })

  const wrapper = shallowMount(schome, {
      data() {
          return {
              Results: Project_outputs,
              ProjectName: 'RM1_SC3'
          }
        },
        localVue,
        store,
        mocks: {
            //$store,
            $router,
            $route: {
                params: {
                    schomeId: schomeId,
                },
                query: { ProjectName: 'RM1_SC3' }
            }
        }
    })

    it('onCommit', async() => { 
            const customCommit = jest.spyOn(wrapper.vm, 'onCommit');
            const storeCommit = jest.spyOn(store, 'commit');
            await wrapper.vm.onCommit()
            await wrapper.vm.$nextTick()
            expect(customCommit).toBeCalled();
            expect(customCommit).toBeCalledTimes(1);  
            expect(storeCommit).toBeCalledTimes(3);  
            expect(store.commit).toHaveBeenCalledWith('set_TestBegin', true)
            expect(store.commit).toHaveBeenCalledWith('set_TestBathy', 'false')
            wrapper.vm.onCommit.mockRestore()
            store.commit.mockRestore()
    })

    it('changePage', async() => {
        await wrapper.vm.changePage('Outputs')
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.RunStatus).toBeDefined
        expect($router.push).toHaveBeenCalledWith({"name": "Outputs"});
    })

    it('computed_property_1', async () => {
        // Project
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.Project = "canaille"
        expect(storeDispatch).toBeCalledTimes(1);  
        expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "canaille")
        store.dispatch.mockRestore()
    })
    it('computed_property_2', async () => {
        // test_begin
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.test_begin = "canaille"
        expect(storeDispatch).toBeCalledTimes(1);  
        expect(store.dispatch).toHaveBeenCalledWith("set_test_beginAction", "canaille")
        store.dispatch.mockRestore()
    })
    it('computed_property_3', async () => {
        // RunStatus
        const storeDispatch = jest.spyOn(store, 'dispatch');
        wrapper.vm.RunStatus = "canaille"
        expect(storeDispatch).toBeCalledTimes(1);  
        expect(store.dispatch).toHaveBeenCalledWith("set_RunStatusAction", "canaille")
        store.dispatch.mockRestore()
    })

});