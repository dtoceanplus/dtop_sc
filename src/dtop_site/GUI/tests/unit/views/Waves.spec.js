// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'

import Waves from '@/views/outputs_page/Waves/index'
import ElementUI from 'element-ui'

import Project_inputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
import Project_outputs from '@/../tests/unit/json/RM1_SC3_outputs.json'
import Waves_basics from '@/../tests/unit/json/temp/Waves_Basics.json'
import Waves_EXT from '@/../tests/unit/json/temp/Waves_EXT.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Waves', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(ElementUI)
  let WavesId = 1;

  const $router = {
      push: jest.fn(),
  }
  const $notify = {
    info: jest.fn(),
  }
  let store = new Vuex.Store({
      state: {
          Project: Project_inputs,
          waves_basics: Waves_basics,
          waves_ext: Waves_EXT,
          image1: '',
          image2: '',
          test: '',
      }
  })
  const wrapper = shallowMount(Waves, {
      data() {
          return {
              Results: Project_outputs,
              ProjectName: 'RM1_SC3'
          }
      },
      localVue,
      store,
      mocks: {
          $router,
          $route: {
              params: {
                WavesId: WavesId,
              },
              query: { ProjectName: 'RM1_SC3' }
          }
      }
  })

	beforeEach( async () => {
		const fakeResult = { data: ['test1', 'test2', 'test3', 'test4', 'test5', 'test6'] };
    //axios.post.mockResolvedValue(fakeResult);
    //await wrapper.vm.loadOutputs()
    await wrapper.vm.$nextTick()
	});

  it('LoadOutputs', async () => {
    const path = `http://localhost:5000/sc/RM1_SC3/loadoutputs`
    axios.resolveWith(path)
    //await wrapper.vm.loadOutputs()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.Results).toBeDefined
    expect(wrapper.vm.waves_basics).toBeDefined
    expect(wrapper.vm.waves_ext).toBeDefined
  })

  it('GetImage', async() => {
    const path = `http://localhost:5000/sc/RM1_SC3/GetCurrentImage`
    axios.resolveWith(path)
    //await wrapper.vm.GetImage()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.image1).toBeDefined
    expect(wrapper.vm.image2).toBeDefined
  })

  it('openSidebar', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Overview of waves results',
      message: 'This table presents an overview of wave conditions giving for each main wave parameters the basic statistics (minimum, maximum, average, median and standard deviation).' +
      '<h3> Acronyms </h3> <b>StD</b>: Standard Deviation<br> <b>Hs</b>: significant wave height<br> <b>Tp</b>: wave peak period<br> <b>CgE</b>: wave energy flux<br> <b> Gamma</b>: JONSWAP peak factor<br> <b> Spr</b>: wave directional spreading',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar2', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar2();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'EJPD Hs-Dp',
      message: 'This figure shows the Empirical Joint Probability Distribution (EJPD) of significant wave height (Hs) and wave peak direction (Dp).<br> Dp is given in ° within a meteorological convention (coming from).',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar3', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar3();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'EJPD Hs-Tp',
      message: 'This figure shows the Empirical Joint Probability Distribution (EJPD) of significant wave height (Hs) and wave peak period (Tp).',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('computed_properties', async () => {
    const storeDispatch = jest.spyOn(store, 'dispatch');
    wrapper.vm.switch_wave = "value"
    expect(storeDispatch).toBeCalledTimes(1);  
    expect(store.dispatch).toHaveBeenCalledWith("set_switch_waveAction", "value")
    store.dispatch.mockRestore()
  })

});