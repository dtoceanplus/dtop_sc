// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'

import NewProject from '@/views/Inputs/index'
import ElementUI from 'element-ui'

import Project_inputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
import Project_outputs from '@/../tests/unit/json/RM1_SC3_outputs.json'

import listlease from '@/../tests/unit/json/listlease.json'
import Consistency from '@/../tests/unit/json/Consistency.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock("axios")

describe('NewProject', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(ElementUI)
  let NewProjectId = 1;

  const $router = {
      push: jest.fn(),
  }
  const $notify = {
    info: jest.fn(),
  }
  const stubs = {
    inputs: `<input type="file" ref="seabedfile" accept=".nc" style="display:none" @change="handleFileUpload_seabed();RunStatus =''" />`
  }
  let store = new Vuex.Store({
      state: {
          Project: Project_inputs,
          dialogFormVisible: false,
          dialog_lease: false,
          dialog_corridor: false,
          dialog_seabed: false,
          dialog_roughness: false,
          dialog_species: false,
          dialog_timeseries: false,
          dialog_bathymetry: false,
          Dialog1: false,
          LogFile: '',
          NewLogFile: '',
          intervalid1: '',
          lease_file: '',
          corridor_file: '',
          seabed_file: '',
          roughness_file: '',
          species_file: '',
          ts_file: '',
          bathy_file: '',
          corridor_files_list: [],
          corridor_file_to_delete: [],
          lease_files_list: [],
          lease_file_to_delete: [],
          seabed_files_list: [],
          seabed_file_to_delete: [],
          roughness_files_list: [],
          roughness_file_to_delete: [],
          species_files_list: [],
          species_file_to_delete: [],
          timeseries_files_list: [],
          timeseries_file_to_delete: [],
          bathymetry_files_list: [],
          bathymetry_file_to_delete: [],
          check12: false,
          check3: false,
          ConsistencyAlert: true,
          DR_present: false,
          Name: 'RM1_SC3'
          }
  })
  const wrapper = shallowMount(NewProject, {
      data() {
          return {
              Results: Project_outputs,
              ProjectName: 'RM1_SC3'
          }
      },
      localVue,
      store,
      stubs,
      mocks: {
          $router,
          $route: {
              params: {
                NewProjectId: NewProjectId,
              },
              query: { ProjectName: 'RM1_SC3' }
          }
      }
    })

  it('triggers events', async () => {
    // wrapper.find('input').trigger('change');
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.lease).toBeDefined;
    expect(wrapper.vm.Project.corr).toBeDefined;
    expect(wrapper.vm.Project.seabed).toBeDefined;
    expect(wrapper.vm.Project.roughness).toBeDefined;
    expect(wrapper.vm.Project.species).toBeDefined;
    expect(wrapper.vm.Project.ts).toBeDefined;
    expect(wrapper.vm.Project.bathymetry).toBeDefined;
  })
  
  it("list_of_lease_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_lease_files()
    expect(wrapper.vm.lease_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.lease_file_name).toEqual([''])
  })

  it("list_of_corridor_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_corridor_files()
    expect(wrapper.vm.corridor_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.corridor_file_name).toEqual([''])
  })

  it("list_of_seabed_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_seabed_files()
    expect(wrapper.vm.seabed_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.seabed_file_name).toEqual('')
  })

  it("list_of_roughness_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_roughness_files()
    expect(wrapper.vm.roughness_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.roughness_file_name).toEqual('')
  })

  it("list_of_species_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_species_files()
    expect(wrapper.vm.species_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.species_file_name).toEqual('')
  })
  
  it("list_of_timeseries_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_timeseries_files()
    expect(wrapper.vm.timeseries_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.ts_file_name).toEqual('')
  })

  it("list_of_bathymetry_files", () => {
    axios.resolveWith(listlease)
    wrapper.vm.list_of_bathymetry_files()
    expect(wrapper.vm.bathymetry_files_list).toEqual(listlease.data)
    expect(wrapper.vm.Project.bathy_file_name).toEqual('')
  })

  it('loadOutputs', async () => {
    axios.resolveWith(Project_outputs)
    expect(wrapper.vm.Results).toEqual.Project_outputs
  })

  it('handleFileUpload_lease', async() => {

      function MockFile() { };
      MockFile.prototype.create = function (name, size, mimeType) {
          name = name || "lease.shp";
          size = size || 1024;
          mimeType = mimeType || 'plain/shp';
          function range(count) {
              var output = "";
              for (var i = 0; i < count; i++) {
                  output += "a";
              }
              return output;
          }
          var blob = new Blob([range(size)], { type: mimeType });
          blob.lastModifiedDate = new Date();
          blob.name = name;
          return blob;
      };

      var mock = new MockFile();
      var file = mock.create();
      expect(file.name).toBe('lease.shp');
      expect(file.size).toBe(1024);

      // await wrapper.vm.handleFileUpload_lease()
      // await wrapper.vm.$nextTick()
      expect(wrapper.vm.name_of_corridor_file).toBeDefined

      let promise = new Promise((resolve, reject) => {
        reader.onload = e => {
          const data = e.target.result
          var shapefile = require("shapefile");
          shapefile.open(data)
            .then(source => source.read()
              .then(function log(result) {
                if (result.done) return;
                const payload = {
                  file: result.value,
                  name: files.name
                }
                const path = `${process.env.VUE_APP_API_URL}/sc/ImportLeaseFile`
                axios
                  .post(path,payload)
                return source.read().then(log);
              }))
            .catch(error => console.error(error.stack));
        }
        reader.readAsArrayBuffer(files)
      })
      expect(promise).toEqual(expect.any(Promise))
      expect(promise).toEqual(new Promise((resolve, reject) => {
        reader.onload = e => {
          const data = e.target.result
          var shapefile = require("shapefile");
          shapefile.open(data)
            .then(source => source.read()
              .then(function log(result) {
                if (result.done) return;
                const payload = {
                  file: result.value,
                  name: files.name
                }
                const path = `${process.env.VUE_APP_API_URL}/sc/ImportLeaseFile`
                axios
                  .post(path,payload)
                return source.read().then(log);
              }))
            .catch(error => console.error(error.stack));
        }
        reader.readAsArrayBuffer(files)
      })
    )
  })

  it('handleFileUpload_corridor', async() => {

    function MockFile() { };
      MockFile.prototype.create = function (name, size, mimeType) {
          name = name || "corridor.shp";
          size = size || 1024;
          mimeType = mimeType || 'plain/shp';
          function range(count) {
              var output = "";
              for (var i = 0; i < count; i++) {
                  output += "a";
              }
              return output;
          }
          var blob = new Blob([range(size)], { type: mimeType });
          blob.lastModifiedDate = new Date();
          blob.name = name;
          return blob;
      };

      var mock = new MockFile();
      var file = mock.create();
      expect(file.name).toBe('corridor.shp');
      expect(file.size).toBe(1024);

      // await wrapper.vm.handleFileUpload_corridor()
      // await wrapper.vm.$nextTick()
      expect(wrapper.vm.name_of_corridor_file).toBeDefined
      expect(wrapper.vm.name_of_corridor_file).toBeDefined

      let promise = new Promise((resolve, reject) => {
        reader.onload = e => {
          const data = e.target.result
          var shapefile = require("shapefile");
          shapefile.open(data)
            .then(source => source.read()
              .then(function log(result) {
                if (result.done) return;
                const payload = {
                  file: result.value,
                  name: files.name
                }
                const path = `${process.env.VUE_APP_API_URL}/sc/ImportLeaseFile`
                axios
                  .post(path,payload)
                return source.read().then(log);
              }))
            .catch(error => console.error(error.stack));
        }
        reader.readAsArrayBuffer(files)
      })
      expect(promise).toEqual(expect.any(Promise))
      expect(promise).toEqual(new Promise((resolve, reject) => {
        reader.onload = e => {
          const data = e.target.result
          var shapefile = require("shapefile");
          shapefile.open(data)
            .then(source => source.read()
              .then(function log(result) {
                if (result.done) return;
                const payload = {
                  file: result.value,
                  name: files.name
                }
                const path = `${process.env.VUE_APP_API_URL}/sc/ImportCorridorFile`
                axios
                  .post(path,payload)
                return source.read().then(log);
              }))
            .catch(error => console.error(error.stack));
        }
        reader.readAsArrayBuffer(files)
      })
    )
  })

  it('handleFileUpload_seabed', async() => {

    function MockFile() { };
      MockFile.prototype.create = function (name, size, mimeType) {
          name = name || "seabed.shp";
          size = size || 1024;
          mimeType = mimeType || 'plain/shp';
          function range(count) {
              var output = "";
              for (var i = 0; i < count; i++) {
                  output += "a";
              }
              return output;
          }
          var blob = new Blob([range(size)], { type: mimeType });
          blob.lastModifiedDate = new Date();
          blob.name = name;
          return blob;
      };

      var mock = new MockFile();
      var file = mock.create();
      expect(file.name).toBe('seabed.shp');
      expect(file.size).toBe(1024);

      // await wrapper.vm.handleFileUpload_seabed()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.seabed_file).toBeDefined
      expect(wrapper.vm.name_of_seabed_file).toBeDefined
  })

  it("DeleteLog", () => {
    const axiosPostMethod = jest.spyOn(axios, 'post');
    const path = `http://sc.localhost/sc/DeleteLog`
    wrapper.vm.DeleteLog()
    expect(axiosPostMethod).toBeCalledTimes(1);  
    expect(axios.post).toHaveBeenCalledWith(path)
    expect(wrapper.vm.waves_basics).toBeDefined
    axios.post.mockRestore()
  })

  it('runProject', async () => {
    const axiosPostMethod = jest.spyOn(axios, 'post');
    const path = `http://sc.localhost/sc/run`
    const payload = {
        name: wrapper.vm.Project.name,
        complexity_level: wrapper.vm.Project.complexity_level,
        run_mode: wrapper.vm.Project.run_mode,
        lease: wrapper.vm.Project.lease,
        corr: wrapper.vm.Project.corr,
        wave: wrapper.vm.Project.wave,
        current: wrapper.vm.Project.current,
        bathymetry: wrapper.vm.Project.bathymetry,
        seabed: wrapper.vm.Project.seabed,
        roughness: wrapper.vm.Project.roughness,
        species: wrapper.vm.Project.species,
        ts: wrapper.vm.Project.ts
    }
    axios.resolveWith({
        url: path,
        params: payload
    })
    await wrapper.vm.runProject(payload)
    await wrapper.vm.$nextTick()
    expect(axiosPostMethod).toBeCalledTimes(1);  
    expect(axios.post).toHaveBeenCalledWith(path, payload)
    expect(wrapper.vm.RunStatus).toBeDefined
    expect(wrapper.vm.project_running).toBeDefined
    expect(wrapper.vm.is_saved).toBeDefined
    axios.post.mockRestore()
  })

  it('changePage', async() => {
    await wrapper.vm.changePage('Outputs')
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "Outputs"});
  })

  it('SaveAs', async () => {
    const path = 'http://localhost:5000/sc/${wrapper.vm.Project.name}/saveProject'
    const payload = {
        name: wrapper.vm.Project.name,
        complexity_level: wrapper.vm.Project.complexity_level,
        run_mode: wrapper.vm.Project.run_mode,
        lease: wrapper.vm.Project.lease,
        corr: wrapper.vm.Project.corr,
        wave: wrapper.vm.Project.wave,
        current: wrapper.vm.Project.current,
        bathymetry: wrapper.vm.Project.bathymetry,
        seabed: wrapper.vm.Project.seabed,
        roughness: wrapper.vm.Project.roughness,
        species: wrapper.vm.Project.species,
        ts: wrapper.vm.Project.ts
    }
    axios.resolveWith(path, payload)
    await wrapper.vm.SaveAs()
    await wrapper.vm.$nextTick()
  })

  it('RedirectToOutput', async() => {
    await wrapper.vm.RedirectToOutput()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "Overview"});
  })

  it('ReadLog', async() => {
    const path = `${process.env.VUE_APP_API_URL}/sc/ReadLog`
    axios.resolveWith(path)
    const timer = jest.spyOn(wrapper.vm, 'ReadLog')
    await wrapper.vm.ReadLog(1000)
    await wrapper.vm.$nextTick()
    //expect(wrapper.vm.NewLogFile).toEqual(wrapper.vm.LogFile.replaceAll(date,"\n"+date).split("\n"))
    expect(wrapper.vm.NewLogFile).toBeDefined
    expect(timer).toHaveBeenCalledTimes(1)
    expect(timer).toHaveBeenCalledWith(1000)
    expect(wrapper.vm.needload).toBeFalsy()
  })

  it('openSidebar', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Databases',
      message: '<h3> Wave level of energy </h3> the wave time series are extracted from the HOMERE database [1].' +
        '<h3> Current level of energy </h3> the wave time series are extracted from the HOMERE database [1]. <br>' +
        '<br>[1] Boudière, E., Maisondieu, C., Ardhuin, F., Accensi, M., Pineau-Guillou, L. and  Lepesqueur,  J.,  2013.A  suitable  metocean  hindcast  database  for  the  design  of  Marine  energy converters. International Journal of Marine Energy 3-4 (2013) e40-e52.',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar2', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar2();
    await wrapper.vm.$nextTick();
    let fakedNotification ={
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Bathymetry',
      message: 'The bathymetry (water depth in meter from MSL) comes from GEBCO (General Bathymetric Chart of the Oceans) [1].<br>' +
        '<br>[1] GEBCO Compilation Group, 2019. GEBCO 2019 Grid (doi:10.5285/836f016a-33be-6ddc-e053-6c86abc0788e)',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar3', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar3();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Geometries',
      message: '<h3> Lease area </h3> Please select in the list the lease area (farm extent) you want to use or import your own (polygon) shapefile.' +
        '<h3> Corridor </h3> Please select in the list the corridor (export cable route) you want to use or import your own (polyline) shapefile.',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar4', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar4();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Databases',
      message: '<h3> Seabed types </h3> Please select in the list the sediment file you want to use or import your own bathymetry file in netCDF format.<br>' +
        'The available sediment files come from the World Sediment Map (SHOM) [1].' +
        '<h3> Roughness length </h3> From  the sediment file, the macroscopic roughness length z0 has been computed, using Nikuradse\'s formula (2.5 times the average diameter D50 of the sediment).<br>' +
        'You can also import your own roughness length file with a z0 in meters and in a netCDF format.' +
        '<h3> Endangered species </h3> Please select in the list the global Species file or import your own one.' +
        'The Species database has been built from Aquamaps [2] and gives the probability of presence of 26 endangered species.' +
        '<h3> Timeseries </h3> Please select in the list the timeseries file you want to use or import your own timeseries either in netCDF format or in CSV format.<br>' +
        'Timeseries of waves, tidal currents, winds and water levels have been extracted from several databases.<br>' +
        'More information is available in the Site Characterisation User Manual.<br>' +
        '<br>[1] GEBCO Compilation Group, 2019. GEBCO 2019 Grid (doi:10.5285/836f016a-33be-6ddc-e053-6c86abc0788e)' +
        '<br>[2] Kaschner, K., K. Kesner-Reyes, C. Garilao, J. Rius-Barile, T. Rees, and R. Froese. 2016.AquaMaps: Predicted range maps for aquatic species. World wide web electronic publication, www.aquamaps.org, Version 10/2019.',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar5', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar5();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Bathymetry',
      message: 'Please select in the list the bathymetry (water depth) you want to use or import your own bathymetry file in netCDF format.<br>' +
        'The available bathymetry files come from GEBCO (General Bathymetric Chart of the Oceans) [1].<br>' +
        '<br>[1] GEBCO  Compilation  Group,  2019.GEBCO  2019  Grid(doi:10.5285/836f016a-33be-6ddc-e053-6c86abc0788e)',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('Check_cplx12', async () => {
    await wrapper.vm.Check_cplx12();
    await wrapper.vm.$nextTick();
    if (wrapper.vm.Project.wave.length !=0 && wrapper.vm.Project.current.length !=0) {
      if ((wrapper.vm.Project.uniform_bathy == true && wrapper.vm.Project.bathymetry.length != 0)||wrapper.vm.Project.uniform_bathy == false) {
        expect(wrapper.vm.check12).toBeTruthy()
      } else {
        expect(wrapper.vm.check12).toBeFalsy()
      }
    } else {
      expect(wrapper.vm.check12).toBeFalsy()
    }
  })

  it('Check_cplx3', async () => {
    await wrapper.vm.Check_cplx3();
    await wrapper.vm.$nextTick();
    if ((wrapper.vm.Project.lease !='./storage/temp/lease//.shp' && wrapper.vm.Project.lease.length != 0) && (wrapper.vm.Project.corr != './storage/temp/corridor//.shp' && wrapper.vm.Project.corr.length != 0) && (wrapper.vm.Project.seabed != './storage/temp/seabed/' && wrapper.vm.Project.seabed.length != 0) && (wrapper.vm.Project.roughness != './storage/temp/roughness/' && wrapper.vm.Project.roughness.length != 0) && (wrapper.vm.Project.species != './storage/temp/species/' && wrapper.vm.Project.species.length != 0) && (wrapper.vm.Project.ts != './storage/temp/timeseries/' && wrapper.vm.Project.ts.length != 0)) {
        if (wrapper.vm.Project.bathymetry.length !=0) {
          expect(wrapper.vm.check3).toBeTruthy()
        } else {
          expect(wrapper.vm.check3).toBeFalsy()
        }
      } else {
        expect(wrapper.vm.check3).toBeFalsy()
      }
  })

  it('DefineCorridor1', async () => {
    wrapper.vm.Project.selected_corridor = 'Example_corridor'
    await wrapper.vm.DefineCorridor();
    expect(wrapper.vm.Project.corr).toBe('./databases/SiteCharacterisation_User_Basic-Database/Geometries/Example_corridor_NEW.shp')
    expect(wrapper.vm.corridor_file).toBe('')
  });

  it('DefineCorridor2', async () => {
    wrapper.vm.Project.selected_corridor = 'RM1_corridor_WGS84'
    await wrapper.vm.DefineCorridor();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.corr).toBe('./databases/SiteCharacterisation_User_VerificationCases-Database/Geometries/' + wrapper.vm.Project.selected_corridor + '.shp')
    expect(wrapper.vm.corridor_file).toBe('')
  });

  it('DefineCorridor3', async () => {
    wrapper.vm.Project.selected_corridor = 'third_test'
    await wrapper.vm.DefineCorridor();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.corr).toBe('./storage/temp/corridor/' + wrapper.vm.Project.selected_corridor + '/'  + wrapper.vm.Project.selected_corridor + '.shp')
    expect(wrapper.vm.corridor_file).toBe('')
  });

  it('DefineSeabed', async () => {
    wrapper.vm.Project.selected_seabed = 'France_SEABED-TYPE_SHOM_450m'
    await wrapper.vm.DefineSeabed();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.seabed).toBe('./databases/SiteCharacterisation_Main-Database/Seabed/' + wrapper.vm.Project.selected_seabed +'.nc')
    expect(wrapper.vm.corridor_file).toBe('')
  });

  it('DefineSeabed2', async () => {
    wrapper.vm.Project.selected_seabed = 'World_SEABED-TYPE_SHOM_9km'
    await wrapper.vm.DefineSeabed();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.seabed).toBe('./databases/SiteCharacterisation_User_World-Database/DirectValues/' + wrapper.vm.Project.selected_seabed +'.nc')
    expect(wrapper.vm.corridor_file).toBe('')
  });

  it('DefineSeabed3', async () => {
    wrapper.vm.Project.selected_seabed = 'third_test'
    await wrapper.vm.DefineSeabed();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.seabed).toBe('./storage/temp/seabed/' + wrapper.vm.Project.selected_seabed)
    expect(wrapper.vm.corridor_file).toBe('')
  });

  it('DefineRoughness', async () => {
    wrapper.vm.Project.selected_roughness = 'France_SEABED-ROUGHNESS-LENGTH_SHOM_450m'
    await wrapper.vm.DefineRoughness();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.roughness).toBe('./databases/SiteCharacterisation_Main-Database/Seabed/' + wrapper.vm.Project.selected_roughness +'.nc')
    expect(wrapper.vm.roughness_file).toBe('')
  });

  it('DefineRoughness2', async () => {
    wrapper.vm.Project.selected_roughness = 'World_SEABED-ROUGHNESS-LENGTH_SHOM_9km'
    await wrapper.vm.DefineRoughness();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.roughness).toBe('./databases/SiteCharacterisation_User_World-Database/DirectValues/' + wrapper.vm.Project.selected_roughness +'.nc')
    expect(wrapper.vm.roughness_file).toBe('')
  });

  it('DefineRoughness3', async () => {
    wrapper.vm.Project.selected_roughness = 'third_test'
    await wrapper.vm.DefineRoughness();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.roughness).toBe('./storage/temp/roughness/' + wrapper.vm.Project.selected_roughness)
    expect(wrapper.vm.roughness_file).toBe('')
  });

  it('DefineSpecies', async () => {
    wrapper.vm.Project.selected_species = 'World_species_9km'
    await wrapper.vm.DefineSpecies();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.species).toBe('./databases/SiteCharacterisation_User_Basic-Database/DirectValues/' + wrapper.vm.Project.selected_species + '.nc')
    expect(wrapper.vm.species_file).toBe('')
  });

  it('DefineSpecies2', async () => {
    wrapper.vm.Project.selected_species = 'second_test'
    await wrapper.vm.DefineSpecies();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.species).toBe('./storage/temp/species/' + wrapper.vm.Project.selected_species)
    expect(wrapper.vm.species_file).toBe('')
  });

  it('DefineTimeseries', async () => {
    wrapper.vm.Project.selected_ts = 'TimesSeries1D_E2RM1'
    await wrapper.vm.DefineTimeseries();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.ts).toBe('./databases/SiteCharacterisation_User_VerificationCases-Database/TimeSeries/' + wrapper.vm.Project.selected_ts + '.nc')
    expect(wrapper.vm.ts_file).toBe('')
  });

  it('DefineTimeseries2', async () => {
    wrapper.vm.Project.selected_ts = 'TimeSeries1D_example'
    await wrapper.vm.DefineTimeseries();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.ts).toBe('./databases/SiteCharacterisation_User_Basic-Database/TimeSeries/' + wrapper.vm.Project.selected_ts + '.csv')
    expect(wrapper.vm.ts_file).toBe('')
  });

  it('DefineTimeseries3', async () => {
    wrapper.vm.Project.selected_ts = 'TimeSeries1D_RM1_TAKOMA'
    await wrapper.vm.DefineTimeseries();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.ts).toBe('./databases/SiteCharacterisation_User_VerificationCases-Database/TimeSeries/' + wrapper.vm.Project.selected_ts + '.csv')
    expect(wrapper.vm.ts_file).toBe('')
  });

  it('DefineTimeseries4', async () => {
    wrapper.vm.Project.selected_ts = 'third-test'
    await wrapper.vm.DefineTimeseries();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.ts).toBe('./storage/temp/timeseries/' + wrapper.vm.Project.selected_ts)
    expect(wrapper.vm.ts_file).toBe('')
  });

  it('DefineBathymetry', async () => {
    wrapper.vm.Project.selected_bathy = 'World_BATHYMETRY_GEBCO2019_9km'
    await wrapper.vm.DefineBathymetry();
    await wrapper.vm.$nextTick();
    // expect(wrapper.vm.Project.bathy).toBe('./databases/SiteCharacterisation_User_World-Database/DirectValues/' + wrapper.vm.Project.selected_bathy + '.nc')
    expect(wrapper.vm.Project.bathymetry).toBeDefined
    expect(wrapper.vm.bathy_file).toBe('')
  });

  it('DefineBathymetry2', async () => {
    wrapper.vm.Project.selected_bathy = 'France_BATHYMETRY_GEBCO2019_450m'
    await wrapper.vm.DefineBathymetry();
    await wrapper.vm.$nextTick();
    // expect(wrapper.vm.Project.bathy).toBe('./databases/SiteCharacterisation_User_France-Database/DirectValues/' + wrapper.vm.Project.selected_bathy + '.nc')
    expect(wrapper.vm.Project.bathymetry).toBeDefined
    expect(wrapper.vm.bathy_file).toBe('')
  });

  it('DefineBathymetry3', async () => {
    wrapper.vm.Project.selected_bathy = 'third-test'
    await wrapper.vm.DefineBathymetry();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.Project.bathymetry).toBe('./storage/temp/bathymetry/' + wrapper.vm.Project.selected_bathy)
    expect(wrapper.vm.bathy_file).toBe('')
  });

  it('CheckConsistency', async () => {
    axios.resolveWith(Consistency)
    wrapper.vm.CheckConsistency()
    expect(wrapper.vm.ConsistencyAlert).toBeDefined
  });

  it('computed_properties', async () => {
    const storeDispatch = jest.spyOn(store, 'dispatch');
    wrapper.vm.Project = "value"
    wrapper.vm.RunStatus = "value"
    wrapper.vm.waves_basics = "value"
    wrapper.vm.waves_ext = "value"
    wrapper.vm.currents_basics = "value"
    wrapper.vm.currents_ext = "value"
    wrapper.vm.winds_basics = "value"
    wrapper.vm.winds_ext = "value"
    wrapper.vm.waterlevels_basics = "value"
    wrapper.vm.waterlevels_ext = "value"
    wrapper.vm.project_running = "value"
    wrapper.vm.is_saved = "value"
    wrapper.vm.selected_lease = "value"
    wrapper.vm.selected_corridor = "value"
    wrapper.vm.selected_seabed = "value"
    wrapper.vm.selected_roughness = "value"
    wrapper.vm.selected_species = "value"
    wrapper.vm.selected_ts = "value"
    wrapper.vm.selected_bathymetry = "value"
    expect(storeDispatch).toBeCalledTimes(12);  
    expect(store.dispatch).toHaveBeenCalledWith("set_ProjectAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_RunStatusAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_waves_basicsAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_waves_extAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_currents_basicsAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_currents_extAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_winds_basicsAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_winds_extAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_waterlevels_basicsAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_waterlevels_extAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_project_runningAction", "value")
    expect(store.dispatch).toHaveBeenCalledWith("set_is_savedAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_leaseAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_corridorAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_seabedAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_roughnessAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_speciesAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_tsAction", "value")
    // expect(store.dispatch).toHaveBeenCalledWith("set_selected_bathymetryAction", "value")
    store.dispatch.mockRestore()
  })

})