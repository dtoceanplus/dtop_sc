// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'

import PDF from '@/views/pdf/download'
import ElementUI from 'element-ui'

import Project_inputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
import Project_outputs from '@/../tests/unit/json/RM1_SC3_outputs.json'
import WaveImages from '@/../tests/unit/json/GetWaveImage.json'
import CurrentImages from '@/../tests/unit/json/GetCurrentImage.json'
import MapsImages from '@/../tests/unit/json/Get2DMapsImage.json'

import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import { data } from 'autoprefixer'

'use strict';

jest.useFakeTimers();

jest.mock("axios", () => ({
    post: jest.fn((_url, _body) => { 
      return new Promise((resolve) => {
        url = _url
        body = _body
        resolve(true)
      })
    }),
    get: jest.fn((_url, _body) => { 
        return new Promise((resolve) => {
          url = _url
          body = _body
          resolve(true)
        })
      }),
  }))

describe('PDF', () => {
    const localVue = createLocalVue()
    localVue.use(Vuex)
    localVue.use(ElementUI)
    let PDFId = 1

    const $router = {
        push: jest.fn(),
    }
    let store = new Vuex.Store({
        state: {
            Project: Project_inputs,
            fullscreenLoading: true,
            ProjectName: '',
            entity_id: '',
            Complexity: '',
            waves_basics: false,
            waves_ext: false,
            currents_basics: false,
            currents_ext: false,
            winds_basics: false,
            winds_ext: false,
            waterlevels_basics: false,
            waterlevels_ext: false,
            is_figures: false,
            image1: '',
            image2: '',
            image3: '',
            image4: '',
            image5: '',
            image6: '',
            image7: '',
            image8: '',
            image9: '',
            switch_current: false,
            switch_wave: false,
            res: {
                data: {
                    value: "value",
                    value: "value",
                    value: "value",
                    value: "value",
                    value: "value",
                    value: "value"
                }
            }
        }
    })
    const wrapper = shallowMount(PDF, {
        data() {
            return {
                Results: Project_outputs,
                ProjectName: 'test_run',
                entity_id: 1
            }
        },
        localVue,
        store,
        mocks: {
            $router,
            $route: {
                params: {
                    PDFId: PDFId,
                },
                query: { ProjectName: 'test_run', entity_id: 1 }
            }
        }
    })

    it('Test ProjectName data', () => {
        expect(wrapper.vm.ProjectName).toEqual('test_run')
        expect(wrapper.vm.Results).toBeDefined
    })

    it('fetchData', async () => {
        await wrapper.vm.fetchData()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.fullscreenLoading).toBeTruthy()
    })

    it('loadOutputs', async () => {
        axios.get.mockResolvedValue(Project_outputs)
        expect(wrapper.vm.Results).toEqual.Project_outputs
    })

    it('GetWaveImages', async () => {
        axios.post.mockResolvedValue(WaveImages)
        await wrapper.vm.GetWaveImages()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.image1).toEqual('data:image/png;base64, ' + WaveImages.data[0])
        expect(wrapper.vm.image2).toEqual('data:image/png;base64, ' + WaveImages.data[1])
        expect(wrapper.vm.image_new1).toEqual('data:image/png;base64, ' + WaveImages.data[2])
    })

    it('GetCurrentImages', async () => { 
        axios.post.mockResolvedValue(CurrentImages)
        await wrapper.vm.GetCurrentImages()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.image3).toEqual('data:image/png;base64, ' + CurrentImages.data[0])
        expect(wrapper.vm.image_new2).toEqual('data:image/png;base64, ' + CurrentImages.data[1])
    })

    it('Get2DImages', async () => { 
        axios.post.mockResolvedValue(MapsImages)
        await wrapper.vm.Get2DImages()
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.image4).toEqual('data:image/png;base64, ' + MapsImages.data[0])
        expect(wrapper.vm.image5).toEqual('data:image/png;base64, ' + MapsImages.data[1])
        expect(wrapper.vm.image6).toEqual('data:image/png;base64, ' + MapsImages.data[2])
        expect(wrapper.vm.image7).toEqual('data:image/png;base64, ' + MapsImages.data[3])
        expect(wrapper.vm.image8).toEqual('data:image/png;base64, ' + MapsImages.data[4])
        expect(wrapper.vm.image9).toEqual('data:image/png;base64, ' + MapsImages.data[5])
    })

})
