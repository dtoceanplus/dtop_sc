// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import Dashboard from '@/views/dashboard/index'
import ElementUI from 'element-ui'

import Vue from 'vue'
import axios from 'axios'

jest.mock('axios')

describe('Dashboard', () => {
    const localVue = createLocalVue()  
    let DashboardId = 2
    const $router = {
        push: jest.fn()
    }

    const wrapper = shallowMount(Dashboard, {
        mocks: {
            $route: {
                params: {
                    DashboardId: DashboardId,
                }
            },
            $router
        }
    })

    it('Name', () => {
        expect(Dashboard.name).toEqual("Dashboard")
    })
})
  
