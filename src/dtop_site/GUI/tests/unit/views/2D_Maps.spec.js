// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'

import Maps_2D from '@/views/outputs_page/2D_Maps/index'
import ElementUI from 'element-ui'

import Project_inputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
import Project_outputs from '@/../tests/unit/json/RM1_SC3_outputs.json'
import Waves_basics from '@/../tests/unit/json/temp/Waves_Basics.json'
import Waves_EXT from '@/../tests/unit/json/temp/Waves_EXT.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('2D_Maps', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(ElementUI)
  let Maps2DId = 1;

  const $router = {
      push: jest.fn(),
  }
  const $notify = {
    info: jest.fn(),
  }
  let store = new Vuex.Store({
      state: {
          Project: Project_inputs,
          waves_basics: Waves_basics,
          waves_ext: Waves_EXT,
          is_figures: false,
          image1: '',
          image2: '',
          image3: '',
          image4: '',
          image5: '',
          image6: ''
      }
  })
  const wrapper = shallowMount(Maps_2D, {
      data() {
          return {
              Results: Project_outputs,
              ProjectName: 'RM1_SC3'
          }
      },
      localVue,
      store,
      mocks: {
          $router,
          $route: {
              params: {
                Maps2DId: Maps2DId,
              },
              query: { ProjectName: 'RM1_SC3' }
          }
      }
  })

  it('LoadOutputs', async () => {
    const path = `http://localhost:5000/sc/RM1_SC3/loadoutputs`
    axios.resolveWith(path)
    //await wrapper.vm.loadOutputs()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.Results).toBeDefined
  })

  it('checkFigures', async() => {
    const path = `${process.env.VUE_APP_API_URL}/sc/${wrapper.vm.Project.name}/checkfigures`
    axios.resolveWith(path)
    wrapper.vm.is_figures = false
    await wrapper.vm.checkFigures()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.is_figures).toBeDefined
    if (wrapper.vm.is_figures === true){
        expect(wrapper.vm.is_figures).toBeTruthy()
    }else{
        expect(wrapper.vm.is_figures).toBeFalsy()
    }
  })

  it('checkFigures2', async() => {
    const path = `${process.env.VUE_APP_API_URL}/sc/${wrapper.vm.Project.name}/checkfigures`
    axios.resolveWith(path)
    wrapper.vm.is_figures = true
    await wrapper.vm.checkFigures()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.is_figures).toBeDefined
    if (wrapper.vm.is_figures === true){
        expect(wrapper.vm.is_figures).toBeTruthy()
    }else{
        expect(wrapper.vm.is_figures).toBeFalsy()
    }
  })

  it('GetImage', async() => {
    const path = `${process.env.VUE_APP_API_URL}/sc/${wrapper.vm.Project.name}/Get2DImage`
    axios.resolveWith(path)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.image1).toBeDefined
    expect(wrapper.vm.image2).toBeDefined
    expect(wrapper.vm.image3).toBeDefined
    expect(wrapper.vm.image4).toBeDefined
    expect(wrapper.vm.image5).toBeDefined
    expect(wrapper.vm.image6).toBeDefined
  })

});