// This is the Site Characterisation module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import { shallowMount, createLocalVue } from '@vue/test-utils'

import Overview from '@/views/outputs_page/Overview/index'
import ElementUI from 'element-ui'

import Project_inputs from '@/../tests/unit/json/RM1_SC3_inputs.json'
import Project_outputs from '@/../tests/unit/json/RM1_SC3_outputs.json'
import Currents_basics from '@/../tests/unit/json/temp/Currents_Basics.json'
import Waves_basics from '@/../tests/unit/json/temp/Waves_Basics.json'
import WaterLevels_basics from '@/../tests/unit/json/temp/WaterLevel_Basics.json'
import Winds_basics from '@/../tests/unit/json/temp/Winds_Basics.json'
import Currents_EXT from '@/../tests/unit/json/temp/Currents_EXT.json'
import Waves_EXT from '@/../tests/unit/json/temp/Waves_EXT.json'
import WaterLevels_EXT from '@/../tests/unit/json/temp/WaterLevel_EXT.json'
import Winds_EXT from '@/../tests/unit/json/temp/Winds_EXT.json'

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import { __createMocks as createStoreMocks } from '@/store';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

jest.mock('@/store')
jest.mock('axios')

describe('Overview', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  localVue.use(ElementUI)
  let OverviewId = 1;

  const $router = {
      push: jest.fn(),
  }
  const $notify = {
    info: jest.fn(),
  }
  let store = new Vuex.Store({
      state: {
          Project: Project_inputs,
          waves_basics: Waves_basics,
          currents_basics: Currents_basics,
          waterlevels_basics: WaterLevels_basics,
          winds_basics: Winds_basics,
          waves_ext: Waves_EXT,
          currents_ext: Currents_EXT,
          winds_ext: Winds_EXT,
          waterlevels_ext: WaterLevels_EXT
      }
  })
  const wrapper = shallowMount(Overview, {
      data() {
          return {
              Results: Project_outputs,
              ProjectName: 'RM1_SC3'
          }
      },
      localVue,
      store,
      mocks: {
          $router,
          $route: {
              params: {
                OverviewId: OverviewId,
              },
              query: { ProjectName: 'RM1_SC3' }
          }
      }
  })

  it('test initial values', () => {
    expect(wrapper.vm.Results).toBeDefined
    expect(wrapper.vm.waves_basics).toBeDefined
    expect(wrapper.vm.waves_ext).toBeDefined
    expect(wrapper.vm.currents_basics).toBeDefined
    expect(wrapper.vm.currents_ext).toBeDefined
    expect(wrapper.vm.winds_basics).toBeDefined
    expect(wrapper.vm.winds_ext).toBeDefined
    expect(wrapper.vm.waterlevels_basics).toBeDefined
    expect(wrapper.vm.waterlevels_ext).toBeDefined
  })

  it('LoadOutputs', async () => {
    const path = `http://localhost:5000/sc/RM1_SC3/loadoutputs`
    axios.resolveWith(path)
    //await wrapper.vm.loadOutputs()
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.Results).toBeDefined
    expect(wrapper.vm.waves_basics).toBeDefined
    expect(wrapper.vm.waves_ext).toBeDefined
    expect(wrapper.vm.currents_basics).toBeDefined
    expect(wrapper.vm.currents_ext).toBeDefined
    expect(wrapper.vm.winds_basics).toBeDefined
    expect(wrapper.vm.winds_ext).toBeDefined
    expect(wrapper.vm.waterlevels_basics).toBeDefined
    expect(wrapper.vm.waterlevels_ext).toBeDefined
  })

  it('openSidebar', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Overview of meteocean conditions',
      message: 'This table presents an overview of metocean conditions giving for each main physical parameters the basic statistics (minimum, average, maximum and standard deviation).' +
        '<h3> Acronyms </h3> <b>StD</b>: Standard Deviation<br> <b>Hs</b>: significant wave height<br> <b>Mag</b>: depth-averaged tidal current magnitude<br> <b>Mag10</b>: 10m-wind magnitude<br> <b> WLEV</b>: water level, bathymetry + surface fluctuations.',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

  it('openSidebar2', async () => {
    const notifyInfo = jest.spyOn($notify, 'info');
    await wrapper.vm.openSidebar2();
    await wrapper.vm.$nextTick();
    let fakedNotification = {
      customClass: 'my_notify',
      dangerouslyUseHTMLString: true,
      title: 'Extreme value of meteocean conditions',
      message: 'This table presents the extreme values of metocean conditions giving for each main physical parameters (waves, tidal currents and winds).<br>' +
        'In this module, GEV (Generalized Extreme Values distribution) is used to compute the extreme values of tidal currents and the GPD (Generalized Pareto Distribution) is used to compute the extreme values of waves and winds. <br>' +
        'More information is available in the Site Characterisation User Manual.<br>' +
        '<h3> Acronyms </h3> <b>RP</b>: Return period<br> <b>Hs</b>: significant wave height<br> <b>Mag</b>: tidal current magnitude<br> <b>Mag10</b>: 10m-wind magnitude.',
      duration: 0,
      position: 'top-right'
    }
    $notify.info(fakedNotification)
    expect(notifyInfo).toBeCalledTimes(1);  
    expect($notify.info).toHaveBeenCalledWith(fakedNotification);
    $notify.info.mockRestore()
  })

});