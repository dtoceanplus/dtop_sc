# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 12:14:25 2020

@author: ykervell
"""
from dtop_site.BusinessLogic import Project
from dtop_site.BusinessLogic.catalogs import early_stage_energy_databases as esed
from pathlib import Path
import os

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage/'))
PATH_USER = os.path.join(working, Path('USER'))
PATH_USER_GEOMETRIES = os.path.join(PATH_USER, Path('Geometries'))
PATH_USER_DIRECTVALUES= os.path.join(PATH_USER, Path('DirectValues'))
PATH_USER_TIMESERIES = os.path.join(PATH_USER, Path('TimeSeries'))


"""def test_Project_init():
    project_name = 'turlututu'
    my_databases = {}  
    my_databases['WAV'] = 'EarlyStage_WestIreland_waves'
    my_databases['CUR'] = 'EarlyStage_WestIreland_currents'
    my_databases['WIN'] = 'EarlyStage_WestIreland_winds'
    my_databases['LEV'] = 'EarlyStage_WestIreland_currents'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    farm_file = 'WestIreland_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    corridor_file = 'WestIreland_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
    DevicesFile = {}
    MixLevel = "Waves-high_Currents-low"
    point_name = esed.Corresp[MixLevel]['database_name']
    DevicesFile['names'] = (point_name,)
    DevicesFile['lons'] = (esed.Corresp[MixLevel]['longitude'],)
    DevicesFile['lats'] = (esed.Corresp[MixLevel]['latitude'],)
    DTOP = Project.MyProject(project_name, my_databases, FarmFile, CorridorFile, DevicesFile, storage)
    assert DTOP

def test_Project_init_and_load():
    project_name = 'turlututu'
    my_databases = {}  
    my_databases['WAV'] = 'EarlyStage_WestIreland_waves'
    my_databases['CUR'] = 'EarlyStage_WestIreland_currents'
    my_databases['WIN'] = 'EarlyStage_WestIreland_winds'
    my_databases['LEV'] = 'EarlyStage_WestIreland_currents'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    farm_file = 'WestIreland_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    corridor_file = 'WestIreland_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
    DevicesFile = {}
    MixLevel = "Waves-high_Currents-low"
    point_name = esed.Corresp[MixLevel]['database_name']
    DevicesFile['names'] = (point_name,)
    DevicesFile['lons'] = (esed.Corresp[MixLevel]['longitude'],)
    DevicesFile['lats'] = (esed.Corresp[MixLevel]['latitude'],)
    DTOP = Project.MyProject(project_name, my_databases, FarmFile, CorridorFile, DevicesFile, storage)
    DTOP.__load_shapefiles__()
    assert DTOP

def test_Project_init_load_and_extract():
    project_name = 'turlututu'
    my_databases = {}  
    my_databases['WAV'] = 'EarlyStage_WestIreland_waves'
    my_databases['CUR'] = 'EarlyStage_WestIreland_currents'
    my_databases['WIN'] = 'EarlyStage_WestIreland_winds'
    my_databases['LEV'] = 'EarlyStage_WestIreland_currents'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    farm_file = 'WestIreland_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    corridor_file = 'WestIreland_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
    DevicesFile = {}
    MixLevel = "Waves-high_Currents-low"
    point_name = esed.Corresp[MixLevel]['database_name']
    DevicesFile['names'] = (point_name,)
    DevicesFile['lons'] = (esed.Corresp[MixLevel]['longitude'],)
    DevicesFile['lats'] = (esed.Corresp[MixLevel]['latitude'],)
    DTOP = Project.MyProject(project_name, my_databases, FarmFile, CorridorFile, DevicesFile, storage)
    DTOP.__load_shapefiles__()
    DTOP.__launch_extractions__('dtop')
    assert DTOP

def test_Project_init_load_extract_and_stats():
    project_name = 'turlututu'
    my_databases = {}  
    my_databases['WAV'] = 'EarlyStage_WestIreland_waves'
    my_databases['CUR'] = 'EarlyStage_WestIreland_currents'
    my_databases['WIN'] = 'EarlyStage_WestIreland_winds'
    my_databases['LEV'] = 'EarlyStage_WestIreland_currents'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    farm_file = 'WestIreland_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    corridor_file = 'WestIreland_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
    DevicesFile = {}
    MixLevel = "Waves-high_Currents-low"
    point_name = esed.Corresp[MixLevel]['database_name']
    DevicesFile['names'] = (point_name,)
    DevicesFile['lons'] = (esed.Corresp[MixLevel]['longitude'],)
    DevicesFile['lats'] = (esed.Corresp[MixLevel]['latitude'],)
    DTOP = Project.MyProject(project_name, my_databases, FarmFile, CorridorFile, DevicesFile, storage)
    DTOP.__load_shapefiles__()
    DTOP.__launch_extractions__('dtop')
    DTOP.__launch_statistics__()
    assert DTOP


def test_Project_init_load_extract_stats_and_store():
    project_name = 'turlututu'
    my_databases = {}  
    level = 2
    my_databases['WAV'] = 'WestGroix_all_1D'
    my_databases['CUR'] = 'WestGroix_all_1D'
    my_databases['WIN'] = 'WestGroix_all_1D'
    my_databases['LEV'] = 'WestGroix_all_1D'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    my_databases['WAV2D'] = 'WestGroix_all_2D'
    my_databases['CUR2D'] = 'WestGroix_all_2D'
    storage = PATH_STORAGE
    farm_file = 'WestGroix_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    corridor_file = 'WestGroix_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
    DTOP = Project.MyProject(project_name, level, my_databases, FarmFile, CorridorFile, storage)
    my_polygon = DTOP.__load_shapefiles__()
    DTOP.__launch_extractions__('dtop')
    DTOP.__extract_2D_values__(my_polygon)
    DTOP.__launch_statistics__()
    DTOP.__plot_results__('WestGroix')
    DTOP.__store_project__()
    assert DTOP
"""
    
def test_Project_init_load_extract_stats_and_multiple_store():
    project_name = 'turlututu'
    my_databases = {}  
    level = 3   # level 3 in 2D
    # GEOMETRIES
    farm_file = 'Example_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_USER_GEOMETRIES, farm_file) 
    corridor_file = 'Example_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_USER_GEOMETRIES, corridor_file)
    # DATABASES
    TS = 'TimeSeries2D_example_short.csv'
    databases_file = os.path.join(PATH_USER_TIMESERIES, TS)
    BA = 'World_GEBCO2019_downgrade20.nc'
    bathy_file = os.path.join(PATH_USER_DIRECTVALUES, BA)
    SE = 'Europa_downgrade20_SEABED-TYPE.nc'
    seabed_file = os.path.join(PATH_USER_DIRECTVALUES, SE)
    RO = 'Europa_downgrade20_SEABED-ROUGHNESS-LENGTH.nc'
    roughness_file = os.path.join(PATH_USER_DIRECTVALUES, RO)
    SP = 'Europa_species.nc'
    species_file = os.path.join(PATH_USER_DIRECTVALUES, SP)
    # Databases
    project_name += "_2D"
    my_databases['WAV2D'] = databases_file
    my_databases['CUR2D'] = databases_file
    my_databases['WAV'] = databases_file.replace('2D', '1D')
    my_databases['CUR'] = databases_file.replace('2D', '1D')
    my_databases['WIN'] = databases_file.replace('2D', '1D')
    my_databases['LEV'] = databases_file.replace('2D', '1D')
    my_databases['BATHY'] = bathy_file
    my_databases['SEABED'] = seabed_file
    my_databases['ROUGHNESS'] = roughness_file
    my_databases['SPECIES'] = species_file
    
    DTOP = Project.MyProject(project_name, level, my_databases, FarmFile, CorridorFile, PATH_STORAGE)
    my_polygon = DTOP.__load_shapefiles__()
    DTOP.__launch_extractions__('dtop')
    DTOP.__extract_2D_values__(my_polygon)
    DTOP.__launch_statistics__()
    DTOP.__plot_results__('WestGroix')
    DTOP.__store_project__()
    assert DTOP
    