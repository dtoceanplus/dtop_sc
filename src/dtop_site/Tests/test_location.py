# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 11:25:05 2020

@author: ykervell
"""
from dtop_site.BusinessLogic.Location import MyPoint, MyPoints
from dtop_site.BusinessLogic import Data
import os
from pathlib import Path
import pytest
from dtop_site.BusinessLogic import Read

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_DATABASES_WAVES = os.path.join(PATH_DATABASES, 'Waves')
PATH_DATABASES_ALL = os.path.join(PATH_DATABASES, 'All')
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))

def test_location():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    assert point

def test_location_get_value():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    value = point.get_value(data_file = driver_file, data_var = 'elevation', lon_var = 'longitude', lat_var = 'latitude')
    assert isinstance(value, float)

def test_bad_name1():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    with pytest.raises(Exception) as rai:
        point.get_value(data_file = driver_file, data_var = 'turlututu', lon_var = 'longitude', lat_var = 'latitude')
    assert str(rai.value) == 'NotVariableStandardName'

def test_bad_name2():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    with pytest.raises(Exception) as rai:
        point.get_value(data_file = driver_file, data_var = 'elevation', lon_var = 'turlututu', lat_var = 'latitude')
    assert str(rai.value) == 'NotLongitudeStandardName'

def test_bad_name3():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    with pytest.raises(Exception) as rai:
        point.get_value(data_file = driver_file, data_var = 'elevation', lon_var = 'longitude', lat_var = 'turlututu')
    assert str(rai.value) == 'NotLatitudeStandardName'

def test_location_slope():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    slope = point.get_local_slope(data_file = driver_file, data_var = 'elevation', lon_var = 'longitude', lat_var = 'latitude')
    assert isinstance(slope, float)

def test_bad_location_slope1():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    with pytest.raises(Exception) as rai:
        point.get_local_slope(data_var = 'elevation', lon_var = 'longitude', lat_var = 'latitude')
    assert str(rai.value) == 'NoDataFile'

def test_bad_location_slope2():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    with pytest.raises(Exception) as rai:
        point.get_local_slope(data_file = driver_file, data_var = 'turlututu', lon_var = 'longitude', lat_var = 'latitude')
    assert str(rai.value) == 'NotVariableStandardName'

def test_bad_location_slope3():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    with pytest.raises(Exception) as rai:
        point.get_local_slope(data_file = driver_file, data_var = 'elevation', lon_var = 'turlututu', lat_var = 'latitude')
    assert str(rai.value) == 'NotLongitudeStandardName'

def test_bad_location_slope4():
    point_name = "test1"
    lon = -4.5
    lat = 48.1
    point = MyPoint(point_name, lon, lat)
    driver_file = '/Users/youen/GIT/dtop-site/src/dtop_site/Databases/Bathymetry/Europa-TEST2_GEBCO2019_downgrade20.nc'
    with pytest.raises(Exception) as rai:
        point.get_local_slope(data_file = driver_file, data_var = 'elevation', lon_var = 'longitude', lat_var = 'turlututu')
    assert str(rai.value) == 'NotLatitudeStandardName'
    
def test_location_get_several_time_series_1D():
    point_name = "test1"
    lon = -5.05
    lat = 48.45
    point = MyPoint(point_name, lon, lat)
    name = "Fromveur_all_1D"
    template = PATH_DATABASES_ALL+'/HOMERE/Fromveur_1point/Fromveur_${yyyy}_1point.nc'
    start = 2000
    end = 2002
    var = ['hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd']
    TV, TST = point.get_time_series_several_variables(data_name = name, data_template = template, year_start = start, year_end = end, data_vars_list = var)
    assert len(TST[:,0])==len(TV)

def test_location_get_several_time_series_2D():
    my_2D_database = 'Fromveur_all_2D'
    my_database = Data.MyData(my_2D_database)
    polygon_file = os.path.join(PATH_GEOMETRIES, 'Fromveur_lease_area.shp')
    polygon = Read.read_shapefile_and_return_polygon(polygon_file)
    points_names, lons, lats = my_database.intersection_with_polygon(polygon)
    points = MyPoints(points_names, lons, lats)
    name = "Fromveur_all_2D"
    template = PATH_DATABASES_ALL+'/HOMERE_2D/Fromveur_${yyyy}_100points.nc'
    start = 2014
    end = 2016
    var = ['hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd']
    TV, TST = points.get_time_series_several_variables(data_name = name, data_template = template, year_start = start, year_end = end, data_vars_list = var)
    assert len(TST[0,:])==len(TV)

def test_locations():
    points_names = []
    lon = []
    lat = []
    with pytest.raises(Exception) as rai:
        MyPoints(points_names, lon, lat)
    assert str(rai.value) == 'NoPoints'
