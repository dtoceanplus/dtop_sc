# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 11:16:41 2020

@author: ykervell
"""
import os
from dtop_site.BusinessLogic import Read
from pathlib import Path

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))


def test_read_shapefiles():
    point_name = "SaintNazaire"
    farm_file = point_name + '_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    corridor_file = point_name + '_corridor.shp'    #request.args['corridorfile']
    CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
    farm_points = Read.read_shapefile(FarmFile)
    corridor_points = Read.read_shapefile(CorridorFile)
    assert True

def test_read_and_compute_shapefiles():
    point_name = "Calais"
    farm_file = point_name + '_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    farm_area = Read.compute_shapefile_area(FarmFile)
    farm_grid_points = Read.read_shapefile_and_create_grid(FarmFile, 5, 5)
    assert True

def test_read_shapefile_and_return_polygon():
    point_name = "Arcachon"
    farm_file = point_name + '_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    polygon = Read.read_shapefile_and_return_polygon(FarmFile)
    assert True
