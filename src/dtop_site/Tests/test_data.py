# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 11:44:10 2020

@author: ykervell
"""

from dtop_site.BusinessLogic import Data
import pytest
import os
from pathlib import Path
from dtop_site.BusinessLogic import Read

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_DATABASES_WAVES = os.path.join(PATH_DATABASES, 'Waves')
PATH_DATABASES_ALL = os.path.join(PATH_DATABASES, 'All')
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))

def test_false_data():
    database = 'turlututu'
    with pytest.raises(Exception) as rai:
        Data.MyData(database)
    assert str(rai.value) == 'NotInCatalog'

def test_data():
    database = 'GEBCO_Europa_downgrade20'
    data = Data.MyData(database)
    assert data

def test_user_data():
    database = 'toto'
    with pytest.raises(Exception) as rai:
        Data.MyData(database, user_data_catalog='my_data.txt')
    assert str(rai.value) == 'NotExistingCatalog'

def test_data2():
    database = 'WestGroix_all_1D'
    data = Data.MyData(database)
    assert data

def test_data_extract_single_value():
    database = 'GEBCO_Europa_downgrade20'
    data = Data.MyData(database)
    point_name = "test1"
    lon = -5.05
    lat = 48.45
    value = data.extract_single_value(point_name, lon, lat)
    assert isinstance(value, float)

def test_data_compute_my_slope():
    database = 'GEBCO_Europa_downgrade20'
    data = Data.MyData(database)
    point_name = "test1"
    lon = -5.05
    lat = 48.45
    value = data.compute_my_slope(point_name, lon, lat)
    assert isinstance(value, float)
    
def test_data_extract_several_time_series():
    database = 'Hossegor_all_1D'
    data = Data.MyData(database)
    point_name = "test1"
    lon = -1.58
    lat = 43.69
    variables = ['hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd']
    start = 2000
    end = 2001
    TV, TS = data.extract_several_time_series(variables, point_name, lon, lat, mylevel=1, year_start = start, year_end = end)
    assert len(TS)==len(TV)

def test_data_extract_several_2D_variables():
    my_2D_database = 'Hossegor_all_2D'
    my_database = Data.MyData(my_2D_database)
    polygon_file = os.path.join(PATH_GEOMETRIES, 'Hossegor_lease_area.shp')
    polygon = Read.read_shapefile_and_return_polygon(polygon_file)
    variables = ['hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd']
    TV, TS = my_database.extract_2D_variables(polygon, variables, mylevel=2)
    assert len(TS[0,:])==len(TV)
