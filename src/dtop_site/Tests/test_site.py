# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 11:46:17 2020

@author: ykervell
"""

from dtop_site.BusinessLogic import Site
from pathlib import Path
import os
from dtop_site.BusinessLogic import Read

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage/'))


def test_Site_init():
    my_databases = {}  
    my_databases['WAV'] = 'Fromveur_all_1D'
    my_databases['CUR'] = 'Fromveur_all_1D'
    my_databases['WIN'] = 'Fromveur_all_1D'
    my_databases['LEV'] = 'Fromveur_all_1D'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    site = Site.MySite(my_databases, storage, mylevel=1)
    assert site

def test_Site_direct_extractions():
    my_databases = {}  
    my_databases['WAV'] = 'Fromveur_all_1D'
    my_databases['CUR'] = 'Fromveur_all_1D'
    my_databases['WIN'] = 'Fromveur_all_1D'
    my_databases['LEV'] = 'Fromveur_all_1D'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    site = Site.MySite(my_databases, storage, mylevel=1)
    farm_file = 'Fromveur_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    farm_grid_points = Read.read_shapefile_and_create_grid(FarmFile, 5, 5)
    F_bathy_extracted_values = site.__extract_single_values_at_points__(farm_grid_points, "bathy")
    assert F_bathy_extracted_values

def test_Site_TS_extractions():
    my_databases = {}  
    my_databases['WAV'] = 'WestGroix_all_1D'
    my_databases['CUR'] = 'WestGroix_all_1D'
    my_databases['WIN'] = 'WestGroix_all_1D'
    my_databases['LEV'] = 'WestGroix_all_1D'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    my_databases['WAV2D'] = 'WestGroix_all_2D'
    my_databases['CUR2D'] = 'WestGroix_all_2D'
    storage = PATH_STORAGE
    site = Site.MySite(my_databases, storage, mylevel=2)
    farm_file = 'WestGroix_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    farm_points = Read.read_shapefile(FarmFile)
    center_farm_point = {}
    center_farm_point['names'] = ('center_farm',)
    center_farm_point['lons'] = (farm_points["lons"][0],)
    center_farm_point['lats'] = (farm_points["lats"][0],)
    TS, TV = site.__extract_several_time_series_at_points__(site.dat_wave, center_farm_point, ['hs', 't0m1', 'spr', 'fp', 'dp', 'cge', 'wlv', 'ucur', 'vcur', 'uwnd', 'vwnd'])
    assert len(TS)==len(TV)

def test_Site_slope():
    my_databases = {}  
    my_databases['WAV'] = 'Fromveur_all_1D'
    my_databases['CUR'] = 'Fromveur_all_1D'
    my_databases['WIN'] = 'Fromveur_all_1D'
    my_databases['LEV'] = 'Fromveur_all_1D'
    my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
    my_databases['SEABED'] = 'Seabed_Types'
    my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
    my_databases['SPECIES'] = 'Species_default'
    storage = PATH_STORAGE
    site = Site.MySite(my_databases, storage, mylevel=1)
    farm_file = 'fromveur_lease_area.shp'  #request.args['farmfile']
    FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
    farm_points = Read.read_shapefile(FarmFile)
    center_farm_point = {}
    center_farm_point['names'] = ('center_farm',)
    center_farm_point['lons'] = (farm_points["lons"][0],)
    center_farm_point['lats'] = (farm_points["lats"][0],)
    slope = site.__compute_slope__(center_farm_point)
    assert isinstance(slope[0], float)