# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_site.BusinessLogic.libraries.dtosc.outputs.Project import Project
#------------------------------------
# @ USER DEFINED IMPORTS START
working= os.getcwd()
PATH_STORAGE = os.path.join(working, "storage")
PATH_GUI = os.path.join(working, "src/dtop_site/GUI/src/assets/Sites_images")
from pathlib import Path
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage'))
PATH_STORAGE_TEMP = './storage/temp'
PATH_USER = os.path.join(working, Path('USER'))
PATH_USER_GEOMETRIES = './USER/Geometries'
PATH_USER_DIRECTVALUES= './USER/DirectValues'
PATH_USER_TIMESERIES = './USER/TimeSeries'

from dtop_site.BusinessLogic.libraries.dtosc.input.Inputs import Inputs
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWaves import EXTWaves
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWaves import BasicWaves
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTCurrents import EXTCurrents
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicCurrents import BasicCurrents
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWinds import EXTWinds
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWinds import BasicWinds
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWaterLevels import EXTWaterLevels
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWaterLevels import BasicWaterLevels
from shutil import copyfile
import base64

import geojson, subprocess
import netCDF4 as nc

from dtop_site.BusinessLogic.libraries.dtosc.dr.digital_representation import digital_representation

import json

# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('representation', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

#####################################################
# Functions for project digital representation load #
#####################################################

# Load project file
def load_Project_dr(ProjectId):
    """Function to load an entity digital representation.

    If ProjectId provided does not match to any existing entity in the database, returns an error message. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of the outputs or an error message.

    """

    if (os.path.exists('./storage/' + ProjectId + '_dr.json')):
        project_name = ProjectId
    else:
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if int(ProjectId) == entity['EntityId']:
                    Entity_name = entity['EntityName']
                
        project_name=Entity_name + '_' + ProjectId
    filename=os.path.join('./storage',project_name+'_dr.json')
    if os.path.exists(filename):
        project=digital_representation()
        project.loadJSON(name=project_name, filePath = filename)
    else:
        project = 'This entity does not exist or the Digital representation was not requested'
    return project

###########
# GET API #
###########

@bp.route('/representation/<ProjectId>', methods=['GET'])
def get_project_dr(ProjectId):
    """Flask blueprint route for getting the digital representation of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns an error message. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of the digital representation or an error message.

    """
    project=load_Project_dr(ProjectId)
    try:
        res=project.prop_rep()
    except:
        res='This entity does not exist of the Digital representation was not requested'
    return jsonify(res)