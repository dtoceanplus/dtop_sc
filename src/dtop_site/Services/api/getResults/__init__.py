# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_site.BusinessLogic.libraries.dtosc.outputs.Project import Project
#------------------------------------
# @ USER DEFINED IMPORTS START
working= os.getcwd()
PATH_STORAGE = os.path.join(working, "storage")
PATH_GUI = os.path.join(working, "src/dtop_site/GUI/src/assets/Sites_images")
from pathlib import Path
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage'))
PATH_STORAGE_TEMP = './storage/temp'
PATH_USER = os.path.join(working, Path('USER'))
PATH_USER_GEOMETRIES = './USER/Geometries'
PATH_USER_DIRECTVALUES= './USER/DirectValues'
PATH_USER_TIMESERIES = './USER/TimeSeries'

from dtop_site.BusinessLogic.libraries.dtosc.input.Inputs import Inputs
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWaves import EXTWaves
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWaves import BasicWaves
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTCurrents import EXTCurrents
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicCurrents import BasicCurrents
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWinds import EXTWinds
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWinds import BasicWinds
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWaterLevels import EXTWaterLevels
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWaterLevels import BasicWaterLevels
from shutil import copyfile
import base64

import geojson, subprocess
import netCDF4 as nc

from dtop_site.BusinessLogic.libraries.dtosc.dr.digital_representation import digital_representation

import json

# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('getResults', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

##############################
# Functions for project load #
##############################

# List all available projects files
@bp.route('/Projects', methods=['GET'])
def get_all_available_projects():
    """Flask blueprint route for getting the list of available entities.
    
    Returns:
        (array): Array of all entities names or a HTTP 404 error.

    """
    [folder,folder_name,files]=list_files('./storage','json')
    return jsonify(files)

# Load project file
def load_Project(ProjectId):
    """Function to load an entity outputs.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of the outputs or a HTTP 404 error.

    """

    if (os.path.exists('./storage/' + ProjectId + '_outputs.h5')):
        project_name = ProjectId
    else:
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if int(ProjectId) == entity['EntityId']:
                    Entity_name = entity['EntityName']
                
        project_name=Entity_name + '_' + ProjectId
    filename=os.path.join('./storage',project_name+'_outputs.h5')
    if os.path.exists(filename):
        project=Project()
        project.loadHDF5(name=project_name, filePath = filename)
    else:
        abort(404)
    return project

# Find object index in array from its name
def find_index_from_name(name,object_array):
    """Function to find the index of name inside object_array.

    If no index is found, returns a 404 error. 

    Args:
        name (str): Name of the object to find.
        object_array (array): Object array in which the function has to search
    
    Returns:
        (int): Index of name of a HTTP 404 error.

    """
    index=-1
    for idx in range(0,len(object_array)):
        if name==object_array[idx].name:
            index=idx
    if index==-1:
        abort(404)
    return index

###########
# GET API #
###########

@bp.route('/<ProjectId>/status', methods=['GET'])
def get_Project_status(ProjectId):
    """Flask blueprint route for getting the status of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (int): Status of the entity or a HTTP 404 error.

    """
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if int(ProjectId) == entity['EntityId']:
                Entity_name = entity['EntityName']
            
    project_name=Entity_name + '_' + ProjectId
    filename=os.path.join('./storage',project_name+'_inputs.json')
    inputs=Inputs()
    inputs.loadJSON(name=project_name, filePath = filename)
    return jsonify(inputs.entity_status)

@bp.route('/<ProjectId>', methods=['GET'])
def get_Project(ProjectId):
    """Flask blueprint route for getting outputs of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm', methods=['GET'])
def get_project_farm(ProjectId):
    """Flask blueprint route for getting outputs.farm of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values', methods=['GET'])
def get_project_farm_direct_values(ProjectId):
    """Flask blueprint route for getting outputs.farm.direct_values of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.direct_values or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/bathymetry', methods=['GET'])
def get_project_farm_direct_values_bathymetry(ProjectId):
    """Flask blueprint route for getting outputs.farm.direct_values.bathymetry of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.direct_values.bathymetry or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.bathymetry.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/seabed_type', methods=['GET'])
def get_project_farm_direct_values_seabed_type(ProjectId):
    """Flask blueprint route for getting outputs.farm.direct_values.seabed_type of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.direct_values.seabed_type or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.seabed_type.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/roughness_length', methods=['GET'])
def get_project_farm_direct_values_roughness_length(ProjectId):
    """Flask blueprint route for getting outputs.farm.direct_values.roughness_length of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.direct_values.roughness_length or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.roughness_length.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/marine_species', methods=['GET'])
def get_project_farm_direct_values_marine_species(ProjectId):
    """Flask blueprint route for getting outputs.farm.direct_values.marine_species of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.direct_values.marine_species or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.marine_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/info', methods=['GET'])
def get_project_farm_info(ProjectId):
    """Flask blueprint route for getting outputs.farm.info of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.info or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.info.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices', methods=['GET'])
def get_project_farm_scenarii_matrices(ProjectId):
    """Flask blueprint route for getting outputs.farm.scenarii_matrices of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.scenarii_matrices or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices/currents', methods=['GET'])
def get_project_farm_scenarii_matrices_currents(ProjectId):
    """Flask blueprint route for getting outputs.farm.scenarii_matrices.currents of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.scenarii_matrices.currents or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.currents.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices/currentsmonthly', methods=['GET'])
def get_project_farm_scenarii_matrices_currentsmonthly(ProjectId):
    """Flask blueprint route for getting outputs.farm.scenarii_matrices.currentsmonthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.scenarii_matrices.currentsmonthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.currentsmonthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices/waves', methods=['GET'])
def get_project_farm_scenarii_matrices_waves(ProjectId):
    """Flask blueprint route for getting outputs.farm.scenarii_matrices.waves of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.farm.scenarii_matrices.waves or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.waves.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor', methods=['GET'])
def get_project_corridor(ProjectId):
    """Flask blueprint route for getting outputs.corridor of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values', methods=['GET'])
def get_project_corridor_direct_values(ProjectId):
    """Flask blueprint route for getting outputs.corridor.direct_values of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor.direct_values or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/bathymetry', methods=['GET'])
def get_project_corridor_direct_values_bathymetry(ProjectId):
    """Flask blueprint route for getting outputs.corridor.direct_values.bathymetry of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor.direct_values.bathymetry or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.bathymetry.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/seabed_type', methods=['GET'])
def get_project_corridor_direct_values_seabed_type(ProjectId):
    """Flask blueprint route for getting outputs.corridor.direct_values.seabed_type of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor.direct_values.seabed_type or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.seabed_type.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/roughness_length', methods=['GET'])
def get_project_corridor_direct_values_roughness_length(ProjectId):
    """Flask blueprint route for getting outputs.corridor.direct_values.roughness_length of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor.direct_values.roughness_length or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.roughness_length.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/marine_species', methods=['GET'])
def get_project_corridor_direct_values_marine_species(ProjectId):
    """Flask blueprint route for getting outputs.corridor.direct_values.marine_species of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor.direct_values.marine_species or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.marine_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/info', methods=['GET'])
def get_project_corridor_info(ProjectId):
    """Flask blueprint route for getting outputs.corridor.info of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.corridor.info or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.corridor.info.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/databases_paths', methods=['GET'])
def get_project_databases_paths(ProjectId):
    """Flask blueprint route for getting outputs.databases_paths of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.databases_paths or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.databases_paths.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point', methods=['GET'])
def get_project_point(ProjectId):
    """Flask blueprint route for getting outputs.point of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/direct_values', methods=['GET'])
def get_project_point_direct_values(ProjectId):
    """Flask blueprint route for getting outputs.point.direct_values of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.direct_values or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.direct_values.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/direct_values/bathymetry', methods=['GET'])
def get_project_point_direct_values_bathymetry(ProjectId):
    """Flask blueprint route for getting outputs.point.direct_values.bathymetry of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.direct_values.bathymetry or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.direct_values.bathymetry.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/direct_values/slope', methods=['GET'])
def get_project_point_direct_values_slope(ProjectId):
    """Flask blueprint route for getting outputs.point.direct_values.slope of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.direct_values.slope or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.direct_values.slope.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/direct_values/seabed_type', methods=['GET'])
def get_project_point_direct_values_seabed_type(ProjectId):
    """Flask blueprint route for getting outputs.point.direct_values.seabed_type of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.direct_values.seabed_type or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.direct_values.seabed_type.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/direct_values/roughness_length', methods=['GET'])
def get_project_point_direct_values_roughness_length(ProjectId):
    """Flask blueprint route for getting outputs.point.direct_values.roughness_length of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.direct_values.roughness_length or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.direct_values.roughness_length.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/direct_values/marine_species', methods=['GET'])
def get_project_point_direct_values_marine_species(ProjectId):
    """Flask blueprint route for getting outputs.point.direct_values.marine_species of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.direct_values.marine_species or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.direct_values.marine_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series', methods=['GET'])
def get_project_point_time_series(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves', methods=['GET'])
def get_project_point_time_series_waves(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/hs', methods=['GET'])
def get_project_point_time_series_waves_hs(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.hs of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.hs or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/tp', methods=['GET'])
def get_project_point_time_series_waves_tp(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.tp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.tp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/dp', methods=['GET'])
def get_project_point_time_series_waves_dp(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.dp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.dp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/te', methods=['GET'])
def get_project_point_time_series_waves_te(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.te of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.te or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.te.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/gamma', methods=['GET'])
def get_project_point_time_series_waves_gamma(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.gamma of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.gamma or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.gamma.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/spr', methods=['GET'])
def get_project_point_time_series_waves_spr(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.spr of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.spr or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.spr.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/waves/CgE', methods=['GET'])
def get_project_point_time_series_waves_CgE(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.waves.CgE of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.waves.CgE or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.waves.CgE.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/currents', methods=['GET'])
def get_project_point_time_series_currents(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.currents of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.currents or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.currents.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/currents/mag', methods=['GET'])
def get_project_point_time_series_currents_mag(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.currents.mag of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.currents.mag or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.currents.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/currents/theta', methods=['GET'])
def get_project_point_time_series_currents_theta(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.currents.theta of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.currents.theta or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.currents.theta.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/currents/U', methods=['GET'])
def get_project_point_time_series_currents_U(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.currents.U of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.currents.U or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.currents.U.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/currents/V', methods=['GET'])
def get_project_point_time_series_currents_V(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.currents.V of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.currents.V or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.currents.V.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/currents/Flux', methods=['GET'])
def get_project_point_time_series_currents_Flux(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.currents.Flux of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.currents.Flux or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.currents.Flux.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/winds', methods=['GET'])
def get_project_point_time_series_winds(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.winds of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.winds or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.winds.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/winds/mag10', methods=['GET'])
def get_project_point_time_series_winds_mag10(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.winds.mag10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.winds.mag10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.winds.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/winds/theta10', methods=['GET'])
def get_project_point_time_series_winds_theta10(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.winds.theta10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.winds.theta10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.winds.theta10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/winds/U10', methods=['GET'])
def get_project_point_time_series_winds_U10(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.winds.U10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.winds.U10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.winds.U10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/winds/V10', methods=['GET'])
def get_project_point_time_series_winds_V10(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.winds.V10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.winds.V10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.winds.V10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/winds/gust10', methods=['GET'])
def get_project_point_time_series_winds_gust10(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.winds.gust10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.winds.gust10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.winds.gust10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/water_levels', methods=['GET'])
def get_project_point_time_series_water_levels(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.water_levels of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.water_levels or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.water_levels.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/water_levels/XE', methods=['GET'])
def get_project_point_time_series_water_levels_XE(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.water_levels.XE of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.water_levels.XE or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.water_levels.XE.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/time_series/water_levels/WLEV', methods=['GET'])
def get_project_point_time_series_water_levels_WLEV(ProjectId):
    """Flask blueprint route for getting outputs.point.time_series.water_levels.WLEV of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.time_series.water_levels.WLEV or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.time_series.water_levels.WLEV.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics', methods=['GET'])
def get_project_point_statistics(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves', methods=['GET'])
def get_project_point_statistics_waves(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/Basic', methods=['GET'])
def get_project_point_statistics_waves_Basic(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.Basic of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.Basic or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/Basic/hs', methods=['GET'])
def get_project_point_statistics_waves_Basic_hs(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.Basic.hs of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.Basic.hs or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.Basic.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/Basic/tp', methods=['GET'])
def get_project_point_statistics_waves_Basic_tp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.Basic.tp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.Basic.tp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.Basic.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/Basic/CgE', methods=['GET'])
def get_project_point_statistics_waves_Basic_CgE(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.Basic.CgE of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.Basic.CgE or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.Basic.CgE.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/Basic/gamma', methods=['GET'])
def get_project_point_statistics_waves_Basic_gamma(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.Basic.gamma of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.Basic.gamma or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.Basic.gamma.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/Basic/spr', methods=['GET'])
def get_project_point_statistics_waves_Basic_spr(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.Basic.spr of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.Basic.spr or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.Basic.spr.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD', methods=['GET'])
def get_project_point_statistics_waves_EPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD/hs', methods=['GET'])
def get_project_point_statistics_waves_EPD_hs(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD.hs of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD.hs or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD/hs_monthly', methods=['GET'])
def get_project_point_statistics_waves_EPD_hs_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD.hs_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD.hs_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.hs_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD/tp', methods=['GET'])
def get_project_point_statistics_waves_EPD_tp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD.tp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD.tp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD/tp_monthly', methods=['GET'])
def get_project_point_statistics_waves_EPD_tp_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD.tp_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD.tp_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.tp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD/dp', methods=['GET'])
def get_project_point_statistics_waves_EPD_dp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD.dp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD.dp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EPD/dp_monthly', methods=['GET'])
def get_project_point_statistics_waves_EPD_dp_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EPD.dp_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EPD.dp_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EPD.dp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD', methods=['GET'])
def get_project_point_statistics_waves_EJPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD/hs_tp', methods=['GET'])
def get_project_point_statistics_waves_EJPD_hs_tp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD.hs_tp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD.hs_tp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD.hs_tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD/hs_tp_monthly', methods=['GET'])
def get_project_point_statistics_waves_EJPD_hs_tp_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD.hs_tp_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD.hs_tp_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD.hs_tp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD/hs_dp', methods=['GET'])
def get_project_point_statistics_waves_EJPD_hs_dp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD.hs_dp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD.hs_dp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD.hs_dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD/hs_dp_monthly', methods=['GET'])
def get_project_point_statistics_waves_EJPD_hs_dp_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD.hs_dp_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD.hs_dp_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD.hs_dp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/ENVS', methods=['GET'])
def get_project_point_statistics_waves_ENVS(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.ENVS of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.ENVS or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.ENVS.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD3v', methods=['GET'])
def get_project_point_statistics_waves_EJPD3v(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD3v of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD3v or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD3v.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD3v/hs_tp_dp', methods=['GET'])
def get_project_point_statistics_waves_EJPD3v_hs_tp_dp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD3v.hs_tp_dp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD3v.hs_tp_dp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD3v.hs_tp_dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD3v/hs_tp_dp_monthly', methods=['GET'])
def get_project_point_statistics_waves_EJPD3v_hs_tp_dp_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD3v.hs_tp_dp_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD3v.hs_tp_dp_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD3v.hs_tp_dp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD3v/hs_tp_dp_flattened', methods=['GET'])
def get_project_point_statistics_waves_EJPD3v_hs_tp_dp_flattened(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD3v.hs_tp_dp_flattened of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD3v.hs_tp_dp_flattened or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD3v.hs_tp_dp_flattened.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened', methods=['GET'])
def get_project_point_statistics_waves_EJPD3v_hs_tp_dp_monthly_flattened(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EJPD3v.hs_tp_dp_monthly_flattened of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EJPD3v.hs_tp_dp_monthly_flattened or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EJPD3v.hs_tp_dp_monthly_flattened.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EXT', methods=['GET'])
def get_project_point_statistics_waves_EXT(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EXT of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EXT or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EXT/hs', methods=['GET'])
def get_project_point_statistics_waves_EXT_hs(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EXT.hs of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EXT.hs or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EXT.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EXT/tp', methods=['GET'])
def get_project_point_statistics_waves_EXT_tp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EXT.tp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EXT.tp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EXT.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EXT/gamma', methods=['GET'])
def get_project_point_statistics_waves_EXT_gamma(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EXT.gamma of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EXT.gamma or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EXT.gamma.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EXC', methods=['GET'])
def get_project_point_statistics_waves_EXC(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EXC of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EXC or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EXC.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/waves/EXC/hs_tp', methods=['GET'])
def get_project_point_statistics_waves_EXC_hs_tp(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.waves.EXC.hs_tp of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.waves.EXC.hs_tp or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.waves.EXC.hs_tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents', methods=['GET'])
def get_project_point_statistics_currents(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/Basic', methods=['GET'])
def get_project_point_statistics_currents_Basic(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.Basic of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.Basic or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/Basic/mag', methods=['GET'])
def get_project_point_statistics_currents_Basic_mag(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.Basic.mag of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.Basic.mag or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.Basic.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/Basic/Flux', methods=['GET'])
def get_project_point_statistics_currents_Basic_Flux(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.Basic.Flux of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.Basic.Flux or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.Basic.Flux.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EPD', methods=['GET'])
def get_project_point_statistics_currents_EPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EPD/mag', methods=['GET'])
def get_project_point_statistics_currents_EPD_mag(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EPD.mag of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EPD.mag or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EPD.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EPD/mag_monthly', methods=['GET'])
def get_project_point_statistics_currents_EPD_mag_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EPD.mag_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EPD.mag_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EPD.mag_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EPD/theta', methods=['GET'])
def get_project_point_statistics_currents_EPD_theta(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EPD.theta of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EPD.theta or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EPD.theta.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EPD/theta_monthly', methods=['GET'])
def get_project_point_statistics_currents_EPD_theta_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EPD.theta_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EPD.theta_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EPD.theta_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EJPD', methods=['GET'])
def get_project_point_statistics_currents_EJPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EJPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EJPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EJPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EJPD/mag_theta', methods=['GET'])
def get_project_point_statistics_currents_EJPD_mag_theta(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EJPD.mag_theta of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EJPD.mag_theta or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EJPD.mag_theta.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EJPD/mag_theta_monthly', methods=['GET'])
def get_project_point_statistics_currents_EJPD_mag_theta_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EJPD.mag_theta_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EJPD.mag_theta_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EJPD.mag_theta_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EXT', methods=['GET'])
def get_project_point_statistics_currents_EXT(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EXT of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EXT or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/currents/EXT/mag', methods=['GET'])
def get_project_point_statistics_currents_EXT_mag(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.currents.EXT.mag of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.currents.EXT.mag or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.currents.EXT.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds', methods=['GET'])
def get_project_point_statistics_winds(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/Basic', methods=['GET'])
def get_project_point_statistics_winds_Basic(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/Basic/mag10', methods=['GET'])
def get_project_point_statistics_winds_Basic_mag10(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic.mag10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic.mag10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.Basic.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EPD', methods=['GET'])
def get_project_point_statistics_winds_EPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic.EPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic.EPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EPD/mag10', methods=['GET'])
def get_project_point_statistics_winds_EPD_mag10(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic.EPD.mag10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic.EPD.mag10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EPD.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EPD/mag10_monthly', methods=['GET'])
def get_project_point_statistics_winds_EPD_mag10_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic.EPD.mag10_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic.EPD.mag10_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EPD.mag10_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EPD/theta10', methods=['GET'])
def get_project_point_statistics_winds_EPD_theta10(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic.EPD.theta10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic.EPD.theta10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EPD.theta10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EPD/theta10_monthly', methods=['GET'])
def get_project_point_statistics_winds_EPD_theta10_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.Basic.EPD.theta10_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.Basic.EPD.theta10_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EPD.theta10_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EJPD', methods=['GET'])
def get_project_point_statistics_winds_EJPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.EJPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.EJPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EJPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EJPD/mag10_theta10', methods=['GET'])
def get_project_point_statistics_winds_EJPD_mag10_theta10(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.EJPD.mag10_theta10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.EJPD.mag10_theta10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EJPD.mag10_theta10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EJPD/mag10_theta10_monthly', methods=['GET'])
def get_project_point_statistics_winds_EJPD_mag10_theta10_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.EJPD.mag10_theta10_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.EJPD.mag10_theta10_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EJPD.mag10_theta10_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EXT', methods=['GET'])
def get_project_point_statistics_winds_EXT(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.EXT of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.EXT or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EXT/mag10', methods=['GET'])
def get_project_point_statistics_winds_EXT_mag10(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.EXT.mag10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.EXT.mag10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EXT.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/winds/EXT/gust10', methods=['GET'])
def get_project_point_statistics_winds_EXT_gust10(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.winds.EXT.gust10 of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.winds.EXT.gust10 or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.winds.EXT.gust10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels', methods=['GET'])
def get_project_point_statistics_water_levels(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/Basic', methods=['GET'])
def get_project_point_statistics_water_levels_Basic(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.Basic of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.Basic or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/Basic/WLEV', methods=['GET'])
def get_project_point_statistics_water_levels_Basic_WLEV(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.Basic.WLEV of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.Basic.WLEV or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.Basic.WLEV.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/EPD', methods=['GET'])
def get_project_point_statistics_water_levels_EPD(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.EPD of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.EPD or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/EPD/WLEV', methods=['GET'])
def get_project_point_statistics_water_levels_EPD_WLEV(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.EPD.WLEV of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.EPD.WLEV or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.EPD.WLEV.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/EPD/WLEV_monthly', methods=['GET'])
def get_project_point_statistics_water_levels_EPD_WLEV_monthly(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.EPD.WLEV_monthly of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.EPD.WLEV_monthly or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.EPD.WLEV_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/EXT', methods=['GET'])
def get_project_point_statistics_water_levels_EXT(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.EXT of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.EXT or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/EXT/WLEVnegative', methods=['GET'])
def get_project_point_statistics_water_levels_EXT_WLEVnegative(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.EXT.WLEVnegative of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.EXT.WLEVnegative or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.EXT.WLEVnegative.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/statistics/water_levels/EXT/WLEVpositive', methods=['GET'])
def get_project_point_statistics_water_levels_EXT_WLEVpositive(ProjectId):
    """Flask blueprint route for getting outputs.point.statistics.water_levels.EXT.WLEVpositive of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.statistics.water_levels.EXT.WLEVpositive or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.statistics.water_levels.EXT.WLEVpositive.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/point/info', methods=['GET'])
def get_project_point_info(ProjectId):
    """Flask blueprint route for getting outputs.point.info of the entity with ID = ProjectId.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of outputs.point.info or a HTTP 404 error.

    """
    project=load_Project(ProjectId)
    try:
        res=project.point.info.prop_rep()
    except:
        abort(404)
    return jsonify(res)

#####################
# Utility functions #
#####################

def list_files(directory, extension):
    """Function to list all files contained in 'directory' with the extension 'entension'.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        direction (str): Directory in which the function has to search.
        extension (str): Entension of the files to list.
    
    Returns:
        (array, array, array): folder, folder_name, files.

    """
    fl=listdir(directory)
    folder=[]
    folder_name=[]
    files=[]
    for i in range(0,len(fl)):
        path=os.path.join(directory, fl[i])
        path=os.path.abspath(path)
        if os.path.isdir(path):
            folder.append(path)
            folder_name.append(fl[i])
        elif path.endswith('.'+extension):
            files.append(path)
    return folder,folder_name,files
