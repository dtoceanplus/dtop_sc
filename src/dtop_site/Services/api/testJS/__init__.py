# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, render_template, url_for, jsonify, request

bp = Blueprint('test_js', __name__)

import os
from pathlib import Path, PurePosixPath

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage'))
PATH_USER = os.path.join(working, Path('USER'))
PATH_USER_GEOMETRIES = os.path.join(PATH_USER, Path('Geometries'))
PATH_USER_DIRECTVALUES= os.path.join(PATH_USER, Path('DirectValues'))
PATH_USER_TIMESERIES = os.path.join(PATH_USER, Path('TimeSeries'))


"""
  @oas [get] /api
  description: Returns api documentation
"""
@bp.route('/')
def doc():
  spec_url=url_for('static', filename='openapi.yaml')
  return render_template('api/index.html', spec_url=spec_url)

# @bp.route('/test_ben', methods=['GET'])
# def test_ben():
#     print("################# I am in test_ben ###############")
#     return "Test 1 OK"

# @bp.route('/test_youen_simple', methods=['GET'])
# def test_youen_simple():
#     print("################# I am in test_youen_simple ###############")
#     MyDict = {}
#     MyDict['def'] = "Test 2 OK"
#     return jsonify(MyDict)

# @bp.route('/test_youen_less_simpler', methods=['POST'], strict_slashes=False)
# def test_youen_less_simpler():
#     print("################# I am in test_youen_less_simpler ###############")
#     name = request.args.get('name')
#     description = request.args.get('description')
#     MyDict = {}
#     MyDict['name'] = name
#     MyDict['description'] = description
#     print("######################## MyDict:", MyDict)
#     return MyDict


# @bp.route('/test_youen', methods=['POST'], strict_slashes=False)
# def test_youen():
#     print("################# I am in test_youen ###############")
#     data = request.args
#     return jsonify(data)

# @bp.route('/test_api_full', methods=['POST'], strict_slashes=False)
# def test_api_full():
#     """
#       @oas [post] /api/test1
#       description: Returns (wave, current, ..) direct values, timeseries
#       and statistics on farm, corridor and device locations
#     """
#     from dtop_site.BusinessLogic import Project
#     from dtop_site.BusinessLogic.catalogs import early_stage_energy_databases as esed
#     import os
#     from datetime import datetime

#     ## Timer
#     t0 = datetime.now()
    
#     #### info from GUI
#     # NAME
#     project_name = request.args['name']
#     level = request.args['level']
    
#     # LEVELS OF ENERGY
#     if int(level) == 3:
#         project_extension = "_CPX" + level
#         project_name += project_extension
#         # GEOMETRIES
#         farm_file = request.args['lease']
#         FarmFile = os.path.join(PATH_USER_GEOMETRIES, farm_file) 
#         corridor_file = request.args['corr']
#         CorridorFile = os.path.join(PATH_USER_GEOMETRIES, corridor_file)

#         # DATABASES
#         TS = request.args['ts']
#         databases_file = os.path.join(PATH_USER_TIMESERIES, TS)
#         BA = request.args['bathy']
#         bathy_file = os.path.join(PATH_USER_DIRECTVALUES, BA)
#         SE = request.args['seabed']
#         seabed_file = os.path.join(PATH_USER_DIRECTVALUES, SE)
#         RO = request.args['roughness']
#         roughness_file = os.path.join(PATH_USER_DIRECTVALUES, RO)
#         SP = request.args['species']
#         species_file = os.path.join(PATH_USER_DIRECTVALUES, SP)
                
#         # BATHYMETRY
#         bathy_value = request.args['bathy']
#         try:
#             bathymetry_value = float(bathy_value)
#         except:
#             bathymetry_value = 'dtop'
    
#         # dictionary of databases
#         my_databases = {}  
#         if "2D" in databases_file:
#             project_name += "_2D"
#             my_databases['WAV2D'] = databases_file
#             my_databases['CUR2D'] = databases_file
#             my_databases['WAV'] = databases_file.replace('2D', '1D')
#             my_databases['CUR'] = databases_file.replace('2D', '1D')
#             my_databases['WIN'] = databases_file.replace('2D', '1D')
#             my_databases['LEV'] = databases_file.replace('2D', '1D')
#         else:
#             project_name += "_1D"
#             my_databases['WAV'] = databases_file
#             my_databases['CUR'] = databases_file
#             my_databases['WIN'] = databases_file
#             my_databases['LEV'] = databases_file
#         my_databases['BATHY'] = bathy_file
#         my_databases['SEABED'] = seabed_file
#         my_databases['ROUGHNESS'] = roughness_file
#         my_databases['SPECIES'] = species_file
#     else: 
#         #(only for complexity levels 1 and 2)
#         WEL = request.args['wav']
#         CEL = request.args['cur']
#         MixLevel = "Waves-" + WEL + "_Currents-" + CEL
#         project_extension = "_CPX" + level + "_" + MixLevel
#         project_name += project_extension
        
#         # test if already computed
#         from os import listdir
#         import shutil
#         for f in listdir(PATH_STORAGE):
#             if f.endswith(project_name + '.json'):
#                 return "name already exists"
#             elif f.endswith(project_extension + '.json'):
#                 shutil.copy(os.path.join(PATH_STORAGE,f), os.path.join(PATH_STORAGE,project_name+'.json'))
#                 shutil.copytree(os.path.join(PATH_STORAGE,'figures',f[:-5]), os.path.join(PATH_STORAGE,'figures',project_name))
#                 return "Project successfully created !"
        
#         point_name = esed.Corresp_HOMERE[MixLevel]['database_name']
#         # HOMERE DATABASE
#         if int(level) == 1:
#             wave_database = point_name + '_all_1D'
#             current_database = point_name + '_all_1D'
#             wind_database = point_name + '_all_1D'
#         elif int(level) == 2:
#             wave_database = point_name + '_all_1D'
#             current_database = point_name + '_all_1D'
#             wind_database = point_name + '_all_1D'
#             wave_2D_database = point_name + '_all_2D'
#             current_2D_database = point_name + '_all_2D'
#         else:
#             return "This complexity level is not yet implemented"
    
#         # GEOMETRIES
#         farm_file = point_name + '_lease_area.shp'  #request.args['farmfile']
#         FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
#         corridor_file = point_name + '_corridor.shp'    #request.args['corridorfile']
#         CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
        
#         # BATHYMETRY
#         try:
#             bathy_value = request.args['bathy']
#             bathymetry_value = float(bathy_value)
#         except:
#             bathymetry_value = 'dtop'
    
#         # dictionary of databases
#         my_databases = {}  
#         my_databases['WAV'] = wave_database
#         my_databases['CUR'] = current_database
#         if int(level) == 2:
#             my_databases['WAV2D'] = wave_2D_database
#             my_databases['CUR2D'] = current_2D_database
#         my_databases['WIN'] = wind_database
#         my_databases['LEV'] = current_database
#         my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
#         my_databases['SEABED'] = 'Seabed_Types'
#         my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
#         my_databases['SPECIES'] = 'Species_default'
    
#     ### Launch Project
#     DTOP = Project.MyProject(project_name, level, my_databases, FarmFile, CorridorFile, PATH_STORAGE)
#     my_polygon = DTOP.__load_shapefiles__()
#     t1 = datetime.now()
#     diff1 = (t1-t0).total_seconds()
#     print("***********************************************************")
#     print(" Load shapefiles in %s seconds" %diff1)
#     print("***********************************************************")
#     DTOP.__launch_extractions__(bathymetry_value)
#     DTOP.__extract_2D_values__(my_polygon)
#     t2 = datetime.now()
#     diff2 = (t2-t1).total_seconds()
#     print("***********************************************************")
#     print(" All extractions done in %s seconds" %diff2)
#     print("***********************************************************")
#     DTOP.__launch_statistics__()
#     t3 = datetime.now()
#     diff3 = (t3-t2).total_seconds()
#     print("***********************************************************")
#     print(" All statistics computed in %s seconds" %diff3)
#     print("***********************************************************")
#     DTOP.__plot_results__(project_name)
#     DTOP.__store_project__()
#     t4 = datetime.now()
#     diff4 = (t4-t3).total_seconds()
#     print("***********************************************************")
#     print(" Project stored in %s seconds" %diff4)
#     print("***********************************************************")
#     t5 = datetime.now()
#     diff5 = (t5-t0).total_seconds()
#     print("***********************************************************")
#     print(" Project finished in %s seconds" %diff5)
#     print("***********************************************************")
    
#     return "Project successfully created !"