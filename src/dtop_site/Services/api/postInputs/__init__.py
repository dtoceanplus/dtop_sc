# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_site.BusinessLogic.libraries.dtosc.outputs.Project import Project
#------------------------------------
# @ USER DEFINED IMPORTS START
working= os.getcwd()
PATH_STORAGE = os.path.join(working, "storage")
PATH_GUI = os.path.join(working, "src/dtop_site/GUI/src/assets/Sites_images")
from pathlib import Path
data_path = Path('databases/SiteCharacterisation_Main-Database/')
PATH_DATABASES = data_path
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/NEW/'))
PATH_STORAGE = os.path.join(working, Path('storage'))
PATH_STORAGE_TEMP = './storage/temp'
PATH_USER = os.path.join(working, Path('USER'))
PATH_USER_GEOMETRIES = './USER/Geometries'
PATH_USER_DIRECTVALUES= './USER/DirectValues'
PATH_USER_TIMESERIES = './USER/TimeSeries'

from dtop_site.BusinessLogic.libraries.dtosc.input.Inputs import Inputs
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWaves import EXTWaves
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWaves import BasicWaves
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTCurrents import EXTCurrents
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicCurrents import BasicCurrents
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWinds import EXTWinds
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWinds import BasicWinds
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.EXTWaterLevels import EXTWaterLevels
from dtop_site.BusinessLogic.libraries.dtosc.point.outputs.BasicWaterLevels import BasicWaterLevels
from shutil import copyfile
import base64

import geojson, subprocess
import netCDF4 as nc

from dtop_site.BusinessLogic.libraries.dtosc.dr.digital_representation import digital_representation

import json

# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('postInputs', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/initialise_Project', methods=['GET'], strict_slashes=False)
def initiate():
    """Flask blueprint route for initializing the inputs JSON structure.

    Returns:
        (dict): JSON structure of inputs.

    """

    if not(os.path.exists('./storage/Entity_table.json')):
        f = open("./storage/Entity_table.json",'w')
        f.write('{"List_of_entity": []}')
        f.close()

    my_inputs = Inputs()

    return jsonify(my_inputs.prop_rep())

def save_inputs_to_storage(my_inputs,ProjectId):
    """Function to save the JSON file containing the inputs of the entity.

    Args:
        my_inputs (dict): JSON structure containing all inputs
        ProjectId (int): Full name of the entity

    """

    # Save the project input
    storagePath = './storage'
    fileName=os.path.join(storagePath, ProjectId + '_inputs.json')
    my_inputs.saveJSON(fileName)

    # Delete existing outputs
    filename=os.path.join(storagePath, ProjectId+'.json')
    if os.path.exists(filename):
        os.remove(filename)

@bp.route('/GetProjectsList', methods=['GET'], strict_slashes=False)
def MyProjects():
    """Flask blueprint route for getting the list of all registred entities.
    
    Returns:
        (dict): JSON structure of the list of registered entities.

    """
    MyTable = list_files_new('./storage','json')
    return jsonify(MyTable)

def list_files_new(directory, extension):
    """Function to list the registered entities.

    Args:
        directory (str): Directory in which the entities are registered.
        extension (str): Extension of the files to list
    
    Returns:
        (dict): JSON structure of the list of registered entities.

    """
    import time
    fl=listdir(directory)
    MyTable = []
    for i, fli in enumerate(fl):
        if(fli[:-12] != 'Temp'):
            if (fli.endswith('_inputs.'+extension)):
                path_file = os.path.join(directory, fl[i])
                path_file = os.path.abspath(path_file)
                dict = {}
                dict['path_file'] = path_file
                tim = os.path.getctime(path_file)
                dict['time'] = time.ctime(tim)
                dict['name'] = fli[:-12]
                MyTable.append(dict)
    return MyTable

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<ProjectId>/DeleteProject', methods=['DELETE'], strict_slashes=False)
def delete(ProjectId):
    """Flask blueprint route for deleting an entity.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (str): 'Entity deleted' or a HTTP 404 error.

    """

    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if not(ProjectId == entity['EntityName'] + '_' + str(entity['EntityId'])):
                testlist.append(entity)
    List['List_of_entity'] = testlist
    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    os.remove('./storage/'+ProjectId+'_inputs.json')

    if (os.path.exists('./storage/'+ProjectId+'_outputs.h5')):
        os.remove('./storage/'+ProjectId+'_outputs.h5')

    if (os.path.exists('./storage/'+ProjectId+'_dr.json')):
        os.remove('./storage/'+ProjectId+'_dr.json')

    if (os.path.exists('./storage/figures/'+ProjectId)):
        src_files = os.listdir('./storage/figures/'+ProjectId)
        for file_name in src_files:
            full_file_name = os.path.join('./storage/figures/', ProjectId+'/'+file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)
        os.rmdir(os.path.join('./storage/figures/', ProjectId))

    return "Entity deleted"

# API that load the input structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<ProjectId>/loadinputs', methods=['GET'], strict_slashes=False)
def load_inputs_structure(ProjectId):
    """Flask blueprint route for getting the inputs of an entity.

    If ProjectId provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): ID of the entity.
    
    Returns:
        (dict): JSON structure of the inputs of the corresponding entity or a HTTP 404 error.

    """

    # Load inputs from stored file
    filename=os.path.join('./storage',ProjectId+'_inputs.json')
    my_inputs=Inputs()
    my_inputs.loadJSON(filePath = filename)

    return jsonify(my_inputs.prop_rep())

# API that load the output structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/loadoutputs', methods=['GET'], strict_slashes=False)
def load_outputs_structure(eid,ProjectId):
    """Flask blueprint route for getting the outputs of an entity divided in 8 variables.

    If no outputs are available return 8 false

    Args:
        ProjectId (str): Name of the entity.
        eid (int): ID of the entity
    
    Returns:
        (dict, dict, dict, dict, dict, dict, dict, dict): JSON structures of the different part of the outputs or a serie of false (false, false, false, false, false, false, false, false).

    """

    if eid == 'none':
        my_filename = ProjectId
    else:
        my_filename = ProjectId + '_' + eid

    if os.path.exists(os.path.join('./storage',my_filename +'_outputs.h5')):

        filename=os.path.join('./storage',my_filename +'_outputs.h5')

        My_Project = Project()
        My_Project.loadHDF5(name=my_filename, filePath=filename)

        my_waves_basics = BasicWaves()
        my_waves_basics = My_Project.point.statistics.waves.Basic

        my_waves_ext = EXTWaves()
        my_waves_ext = My_Project.point.statistics.waves.EXT

        my_currents_basics = BasicCurrents()
        my_currents_basics = My_Project.point.statistics.currents.Basic

        my_currents_ext = EXTCurrents()
        my_currents_ext = My_Project.point.statistics.currents.EXT

        my_winds_basics = BasicWinds()
        my_winds_basics = My_Project.point.statistics.winds.Basic

        my_winds_ext = EXTWinds()
        my_winds_ext = My_Project.point.statistics.winds.EXT

        my_waterlevel_basics = BasicWaterLevels()
        my_waterlevel_basics = My_Project.point.statistics.water_levels.Basic

        my_waterlevel_ext = EXTWaterLevels()
        my_waterlevel_ext = My_Project.point.statistics.water_levels.EXT


        return jsonify(my_waves_basics.prop_rep(), my_waves_ext.prop_rep(), my_currents_basics.prop_rep(), my_currents_ext.prop_rep(), my_winds_basics.prop_rep(), my_winds_ext.prop_rep(), my_waterlevel_basics.prop_rep(), my_waterlevel_ext.prop_rep())

    else:

        return jsonify(False, False, False, False, False, False, False, False)

# API that load the output structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/checkfigures', methods=['GET'], strict_slashes=False)
def check_figures(eid,ProjectId):
    """Flask blueprint route for checking if an image exists.

    If the combinaison of ProjectId + '_' + eid provided does not match to any existing entity in the database, returns a 404 error.

    Args:
        ProjectId (int): Name of the entity.
        eid (int): ID of the entity
    
    Returns:
        (bool): boolean or a HTTP 404 error.

    """

    if os.path.exists(os.path.join('./storage/figures/',ProjectId + '_' + eid+'/MAP_mag-max.png')):
        return jsonify(True)
    else:
        return jsonify(False)

# API that save the status structure
@bp.route('/<ProjectId>/saveProject', methods=['POST'], strict_slashes=False)
def save_inputstatus(ProjectId):
    """Flask blueprint route for saving the entity inputs.

    If outputs already exists, this api would delete them to avoid conflict between inputs and outputs.

    Args:
        ProjectId (int): Name of the entity.
    
    Returns:
        (dict): JSON structure of the saved inputs.

    """

    # Create status structure from request
    my_input = Inputs()

    # Load status from request
    my_input_rep = request.get_json()
    my_input.loadFromJSONDict(my_input_rep)
    my_input.name = ProjectId

    if (my_input.entity_id == ''):
        cpt = 1
        testlist = []

        # Get the ID
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                testlist.append(entity['EntityId'])

        while cpt in testlist:
            cpt = cpt+1

        List['List_of_entity'].append({"EntityName": ProjectId, "EntityId": cpt})
        my_input.entity_id = cpt

        ListtoWrite = json.dumps(List)
        f=open('./storage/Entity_table.json', "w")
        f.write(ListtoWrite)
        f.close()

        # Save the input status structure
        save_input_to_storage(my_input,ProjectId + '_' + str(cpt))
    else:

        # Get the ID
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if (int(my_input.entity_id) == entity['EntityId'] and my_input.name == entity['EntityName']):
                    ProjectId = entity['EntityName'] + '_' + str(entity['EntityId'])

        save_input_to_storage(my_input,ProjectId)
    
    if (os.path.exists('./storage/' + ProjectId + '_outputs.h5')):
        os.remove('./storage/' + ProjectId + '_outputs.h5')

    return jsonify(my_input.prop_rep())

# API that save the status structure
@bp.route('/<eid>/<ProjectId>/GetWaveImage', methods=['POST'], strict_slashes=False)
def load_wave_image(eid,ProjectId):
    """Flask blueprint route for getting the images of the waves outputs.

    If the combinaison of ProjectId + '_' + eid provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): Name of the entity.
        eid (int): ID of the entity.
    
    Returns:
        (str, str, str): Base64 encoded images or a HTTP 404 error.

    """

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/EJPD_hs-dp.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag1 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/EJPD_hs-tp.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag2 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/Windrose_hs-dp.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag3 = data_uri

    return jsonify(img_tag1, img_tag2, img_tag3)

# API that save the status structure
@bp.route('/<eid>/<ProjectId>/GetCurrentImage', methods=['POST'], strict_slashes=False)
def load_current_image(eid,ProjectId):
    """Flask blueprint route for getting the images of the current outputs.

    If the combinaison of ProjectId + '_' + eid provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): Name of the entity.
        eid (int): ID of the entity.
    
    Returns:
        (str, str, str): Base64 encoded images or a HTTP 404 error.

    """

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/EJPD_mag-theta.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag1 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/Windrose_mag-theta.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag2 = data_uri

    return jsonify(img_tag1, img_tag2)

# API that save the status structure
@bp.route('/<eid>/<ProjectId>/Get2DImage', methods=['POST'], strict_slashes=False)
def load_2d_image(eid,ProjectId):
    """Flask blueprint route for getting the images of the 2DMaps outputs.

    If the combinaison of ProjectId + '_' + eid provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        ProjectId (int): Name of the entity.
        eid (int): ID of the entity.
    
    Returns:
        (str, str, str): Base64 encoded images or a HTTP 404 error.

    """

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/MAP_bathymetry.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag1 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/MAP_seabed.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag2 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/MAP_mag-mean.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag3 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/MAP_mag-max.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag4 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/MAP_hs-mean.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag5 = data_uri

    path_to_image = os.path.join('./storage/figures/',ProjectId + '_' + eid + '/MAP_hs-max.png')
    data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
    img_tag6 = data_uri

    return jsonify(img_tag1, img_tag2, img_tag3, img_tag4, img_tag5, img_tag6)

def save_input_to_storage(my_input,ProjectId):
    """Function to save the inputs of the entity.

    Args:
        my_input (dict): JSON structure of the inputs.
        ProjectId (int): Full name of the entity (name + '_' + ID).

    """

    storagePath = './storage'
    if (os.path.exists(os.path.join(storagePath,ProjectId + '_inputs.json'))):
        my_input_temp = Inputs()
        my_input_temp.loadJSON(filePath = os.path.join(storagePath,ProjectId + '_inputs.json'))
        my_input.entity_id = my_input_temp.entity_id
    else:
        cpt = 1
        test_id = []
        fl = listdir(storagePath)
        for i, fli in enumerate(fl):
            if (fli[-12:] == '_inputs.json' and fli[:-12] != my_input.name):
                my_input_temp = Inputs()
                my_input_temp.loadJSON(filePath = './storage/'+fli)
                test_id.append(int(my_input_temp.entity_id))

        while (cpt in test_id):
            cpt = cpt+1

        my_input.entity_id = str(cpt)

    fileName=os.path.join(storagePath,ProjectId + '_inputs.json')
    my_input.saveJSON(fileName)

def list_files4(directory, extension):
    """Function to list the files with extension 'extension' in the directory 'directory'.

    If the directory provided does not match to any existing one, returns a 404 error.

    Args:
        directory (str): Path the the directory in which the function will list the files.
        extension (str): Extension of the files to take into account.
    
    Returns:
        (dict): Dict object containing the list or a HTTP 404 error.

    """
    import time
    fl=listdir(directory)
    MyTable = []
    for i, fli in enumerate(fl):
        if fli.endswith('.'+extension):
            path_file = os.path.join(directory, fl[i])
            path_file = os.path.abspath(path_file)
            dict = {}
            dict['path_file'] = path_file
            tim = os.path.getctime(path_file)
            dict['time'] = time.ctime(tim)
            dict['name'] = fli[:-5]
            MyTable.append(dict)
    return MyTable

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/DeleteLog', methods=['POST'], strict_slashes=False)
def delete_log():
    """Flask blueprint route for deleting the log file.
    
    Returns:
        (str): 'LogFile deleted'.

    """

    # Delete the Log File
    if os.path.exists("./sc.log"):
        os.remove("./sc.log")
        f = open("./sc.log", "w")
        f.close()

    return "LogFile deleted"

# API that check the consistency of the lease area and the corridor
@bp.route('/Checkconsistency', methods=['POST'], strict_slashes=False)
def Check_consistency():
    """To check consistency between the 2 geometry shapefiles.
    
    Returns:
        (str): Boolean.

    """

    required_data = request.get_json()
    print(required_data)
    FarmFile = required_data['lease']
    Corridor = required_data['corr']
    
    import geopandas as gpd
    import numpy as np
    
    acceptable_distance = 0.1   # in degree
    
    data_lease = gpd.read_file(FarmFile)
    data_corr = gpd.read_file(Corridor)
    lease = [i for i in data_lease.geometry]
    corr = [i for i in data_corr.geometry]
    distances = []
    # Here, we check the distance between the centroïd of the lease area and 
    # each point of the corridor
    if corr[0].geom_type == "LineString":
        for i,(cox,coy) in enumerate(zip(corr[0].coords.xy[0],corr[0].coords.xy[1])):
            dist = np.sqrt((lease[0].centroid.x-cox)**2 + (lease[0].centroid.y-coy)**2)
            distances.append(dist)
    else:
        for i,(cox,coy) in enumerate(zip(corr[0].boundary.coords.xy[0],corr[0].boundary.coords.xy[1])):
            dist = np.sqrt((lease[0].centroid.x-cox)**2 + (lease[0].centroid.y-coy)**2)
            distances.append(dist)
        

    bool_consistency = min(distances) < acceptable_distance
    
    return jsonify(str(bool_consistency))

@bp.route('/run', methods=['POST'], strict_slashes=False)
def run():
    """Flask blueprint route for running the calculation of the module.

    Returns:
        (str): 'Project successfully created !' or 'Project terminated with an error ! Check the log for more information.'.

    """
    
    """
      @oas [post] /api/test1
      description: Returns (wave, current, ..) direct values, timeseries
      and statistics on farm, corridor and device locations
    """
    from dtop_site.BusinessLogic import Project
    from dtop_site.BusinessLogic.catalogs import early_stage_energy_databases as esed
    import os
    from datetime import datetime

    ## Timer
    t0 = datetime.now()

    #### info from GUI
    # NAME
    required_data = request.get_json()
    project_name = required_data['name']
    level = required_data['level']

    try:
        # LEVELS OF ENERGY
        if int(level) == 3:
            project_name += '_' + required_data['entity_id']
            # GEOMETRIES
            FarmFile = required_data['lease']
            CorridorFile = required_data['corr']

            # DATABASES
            databases_file = required_data['ts']
            databases_file2 = required_data['ts2']
            bathy_file = required_data['bathy']        
            seabed_file = required_data['seabed']
            roughness_file = required_data['roughness']
            species_file = required_data['species']

            checkif2D = required_data['checkTS']

            # dictionary of databases
            my_databases = {}
            if checkif2D:
                my_databases['WAV2D'] = databases_file2
                my_databases['CUR2D'] = databases_file2
                my_databases['WAV'] = databases_file.replace('2D', '1D')
                my_databases['CUR'] = databases_file.replace('2D', '1D')
                my_databases['WIN'] = databases_file.replace('2D', '1D')
                my_databases['LEV'] = databases_file.replace('2D', '1D')
            else:
                my_databases['WAV'] = databases_file
                my_databases['CUR'] = databases_file
                my_databases['WIN'] = databases_file
                my_databases['LEV'] = databases_file
            my_databases['SEABED'] = seabed_file
            my_databases['ROUGHNESS'] = roughness_file
            my_databases['SPECIES'] = species_file
            # BATHYMETRY
            bathy_value = required_data['bathy']
            try:
                bathymetry_value = float(bathy_value)
                my_databases['BATHY'] = './databases/SiteCharacterisation_User_World-Database/DirectValues/World_BATHYMETRY_GEBCO2019_9km.nc'   #'World_BATHYMETRY_GEBCO2019_9km')
            except:
                bathymetry_value = 'dtop'
                my_databases['BATHY'] = bathy_file

        else:
            #(only for complexity levels 1 and 2)
            WEL = required_data['wav']
            CEL = required_data['cur']
            MixLevel = "Waves-" + WEL + "_Currents-" + CEL
            
            project_name += '_' + required_data['entity_id']

            point_name = esed.Corresp_HOMERE[MixLevel]['database_name']
            # HOMERE DATABASE
            if int(level) == 1:
                wave_database = point_name + '_all_1D'
                current_database = point_name + '_all_1D'
                wind_database = point_name + '_all_1D'
            elif int(level) == 2:
                wave_database = point_name + '_all_1D'
                current_database = point_name + '_all_1D'
                wind_database = point_name + '_all_1D'
                wave_2D_database = point_name + '_all_2D'
                current_2D_database = point_name + '_all_2D'
            else:
                return "This complexity level is not yet implemented"

            # GEOMETRIES
            farm_file = point_name + '_lease_area.shp'  #required_data['farmfile']
            FarmFile = os.path.join(PATH_GEOMETRIES, farm_file)
            corridor_file = point_name + '_corridor_NEW.shp'    #required_data['corridorfile']
            CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)

            # BATHYMETRY
            try:
                bathy_value = required_data['bathy']
                bathymetry_value = float(bathy_value)
            except:
                bathymetry_value = 'dtop'

            # dictionary of databases
            my_databases = {}
            my_databases['WAV'] = wave_database
            my_databases['CUR'] = current_database
            if int(level) == 2:
                my_databases['WAV2D'] = wave_2D_database
                my_databases['CUR2D'] = current_2D_database
            my_databases['WIN'] = wind_database
            my_databases['LEV'] = current_database
            my_databases['BATHY'] = 'France_BATHYMETRY_GEBCO2019_450m'
            my_databases['SEABED'] = 'France_SEABED-TYPE_SHOM_450m'
            my_databases['ROUGHNESS'] = 'France_SEABED-ROUGHNESS-LENGTH_SHOM_450m'
            my_databases['SPECIES'] = 'Species_default'

        ### Launch Project
        DTOP = Project.MyProject(project_name, level, my_databases, FarmFile, CorridorFile, PATH_STORAGE)
        my_polygon = DTOP.__load_shapefiles__()
        t1 = datetime.now()
        diff1 = (t1-t0).total_seconds()
        print("***********************************************************")
        print(" Load shapefiles in %s seconds" %diff1)
        print("***********************************************************")
        DTOP.__launch_extractions__(bathymetry_value)
        DTOP.__extract_2D_values__(my_polygon)
        t2 = datetime.now()
        diff2 = (t2-t1).total_seconds()
        print("***********************************************************")
        print(" All extractions done in %s seconds" %diff2)
        print("***********************************************************")
        DTOP.__launch_statistics__()
        t3 = datetime.now()
        diff3 = (t3-t2).total_seconds()
        print("***********************************************************")
        print(" All statistics computed in %s seconds" %diff3)
        print("***********************************************************")
        DTOP.__plot_results__(project_name)
        DTOP.__store_project__()
        t4 = datetime.now()
        diff4 = (t4-t3).total_seconds()
        print("***********************************************************")
        print(" Project stored in %s seconds" %diff4)
        print("***********************************************************")

        # Load the full body of the project results
        from dtop_site.BusinessLogic.libraries.dtosc.outputs.Project import Project

        filename=os.path.join('./storage', project_name + '_outputs.h5')
        my_results=Project()
        my_results.loadHDF5(name=project_name, filePath = filename)

        if (required_data['DR_present'] == 'true'):
            # Save the Digital Representation
            dr_name = './storage/' + project_name + '_dr.json'
            my_dr = digital_representation()
            my_dr.populate(my_results)
            my_dr.saveJSON(dr_name)
        else:
            if os.path.exists('./storage/' + project_name + '_dr.json'):
                os.remove('./storage/' + project_name + '_dr.json')

        t5 = datetime.now()
        diff5 = (t5-t0).total_seconds()
        print("***********************************************************")
        print(" Project finished in %s seconds" %diff5)
        print("***********************************************************")

        return "Project successfully created !"

    except Exception as e:
        
        return "Project terminated with an error ! Check the log for more information."

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/ReadLog', methods=['GET'], strict_slashes=False)
def read_log():
    """Flask blueprint route for getting the LogFile content.

    If no there is no existing LogFile returns 'No existing LogFile.
    
    Returns:
        (str): LogFile content or 'No existing LogFile'.

    """
    
    # Extract the actual date
    from datetime import datetime
    today = datetime.today()
    date = str(today)[0:10]

    if (os.path.exists("./sc.log")):

        # Extract the Log File
        logfile = open("./sc.log","r") 

        # Return the file and the date
        return(logfile.read())
    
    else:
        return('No LogFile')

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<FileType>/<FileId>/single_file', methods=['POST'], strict_slashes=False)
def import_single_file(FileType, FileId):
    """Flask blueprint route for importing a single file.

    Args:
        FileType (str): Category corresponding to the type of file imported in the frontend by the user. Match with a folder name in the storage directory.
        FileId (str): Name of the file.
    
    Returns:
        (str): 'File imported'.

    """

    response = request.files
    test = response.copy()
    test['file'].save('./storage/temp/'+FileType+'/'+FileId)
    
    return('File imported')

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<FilesType>/<FilesId>/multiple_file', methods=['POST'], strict_slashes=False)
def import_multiple_files(FilesType, FilesId):
    """Flask blueprint route for importing a single file.

    Args:
        FileType (str): Category corresponding to the type of file imported in the frontend by the user. Match with a folder name in the storage directory.
        FileId (str): Prefix used to save the files.
    
    Returns:
        (str): 'Files imported'.

    """
        
    response = request.files
    test = response.copy()
    filenames = FilesId.split(',')
    
    if not (os.path.exists('./storage/temp/'+FilesType+'/'+filenames[0][:-4])):
        os.mkdir('./storage/temp/'+FilesType+'/'+filenames[0][:-4])

    for i in range(4):
        test['file['+str(i)+']'].save('./storage/temp/'+FilesType+'/'+filenames[0][:-4]+'/'+filenames[i])
    
    return('Files imported')

# API that list the lease files imported by the user
@bp.route('/listlease', methods=['GET'], strict_slashes=False)
def listfiles1():
    """Flask blueprint route for getting the list of available lease area database.

    If no lease area database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/lease'):
        list_of_files = os.listdir('./storage/temp/lease')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/lease')
        return jsonify('')

# API that list the corridor files imported by the user
@bp.route('/listcorridor', methods=['GET'], strict_slashes=False)
def listfiles2():
    """Flask blueprint route for getting the list of available corridor database.

    If no corridor database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/corridor'):
        list_of_files = os.listdir('./storage/temp/corridor')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/corridor')
        return jsonify('')

# API that list the seabed files imported by the user
@bp.route('/listseabed', methods=['GET'], strict_slashes=False)
def listfiles3():
    """Flask blueprint route for getting the list of available seabed database.

    If no seabed database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/seabed'):
        list_of_files = os.listdir('./storage/temp/seabed')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/seabed')
        return jsonify('')
        
# API that list the roughness length files imported by the user
@bp.route('/listroughness', methods=['GET'], strict_slashes=False)
def listfiles4():
    """Flask blueprint route for getting the list of available roughness database.

    If no roughness database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/roughness'):
        list_of_files = os.listdir('./storage/temp/roughness')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/roughness')
        return jsonify('')

# API that list the species files imported by the user
@bp.route('/listspecies', methods=['GET'], strict_slashes=False)
def listfiles5():
    """Flask blueprint route for getting the list of available species database.

    If no species database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/species'):
        list_of_files = os.listdir('./storage/temp/species')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/species')
        return jsonify('')

# API that list the timeseries files imported by the user
@bp.route('/listtimeseries', methods=['GET'], strict_slashes=False)
def listfiles6():
    """Flask blueprint route for getting the list of available timeseries database.

    If no timeseries database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/timeseries'):
        list_of_files = os.listdir('./storage/temp/timeseries')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/timeseries')
        return jsonify('')

# API that list the timeseries files imported by the user
@bp.route('/listtimeseries2', methods=['GET'], strict_slashes=False)
def listfiles62():
    """Flask blueprint route for getting the list of available 2D timeseries database.

    If no 2D timeseries database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/timeseries2D'):
        list_of_files = os.listdir('./storage/temp/timeseries2D')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/timeseries2D')
        return jsonify('')

# API that list the bathymetry files imported by the user
@bp.route('/listbathymetry', methods=['GET'], strict_slashes=False)
def listfiles7():
    """Flask blueprint route for getting the list of available bathymetry database.

    If no bathymetry database is available, returns ''.
    
    Returns:
        (dict): JSON structure of the list of files or ''.

    """
    try:
        os.mkdir('./storage/temp')
    except:
        print('')
    if os.path.exists('./storage/temp/bathymetry'):
        list_of_files = os.listdir('./storage/temp/bathymetry')
        return jsonify(list_of_files)
    else:
        os.mkdir('./storage/temp/bathymetry')
        return jsonify('')

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/DeleteImportedFiles', methods=['POST'], strict_slashes=False)
def delete_imported_files():
    """Flask blueprint route for deleting the imported files of a category (selected by the user through the frontend).
    
    Returns:
        (str): 'Files deleted'.

    """
    
    required_data = request.get_json()
    filetype = required_data['type']
    filenames = required_data['names']
    for i in filenames:
        os.remove('./storage/temp/' + filetype + '/' + i)
    
    return('Files deleted')

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/DeleteImportedFolder', methods=['POST'], strict_slashes=False)
def delete_imported_folder():
    """Flask blueprint route for deleting the imported folder of a category (selected by the user through the frontend).
    
    Returns:
        (str): 'Folders deleted'.

    """
    
    required_data = request.get_json()
    filetype = required_data['type']
    filenames = required_data['names']
    
    for i in filenames:
        list_of_files = os.listdir('./storage/temp/' + filetype + '/' + i)
        for j in list_of_files:
            os.remove('./storage/temp/' + filetype + '/' + i + '/' + j)
        os.rmdir('./storage/temp/' + filetype + '/' + i)
    
    return('Folders deleted')

# @ USER DEFINED FUNCTIONS END
#------------------------------------