# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, render_template, url_for, jsonify, request

bp = Blueprint('api_early_final', __name__)

import os
from pathlib import Path, PurePosixPath

working= os.getcwd()
data_path = Path('src/dtop_site/Databases/')
PATH_DATABASES = os.path.join(working, data_path)
PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/EXAMPLES/'))

"""
  @oas [get] /api
  description: Returns api documentation
"""
@bp.route('/')
def doc():
  spec_url=url_for('static', filename='openapi.yaml')
  return render_template('api/index.html', spec_url=spec_url)

class MyEarly:
    """ Return information for early stage """

    def __init__(self):
        """Initializes the MyEarly instance."""

#        self.project_name = request.args['site_name']
#        self.site_description = request.args['site_description']

    @bp.route('/earlyfinal', methods=['GET'])
    def return_all():
        """
          @oas [get] /api/earlyfinal
          description: Returns (wave, current, ..) direct values, timeseries
          and statistics on farm, corridor and device locations
        """
#        from dtop_site.BusinessLogic import Site
        from dtop_site.BusinessLogic import Project
#        from dtop_site.BusinessLogic.catalogs import variables_info
        from dtop_site.BusinessLogic.catalogs import early_stage_energy_databases as esed
        import os
#        import numpy as np
#        from dtop_site.BusinessLogic import Read
#        from dtop_site.BusinessLogic import useful_functions as uf
        from datetime import datetime

#        FILL_VALUE = 9999
        
        ## Timer
        t0 = datetime.now()
        
        #### info from GUI
        # NAME
        project_name = request.args['site_name']

        # LEVELS OF ENERGY (only for Early stage)
        WEL = request.args['Wave_energy_level']
        CEL = request.args['Current_energy_level']
        MixLevel = "Waves-" + WEL + "_Currents-" + CEL
        point_name = esed.Corresp[MixLevel]['database_name']
        wave_database = "EarlyStage_" + point_name + "_waves"
        current_database = "EarlyStage_" + point_name + "_currents"
        wind_database = "EarlyStage_" + point_name + "_winds"

        # GEOMETRIES
        farm_file = point_name + '_lease_area.shp'  #request.args['farmfile']
        FarmFile = os.path.join(PATH_GEOMETRIES, farm_file) 
        corridor_file = point_name + '_corridor.shp'    #request.args['corridorfile']
        CorridorFile = os.path.join(PATH_GEOMETRIES, corridor_file)
#        DevicesFile = "EarlyStage"
        DevicesFile = {}
        DevicesFile['names'] = (point_name,)
        DevicesFile['lons'] = (esed.Corresp[MixLevel]['longitude'],)
        DevicesFile['lats'] = (esed.Corresp[MixLevel]['latitude'],)
        
        # BATHYMETRY
        bathy_value = request.args['bathym']
        try:
            bathymetry_value = float(bathy_value)
        except:
            bathymetry_value = 'dtop'

        # dictionary of databases
        my_databases = {}  
        my_databases['WAV'] = wave_database
        my_databases['CUR'] = current_database
        my_databases['WIN'] = wind_database
        my_databases['LEV'] = current_database
        my_databases['BATHY'] = 'GEBCO_Europa_downgrade20'
        my_databases['SEABED'] = 'Seabed_Types'
        my_databases['ROUGHNESS'] = 'Seabed_Roughness_Length'
        my_databases['SPECIES'] = 'Species_default'
        
        ### Launch Project
        DTOP = Project.MyProject(project_name, my_databases, FarmFile, CorridorFile, DevicesFile, '../../../storage')
        DTOP.__load_shapefiles__()
        t1 = datetime.now()
        diff1 = (t1-t0).total_seconds()
        print("***********************************************************")
        print(" Load shapefiles in %s seconds" %diff1)
        print("***********************************************************")
        DTOP.__launch_extractions__(bathymetry_value)
        t2 = datetime.now()
        diff2 = (t2-t1).total_seconds()
        print("***********************************************************")
        print(" All extractions done in %s seconds" %diff2)
        print("***********************************************************")
        DTOP.__extract_2D_values__()
        DTOP.__launch_statistics__()
        t3 = datetime.now()
        diff3 = (t3-t2).total_seconds()
        print("***********************************************************")
        print(" All statistics computed in %s seconds" %diff3)
        print("***********************************************************")
        DTOP.__store_project__()
        t4 = datetime.now()
        diff4 = (t4-t3).total_seconds()
        print("***********************************************************")
        print(" Project stored in %s seconds" %diff4)
        print("***********************************************************")
        t5 = datetime.now()
        diff5 = (t5-t0).total_seconds()
        print("***********************************************************")
        print(" Project finished in %s seconds" %diff5)
        print("***********************************************************")
        
        return "json file available in storage"