# This is the Site Characterisation module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Youen Kervella, Nicolas Michelet, Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_site.BusinessLogic.libraries.dtosc.input.Inputs import Inputs
from dtop_site.Services.api.postInputs import save_input_to_storage
import json

bp = Blueprint('mm_integration', __name__)

# API that create an entity defined by the name defined in the Maine Module
@bp.route('/', methods=['POST'], strict_slashes=False)
def create_entity():
    """Flask blueprint route used by the Main Module to create an entity.
    
    Returns:
        (dict): EntityId (int) - ID of the created entity , EntityStatus (int): Status of the created entity.

        Or a HTTP 404/400 error.

    """

    if not(os.path.exists('./storage/Entity_table.json')):
        f = open("./storage/Entity_table.json",'w')
        f.write('{"List_of_entity": []}')
        f.close()

    request_body = request.get_json()
    if not request_body:
        return ('No request body provided'), 400

    EntityName = request_body.get('name')
    cplx = request_body.get('cplx')
    cpt = 1
    testlist = []

    # Get the ID
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            testlist.append(entity['EntityId'])

    while cpt in testlist:
        cpt = cpt+1

    List['List_of_entity'].append({"EntityName": EntityName, "EntityId": cpt})

    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    # Create input structure
    my_input = Inputs(EntityName)
    my_input.EntityId = cpt
    my_input.name = EntityName
    my_input.complexity_level = cplx

    # Save the input structure
    save_input_to_storage(my_input,EntityName+'_'+str(my_input.EntityId))

    return ({"EntityId": cpt, "EntityStatus": 0}),201

def delete_entity_by_name(Entity_name):
    """Function to delete an entity by name.

    If Entity_name provided does not match to any existing entity in the database, returns a 404 error. 

    Args:
        Entity_name (str): The full name of an entity.

    """
    if (os.path.exists('./storage/'+Entity_name+'_inputs.json')):
        os.remove('./storage/'+Entity_name+'_inputs.json')

    if (os.path.exists('./storage/'+Entity_name+'_outputs.h5')):
        os.remove('./storage/'+Entity_name+'_outputs.h5')

    if (os.path.exists('./storage/'+Entity_name+'_dr.json')):
        os.remove('./storage/'+Entity_name+'_dr.json')

    if (os.path.exists('./storage/figures/'+Entity_name)):
        src_files = os.listdir('./storage/figures/'+Entity_name)
        for file_name in src_files:
            full_file_name = os.path.join('./storage/figures/', Entity_name+'/'+file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)
        os.rmdir(os.path.join('./storage/figures/', Entity_name))


# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<Entity_id>/', methods=['DELETE'], strict_slashes=False)
def delete_entity(Entity_id):
    """Flask blueprint route for deleting an entity.

    If Entity_id provided does not match to any existing entity in the database, returns a 500 error. 

    Args:
        Entity_id (int): The ID of the entity to delete.
    
    Returns:
        (str): "Entity deleted" or a HTTP 404/500 error.

    """

    Entity_name = 'error'
    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if int(Entity_id) == entity['EntityId']:
                Entity_name = entity['EntityName']
            else:
                testlist.append(entity)

    List['List_of_entity'] = testlist
    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    if (Entity_name == 'error'):
        return "Error", 500
    else:
        delete_entity_by_name(Entity_name + '_' + str(Entity_id))
        return ("Entity deleted"),200

@bp.route('/<eid>/Get_Entity_Name', methods=['GET'])
def get_entity_name(eid):
    """Flask blueprint route for getting the name of the entity with ID = eid.

    If eid provided does not match to any existing entity in the database, returns a 500 error. 

    Args:
        eid (int): The ID of an entity.
    
    Returns:
        (str): Full entity name or a HTTP 404/500 error.

    """
    Entity_name = 'error'
    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if int(eid) == entity['EntityId']:
                Entity_name = entity['EntityName']

    if (Entity_name == 'error'):
        return "This entity does not exist", 500
    else:
        return (Entity_name + '_' + str(eid)),200