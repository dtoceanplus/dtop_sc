from flask import Blueprint, render_template, url_for, request

#from dtop_site.BusinessLogic import foo


bp = Blueprint('main', __name__)


@bp.route('/')
def index():
  return render_template('EarlyStageFINAL2.html')
