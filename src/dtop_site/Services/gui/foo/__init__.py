# This package uses service logic API.
# It provides GUI via HTTP API.
# It create a blueprint and define routes.

# Templates: http://flask.pocoo.org/docs/1.0/blueprints/#templates

from flask import Blueprint, render_template, url_for, request

from dtop_site.BusinessLogic import foo


bp = Blueprint('gui_foo', __name__)


@bp.route('/foo')
def index():
  # Force access to service level via HTTP request. For example purpose.

  # Get url to sevice.foo.index()
  # It works because service and gui is a single service now.
  # For access to different service you should get url in other way.
  foo_index_url = url_for('api_foo.index')
  url = request.url_root + foo_index_url

  # access to Service API
  # import requests
  # text = requests.get(url).text
  content = 'titi (in Services/gui/foo/__init.py__)'

  return render_template('gui/index.html', content=content)
