import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import CatalogTable from './CatalogTable.vue'
import Port from './Port.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/ports'
  },
  {
    path: '/ports',
    component: CatalogTable
  },
  {
    path: '/ports/:id',
    component: Port
  }
];

const router = new VueRouter({
  routes
});

new Vue({
  el: '#app',
  // router,
  render: h => h(App)
})
