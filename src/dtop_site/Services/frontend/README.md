# Catalog Service Frontend Package

This package demonstrates creation of GUI components using
[Vue.js](https://vuejs.org/)

## Getting Started

These instructions will guide you to start development environment and
to build a package.

### Prerequisites

You need to install:

- [Node.js](https://nodejs.org/).

### Installing

Install dependencies:
```DOS .bat
npm install
```

### Running Development Environment

Run development server (at http://localhost:8080/):
```DOS .bat
npm run dev
```

### Building Package

Build package for production with minification:
```DOS .bat
npm run build
```

This command will build package and place it to [Catalog Service](..).

## Using Vue CLI

This project was generated with [Vue CLI](https://cli.vuejs.org/).

Install Vue CLI:
```DOS .bat
npm install -g @vue/cli
vue --version
```

Create a project:
```DOS .bat
vue init webpack-simple frontend
cd frontend
npm install
npm run dev
```

## Using webpack

[webpack](https://webpack.js.org) is used for package bundle.

For detailed explanation on how things work, consult the
[docs for vue-loader](http://vuejs.github.io/vue-loader).
