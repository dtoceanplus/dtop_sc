import os
from flask import Flask, request, make_response, jsonify
from flask_babel import Babel
from flask_cors import CORS
import logging

#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------


babel = Babel()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}})
    babel.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    # Registering Blueprints
    from .api import getResults as res
    app.register_blueprint(res.bp, url_prefix='/sc')

#app = Flask(__name__)
#@app.before_first_request
#def setup_logging():
#    if not app.debug:
#        # In production mode, add log handler to sys.stderr.
#        app.logger.addHandler(logging.StreamHandler())
#        app.logger.setLevel(logging.INFO)
#------------------------------------
# @ USER DEFINED BLUEPRINT REGISTRATION START
# @ USER DEFINED BLUEPRINT REGISTRATION END
#------------------------------------



#------------------------------------
# @ USER DEFINED FUNCTIONS START

    from .api import EarlyStageFINAL as ESF
    app.register_blueprint(ESF.bp, url_prefix='/api')

    from .api import testJS as JS
    app.register_blueprint(JS.bp, url_prefix='/api')

    from .api import getResults as res
    app.register_blueprint(res.bp, url_prefix='/sc')

    from .api import postInputs as pi
    app.register_blueprint(pi.bp, url_prefix='/sc')

    from .api import mm_integration as mmi
    app.register_blueprint(mmi.bp, url_prefix='/sc')

    from .api import representation as dr
    app.register_blueprint(dr.bp)

#    from .gui import foo as foo_gui
#    app.register_blueprint(foo_gui.bp, url_prefix='/gui')

    from .gui import main
    app.register_blueprint(main.bp)

    if os.environ.get("FLASK_ENV") == "development":
        from .api.integration import provider_states
        app.register_blueprint(provider_states.bp)

# @ USER DEFINED FUNCTIONS END
#------------------------------------

    return app

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])   
