import os
from flask import Flask, request, make_response, jsonify
from flask_babel import Babel
from flask_cors import CORS

#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------


babel = Babel()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}})
    babel.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    # Registering Blueprints
    from .api import getResults as res
    app.register_blueprint(res.bp, url_prefix='/sc')

#------------------------------------
# @ USER DEFINED BLUEPRINT REGISTRATION START
# @ USER DEFINED BLUEPRINT REGISTRATION END
#------------------------------------



#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

    return app

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])   
