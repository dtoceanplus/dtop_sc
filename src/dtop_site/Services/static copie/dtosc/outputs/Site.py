import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ..farm.outputs import FarmResults
from ..corridor.outputs import CorridorResults
from ..databases.outputs import DatabasesPaths
from ..devices.outputs import DeviceResults
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Site():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._SiteID=''
        self._farm=FarmResults.FarmResults()
        self._corridor=CorridorResults.CorridorResults()
        self._databases_paths=DatabasesPaths.DatabasesPaths()
        self._devices=[]
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def SiteID(self): # pragma: no cover
        return self._SiteID
    #------------
    @ property
    def farm(self): # pragma: no cover
        return self._farm
    #------------
    @ property
    def corridor(self): # pragma: no cover
        return self._corridor
    #------------
    @ property
    def databases_paths(self): # pragma: no cover
        return self._databases_paths
    #------------
    @ property
    def devices(self): # pragma: no cover
        return self._devices
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ SiteID.setter
    def SiteID(self,val): # pragma: no cover
        self._SiteID=str(val)
    #------------
    @ farm.setter
    def farm(self,val): # pragma: no cover
        self._farm=val
    #------------
    @ corridor.setter
    def corridor(self,val): # pragma: no cover
        self._corridor=val
    #------------
    @ databases_paths.setter
    def databases_paths(self,val): # pragma: no cover
        self._databases_paths=val
    #------------
    @ devices.setter
    def devices(self,val): # pragma: no cover
        self._devices=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:Site"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:Site"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("SiteID"):
            rep["SiteID"] = self.SiteID
        if self.is_set("farm"):
            if (short and not(deep)):
                rep["farm"] = self.farm.type_rep()
            else:
                rep["farm"] = self.farm.prop_rep(short, deep)
        if self.is_set("corridor"):
            if (short and not(deep)):
                rep["corridor"] = self.corridor.type_rep()
            else:
                rep["corridor"] = self.corridor.prop_rep(short, deep)
        if self.is_set("databases_paths"):
            if (short and not(deep)):
                rep["databases_paths"] = self.databases_paths.type_rep()
            else:
                rep["databases_paths"] = self.databases_paths.prop_rep(short, deep)
        if self.is_set("devices"):
            rep["devices"] = []
            for i in range(0,len(self.devices)):
                if (short and not(deep)):
                    itemType = self.devices[i].type_rep()
                    rep["devices"].append( itemType )
                else:
                    rep["devices"].append( self.devices[i].prop_rep(short, deep) )
        else:
            rep["devices"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "SiteID"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "farm"
        try :
            if data[varName] != None:
                self.farm=FarmResults.FarmResults()
                self.farm.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "corridor"
        try :
            if data[varName] != None:
                self.corridor=CorridorResults.CorridorResults()
                self.corridor.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "databases_paths"
        try :
            if data[varName] != None:
                self.databases_paths=DatabasesPaths.DatabasesPaths()
                self.databases_paths.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "devices"
        try :
            if data[varName] != None:
                self.devices=[]
                for i in range(0,len(data[varName])):
                    self.devices.append(DeviceResults.DeviceResults())
                    self.devices[i].loadFromJSONDict(data[varName][i])
            else:
                self.devices = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
