import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class SMCurrents():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._x=np.zeros(shape=(1), dtype=float)
        self._y=np.zeros(shape=(1), dtype=float)
        self._U=np.zeros(shape=(1,1,1), dtype=float)
        self._V=np.zeros(shape=(1,1,1), dtype=float)
        self._TI=np.zeros(shape=(1,1,1), dtype=float)
        self._SSH=np.zeros(shape=(1,1,1), dtype=float)
        self._p=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def x(self): # pragma: no cover
        return self._x
    #------------
    @ property
    def y(self): # pragma: no cover
        return self._y
    #------------
    @ property
    def U(self): # pragma: no cover
        return self._U
    #------------
    @ property
    def V(self): # pragma: no cover
        return self._V
    #------------
    @ property
    def TI(self): # pragma: no cover
        return self._TI
    #------------
    @ property
    def SSH(self): # pragma: no cover
        return self._SSH
    #------------
    @ property
    def p(self): # pragma: no cover
        return self._p
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ x.setter
    def x(self,val): # pragma: no cover
        self._x=val
    #------------
    @ y.setter
    def y(self,val): # pragma: no cover
        self._y=val
    #------------
    @ U.setter
    def U(self,val): # pragma: no cover
        self._U=val
    #------------
    @ V.setter
    def V(self,val): # pragma: no cover
        self._V=val
    #------------
    @ TI.setter
    def TI(self,val): # pragma: no cover
        self._TI=val
    #------------
    @ SSH.setter
    def SSH(self,val): # pragma: no cover
        self._SSH=val
    #------------
    @ p.setter
    def p(self,val): # pragma: no cover
        self._p=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:SMCurrents"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:SMCurrents"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("x"):
            if (short):
                rep["x"] = str(self.x.shape)
            else:
                try:
                    rep["x"] = self.x.tolist()
                except:
                    rep["x"] = self.x
        if self.is_set("y"):
            if (short):
                rep["y"] = str(self.y.shape)
            else:
                try:
                    rep["y"] = self.y.tolist()
                except:
                    rep["y"] = self.y
        if self.is_set("U"):
            if (short):
                rep["U"] = str(self.U.shape)
            else:
                try:
                    rep["U"] = self.U.tolist()
                except:
                    rep["U"] = self.U
        if self.is_set("V"):
            if (short):
                rep["V"] = str(self.V.shape)
            else:
                try:
                    rep["V"] = self.V.tolist()
                except:
                    rep["V"] = self.V
        if self.is_set("TI"):
            if (short):
                rep["TI"] = str(self.TI.shape)
            else:
                try:
                    rep["TI"] = self.TI.tolist()
                except:
                    rep["TI"] = self.TI
        if self.is_set("SSH"):
            if (short):
                rep["SSH"] = str(self.SSH.shape)
            else:
                try:
                    rep["SSH"] = self.SSH.tolist()
                except:
                    rep["SSH"] = self.SSH
        if self.is_set("p"):
            if (short):
                rep["p"] = str(self.p.shape)
            else:
                try:
                    rep["p"] = self.p.tolist()
                except:
                    rep["p"] = self.p
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "x"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "y"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "U"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "V"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "TI"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "SSH"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "p"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
