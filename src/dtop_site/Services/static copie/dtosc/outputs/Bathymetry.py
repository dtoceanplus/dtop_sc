import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Bathymetry():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._latitude=np.zeros(shape=(1), dtype=float)
        self._longitude=np.zeros(shape=(1), dtype=float)
        self._value=np.zeros(shape=(1), dtype=float)
        self._unit='m from msl'
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def latitude(self): # pragma: no cover
        return self._latitude
    #------------
    @ property
    def longitude(self): # pragma: no cover
        return self._longitude
    #------------
    @ property
    def value(self): # pragma: no cover
        return self._value
    #------------
    @ property
    def unit(self): # pragma: no cover
        return self._unit
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ latitude.setter
    def latitude(self,val): # pragma: no cover
        self._latitude=val
    #------------
    @ longitude.setter
    def longitude(self,val): # pragma: no cover
        self._longitude=val
    #------------
    @ value.setter
    def value(self,val): # pragma: no cover
        self._value=val
    #------------
    @ unit.setter
    def unit(self,val): # pragma: no cover
        self._unit=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:Bathymetry"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:Bathymetry"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("latitude"):
            if (short):
                rep["latitude"] = str(self.latitude.shape)
            else:
                try:
                    rep["latitude"] = self.latitude.tolist()
                except:
                    rep["latitude"] = self.latitude
        if self.is_set("longitude"):
            if (short):
                rep["longitude"] = str(self.longitude.shape)
            else:
                try:
                    rep["longitude"] = self.longitude.tolist()
                except:
                    rep["longitude"] = self.longitude
        if self.is_set("value"):
            if (short):
                rep["value"] = str(self.value.shape)
            else:
                try:
                    rep["value"] = self.value.tolist()
                except:
                    rep["value"] = self.value
        if self.is_set("unit"):
            rep["unit"] = self.unit
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "latitude"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "longitude"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "value"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "unit"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
