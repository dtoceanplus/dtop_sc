import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EXT():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._latitude=0.0
        self._longitude=0.0
        self._return_periods=np.zeros(shape=(1), dtype=float)
        self._return_values=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def latitude(self): # pragma: no cover
        return self._latitude
    #------------
    @ property
    def longitude(self): # pragma: no cover
        return self._longitude
    #------------
    @ property
    def return_periods(self): # pragma: no cover
        return self._return_periods
    #------------
    @ property
    def return_values(self): # pragma: no cover
        return self._return_values
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ latitude.setter
    def latitude(self,val): # pragma: no cover
        self._latitude=float(val)
    #------------
    @ longitude.setter
    def longitude(self,val): # pragma: no cover
        self._longitude=float(val)
    #------------
    @ return_periods.setter
    def return_periods(self,val): # pragma: no cover
        self._return_periods=val
    #------------
    @ return_values.setter
    def return_values(self,val): # pragma: no cover
        self._return_values=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:EXT"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:outputs:EXT"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("latitude"):
            rep["latitude"] = self.latitude
        if self.is_set("longitude"):
            rep["longitude"] = self.longitude
        if self.is_set("return_periods"):
            if (short):
                rep["return_periods"] = str(self.return_periods.shape)
            else:
                try:
                    rep["return_periods"] = self.return_periods.tolist()
                except:
                    rep["return_periods"] = self.return_periods
        if self.is_set("return_values"):
            if (short):
                rep["return_values"] = str(self.return_values.shape)
            else:
                try:
                    rep["return_values"] = self.return_values.tolist()
                except:
                    rep["return_values"] = self.return_values
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "latitude"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "longitude"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "return_periods"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "return_values"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
