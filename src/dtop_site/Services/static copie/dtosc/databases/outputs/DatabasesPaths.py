import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DatabasesPaths():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._LeaseArea=''
        self._Corridor=''
        self._Devices=''
        self._Bathymetry=''
        self._Seabed=''
        self._Roughness=''
        self._Species=''
        self._Waves=''
        self._Currents=''
        self._Winds=''
        self._Levels=''
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def LeaseArea(self): # pragma: no cover
        return self._LeaseArea
    #------------
    @ property
    def Corridor(self): # pragma: no cover
        return self._Corridor
    #------------
    @ property
    def Devices(self): # pragma: no cover
        return self._Devices
    #------------
    @ property
    def Bathymetry(self): # pragma: no cover
        return self._Bathymetry
    #------------
    @ property
    def Seabed(self): # pragma: no cover
        return self._Seabed
    #------------
    @ property
    def Roughness(self): # pragma: no cover
        return self._Roughness
    #------------
    @ property
    def Species(self): # pragma: no cover
        return self._Species
    #------------
    @ property
    def Waves(self): # pragma: no cover
        return self._Waves
    #------------
    @ property
    def Currents(self): # pragma: no cover
        return self._Currents
    #------------
    @ property
    def Winds(self): # pragma: no cover
        return self._Winds
    #------------
    @ property
    def Levels(self): # pragma: no cover
        return self._Levels
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ LeaseArea.setter
    def LeaseArea(self,val): # pragma: no cover
        self._LeaseArea=str(val)
    #------------
    @ Corridor.setter
    def Corridor(self,val): # pragma: no cover
        self._Corridor=str(val)
    #------------
    @ Devices.setter
    def Devices(self,val): # pragma: no cover
        self._Devices=str(val)
    #------------
    @ Bathymetry.setter
    def Bathymetry(self,val): # pragma: no cover
        self._Bathymetry=str(val)
    #------------
    @ Seabed.setter
    def Seabed(self,val): # pragma: no cover
        self._Seabed=str(val)
    #------------
    @ Roughness.setter
    def Roughness(self,val): # pragma: no cover
        self._Roughness=str(val)
    #------------
    @ Species.setter
    def Species(self,val): # pragma: no cover
        self._Species=str(val)
    #------------
    @ Waves.setter
    def Waves(self,val): # pragma: no cover
        self._Waves=str(val)
    #------------
    @ Currents.setter
    def Currents(self,val): # pragma: no cover
        self._Currents=str(val)
    #------------
    @ Winds.setter
    def Winds(self,val): # pragma: no cover
        self._Winds=str(val)
    #------------
    @ Levels.setter
    def Levels(self,val): # pragma: no cover
        self._Levels=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:databases:outputs:DatabasesPaths"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:databases:outputs:DatabasesPaths"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("LeaseArea"):
            rep["LeaseArea"] = self.LeaseArea
        if self.is_set("Corridor"):
            rep["Corridor"] = self.Corridor
        if self.is_set("Devices"):
            rep["Devices"] = self.Devices
        if self.is_set("Bathymetry"):
            rep["Bathymetry"] = self.Bathymetry
        if self.is_set("Seabed"):
            rep["Seabed"] = self.Seabed
        if self.is_set("Roughness"):
            rep["Roughness"] = self.Roughness
        if self.is_set("Species"):
            rep["Species"] = self.Species
        if self.is_set("Waves"):
            rep["Waves"] = self.Waves
        if self.is_set("Currents"):
            rep["Currents"] = self.Currents
        if self.is_set("Winds"):
            rep["Winds"] = self.Winds
        if self.is_set("Levels"):
            rep["Levels"] = self.Levels
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "LeaseArea"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Corridor"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Devices"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Bathymetry"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Roughness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Species"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Waves"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Currents"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Winds"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Levels"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
