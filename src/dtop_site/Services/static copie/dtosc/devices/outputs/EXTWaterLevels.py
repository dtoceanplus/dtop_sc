import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import EXT
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EXTWaterLevels():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._WLEVnegative=EXT.EXT()
        self._WLEVpositive=EXT.EXT()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def WLEVnegative(self): # pragma: no cover
        return self._WLEVnegative
    #------------
    @ property
    def WLEVpositive(self): # pragma: no cover
        return self._WLEVpositive
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ WLEVnegative.setter
    def WLEVnegative(self,val): # pragma: no cover
        self._WLEVnegative=val
    #------------
    @ WLEVpositive.setter
    def WLEVpositive(self,val): # pragma: no cover
        self._WLEVpositive=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EXTWaterLevels"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EXTWaterLevels"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("WLEVnegative"):
            if (short and not(deep)):
                rep["WLEVnegative"] = self.WLEVnegative.type_rep()
            else:
                rep["WLEVnegative"] = self.WLEVnegative.prop_rep(short, deep)
        if self.is_set("WLEVpositive"):
            if (short and not(deep)):
                rep["WLEVpositive"] = self.WLEVpositive.type_rep()
            else:
                rep["WLEVpositive"] = self.WLEVpositive.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "WLEVnegative"
        try :
            if data[varName] != None:
                self.WLEVnegative=EXT.EXT()
                self.WLEVnegative.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "WLEVpositive"
        try :
            if data[varName] != None:
                self.WLEVpositive=EXT.EXT()
                self.WLEVpositive.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
