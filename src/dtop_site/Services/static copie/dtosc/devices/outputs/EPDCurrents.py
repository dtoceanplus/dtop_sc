import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import EPD
from ...outputs import EPDMonthly
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EPDCurrents():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._mag=EPD.EPD()
        self._mag_monthly=EPDMonthly.EPDMonthly()
        self._theta=EPD.EPD()
        self._theta_monthly=EPDMonthly.EPDMonthly()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def mag(self): # pragma: no cover
        return self._mag
    #------------
    @ property
    def mag_monthly(self): # pragma: no cover
        return self._mag_monthly
    #------------
    @ property
    def theta(self): # pragma: no cover
        return self._theta
    #------------
    @ property
    def theta_monthly(self): # pragma: no cover
        return self._theta_monthly
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ mag.setter
    def mag(self,val): # pragma: no cover
        self._mag=val
    #------------
    @ mag_monthly.setter
    def mag_monthly(self,val): # pragma: no cover
        self._mag_monthly=val
    #------------
    @ theta.setter
    def theta(self,val): # pragma: no cover
        self._theta=val
    #------------
    @ theta_monthly.setter
    def theta_monthly(self,val): # pragma: no cover
        self._theta_monthly=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EPDCurrents"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EPDCurrents"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("mag"):
            if (short and not(deep)):
                rep["mag"] = self.mag.type_rep()
            else:
                rep["mag"] = self.mag.prop_rep(short, deep)
        if self.is_set("mag_monthly"):
            if (short and not(deep)):
                rep["mag_monthly"] = self.mag_monthly.type_rep()
            else:
                rep["mag_monthly"] = self.mag_monthly.prop_rep(short, deep)
        if self.is_set("theta"):
            if (short and not(deep)):
                rep["theta"] = self.theta.type_rep()
            else:
                rep["theta"] = self.theta.prop_rep(short, deep)
        if self.is_set("theta_monthly"):
            if (short and not(deep)):
                rep["theta_monthly"] = self.theta_monthly.type_rep()
            else:
                rep["theta_monthly"] = self.theta_monthly.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "mag"
        try :
            if data[varName] != None:
                self.mag=EPD.EPD()
                self.mag.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mag_monthly"
        try :
            if data[varName] != None:
                self.mag_monthly=EPDMonthly.EPDMonthly()
                self.mag_monthly.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "theta"
        try :
            if data[varName] != None:
                self.theta=EPD.EPD()
                self.theta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "theta_monthly"
        try :
            if data[varName] != None:
                self.theta_monthly=EPDMonthly.EPDMonthly()
                self.theta_monthly.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
