import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import BasicStats
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class BasicWaves():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._hs=BasicStats.BasicStats()
        self._tp=BasicStats.BasicStats()
        self._CgE=BasicStats.BasicStats()
        self._gamma=BasicStats.BasicStats()
        self._spr=BasicStats.BasicStats()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def hs(self): # pragma: no cover
        return self._hs
    #------------
    @ property
    def tp(self): # pragma: no cover
        return self._tp
    #------------
    @ property
    def CgE(self): # pragma: no cover
        return self._CgE
    #------------
    @ property
    def gamma(self): # pragma: no cover
        return self._gamma
    #------------
    @ property
    def spr(self): # pragma: no cover
        return self._spr
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ hs.setter
    def hs(self,val): # pragma: no cover
        self._hs=val
    #------------
    @ tp.setter
    def tp(self,val): # pragma: no cover
        self._tp=val
    #------------
    @ CgE.setter
    def CgE(self,val): # pragma: no cover
        self._CgE=val
    #------------
    @ gamma.setter
    def gamma(self,val): # pragma: no cover
        self._gamma=val
    #------------
    @ spr.setter
    def spr(self,val): # pragma: no cover
        self._spr=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:BasicWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:BasicWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("hs"):
            if (short and not(deep)):
                rep["hs"] = self.hs.type_rep()
            else:
                rep["hs"] = self.hs.prop_rep(short, deep)
        if self.is_set("tp"):
            if (short and not(deep)):
                rep["tp"] = self.tp.type_rep()
            else:
                rep["tp"] = self.tp.prop_rep(short, deep)
        if self.is_set("CgE"):
            if (short and not(deep)):
                rep["CgE"] = self.CgE.type_rep()
            else:
                rep["CgE"] = self.CgE.prop_rep(short, deep)
        if self.is_set("gamma"):
            if (short and not(deep)):
                rep["gamma"] = self.gamma.type_rep()
            else:
                rep["gamma"] = self.gamma.prop_rep(short, deep)
        if self.is_set("spr"):
            if (short and not(deep)):
                rep["spr"] = self.spr.type_rep()
            else:
                rep["spr"] = self.spr.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "hs"
        try :
            if data[varName] != None:
                self.hs=BasicStats.BasicStats()
                self.hs.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "tp"
        try :
            if data[varName] != None:
                self.tp=BasicStats.BasicStats()
                self.tp.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "CgE"
        try :
            if data[varName] != None:
                self.CgE=BasicStats.BasicStats()
                self.CgE.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "gamma"
        try :
            if data[varName] != None:
                self.gamma=BasicStats.BasicStats()
                self.gamma.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "spr"
        try :
            if data[varName] != None:
                self.spr=BasicStats.BasicStats()
                self.spr.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
