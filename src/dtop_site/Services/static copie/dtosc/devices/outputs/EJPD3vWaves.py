import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import EJPD3v
from ...outputs import EJPD3vMonthly
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EJPD3vWaves():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._hs_tp_dp=EJPD3v.EJPD3v()
        self._hs_tp_dp_monthly=EJPD3vMonthly.EJPD3vMonthly()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def hs_tp_dp(self): # pragma: no cover
        return self._hs_tp_dp
    #------------
    @ property
    def hs_tp_dp_monthly(self): # pragma: no cover
        return self._hs_tp_dp_monthly
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ hs_tp_dp.setter
    def hs_tp_dp(self,val): # pragma: no cover
        self._hs_tp_dp=val
    #------------
    @ hs_tp_dp_monthly.setter
    def hs_tp_dp_monthly(self,val): # pragma: no cover
        self._hs_tp_dp_monthly=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EJPD3vWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EJPD3vWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("hs_tp_dp"):
            if (short and not(deep)):
                rep["hs_tp_dp"] = self.hs_tp_dp.type_rep()
            else:
                rep["hs_tp_dp"] = self.hs_tp_dp.prop_rep(short, deep)
        if self.is_set("hs_tp_dp_monthly"):
            if (short and not(deep)):
                rep["hs_tp_dp_monthly"] = self.hs_tp_dp_monthly.type_rep()
            else:
                rep["hs_tp_dp_monthly"] = self.hs_tp_dp_monthly.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "hs_tp_dp"
        try :
            if data[varName] != None:
                self.hs_tp_dp=EJPD3v.EJPD3v()
                self.hs_tp_dp.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "hs_tp_dp_monthly"
        try :
            if data[varName] != None:
                self.hs_tp_dp_monthly=EJPD3vMonthly.EJPD3vMonthly()
                self.hs_tp_dp_monthly.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
