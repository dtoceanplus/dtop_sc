import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from . import BasicWaves
from . import EPDWaves
from . import EJPDWaves
from . import ENVSWaves
from . import EJPD3vWaves
from . import EXTWaves
from . import EXCWaves
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class StatsWaves():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._Basic=BasicWaves.BasicWaves()
        self._EPD=EPDWaves.EPDWaves()
        self._EJPD=EJPDWaves.EJPDWaves()
        self._ENVS=ENVSWaves.ENVSWaves()
        self._EJPD3v=EJPD3vWaves.EJPD3vWaves()
        self._EXT=EXTWaves.EXTWaves()
        self._EXC=EXCWaves.EXCWaves()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def Basic(self): # pragma: no cover
        return self._Basic
    #------------
    @ property
    def EPD(self): # pragma: no cover
        return self._EPD
    #------------
    @ property
    def EJPD(self): # pragma: no cover
        return self._EJPD
    #------------
    @ property
    def ENVS(self): # pragma: no cover
        return self._ENVS
    #------------
    @ property
    def EJPD3v(self): # pragma: no cover
        return self._EJPD3v
    #------------
    @ property
    def EXT(self): # pragma: no cover
        return self._EXT
    #------------
    @ property
    def EXC(self): # pragma: no cover
        return self._EXC
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ Basic.setter
    def Basic(self,val): # pragma: no cover
        self._Basic=val
    #------------
    @ EPD.setter
    def EPD(self,val): # pragma: no cover
        self._EPD=val
    #------------
    @ EJPD.setter
    def EJPD(self,val): # pragma: no cover
        self._EJPD=val
    #------------
    @ ENVS.setter
    def ENVS(self,val): # pragma: no cover
        self._ENVS=val
    #------------
    @ EJPD3v.setter
    def EJPD3v(self,val): # pragma: no cover
        self._EJPD3v=val
    #------------
    @ EXT.setter
    def EXT(self,val): # pragma: no cover
        self._EXT=val
    #------------
    @ EXC.setter
    def EXC(self,val): # pragma: no cover
        self._EXC=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:StatsWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:StatsWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("Basic"):
            if (short and not(deep)):
                rep["Basic"] = self.Basic.type_rep()
            else:
                rep["Basic"] = self.Basic.prop_rep(short, deep)
        if self.is_set("EPD"):
            if (short and not(deep)):
                rep["EPD"] = self.EPD.type_rep()
            else:
                rep["EPD"] = self.EPD.prop_rep(short, deep)
        if self.is_set("EJPD"):
            if (short and not(deep)):
                rep["EJPD"] = self.EJPD.type_rep()
            else:
                rep["EJPD"] = self.EJPD.prop_rep(short, deep)
        if self.is_set("ENVS"):
            if (short and not(deep)):
                rep["ENVS"] = self.ENVS.type_rep()
            else:
                rep["ENVS"] = self.ENVS.prop_rep(short, deep)
        if self.is_set("EJPD3v"):
            if (short and not(deep)):
                rep["EJPD3v"] = self.EJPD3v.type_rep()
            else:
                rep["EJPD3v"] = self.EJPD3v.prop_rep(short, deep)
        if self.is_set("EXT"):
            if (short and not(deep)):
                rep["EXT"] = self.EXT.type_rep()
            else:
                rep["EXT"] = self.EXT.prop_rep(short, deep)
        if self.is_set("EXC"):
            if (short and not(deep)):
                rep["EXC"] = self.EXC.type_rep()
            else:
                rep["EXC"] = self.EXC.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "Basic"
        try :
            if data[varName] != None:
                self.Basic=BasicWaves.BasicWaves()
                self.Basic.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EPD"
        try :
            if data[varName] != None:
                self.EPD=EPDWaves.EPDWaves()
                self.EPD.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EJPD"
        try :
            if data[varName] != None:
                self.EJPD=EJPDWaves.EJPDWaves()
                self.EJPD.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "ENVS"
        try :
            if data[varName] != None:
                self.ENVS=ENVSWaves.ENVSWaves()
                self.ENVS.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EJPD3v"
        try :
            if data[varName] != None:
                self.EJPD3v=EJPD3vWaves.EJPD3vWaves()
                self.EJPD3v.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXT"
        try :
            if data[varName] != None:
                self.EXT=EXTWaves.EXTWaves()
                self.EXT.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "EXC"
        try :
            if data[varName] != None:
                self.EXC=EXCWaves.EXCWaves()
                self.EXC.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
