import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from . import StatsWaves
from . import StatsCurrents
from . import StatsWinds
from . import StatsWaterLevels
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Statistics():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._waves=StatsWaves.StatsWaves()
        self._currents=StatsCurrents.StatsCurrents()
        self._winds=StatsWinds.StatsWinds()
        self._water_levels=StatsWaterLevels.StatsWaterLevels()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def waves(self): # pragma: no cover
        return self._waves
    #------------
    @ property
    def currents(self): # pragma: no cover
        return self._currents
    #------------
    @ property
    def winds(self): # pragma: no cover
        return self._winds
    #------------
    @ property
    def water_levels(self): # pragma: no cover
        return self._water_levels
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ waves.setter
    def waves(self,val): # pragma: no cover
        self._waves=val
    #------------
    @ currents.setter
    def currents(self,val): # pragma: no cover
        self._currents=val
    #------------
    @ winds.setter
    def winds(self,val): # pragma: no cover
        self._winds=val
    #------------
    @ water_levels.setter
    def water_levels(self,val): # pragma: no cover
        self._water_levels=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:Statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:Statistics"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("waves"):
            if (short and not(deep)):
                rep["waves"] = self.waves.type_rep()
            else:
                rep["waves"] = self.waves.prop_rep(short, deep)
        if self.is_set("currents"):
            if (short and not(deep)):
                rep["currents"] = self.currents.type_rep()
            else:
                rep["currents"] = self.currents.prop_rep(short, deep)
        if self.is_set("winds"):
            if (short and not(deep)):
                rep["winds"] = self.winds.type_rep()
            else:
                rep["winds"] = self.winds.prop_rep(short, deep)
        if self.is_set("water_levels"):
            if (short and not(deep)):
                rep["water_levels"] = self.water_levels.type_rep()
            else:
                rep["water_levels"] = self.water_levels.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "waves"
        try :
            if data[varName] != None:
                self.waves=StatsWaves.StatsWaves()
                self.waves.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "currents"
        try :
            if data[varName] != None:
                self.currents=StatsCurrents.StatsCurrents()
                self.currents.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "winds"
        try :
            if data[varName] != None:
                self.winds=StatsWinds.StatsWinds()
                self.winds.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "water_levels"
        try :
            if data[varName] != None:
                self.water_levels=StatsWaterLevels.StatsWaterLevels()
                self.water_levels.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
