import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import DirectValuesDevices
from . import TimeSeries
from . import Statistics
from ...outputs import Info
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DeviceResults():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._direct_values=DirectValuesDevices.DirectValuesDevices()
        self._time_series=TimeSeries.TimeSeries()
        self._statistics=Statistics.Statistics()
        self._info=Info.Info()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def direct_values(self): # pragma: no cover
        return self._direct_values
    #------------
    @ property
    def time_series(self): # pragma: no cover
        return self._time_series
    #------------
    @ property
    def statistics(self): # pragma: no cover
        return self._statistics
    #------------
    @ property
    def info(self): # pragma: no cover
        return self._info
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ direct_values.setter
    def direct_values(self,val): # pragma: no cover
        self._direct_values=val
    #------------
    @ time_series.setter
    def time_series(self,val): # pragma: no cover
        self._time_series=val
    #------------
    @ statistics.setter
    def statistics(self,val): # pragma: no cover
        self._statistics=val
    #------------
    @ info.setter
    def info(self,val): # pragma: no cover
        self._info=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:DeviceResults"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:DeviceResults"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("direct_values"):
            if (short and not(deep)):
                rep["direct_values"] = self.direct_values.type_rep()
            else:
                rep["direct_values"] = self.direct_values.prop_rep(short, deep)
        if self.is_set("time_series"):
            if (short and not(deep)):
                rep["time_series"] = self.time_series.type_rep()
            else:
                rep["time_series"] = self.time_series.prop_rep(short, deep)
        if self.is_set("statistics"):
            if (short and not(deep)):
                rep["statistics"] = self.statistics.type_rep()
            else:
                rep["statistics"] = self.statistics.prop_rep(short, deep)
        if self.is_set("info"):
            if (short and not(deep)):
                rep["info"] = self.info.type_rep()
            else:
                rep["info"] = self.info.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "direct_values"
        try :
            if data[varName] != None:
                self.direct_values=DirectValuesDevices.DirectValuesDevices()
                self.direct_values.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "time_series"
        try :
            if data[varName] != None:
                self.time_series=TimeSeries.TimeSeries()
                self.time_series.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "statistics"
        try :
            if data[varName] != None:
                self.statistics=Statistics.Statistics()
                self.statistics.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "info"
        try :
            if data[varName] != None:
                self.info=Info.Info()
                self.info.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
