import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import EPD
from ...outputs import EPDMonthly
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class EPDWaves():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._hs=EPD.EPD()
        self._hs_monthly=EPDMonthly.EPDMonthly()
        self._tp=EPD.EPD()
        self._tp_monthly=EPDMonthly.EPDMonthly()
        self._dp=EPD.EPD()
        self._dp_monthly=EPDMonthly.EPDMonthly()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def hs(self): # pragma: no cover
        return self._hs
    #------------
    @ property
    def hs_monthly(self): # pragma: no cover
        return self._hs_monthly
    #------------
    @ property
    def tp(self): # pragma: no cover
        return self._tp
    #------------
    @ property
    def tp_monthly(self): # pragma: no cover
        return self._tp_monthly
    #------------
    @ property
    def dp(self): # pragma: no cover
        return self._dp
    #------------
    @ property
    def dp_monthly(self): # pragma: no cover
        return self._dp_monthly
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ hs.setter
    def hs(self,val): # pragma: no cover
        self._hs=val
    #------------
    @ hs_monthly.setter
    def hs_monthly(self,val): # pragma: no cover
        self._hs_monthly=val
    #------------
    @ tp.setter
    def tp(self,val): # pragma: no cover
        self._tp=val
    #------------
    @ tp_monthly.setter
    def tp_monthly(self,val): # pragma: no cover
        self._tp_monthly=val
    #------------
    @ dp.setter
    def dp(self,val): # pragma: no cover
        self._dp=val
    #------------
    @ dp_monthly.setter
    def dp_monthly(self,val): # pragma: no cover
        self._dp_monthly=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EPDWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:EPDWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("hs"):
            if (short and not(deep)):
                rep["hs"] = self.hs.type_rep()
            else:
                rep["hs"] = self.hs.prop_rep(short, deep)
        if self.is_set("hs_monthly"):
            if (short and not(deep)):
                rep["hs_monthly"] = self.hs_monthly.type_rep()
            else:
                rep["hs_monthly"] = self.hs_monthly.prop_rep(short, deep)
        if self.is_set("tp"):
            if (short and not(deep)):
                rep["tp"] = self.tp.type_rep()
            else:
                rep["tp"] = self.tp.prop_rep(short, deep)
        if self.is_set("tp_monthly"):
            if (short and not(deep)):
                rep["tp_monthly"] = self.tp_monthly.type_rep()
            else:
                rep["tp_monthly"] = self.tp_monthly.prop_rep(short, deep)
        if self.is_set("dp"):
            if (short and not(deep)):
                rep["dp"] = self.dp.type_rep()
            else:
                rep["dp"] = self.dp.prop_rep(short, deep)
        if self.is_set("dp_monthly"):
            if (short and not(deep)):
                rep["dp_monthly"] = self.dp_monthly.type_rep()
            else:
                rep["dp_monthly"] = self.dp_monthly.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "hs"
        try :
            if data[varName] != None:
                self.hs=EPD.EPD()
                self.hs.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "hs_monthly"
        try :
            if data[varName] != None:
                self.hs_monthly=EPDMonthly.EPDMonthly()
                self.hs_monthly.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "tp"
        try :
            if data[varName] != None:
                self.tp=EPD.EPD()
                self.tp.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "tp_monthly"
        try :
            if data[varName] != None:
                self.tp_monthly=EPDMonthly.EPDMonthly()
                self.tp_monthly.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "dp"
        try :
            if data[varName] != None:
                self.dp=EPD.EPD()
                self.dp.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "dp_monthly"
        try :
            if data[varName] != None:
                self.dp_monthly=EPDMonthly.EPDMonthly()
                self.dp_monthly.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
