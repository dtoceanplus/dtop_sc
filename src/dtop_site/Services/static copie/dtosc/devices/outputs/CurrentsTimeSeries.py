import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ...outputs import LocalisedTimeSeries
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class CurrentsTimeSeries():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._mag=LocalisedTimeSeries.LocalisedTimeSeries()
        self._theta=LocalisedTimeSeries.LocalisedTimeSeries()
        self._U=LocalisedTimeSeries.LocalisedTimeSeries()
        self._V=LocalisedTimeSeries.LocalisedTimeSeries()
        self._Flux=LocalisedTimeSeries.LocalisedTimeSeries()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def mag(self): # pragma: no cover
        return self._mag
    #------------
    @ property
    def theta(self): # pragma: no cover
        return self._theta
    #------------
    @ property
    def U(self): # pragma: no cover
        return self._U
    #------------
    @ property
    def V(self): # pragma: no cover
        return self._V
    #------------
    @ property
    def Flux(self): # pragma: no cover
        return self._Flux
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ mag.setter
    def mag(self,val): # pragma: no cover
        self._mag=val
    #------------
    @ theta.setter
    def theta(self,val): # pragma: no cover
        self._theta=val
    #------------
    @ U.setter
    def U(self,val): # pragma: no cover
        self._U=val
    #------------
    @ V.setter
    def V(self,val): # pragma: no cover
        self._V=val
    #------------
    @ Flux.setter
    def Flux(self,val): # pragma: no cover
        self._Flux=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:CurrentsTimeSeries"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:CurrentsTimeSeries"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("mag"):
            if (short and not(deep)):
                rep["mag"] = self.mag.type_rep()
            else:
                rep["mag"] = self.mag.prop_rep(short, deep)
        if self.is_set("theta"):
            if (short and not(deep)):
                rep["theta"] = self.theta.type_rep()
            else:
                rep["theta"] = self.theta.prop_rep(short, deep)
        if self.is_set("U"):
            if (short and not(deep)):
                rep["U"] = self.U.type_rep()
            else:
                rep["U"] = self.U.prop_rep(short, deep)
        if self.is_set("V"):
            if (short and not(deep)):
                rep["V"] = self.V.type_rep()
            else:
                rep["V"] = self.V.prop_rep(short, deep)
        if self.is_set("Flux"):
            if (short and not(deep)):
                rep["Flux"] = self.Flux.type_rep()
            else:
                rep["Flux"] = self.Flux.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "mag"
        try :
            if data[varName] != None:
                self.mag=LocalisedTimeSeries.LocalisedTimeSeries()
                self.mag.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "theta"
        try :
            if data[varName] != None:
                self.theta=LocalisedTimeSeries.LocalisedTimeSeries()
                self.theta.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "U"
        try :
            if data[varName] != None:
                self.U=LocalisedTimeSeries.LocalisedTimeSeries()
                self.U.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "V"
        try :
            if data[varName] != None:
                self.V=LocalisedTimeSeries.LocalisedTimeSeries()
                self.V.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "Flux"
        try :
            if data[varName] != None:
                self.Flux=LocalisedTimeSeries.LocalisedTimeSeries()
                self.Flux.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
