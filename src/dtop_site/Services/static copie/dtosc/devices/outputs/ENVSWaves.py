import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class ENVSWaves():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._latitude=0.0
        self._longitude=0.0
        self._hs_dp_proba=np.zeros(shape=(1), dtype=float)
        self._intervals_hs_min=np.zeros(shape=(1), dtype=float)
        self._intervals_hs_max=np.zeros(shape=(1), dtype=float)
        self._intervals_dp_min=np.zeros(shape=(1), dtype=float)
        self._intervals_dp_max=np.zeros(shape=(1), dtype=float)
        self._associated_tp=np.zeros(shape=(1), dtype=float)
        self._associated_cur=np.zeros(shape=(1), dtype=float)
        self._associated_dircur=np.zeros(shape=(1), dtype=float)
        self._associated_wind=np.zeros(shape=(1), dtype=float)
        self._associated_dirwind=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def latitude(self): # pragma: no cover
        return self._latitude
    #------------
    @ property
    def longitude(self): # pragma: no cover
        return self._longitude
    #------------
    @ property
    def hs_dp_proba(self): # pragma: no cover
        return self._hs_dp_proba
    #------------
    @ property
    def intervals_hs_min(self): # pragma: no cover
        return self._intervals_hs_min
    #------------
    @ property
    def intervals_hs_max(self): # pragma: no cover
        return self._intervals_hs_max
    #------------
    @ property
    def intervals_dp_min(self): # pragma: no cover
        return self._intervals_dp_min
    #------------
    @ property
    def intervals_dp_max(self): # pragma: no cover
        return self._intervals_dp_max
    #------------
    @ property
    def associated_tp(self): # pragma: no cover
        return self._associated_tp
    #------------
    @ property
    def associated_cur(self): # pragma: no cover
        return self._associated_cur
    #------------
    @ property
    def associated_dircur(self): # pragma: no cover
        return self._associated_dircur
    #------------
    @ property
    def associated_wind(self): # pragma: no cover
        return self._associated_wind
    #------------
    @ property
    def associated_dirwind(self): # pragma: no cover
        return self._associated_dirwind
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ latitude.setter
    def latitude(self,val): # pragma: no cover
        self._latitude=float(val)
    #------------
    @ longitude.setter
    def longitude(self,val): # pragma: no cover
        self._longitude=float(val)
    #------------
    @ hs_dp_proba.setter
    def hs_dp_proba(self,val): # pragma: no cover
        self._hs_dp_proba=val
    #------------
    @ intervals_hs_min.setter
    def intervals_hs_min(self,val): # pragma: no cover
        self._intervals_hs_min=val
    #------------
    @ intervals_hs_max.setter
    def intervals_hs_max(self,val): # pragma: no cover
        self._intervals_hs_max=val
    #------------
    @ intervals_dp_min.setter
    def intervals_dp_min(self,val): # pragma: no cover
        self._intervals_dp_min=val
    #------------
    @ intervals_dp_max.setter
    def intervals_dp_max(self,val): # pragma: no cover
        self._intervals_dp_max=val
    #------------
    @ associated_tp.setter
    def associated_tp(self,val): # pragma: no cover
        self._associated_tp=val
    #------------
    @ associated_cur.setter
    def associated_cur(self,val): # pragma: no cover
        self._associated_cur=val
    #------------
    @ associated_dircur.setter
    def associated_dircur(self,val): # pragma: no cover
        self._associated_dircur=val
    #------------
    @ associated_wind.setter
    def associated_wind(self,val): # pragma: no cover
        self._associated_wind=val
    #------------
    @ associated_dirwind.setter
    def associated_dirwind(self,val): # pragma: no cover
        self._associated_dirwind=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:ENVSWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosc:devices:outputs:ENVSWaves"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("latitude"):
            rep["latitude"] = self.latitude
        if self.is_set("longitude"):
            rep["longitude"] = self.longitude
        if self.is_set("hs_dp_proba"):
            if (short):
                rep["hs_dp_proba"] = str(self.hs_dp_proba.shape)
            else:
                try:
                    rep["hs_dp_proba"] = self.hs_dp_proba.tolist()
                except:
                    rep["hs_dp_proba"] = self.hs_dp_proba
        if self.is_set("intervals_hs_min"):
            if (short):
                rep["intervals_hs_min"] = str(self.intervals_hs_min.shape)
            else:
                try:
                    rep["intervals_hs_min"] = self.intervals_hs_min.tolist()
                except:
                    rep["intervals_hs_min"] = self.intervals_hs_min
        if self.is_set("intervals_hs_max"):
            if (short):
                rep["intervals_hs_max"] = str(self.intervals_hs_max.shape)
            else:
                try:
                    rep["intervals_hs_max"] = self.intervals_hs_max.tolist()
                except:
                    rep["intervals_hs_max"] = self.intervals_hs_max
        if self.is_set("intervals_dp_min"):
            if (short):
                rep["intervals_dp_min"] = str(self.intervals_dp_min.shape)
            else:
                try:
                    rep["intervals_dp_min"] = self.intervals_dp_min.tolist()
                except:
                    rep["intervals_dp_min"] = self.intervals_dp_min
        if self.is_set("intervals_dp_max"):
            if (short):
                rep["intervals_dp_max"] = str(self.intervals_dp_max.shape)
            else:
                try:
                    rep["intervals_dp_max"] = self.intervals_dp_max.tolist()
                except:
                    rep["intervals_dp_max"] = self.intervals_dp_max
        if self.is_set("associated_tp"):
            if (short):
                rep["associated_tp"] = str(self.associated_tp.shape)
            else:
                try:
                    rep["associated_tp"] = self.associated_tp.tolist()
                except:
                    rep["associated_tp"] = self.associated_tp
        if self.is_set("associated_cur"):
            if (short):
                rep["associated_cur"] = str(self.associated_cur.shape)
            else:
                try:
                    rep["associated_cur"] = self.associated_cur.tolist()
                except:
                    rep["associated_cur"] = self.associated_cur
        if self.is_set("associated_dircur"):
            if (short):
                rep["associated_dircur"] = str(self.associated_dircur.shape)
            else:
                try:
                    rep["associated_dircur"] = self.associated_dircur.tolist()
                except:
                    rep["associated_dircur"] = self.associated_dircur
        if self.is_set("associated_wind"):
            if (short):
                rep["associated_wind"] = str(self.associated_wind.shape)
            else:
                try:
                    rep["associated_wind"] = self.associated_wind.tolist()
                except:
                    rep["associated_wind"] = self.associated_wind
        if self.is_set("associated_dirwind"):
            if (short):
                rep["associated_dirwind"] = str(self.associated_dirwind.shape)
            else:
                try:
                    rep["associated_dirwind"] = self.associated_dirwind.tolist()
                except:
                    rep["associated_dirwind"] = self.associated_dirwind
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "latitude"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "longitude"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "hs_dp_proba"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_hs_min"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_hs_max"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_dp_min"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "intervals_dp_max"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_tp"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_cur"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_dircur"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_wind"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "associated_dirwind"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
