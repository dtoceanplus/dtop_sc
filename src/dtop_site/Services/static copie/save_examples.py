#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 11:32:32 2020

@author: youen
"""

import json
import yaml
import os
PATH_SCHEMAS = '/Users/youen/GIT/dtop-site/src/dtop_site/Services/static/schemas'
PATH_EXAMPLES = '/Users/youen/GIT/dtop-site/src/dtop_site/Services/static/schemas/examples'

# Open complete json file (from SC run)
file = '/Users/youen/GIT/dtop-site/storage/Modif_ID_CPX3_2D.json'
with open(file) as jsonfile:
    data = json.load(jsonfile)

# Open openapi.yaml file (in static)
with open('/Users/youen/GIT/dtop-site/src/dtop_site/Services/static/openapi.yaml', 'r') as stream:
    myYaml = yaml.load(stream)
yaml_paths = myYaml['paths']

for p in yaml_paths:
    ref_file = myYaml['paths'][p]['$ref']
    # routes of the dico
    routes = p.split('/')[3:]
    for elt in routes:
        if elt.startswith('{'):
            routes.remove(elt)
    mystring = ''
    for elt in routes:
        if elt == 'devices':
            mystring += "['" + str(elt) + "'][0]"
        else:
            mystring += "['" + str(elt) + "']"
    cmd = "my_data = data" + mystring
    exec(cmd)
    # open file in 'paths' folder
    with open(ref_file) as myref:
        sc = yaml.load(myref)
        my_schema = sc['get']['responses'][200]['content']['application/json']['schema']['$ref']
        my_schema_file = os.path.join(PATH_SCHEMAS,(my_schema.split('#')[0]).split('/')[-1])
        # open file in 'schemas' folder
        with open(my_schema_file) as mysc:
            ex = yaml.load(mysc)
            for key in ex.keys():
                if key.startswith('dtosc'):
                    my_key = key 
            my_example = ex[my_key]['example']['$ref']
            my_outfile = os.path.join(PATH_SCHEMAS, my_example)
            if os.path.isfile(my_outfile):
                print("Example %s already exists" %my_outfile)
            else:
                print("Example %s will be created" %my_outfile) 
                with open(my_outfile, 'w') as jsonfile:
                    json.dump(my_data, jsonfile)