from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
from dtop_site.BusinessLogic.libraries.dtosc.outputs.Site import Site
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('getResults', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

##############################
# Functions for project load #
##############################

# List all available projects files
@bp.route('/Projects', methods=['GET'])
def get_all_available_projects():
    [folder,folder_name,files]=list_files('./storage','json')
    return jsonify(files)

# Load project file
def load_Project(ProjectId):
    project_name=ProjectId
    filename=os.path.join('./storage',project_name+'.json')
    if os.path.exists(filename):
        project=Site()
        project.loadJSON(filePath = filename)
    else:
        abort(404)
    return project

# Find object index in array from its name
def find_index_from_name(name,object_array):
    index=-1
    for idx in range(0,len(object_array)):
        if name==object_array[idx].name:
            index=idx
    if index==-1:
        abort(404)
    return index

###########
# GET API #
###########

@bp.route('/<ProjectId>', methods=['GET'])
def get_Project(ProjectId):
    project=load_Project(ProjectId)
    return jsonify(project.prop_rep())


@bp.route('/<ProjectId>/farm', methods=['GET'])
def get_project_farm(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values', methods=['GET'])
def get_project_farm_direct_values(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/bathymetry', methods=['GET'])
def get_project_farm_direct_values_bathymetry(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.bathymetry.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/seabed_type', methods=['GET'])
def get_project_farm_direct_values_seabed_type(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.seabed_type.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/roughness_length', methods=['GET'])
def get_project_farm_direct_values_roughness_length(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.roughness_length.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/direct_values/marine_species', methods=['GET'])
def get_project_farm_direct_values_marine_species(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.direct_values.marine_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/info', methods=['GET'])
def get_project_farm_info(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.info.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices', methods=['GET'])
def get_project_farm_scenarii_matrices(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices/currents', methods=['GET'])
def get_project_farm_scenarii_matrices_currents(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.currents.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/farm/scenarii_matrices/waves', methods=['GET'])
def get_project_farm_scenarii_matrices_waves(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.farm.scenarii_matrices.waves.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor', methods=['GET'])
def get_project_corridor(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values', methods=['GET'])
def get_project_corridor_direct_values(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/bathymetry', methods=['GET'])
def get_project_corridor_direct_values_bathymetry(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.bathymetry.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/seabed_type', methods=['GET'])
def get_project_corridor_direct_values_seabed_type(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.seabed_type.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/roughness_length', methods=['GET'])
def get_project_corridor_direct_values_roughness_length(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.roughness_length.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/direct_values/marine_species', methods=['GET'])
def get_project_corridor_direct_values_marine_species(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.direct_values.marine_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/corridor/info', methods=['GET'])
def get_project_corridor_info(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.corridor.info.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/databases_paths', methods=['GET'])
def get_project_databases_paths(ProjectId):
    project=load_Project(ProjectId)
    try:
        res=project.databases_paths.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>', methods=['GET'])
def get_project_devices(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/direct_values', methods=['GET'])
def get_project_devices_direct_values(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].direct_values.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/direct_values/bathymetry', methods=['GET'])
def get_project_devices_direct_values_bathymetry(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].direct_values.bathymetry.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/direct_values/slope', methods=['GET'])
def get_project_devices_direct_values_slope(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].direct_values.slope.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/direct_values/seabed_type', methods=['GET'])
def get_project_devices_direct_values_seabed_type(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].direct_values.seabed_type.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/direct_values/roughness_length', methods=['GET'])
def get_project_devices_direct_values_roughness_length(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].direct_values.roughness_length.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/direct_values/marine_species', methods=['GET'])
def get_project_devices_direct_values_marine_species(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].direct_values.marine_species.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series', methods=['GET'])
def get_project_devices_time_series(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves', methods=['GET'])
def get_project_devices_time_series_waves(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/hs', methods=['GET'])
def get_project_devices_time_series_waves_hs(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/tp', methods=['GET'])
def get_project_devices_time_series_waves_tp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/dp', methods=['GET'])
def get_project_devices_time_series_waves_dp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/te', methods=['GET'])
def get_project_devices_time_series_waves_te(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.te.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/gamma', methods=['GET'])
def get_project_devices_time_series_waves_gamma(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.gamma.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/spr', methods=['GET'])
def get_project_devices_time_series_waves_spr(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.spr.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/waves/CgE', methods=['GET'])
def get_project_devices_time_series_waves_CgE(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.waves.CgE.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/currents', methods=['GET'])
def get_project_devices_time_series_currents(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.currents.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/currents/mag', methods=['GET'])
def get_project_devices_time_series_currents_mag(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.currents.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/currents/theta', methods=['GET'])
def get_project_devices_time_series_currents_theta(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.currents.theta.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/currents/U', methods=['GET'])
def get_project_devices_time_series_currents_U(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.currents.U.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/currents/V', methods=['GET'])
def get_project_devices_time_series_currents_V(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.currents.V.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/currents/Flux', methods=['GET'])
def get_project_devices_time_series_currents_Flux(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.currents.Flux.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/winds', methods=['GET'])
def get_project_devices_time_series_winds(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.winds.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/winds/mag10', methods=['GET'])
def get_project_devices_time_series_winds_mag10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.winds.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/winds/theta10', methods=['GET'])
def get_project_devices_time_series_winds_theta10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.winds.theta10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/winds/U10', methods=['GET'])
def get_project_devices_time_series_winds_U10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.winds.U10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/winds/V10', methods=['GET'])
def get_project_devices_time_series_winds_V10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.winds.V10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/winds/gust10', methods=['GET'])
def get_project_devices_time_series_winds_gust10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.winds.gust10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/water_levels', methods=['GET'])
def get_project_devices_time_series_water_levels(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.water_levels.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/water_levels/XE', methods=['GET'])
def get_project_devices_time_series_water_levels_XE(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.water_levels.XE.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/time_series/water_levels/WLEV', methods=['GET'])
def get_project_devices_time_series_water_levels_WLEV(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].time_series.water_levels.WLEV.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics', methods=['GET'])
def get_project_devices_statistics(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves', methods=['GET'])
def get_project_devices_statistics_waves(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/Basic', methods=['GET'])
def get_project_devices_statistics_waves_Basic(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/Basic/hs', methods=['GET'])
def get_project_devices_statistics_waves_Basic_hs(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.Basic.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/Basic/tp', methods=['GET'])
def get_project_devices_statistics_waves_Basic_tp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.Basic.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/Basic/CgE', methods=['GET'])
def get_project_devices_statistics_waves_Basic_CgE(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.Basic.CgE.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/Basic/gamma', methods=['GET'])
def get_project_devices_statistics_waves_Basic_gamma(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.Basic.gamma.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/Basic/spr', methods=['GET'])
def get_project_devices_statistics_waves_Basic_spr(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.Basic.spr.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD', methods=['GET'])
def get_project_devices_statistics_waves_EPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD/hs', methods=['GET'])
def get_project_devices_statistics_waves_EPD_hs(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD/hs_monthly', methods=['GET'])
def get_project_devices_statistics_waves_EPD_hs_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.hs_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD/tp', methods=['GET'])
def get_project_devices_statistics_waves_EPD_tp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD/tp_monthly', methods=['GET'])
def get_project_devices_statistics_waves_EPD_tp_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.tp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD/dp', methods=['GET'])
def get_project_devices_statistics_waves_EPD_dp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EPD/dp_monthly', methods=['GET'])
def get_project_devices_statistics_waves_EPD_dp_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EPD.dp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD', methods=['GET'])
def get_project_devices_statistics_waves_EJPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD/hs_tp', methods=['GET'])
def get_project_devices_statistics_waves_EJPD_hs_tp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD.hs_tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD/hs_tp_monthly', methods=['GET'])
def get_project_devices_statistics_waves_EJPD_hs_tp_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD.hs_tp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD/hs_dp', methods=['GET'])
def get_project_devices_statistics_waves_EJPD_hs_dp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD.hs_dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD/hs_dp_monthly', methods=['GET'])
def get_project_devices_statistics_waves_EJPD_hs_dp_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD.hs_dp_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/ENVS', methods=['GET'])
def get_project_devices_statistics_waves_ENVS(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.ENVS.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD3v', methods=['GET'])
def get_project_devices_statistics_waves_EJPD3v(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD3v.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EJPD3v/hs_tp_dp', methods=['GET'])
def get_project_devices_statistics_waves_EJPD3v_hs_tp_dp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EJPD3v.hs_tp_dp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EXT', methods=['GET'])
def get_project_devices_statistics_waves_EXT(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EXT/hs', methods=['GET'])
def get_project_devices_statistics_waves_EXT_hs(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EXT.hs.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EXT/tp', methods=['GET'])
def get_project_devices_statistics_waves_EXT_tp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EXT.tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EXT/gamma', methods=['GET'])
def get_project_devices_statistics_waves_EXT_gamma(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EXT.gamma.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EXC', methods=['GET'])
def get_project_devices_statistics_waves_EXC(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EXC.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/waves/EXC/hs_tp', methods=['GET'])
def get_project_devices_statistics_waves_EXC_hs_tp(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.waves.EXC.hs_tp.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents', methods=['GET'])
def get_project_devices_statistics_currents(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/Basic', methods=['GET'])
def get_project_devices_statistics_currents_Basic(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/Basic/mag', methods=['GET'])
def get_project_devices_statistics_currents_Basic_mag(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.Basic.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/Basic/Flux', methods=['GET'])
def get_project_devices_statistics_currents_Basic_Flux(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.Basic.Flux.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EPD', methods=['GET'])
def get_project_devices_statistics_currents_EPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EPD/mag', methods=['GET'])
def get_project_devices_statistics_currents_EPD_mag(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EPD.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EPD/mag_monthly', methods=['GET'])
def get_project_devices_statistics_currents_EPD_mag_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EPD.mag_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EPD/theta', methods=['GET'])
def get_project_devices_statistics_currents_EPD_theta(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EPD.theta.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EPD/theta_monthly', methods=['GET'])
def get_project_devices_statistics_currents_EPD_theta_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EPD.theta_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EJPD', methods=['GET'])
def get_project_devices_statistics_currents_EJPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EJPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EJPD/mag_theta', methods=['GET'])
def get_project_devices_statistics_currents_EJPD_mag_theta(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EJPD.mag_theta.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EJPD/mag_theta_monthly', methods=['GET'])
def get_project_devices_statistics_currents_EJPD_mag_theta_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EJPD.mag_theta_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EXT', methods=['GET'])
def get_project_devices_statistics_currents_EXT(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/currents/EXT/mag', methods=['GET'])
def get_project_devices_statistics_currents_EXT_mag(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.currents.EXT.mag.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds', methods=['GET'])
def get_project_devices_statistics_winds(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/Basic', methods=['GET'])
def get_project_devices_statistics_winds_Basic(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/Basic/mag10', methods=['GET'])
def get_project_devices_statistics_winds_Basic_mag10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.Basic.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EPD', methods=['GET'])
def get_project_devices_statistics_winds_EPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EPD/mag10', methods=['GET'])
def get_project_devices_statistics_winds_EPD_mag10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EPD.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EPD/mag10_monthly', methods=['GET'])
def get_project_devices_statistics_winds_EPD_mag10_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EPD.mag10_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EPD/theta10', methods=['GET'])
def get_project_devices_statistics_winds_EPD_theta10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EPD.theta10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EPD/theta10_monthly', methods=['GET'])
def get_project_devices_statistics_winds_EPD_theta10_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EPD.theta10_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EJPD', methods=['GET'])
def get_project_devices_statistics_winds_EJPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EJPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EJPD/mag10_theta10', methods=['GET'])
def get_project_devices_statistics_winds_EJPD_mag10_theta10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EJPD.mag10_theta10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EJPD/mag10_theta10_monthly', methods=['GET'])
def get_project_devices_statistics_winds_EJPD_mag10_theta10_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EJPD.mag10_theta10_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EXT', methods=['GET'])
def get_project_devices_statistics_winds_EXT(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EXT/mag10', methods=['GET'])
def get_project_devices_statistics_winds_EXT_mag10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EXT.mag10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/winds/EXT/gust10', methods=['GET'])
def get_project_devices_statistics_winds_EXT_gust10(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.winds.EXT.gust10.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels', methods=['GET'])
def get_project_devices_statistics_water_levels(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/Basic', methods=['GET'])
def get_project_devices_statistics_water_levels_Basic(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.Basic.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/Basic/WLEV', methods=['GET'])
def get_project_devices_statistics_water_levels_Basic_WLEV(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.Basic.WLEV.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/EPD', methods=['GET'])
def get_project_devices_statistics_water_levels_EPD(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.EPD.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/EPD/WLEV', methods=['GET'])
def get_project_devices_statistics_water_levels_EPD_WLEV(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.EPD.WLEV.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/EPD/WLEV_monthly', methods=['GET'])
def get_project_devices_statistics_water_levels_EPD_WLEV_monthly(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.EPD.WLEV_monthly.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/EXT', methods=['GET'])
def get_project_devices_statistics_water_levels_EXT(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.EXT.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/EXT/WLEVnegative', methods=['GET'])
def get_project_devices_statistics_water_levels_EXT_WLEVnegative(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.EXT.WLEVnegative.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/statistics/water_levels/EXT/WLEVpositive', methods=['GET'])
def get_project_devices_statistics_water_levels_EXT_WLEVpositive(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].statistics.water_levels.EXT.WLEVpositive.prop_rep()
    except:
        abort(404)
    return jsonify(res)

@bp.route('/<ProjectId>/devices/<devicesId>/info', methods=['GET'])
def get_project_devices_info(ProjectId,devicesId):
    project=load_Project(ProjectId)
    try:
        devices_idx=find_index_from_name(devicesId,project.devices)
        res=project.devices[devices_idx].info.prop_rep()
    except:
        abort(404)
    return jsonify(res)

#####################
# Utility functions #
#####################

def list_files(directory, extension):
    fl=listdir(directory)
    folder=[]
    folder_name=[]
    files=[]
    for i in range(0,len(fl)):
        path=os.path.join(directory, fl[i])
        path=os.path.abspath(path)
        if os.path.isdir(path):
            folder.append(path)
            folder_name.append(fl[i])
        elif path.endswith('.'+extension):
            files.append(path)
    return folder,folder_name,files
