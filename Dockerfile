FROM continuumio/miniconda3

RUN conda config --env --set always_yes true

COPY environment.yml .
RUN conda env create -f environment.yml

RUN echo "source activate dtop_site" >> ~/.bashrc
ENV PATH /opt/conda/envs/dtop_site/bin:$PATH

COPY . /app
WORKDIR /app

RUN pip install -e .

RUN conda install -c conda-forge nodejs==12.19.0
ENV NODE_OPTIONS=--max_old_space_size=8192
RUN npm install dredd@12.2.1 --global
