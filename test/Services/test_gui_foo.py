# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest

from dtop_site.Services import create_app


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
    })

    yield app


@pytest.fixture
def client(app):
    client = app.test_client()

    yield client


def test_foo_en(client):
    response = client.get('/gui/foo')
    assert b'Hello from gui foo!' in response.data
    assert b'Content of foo() is:' in response.data

def test_foo_fr(client):
    # Test French version of site.
    response = client.get('/gui/foo', headers=[('Accept-Language', 'fr')])
    assert b'Bonjour de gui foo!' in response.data
    assert b'Le contenu de foo () est:' in response.data