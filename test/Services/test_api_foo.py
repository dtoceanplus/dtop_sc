# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest

from dtop_site import Services


@pytest.fixture
def client():
    client = Services.create_app().test_client()

    yield client


def test_foo(client):
    response = client.get('/api/foo')
    assert response.data == b'toto'