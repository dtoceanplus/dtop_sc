.. _sc-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Site Characterisation tool. They are intended for those who are new to the tool.

In integrated mode this tool is the first to be called like Machine Characterisation. Therefore, is works the same way in standalone and in integrated mode.

Use Site Characterisation at low and medium complexity level in standalone mode
-------------------------------------------------------------------------------

If no study site is selected or no databases are available to import into the model, use the low and medium complexity level of the Site Characterisation module.

1.	If required, create a new complexity level 1 or 2 study, as described in section 2.2.

2.	Select the requested level of energy for the wave and current between low, medium and high

3.	Choose if a uniform bathymetry is required for the study
   - If not, the default database will be used
   - If it is required, enter the water depth in meters

4.	Click on “Run Module” to launch the computation
   - If it has not already been done, enter a name for your project in the “Save your inputs before running” pop up
   - Click on “Save” to save your project under the indicated name

5.	You can follow the progress of your project in the log section.

6.	If the project is successful, click on the “See Results” button to access the first results page “Overview”

7.	On the sidebar (on the left of the window), click on Waves, currents or 2D Maps to navigate to these pages.

8.	(Optional) Click on “Export results to PDF” to export all results pages in a single PDF document

Use Site Characterisation at high complexity level in standalone mode
---------------------------------------------------------------------

To perform a calculation on a given site using default or imported databases use the full complexity (level 3) version of the Site Characterisation module.

1.	If required, create a new complexity level 3 study, as described in section 2.2.

2.	Select a default example in the list or select “Import”
	- If “Import” is selected for the lease/corridor field, select a shapefile to import. The other required files (.shx, .prj, .dbf) will be extrapolated from the shapefile by the module.
	- If “Import” is selected for the Seabed Type, the Roughness Length or the Species fields, select a Netcdf file to import. The required construction of these files are detailed in the technical note.
	- If “Import” is selected for the Timeseries field, select a Netcdf file or a .csv file to import. The construction of these files is detailed in “How to” note.

3.	Choose if a uniform bathymetry is required for the study
	- If not, select a default example or “Import” to import a Netcdf file describing the bathymetry.
	- If it is required, enter the water depth in meters

4.	Click on “Run Module” to launch the calculation
	- If it has not already been done, enter a name for your project in the “Save your inputs before running” pop up
	- Click on “Save” to save your project under the indicated name

5.	You can follow the progress of your project in the log section.

6.	If the calculation is successful, click on the “See Results” button to access to the first results page “Overview”

7.	On the sidebar (on the left of the window), click on Waves, currents or 2D Maps to navigate to these pages. 

8.	(Optional) Click on “Export results to PDF” to export all results pages in a single PDF document
