.. _sc-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Site Characterisation module. 
These guides are intended for users who have previously completed all the :ref:`Site Characterisation tutorials <sc-tutorials>` and have a good knowledge of the features and workings of the Site Characterisation module. 
While the tutorials give an introduction to the basic usage of the module, these *how to guides* tackle slightly more advanced topics, such as how to use the results of a Site Characterisation assessment as inputs to the Structured Innovation module.
 
#. :ref:`sc-prepare-data-how-to`

.. toctree::
   :maxdepth: 1
   :hidden:

   sc_prepare_data