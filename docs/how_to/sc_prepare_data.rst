.. _sc-prepare-data-how-to:

How-to Guides
=============

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Site Characterisation module. 
These guides are intended for users who have previously completed all the :ref:`Site Characterisation tutorials <sc-tutorials>` and have a good knowledge of the features and workings of the Site Characterisation module. 
While the tutorials give an introduction to the basic usage of the module, these *how to guides* tackle slightly more advanced topics, such as how to use the results of a Site Characterisation assessment as inputs to the Structured Innovation module.

For complexity level 3, the user can enter their own files. If he does not have all the necessary input files for the SC module, he will also be able to use the DTOceanPlus databases of the previous 2 levels of complexity. 

In order to use his own data, the user must respect certain formats which are described below.
Note that all of the following files are georeferenced files that use the World Geodetic System 1984 (WGS84, EPSG:4326).

Geometries
----------
Geometries files, that is to say files describing the lease area and the corridor of your project, must be in shapefile format (https://en.wikipedia.org/wiki/Shapefile).

The easiest way to generate shapefiles for lease area and corridor (if you haven't your own files) is to use an open-source software like QGIS (https://qgis.org/en/site/forusers/download.html), and to follow a tutorial like this one: https://docs.qgis.org/2.14/en/docs/training_manual/create_vector_data/create_new_vector.html.

Note that the lease area must be a polygon shapefile (2D) and the corridor can be either a polygone or a polyline (at CPX1 only) shapefile.

Direct values formats
---------------------

Direct values databases must be in NetCDF format (https://www.unidata.ucar.edu/software/netcdf/).

Files are a structured matrix whose dimensions are longitude and latitude. Examples can be found in the Databases folder of the module. 

Possible names for the longitude variable are: `longitude`, `lon`, `x` or `X`.

Possible names for the latitude variable are: `latitude`, `lat`, `y` or `Y`.

Possible names for the bathymetry variable are: `H0`, `Band1`, `elevation`, `Bathymetry`, `DEPTH` or `depth`. Convention is positive values in the ocean, referenced to the mean sea level.

Possible names for the seabed type variable are: `seabed_type` or `sediment_type`.

Possible name for the roughness length variable is: `roughness_length`. 

User inputs in terms of endangered species are possible via the ESA tool graphical user interface.


Timeseries
----------

Temporal databases must be either in NetCDF format or in CSV format (https://en.wikipedia.org/wiki/Comma-separated_values).

For the NetCDF format, the file must contain all variables and dimensions of the variables are time,longitude,latitude.

For the CSV, the delimiter is the character “,” and all the variables must be in the same file.

For 1D timeseries, needed variables are the following (if a variable is missing, fill the column with zero “0”): `hs` (significant wave height), `t0m1` (wave energy period), `spr` (wave directional spreading), `fp` (wave peak frequency), `dp` (wave peak direction), `cge` (wave energy flux), `wlv` (water level fluctuation), `ucur` (zonal component of tidal current), `vcur` (meridional component of tidal current), `uwnd` (zonal component of 10m-wind), `vwnd` (meridional component of 10m-wind).

Figure 0.1 shows an example of CSV file that can be used in the SC module (1D timeseries).

.. figure:: ../images/inputs_csv_1D.png
    :width: 700px
    :align: center
    :height: 100px
    :figclass: align-center

    Figure 0.1: Example of CSV File for 1D timeseries.
..

For 2D timeseries, needed variables are the following (if a variable is missing, fill the column with zero “0”): `hs` (significant wave height), `fp` (wave peak frequency), `dp` (wave peak direction), `wlv` (water level fluctuation), `ucur` (zonal component of tidal current), `vcur` (meridional component of tidal current).
Note that in the case of using a 2D timeseries, you still have to provide a 1D timeseries for statistics computation.
Figure 0.2 shows an example of CSV file that can be used in the SC module (2D timeseries), which indicates that all the points (couple of longitude/latitude) must be specified at each time.

.. figure:: ../images/inputs_csv_2D.png
    :width: 700px
    :align: center
    :height: 150px
    :figclass: align-center

    Figure 0.2: Example of CSV File for 2D timeseries.
..