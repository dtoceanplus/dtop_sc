.. DTOcean+ SC documentation master file, created by
   sphinx-quickstart on Mon Nov  9 16:51:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _sc-home:

**********************
Site Characterisation
**********************

Introduction
============

The Site Characterisation module (SC) receives as input the environmental data of the chosen site and provides the user the main characteristics of this site in terms of bathymetry, seabed types, marine species, waves, tidal currents, winds and water levels. It includes time series of pertinent parameters as well as statistics on these parameters like probability distributions, scatter diagrams or extreme values.

Structure
=========

This module's documentation is divided into four main sections :

- :ref:`Tutorials <sc-tutorials>` that give step-by-step instructions on using Site Characterisation for new users.

- :ref:`How-to Guides <sc-how-to>` that show how to achieve specific outcomes using Site Characterisation.

- An :ref:`explanation <sc-explanation>` section gives technical background on how the tool works.

- The :ref:`API reference <sc-reference>` section documents the code of modules, classes, API, and GUI.

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/sc_prepare_data
   explanation/index
   reference/index

Functionalities
==================

The SC Module realises the following tasks:

- Database extractions: input databases are selected by the user, and the SC module reads all the input files and extracts the requested variables.

- Compute statistics based on the extractions: based on the extracted variables, a list of statistics is computed, from basic ones to multivariate extreme values analysis.

The complete execution time of the SC module (extraction + statistics computation) is approximately 10 minutes.

Workflow for using the Site Characterisation module
===================================================

The workflow for using the Site Characterisation module can be summarised as 1) provide inputs, 2) perform an assessment depending on the complexity level, and 3) view the results, as shown in the following figure.

.. image:: images/SC-process.svg
   :width: 600px
   :align: center

Overview of Site Characterisation data requirements
===================================================

This section summarises the types of input data required to run the Site Characterisation module. 
At complexity levels 1 and 2, the user can select an example site based on the level of wave and tidal energy (low/medium/high). 
At complexity level 3 the user can upload their own site data.


+--------------+----------------+----------------+-----------------+
| Section      | Complexity 1   | Complexity 2   | Complexity 3    |
+==============+================+================+=================+
| Waves        | Select level   | Select level   | Time series of  |
|              | of energy      | of energy      | all variables   |
+--------------+----------------+----------------+-----------------+
| Tidal        | Select level   | Select level   | Time series of  |
| current      | of energy      | of energy      | all variables   |
+--------------+----------------+----------------+-----------------+
| Bathymetry   | None or a      | None or a      |Constant value   |
|              | constant value | constant value |or a Netcdf file |
+--------------+----------------+----------------+-----------------+
| Lease Area   | None           | None           | Shapefile of    |
|              |                |                | the lease area  |
+--------------+----------------+----------------+-----------------+
| Export cable | None           | None           | Shapefile of    |
| Corridor     |                |                | the corridor    |
+--------------+----------------+----------------+-----------------+
| Seabed type  | None           | None           | Netcdf file     |
+--------------+----------------+----------------+-----------------+
| Roughness    | None           | None           | Netcdf file.    |
| length       |                |                | Expressed as    |
|              |                |                | :math:`z_0` in m|
+--------------+----------------+----------------+-----------------+
| Species      | None           | None           | Netcdf file.    |
|              |                |                | Expressed as %  |
+--------------+----------------+----------------+-----------------+

The format of each files is described into the :ref:`How-To Guides <sc-prepare-data-how-to>`.