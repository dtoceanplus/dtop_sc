.. _sc-explanation:

*********************
Background and theory
*********************

Databases
==========

Direct values
-------------
Some databases are fixed in time, this means that they were created at a given time and they do not include a temporal dimension. The values retrieved from these databases are called "direct values" (for example, the bathymetry or the bottom superficial sediment typology).

These databases and how their variables are extracted are presented :ref:`here <databases-explanation-DV>`.

Time Series
-----------

Some databases are time-dependent, that means that they include a temporal dimension with several time steps. The values retrieved from these databases are called "time series" (for example, the significant wave height or the tidal current magnitude).

For complexity levels 1 and 2, the time series are all extracted from the HOMERE database. Indeed, this database which currently covers the coasts of metropolitan France will soon be extended to Europe, from the North of Scotland to the South of Portugal, as part of the ResourceCode project (http://www.emec.org.uk/projects/ocean-energy-projects/tool-development/resourcecode-project/).

This database and how its variables are extracted are presented :ref:`here <databases-explanation-TS>`.

User databases
--------------

For complexity level 3, the user can enter their own files. If he does not have all the necessary input files for the SC module, he will also be able to use the DTOceanPlus databases of the previous 2 levels of complexity.

In order to use his own data, the user must respect certain formats which are described here: :ref:`sc-prepare-data-how-to`.


Statistics
==========

Statistics definitions
----------------------

**Basic Statistics**

Basic statistics are computed for every variable extracted from the databases presented in the previous section. Basic statistics include mean, min, max, median and std values, where:

* The mean is the average value of the timeseries, i.e. the sum of individual values over time divided by the number of individual values. 
* The min and max are respectively the lowest and the highest individual values of the timeseries.
* The median is a simple measure of central tendency. To find the median, the individual values are arranged in order from smallest to largest value. If there is an odd number of observations, the median is the middle value. If there is an even number of observations, the median is the average of the two middle values.
* The standard deviation std is a numerical value used to indicate how widely individuals in a group vary. If individual values vary greatly from the group mean, the standard deviation is big; and vice versa.

Figure 0.1 is provided to the user in order to have an overview of the basic statistics on a site.

.. figure:: ../images/basic_stats.png
    :width: 500px
    :align: center
    :height: 200px
    :figclass: align-center

    Figure 0.1: Overview of basic statistics at a given site.
..

**EPD**

EPD (Empirical Probability Distribution) represents the distribution of the variable, directly extracted from the database.
It shows the number of occurrence of the variable inside a range of bins.
In SC module, this statistic is also available broken down by months.

Figure 0.2 shows an example of an EPD statistic on Hs (significant wave height).

.. figure:: ../images/EPD.png
    :width: 700px
    :align: center
    :height: 400px
    :figclass: align-center

    Figure 0.2: Emprirical Probability Distribution of Hs at a given site.
..

**EJPD**

EJPD (Empirical Joint Probability Distribution) represents the distribution of two variables, considered together. 
It shows the number of occurrence inside bins.
In SC module, this statistic is also available broken down by months.

Figure 0.3 shows an example, provided to the user, of an EJPD statistic on Hs (significant wave height) and Tp (wave peak period).

.. figure:: ../images/EJPD.png
    :width: 600px
    :align: center
    :height: 400px
    :figclass: align-center

    Figure 0.3: Emprirical Joint Probability Distribution of Hs and Tp at a given site.
..

**ENVS**

This statistic computes the wave environments Hs/Dp in order to calculate the fatigue analysis in the module Station Keeping of the DTOcean+ suite.
It uses the statistic EJPD to jointly cut Hs and Tp by bins and then classifies the results from the most probable environment to the less probable one.
It also associates to each of these environments the mean wave peak period (Tp), the maximum current speed and its associated current direction, the maximum wind speed and its associated wind direction. 
More information is available in deliverable D5.6 “Station Keeping Tools - alpha version” available `here <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D5.6-Station-keeping-tools-alpha-version>`_.

**EXT**

EXT (EXTreme) statistic is based on an Extreme values analysis. It uses probabilistic laws to predict extreme events (also called extreme values, or return values) for a particular phenomenon, over large return periods that usually exceed the duration of the measured or modelled data.

There are two definitions of an extreme event, given by the two theorems of the extreme value theory.

* The Fisher-Tippett-Gnedenko theorem (known as the first theorem of the extreme value theory) states that the distribution of block maxima can be accurately described by a Generalized Extreme Values distribution (GEV).
* The Pickands-Balkema-de Haan theorem (known as the second theorem of the extreme value theory) states that the distribution of peaks over a threshold (POT) can be accurately described by a Generalized Pareto distribution (GPD).
In the SC module, GEV is used to compute the extreme values of tidal currents and the GPD is used to compute the extreme values of waves and winds.

Figure 0.4 is provided to the user in order to have an overview of the extreme values for each parameter on a site.

.. figure:: ../images/EXT.png
    :width: 500px
    :align: center
    :height: 200px
    :figclass: align-center

    Figure 0.4: Overview of extreme conditions at a given site.
..

**EXC**

EXC (Extreme Contours) is the statistics extreme contours, also known as environmental contour, this statistic represents a rational procedure for defining an extreme sea stat condition. The objective is to define contours in the environmental parameter space along which extreme responses with given return period should lie (Winterstein et al., 1993) (DNV-RP-C205, 3.7.2).

The extreme contours represent extreme conditions for the governing variable (in SC module, the significant wave height) and the expected associated value of a second variable (in SC module, the wave peak period). In SC module, extreme contours are computed following the IFORM approach (Figure 0.5).

.. figure:: ../images/EXC.png
    :width: 600px
    :align: center
    :height: 350px
    :figclass: align-center

    Figure 0.5: Extreme contours of Hs and Tp at a given site.
..

Statistics list
---------------

The following tables present all the statistics that are computed in the SC module.

.. csv-table:: Table 0.1: List of wave statistics computed in SC module.
   :header: "Wave variable", "units"
   :widths: 150, 50
   :align: center

   "hs basics (mean, min, max, median, std)", "m"
   "tp basics (mean, min, max, median, std)",	"s"
   "CgE basics (mean, min, max, median, std)",	"kW/m"
   "gamma basics (mean, min, max, median, std)",	 ""
   "spr basics (mean, min, max, median, std)",	"°"
   "EPD hs (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD hs (Empirical Probability Distribution)",	"occurrences"
   "EPD dp (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD dp (Empirical Probability Distribution)",	"occurrences"
   "EPD tp (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD tp (Empirical Probability Distribution)",	"occurrences"
   "EJPD hs/tp (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD hs/tp (Empirical Joint Probability Distribution)",	"occurrences"
   "EJPD hs/dp (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD hs/dp (Empirical Joint Probability Distribution)",	"occurrences"
   "EJPD3v hs/dp/tp (Empirical Joint Probability Distribution with 3 variables)",	"occurrences"
   "EXT hs (extreme return values)",	"m"
   "EXT tp (extreme return values)",	"s"
   "EXC hs/tp (multivariate extreme return values or contours)",	"m ; s"
..
.. csv-table:: Table 0.2: List of current statistics computed in SC module.
   :header: "Current variable", "units"
   :widths: 150, 50
   :align: center

   "mag basics (mean, min, max, median, std)",	"m/s"
   "Flux basics (mean, min, max, median, std)",	"W/m2"
   "Flux std",	"W/m2"
   "EPD mag (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD mag (Empirical Probability Distribution)",	"occurrences"
   "EPD theta (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD theta (Empirical Probability Distribution)",	"occurrences"
   "EJPD mag/theta (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD mag/theta (Empirical Joint Probability Distribution)",	"occurrences"
   "EXT mag (extreme return values)",	"m/s"
..
.. csv-table:: Table 0.3: List of wind statistics computed in SC module.
   :header: "Current variable", "units"
   :widths: 150, 50
   :align: center

   "mag10 basics (mean, min, max, median, std)",	"m/s"
   "EPD mag10 (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD mag10 (Empirical Probability Distribution)",	"occurrences"
   "EPD theta10 (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD theta10 (Empirical Probability Distribution)",	"occurrences"
   "EJPD mag10/theta10 (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD mag10/theta10 (Empirical Joint Probability Distribution)",	"occurrences"
   "EXT mag10 (extreme return values)",	"m/s"
   "EXT gust10 (extreme return values)", "m/s"
..
.. csv-table:: Table 0.4: List of water level statistics computed in SC module.
   :header: "Current variable", "units"
   :widths: 150, 50
   :align: center

   "WLEV basics (mean, min, max, median, std)",	"m"
   "EPD WLEV (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD WLEV (Empirical Probability Distribution)",	"occurrences"
   "EXT WLEVnegative (extreme return values)",	"m"
   "EXT WLEVpositive (extreme return values)",	"m"
..