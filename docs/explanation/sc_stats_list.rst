.. _statistics-explanation-list:

Statistics list
***************

The following tables present all the statistics that are computed in the SC module.

.. csv-table:: Table 0.1: List of wave statistics computed in SC module.
   :header: "Wave variable", "units"
   :widths: 150, 50

   "hs basics (mean, min, max, median, std)", "m"
   "tp basics (mean, min, max, median, std)",	"s"
   "CgE basics (mean, min, max, median, std)",	"kW/m"
   "gamma basics (mean, min, max, median, std)",	 ""
   "spr basics (mean, min, max, median, std)",	"°"
   "EPD hs (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD hs (Empirical Probability Distribution)",	"occurrences"
   "EPD dp (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD dp (Empirical Probability Distribution)",	"occurrences"
   "EPD tp (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD tp (Empirical Probability Distribution)",	"occurrences"
   "EJPD hs/tp (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD hs/tp (Empirical Joint Probability Distribution)",	"occurrences"
   "EJPD hs/dp (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD hs/dp (Empirical Joint Probability Distribution)",	"occurrences"
   "EJPD3v hs/dp/tp (Empirical Joint Probability Distribution with 3 variables)",	"occurrences"
   "EXT hs (extreme return values)",	"m"
   "EXT tp (extreme return values)",	"s"
   "EXC hs/tp (multivariate extreme return values or contours)",	"m ; s"
..
.. csv-table:: Table 0.2: List of current statistics computed in SC module.
   :header: "Current variable", "units"
   :widths: 150, 50

   "mag basics (mean, min, max, median, std)",	"m/s"
   "Flux basics (mean, min, max, median, std)",	"W/m2"
   "Flux std",	"W/m2"
   "EPD mag (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD mag (Empirical Probability Distribution)",	"occurrences"
   "EPD theta (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD theta (Empirical Probability Distribution)",	"occurrences"
   "EJPD mag/theta (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD mag/theta (Empirical Joint Probability Distribution)",	"occurrences"
   "EXT mag (extreme return values)",	"m/s"
..
.. csv-table:: Table 0.3: List of wind statistics computed in SC module.
   :header: "Current variable", "units"
   :widths: 150, 50

   "mag10 basics (mean, min, max, median, std)",	"m/s"
   "EPD mag10 (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD mag10 (Empirical Probability Distribution)",	"occurrences"
   "EPD theta10 (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD theta10 (Empirical Probability Distribution)",	"occurrences"
   "EJPD mag10/theta10 (Empirical Joint Probability Distribution)",	"occurrences"
   "monthly-EJPD mag10/theta10 (Empirical Joint Probability Distribution)",	"occurrences"
   "EXT mag10 (extreme return values)",	"m/s"
   "EXT gust10 (extreme return values)", "m/s"
..
.. csv-table:: Table 0.4: List of water level statistics computed in SC module.
   :header: "Current variable", "units"
   :widths: 150, 50

   "WLEV basics (mean, min, max, median, std)",	"m"
   "EPD WLEV (Empirical Probability Distribution)",	"occurrences"
   "monthly-EPD WLEV (Empirical Probability Distribution)",	"occurrences"
   "EXT WLEVnegative (extreme return values)",	"m"
   "EXT WLEVpositive (extreme return values)",	"m"
..