.. _databases-explanation:

Direct values
*************
Some databases are fixed in time, this means that they were created at a given time and they do not include a temporal dimension. The values retrieved from these databases are called "direct values" (for example, the bathymetry or the bottom superficial sediment typology).
These databases and how their variables are extracted are presented below. 

Databases
=========

This section presents the direct values databases used in the SC module.

GEBCO_2019:
-----------
GEBCO's aim is to provide the most authoritative publicly available bathymetry of the world's oceans. It operates under the joint auspices of the International Hydrographic Organization (IHO) and the Intergovernmental Oceanographic Commission (IOC) (of UNESCO).

The GEBCO_2019 Grid [GEBCO] is the latest global bathymetric product released by the General Bathymetric Chart of the Oceans (GEBCO) and has been developed through the Nippon Foundation-GEBCO Seabed 2030 Project. This is a collaborative project between the Nippon Foundation of Japan and GEBCO. The Seabed 2030 Project aims to bring together all available bathymetric data to produce the definitive map of the world ocean floor and make it available to all.

The Nippon Foundation of Japan is a non-profit philanthropic organisation active around the world. GEBCO is an international group of mapping experts developing a range of bathymetric data sets and data products, operating under the joint auspices of the International Hydrographic Organization (IHO) and UNESCO's Intergovernmental Oceanographic Commission (IOC).

The GEBCO_2019 product provides global coverage (Figure 0.1), spanning 89° 59' 52.5''N, 179° 59' 52.5''W to 89° 59' 52.5''S, 179° 59' 52.5''E on a 15 arc-second grid. It consists of 86400 rows x 43200 columns, giving 3,732,480,000 data points. The data values are pixel-center registered i.e. they refer to elevations at the center of grid cells.

.. figure:: ../images/gebco_2019_grid_image.jpg
    :width: 900px
    :align: center
    :height: 400px
    :figclass: align-center

    Figure 0.1: GEBCO_2019 OVERVIEW.
..

The variable extracted from this database is **H0**, the depth of the water, i.e. the height of water between the bottom and the local mean sea level. The unit associated with the variable is **m from MSL** (meters from the mean sea level) within the convention that values are positive in water.

Note that for reasons of disk space, the bathymetry present in the SC module has been degraded (only one mesh out of 20 has been retained).

World Sediment Map (SHOM):
--------------------------

The map of the world [Garlan et al, 2018] is initially based on a map of the oceans entitled Sedimentological Map of the World published by UNESCO, digitised by SHOM in 1995. This fairly coarse map is intended to provide basic information on the nature of all seabed. In the second step, this map was progressively improved by integrating more precise maps, produced by SHOM or digitized from published documents. This version of the world map is the third published version. It includes the cards listed at the end of the manual. These maps are integrated into the world map when their quality and the interest of their content motivated their integration and validation in the SHOM Sedimentological Database (BDSS), and when their scale is less than or equal to 1: 500,000 (Figure 0.2).

.. figure:: ../images/world_sediment_map.jpg
    :width: 900px
    :align: center
    :height: 400px
    :figclass: align-center

    Figure 0.2: World Sediment Map (source: SHOM).
..

The variable extracted from this database is the **type of sediment** (rocks, pebbles, sands, ...). There is **no unit** associated with this variable.

From this database, the macroscopic roughness length **z0** is also computed. To do this, we use Nikuradse's formula which says that the gross roughness is equal to 2.5 times the average diameter (D50) of the sediment. **z0** is given in **meters**.

Table X gives the correspondence between the type of bottom sediment and the macroscopic roughness length.

.. csv-table:: Table X: Bottom sediment types and roughness length
   :header: "Sediment type", "D50 [mm]", "Z0 [m]"
   :widths: 50, 50, 50

   "Rocks", 100, 0.250
   "Peebles", 50, 0.125
   "Gravels", 10, 0.025
   "Sands", 1, 0.0025
   "Fine sands", 0.1, 0.00025
   "Mud", 0.02, 0.00005
   "No data", 0, 0
..

Endangered species:
-------------------

A local database of maps of large-scale probability of presence for each species is integrated to Site Characterisation module. 

This database including the global geographical information of all species has been built from AquaMaps [Kaschner et al., 2016]. This collaborative project aims at producing computer-generated (and ultimately, expert reviewed) predicted global distribution maps for marine species on a 0.5 x 0.5-degree grid of the oceans. 

Models are constructed from estimates of the environmental tolerance of a given species with respect to depth, salinity, temperature, primary productivity, and its association with sea ice or coastal areas. Maps represent mean annual distributions of species and do not account for changes in species occurrence due to migration or unusual environmental events such as El Niño. They are based on data available through online species databases such as FishBase and SeaLifeBase and species occurrence records from OBIS or GBIF and using an environmental envelope model in conjunction with expert input.

More information is available in deliverable D6.5 “Environmental and Social Acceptance Tools - alpha version” available `here <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D6.5-Environmental-and-Social-Acceptance-Tools-alpha-version>`_.

The variable extracted from this database is the **probability of presence** of 26 endangered species listed in international and European conventions. The unit associated with the variable is **%**.


Extraction
==========

The databases we just mentioned are in `netCDF <https://www.unidata.ucar.edu/software/netcdf/>`_ format. NetCDF (Network Common Data Form) is a set of software libraries and machine-independent data formats that support the creation, access and sharing of array-oriented scientific data. It is also a community standard for sharing scientific data. The Unidata Program Center supports and maintains netCDF programming interfaces for C, C++, Java and Fortran. NetCDF format is self-describing, portable, scalable, appendable, sharable and archivable.

These netCDF files are stored in a folder of the SC module called “Databases” and are referenced through a Python dictionary.

Data extraction scripts refer to this dictionary according to the user's choices to read the files and extract the necessary variables. This extraction is carried out at the points of the site of interest thanks to the geo-referencing (longitudes and latitudes in WGS84) of the databases.

For certain variables such as H0 (bathymetry), a spatial interpolation is also carried out in order to take into account the points in the vicinity by achieving an average weighted by the distance to the point of interest.


Time Series
***********

Some databases are time-dependent, that means that they include a temporal dimension with several time steps. The values retrieved from these databases are called "time series" (for example, the significant wave height or the tidal current magnitude).

For complexity levels 1 and 2, the time series are all extracted from the HOMERE database. Indeed, this database which currently covers the coasts of metropolitan France will soon be extended to Europe, from the North of Scotland to the South of Portugal, as part of the ResourceCode project (http://www.emec.org.uk/projects/ocean-energy-projects/tool-development/resourcecode-project/).

This database and how its variables are extracted are presented below. 

Databases
=========

HOMERE database was performed by Ifremer using the numerical wave model WaveWatchIII® (WW3) version 4.09 [Boudière et al., 2013]. WW3 is a third-generation spectral wave model based on the conservation equation for the density of wave action. 

The propagation scheme used in this configuration is an explicit propagation for unstructured grid [Roland, 2008; Roland, 2009]. The use of unstructured meshes permits to adapt the grid resolution at different scales in the same computational domain, from the coastal zone (refined mesh of ~ 100-200m) to offshore (mesh ~ 10km). 

The time step is about 1 hour. It extends from 43.29°N to 52.90°N and from 8.54°W to 4.72°E and covers a period from 1994 to 2016. 

The setup used in this configuration for the generation and dissipation of waves [Ardhuin 2009; Ardhuin, 2010] is the one that was developed during the research project IOWAGA (Integrated Ocean Waves for Geophysical Applications) and tested in preparation mode in the context of operational demonstrator Previmer [Lecornu, 2008]. The evolution and nonlinear wave interactions are modelled by the DIA method (Discrete Interaction Approximation) [Hasselmann, 1985]. 

The simulated sea-state conditions were performed on a high-resolution bathymetry stretching from southern North Sea to the northern coast of Spain, covering the entire continental shelf of the Bay of Biscay. The bathymetry was obtained using data from the SHOM (Hydrographic and Oceanographic Service of the Navy) for the coastline and measurement campaigns conducted by IFREMER and SHOM for the entire field: 100m and 500m DTM [Loubrieu, 2008]. 

The wind fields used to force the model are from CFSR reanalysis (Climate Forecast System Reanalysis, [Saha, 2010]) conducted in 2010 by the NCEP (National Centres for Environmental Prediction). These wind fields were re-analysed over the period 1979-2009. Their spatial resolution varies from 0.25° at the equator up to 0.5° higher latitudes. 

Currents, water levels and storm surges were calculated using the hydrodynamic code MARS2D (Model for Applications at Regional Scale). MARS2D is a model developed by IFREMER [Lazure, 2008] and based on shallow water equations. It consists of seven nested models whose resolution differs according to rank (ranks 0, 1 and 2). 

Data from Météo-France were used as meteorological forcing for the model MARS2D. Ranks 0 and 1 are forced using data from meteorological model ARPEGE 0.5° [Broker, 1991; Broker 1994] with a 6-hours’ time step. Models of rank 2, with higher resolution, are forced with data from the meteorological model AROME 0.025° [Seity, 2011] with a 1-hour time step. 

To make easier the processing of ocean current data and water levels used as inputs by the wave model, an atlas of harmonic components was computed. A replay of tide data was performed over one year (2008) and an analysis of harmonic components of the tide for each of the seven models too. Tides and tidal currents can thus be evaluated for each year over the entire field. Tidal harmonics and water levels are updated every 30 minutes and are interpolated onto the wave model mesh. 

A large set of in situ data from various sources, including ocean surveys is at disposal for validation purpose. 

Comparison was made with data from the Cetmef CANDHIS buoys network and Météo-France buoys along the French coast. 

From this database, several variables are extracted: 
* Waves: significant wave height (Hs), wave peak period (Tp), wave peak direction (Dp), wave energy period (T0m1 or Te), wave energy flux (CgE);
* Tidal currents: zonal component of tidal current (Ucur) and meridional component of tidal current (Vcur);
* Winds: zonal component of wind speed (Uwnd) and meridional component of wind speed (Vwnd);
* Water levels: water level fluctuations (Wlev).

For complexity level 1, variables are extracted in 1D, just at the location of interest. For complexity level 2, variables are extracted in 2D, the closest points of the database which are in the area of interest (i.e. the lease area of the project) are extracted.

From the time series database described above, sites representing different levels of energy for waves and currents have been defined; this procedure is described in the following section.

Levels of energy
================

For the first 2 levels of complexity, the user chooses his site according to the energy levels of the waves and currents he wishes. He can choose 3 energy levels (Low, Medium and High) for waves and the same for currents. There are therefore 9 (3x3) sites representative of the energy levels that are available for the first 2 levels of complexity.

Figure 0.3 shows the 9 sites (HOMERE database) that were selected.

.. figure:: ../images/representative_points.png
    :width: 800px
    :align: center
    :height: 580px
    :figclass: align-center

    Figure 0.3: Representative points for the 2 first levels of complexity.
..

At complexity level 1, the extractions are carried out at 1 point on the site (the center of the lease area).

At complexity level 2, the extractions are performed at several points in the 2D database which are located in the lease area. Figure 0.4 shows the points of the HOMERE database (black circles) around a point of interest and also the lease area and the corridor related to this site.
Indeed, for the first 2 levels of complexity, the lease areas and the corridors are automatically defined. For complexity level 3, the user will enter their lease area and their corridor in the form of a shapefile file (ESRI).

For the last level of complexity (level 3), the user will be able to upload their own databases (anywhere in the world), in one or two dimensions (see section :ref:`sc-prepare-data-how-to`). Note that the 2-dimensional databases offer more details to the calculations performed by the other modules of the DTOcean+ suite, especially in terms of device placement.

.. figure:: ../images/site_level2.png
    :width: 800px
    :align: center
    :height: 580px
    :figclass: align-center

    Figure 0.4: Example of site for complexity level 2.
..

Extraction
==========

The databases that we mentioned previously consist of monthly or annual files in netCDF format.

The SC module will therefore extract these files, whatever their number, by referring to a template defined in a Python dictionary (easily accessible for any code evolution).

In the case of 2D databases, a mesh regrid is performed in order to obtain a regular grid on the right of way of the lease area. The mesh size obtained at the end of this regrid is optimized so as not to degrade the input data.

Timeseries list
===============

