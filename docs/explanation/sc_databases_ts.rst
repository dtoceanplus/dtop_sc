.. _databases-explanation-TS:

Time Series
***********

Databases
=========

HOMERE database was performed by Ifremer using the numerical wave model WaveWatchIII® (WW3) version 4.09 [Boudière et al., 2013]. WW3 is a third-generation spectral wave model based on the conservation equation for the density of wave action. 

The propagation scheme used in this configuration is an explicit propagation for unstructured grid [Roland, 2008; Roland, 2009]. The use of unstructured meshes permits to adapt the grid resolution at different scales in the same computational domain, from the coastal zone (refined mesh of ~ 100-200m) to offshore (mesh ~ 10km). 

The time step is about 1 hour. It extends from 43.29°N to 52.90°N and from 8.54°W to 4.72°E and covers a period from 1994 to 2016. 

The setup used in this configuration for the generation and dissipation of waves [Ardhuin 2009; Ardhuin, 2010] is the one that was developed during the research project IOWAGA (Integrated Ocean Waves for Geophysical Applications) and tested in preparation mode in the context of operational demonstrator Previmer [Lecornu, 2008]. The evolution and nonlinear wave interactions are modelled by the DIA method (Discrete Interaction Approximation) [Hasselmann, 1985]. 

The simulated sea-state conditions were performed on a high-resolution bathymetry stretching from southern North Sea to the northern coast of Spain, covering the entire continental shelf of the Bay of Biscay. The bathymetry was obtained using data from the SHOM (Hydrographic and Oceanographic Service of the Navy) for the coastline and measurement campaigns conducted by IFREMER and SHOM for the entire field: 100m and 500m DTM [Loubrieu, 2008]. 

The wind fields used to force the model are from CFSR reanalysis (Climate Forecast System Reanalysis, [Saha, 2010]) conducted in 2010 by the NCEP (National Centres for Environmental Prediction). These wind fields were re-analysed over the period 1979-2009. Their spatial resolution varies from 0.25° at the equator up to 0.5° higher latitudes. 

Currents, water levels and storm surges were calculated using the hydrodynamic code MARS2D (Model for Applications at Regional Scale). MARS2D is a model developed by IFREMER [Lazure, 2008] and based on shallow water equations. It consists of seven nested models whose resolution differs according to rank (ranks 0, 1 and 2). 

Data from Météo-France were used as meteorological forcing for the model MARS2D. Ranks 0 and 1 are forced using data from meteorological model ARPEGE 0.5° [Broker, 1991; Broker 1994] with a 6-hours’ time step. Models of rank 2, with higher resolution, are forced with data from the meteorological model AROME 0.025° [Seity, 2011] with a 1-hour time step. 

To make easier the processing of ocean current data and water levels used as inputs by the wave model, an atlas of harmonic components was computed. A replay of tide data was performed over one year (2008) and an analysis of harmonic components of the tide for each of the seven models too. Tides and tidal currents can thus be evaluated for each year over the entire field. Tidal harmonics and water levels are updated every 30 minutes and are interpolated onto the wave model mesh. 

A large set of in situ data from various sources, including ocean surveys is at disposal for validation purpose. 

Comparison was made with data from the Cetmef CANDHIS buoys network and Météo-France buoys along the French coast. 

From this database, several variables are extracted: 

* Waves: significant wave height (Hs), wave peak period (Tp), wave peak direction (Dp), wave energy period (T0m1 or Te), wave energy flux (CgE);
* Tidal currents: zonal component of tidal current (Ucur) and meridional component of tidal current (Vcur);
* Winds: zonal component of wind speed (Uwnd) and meridional component of wind speed (Vwnd);
* Water levels: water level fluctuations (Wlev).

For complexity level 1, variables are extracted in 1D, just at the location of interest. For complexity level 2, variables are extracted in 2D, the closest points of the database which are in the area of interest (i.e. the lease area of the project) are extracted.

From the time series database described above, sites representing different levels of energy for waves and currents have been defined; this procedure is described in the following section.

Levels of energy
================

For the first 2 levels of complexity, the user chooses his site according to the energy levels of the waves and currents he wishes. He can choose 3 energy levels (Low, Medium and High) for waves and the same for currents. There are therefore 9 (3x3) sites representative of the energy levels that are available for the first 2 levels of complexity.

Figure 0.3 shows the 9 sites (HOMERE database) that were selected.

.. figure:: ../images/representative_points.png
    :width: 800px
    :align: center
    :height: 580px
    :figclass: align-center

    Figure 0.3: Representative points for the 2 first levels of complexity.
..

At complexity level 1, the extractions are carried out at 1 point on the site (the center of the lease area).

At complexity level 2, the extractions are performed at several points in the 2D database which are located in the lease area. Figure 0.4 shows the points of the HOMERE database (black circles) around a point of interest and also the lease area and the corridor related to this site.
Indeed, for the first 2 levels of complexity, the lease areas and the corridors are automatically defined. For complexity level 3, the user will enter their lease area and their corridor in the form of a shapefile file (ESRI).

For the last level of complexity (level 3), the user will be able to upload their own databases (anywhere in the world), in one or two dimensions (see section :ref:`sc-prepare-data-how-to`). Note that the 2-dimensional databases offer more details to the calculations performed by the other modules of the DTOcean+ suite, especially in terms of device placement.

.. figure:: ../images/site_level2.png
    :width: 800px
    :align: center
    :height: 580px
    :figclass: align-center

    Figure 0.4: Example of site for complexity level 2.
..

Extraction
==========

The databases that we mentioned previously consist of monthly or annual files in netCDF format.

The SC module will therefore extract these files, whatever their number, by referring to a template defined in a Python dictionary (easily accessible for any code evolution).

In the case of 2D databases, a mesh regrid is performed in order to obtain a regular grid on the right of way of the lease area. The mesh size obtained at the end of this regrid is optimized so as not to degrade the input data.

Timeseries list
===============

Tables 0.2 to 0.5 present the list of timeseries that are extracted from the 2D databases.

.. csv-table:: Table 0.2: List of wave timeseries extracted from 2D databases.
   :header: "Wave variable", "units"
   :widths: 150, 50

   "hs (significant wave height)", "m"
   "tp (wave peak period)", "s"
   "dp (wave peak direction, coming from)",	"°"
   "te (wave energy period)", "s"
   "CgE (wave energy flux)",	"kW/m"
   "gamma (JONSWAP peak shape parameter)", " "	 
   "spr (wave directional spreading)",	"°"
..
.. csv-table:: Table 0.3: List of tidal current timeseries extracted from 2D databases.
   :header: "Current variable", "units"
   :widths: 150, 50

   "mag (current velocity)",	"m/s"
   "theta (current direction, going to)",	"°"
   "U (current zonal velocity)",	"m/s"
   "V (current meridional velocity)",	"m/s"
   "Cp (current available power)",	"W/m2"
..
.. csv-table:: Table 0.4: List of wind timeseries extracted from 2D databases.
   :header: "Wind variable", "units"
   :widths: 150, 50
   
   "mag10 (10m-wind velocity)",	"m/s"
   "theta10 (10m-wind direction, coming from)",	"°"
   "U10 (10m-wind zonal velocity)",	"m/s"
   "V10 (10m-wind meridional velocity)", "m/s"
   "gust10 (10m-wind gusts)", "m/s"
..
.. csv-table:: Table 0.5: List of water level timeseries extracted from 2D databases.
   :header: "Water level variable", "units"
   :widths: 150, 50
   
   "XE (water surface fluctuation, relative to MSL)",	"m"
   "WLEV (water level, relative to bottom => XE + bathymetry)",	"m"
..

