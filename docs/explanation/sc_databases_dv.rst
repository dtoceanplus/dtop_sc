.. _databases-explanation-DV:

Direct values
*************

Databases
=========

This section presents the direct values databases used in the SC module.

GEBCO_2019:
-----------
GEBCO's aim is to provide the most authoritative publicly available bathymetry of the world's oceans. It operates under the joint auspices of the International Hydrographic Organization (IHO) and the Intergovernmental Oceanographic Commission (IOC) (of UNESCO).

The GEBCO_2019 Grid [GEBCO] is the latest global bathymetric product released by the General Bathymetric Chart of the Oceans (GEBCO) and has been developed through the Nippon Foundation-GEBCO Seabed 2030 Project. This is a collaborative project between the Nippon Foundation of Japan and GEBCO. The Seabed 2030 Project aims to bring together all available bathymetric data to produce the definitive map of the world ocean floor and make it available to all.

The Nippon Foundation of Japan is a non-profit philanthropic organisation active around the world. GEBCO is an international group of mapping experts developing a range of bathymetric data sets and data products, operating under the joint auspices of the International Hydrographic Organization (IHO) and UNESCO's Intergovernmental Oceanographic Commission (IOC).

The GEBCO_2019 product provides global coverage (Figure 0.1), spanning 89° 59' 52.5''N, 179° 59' 52.5''W to 89° 59' 52.5''S, 179° 59' 52.5''E on a 15 arc-second grid. It consists of 86400 rows x 43200 columns, giving 3,732,480,000 data points. The data values are pixel-center registered i.e. they refer to elevations at the center of grid cells.

.. figure:: ../images/gebco_2019_grid_image.jpg
    :width: 900px
    :align: center
    :height: 400px
    :figclass: align-center

    Figure 0.1: GEBCO_2019 OVERVIEW.
..

The variable extracted from this database is **H0**, the depth of the water, i.e. the height of water between the bottom and the local mean sea level. The unit associated with the variable is **m from MSL** (meters from the mean sea level) within the convention that values are positive in water.

Note that for reasons of disk space, the bathymetry present in the SC module has been degraded (only one mesh out of 20 has been retained).

World Sediment Map (SHOM):
--------------------------

The map of the world [Garlan et al, 2018] is initially based on a map of the oceans entitled Sedimentological Map of the World published by UNESCO, digitised by SHOM in 1995. This fairly coarse map is intended to provide basic information on the nature of all seabed. In the second step, this map was progressively improved by integrating more precise maps, produced by SHOM or digitized from published documents. This version of the world map is the third published version. It includes the cards listed at the end of the manual. These maps are integrated into the world map when their quality and the interest of their content motivated their integration and validation in the SHOM Sedimentological Database (BDSS), and when their scale is less than or equal to 1: 500,000 (Figure 0.2).

.. figure:: ../images/world_sediment_map.jpg
    :width: 900px
    :align: center
    :height: 400px
    :figclass: align-center

    Figure 0.2: World Sediment Map (source: SHOM).
..

The variable extracted from this database is the **type of sediment** (gravels, pebbles, sands, clays, ...). There is **no unit** associated with this variable.

From this database, the macroscopic roughness length **z0** is also computed. To do this, we use Nikuradse's formula which says that the gross roughness is equal to 2.5 times the average diameter (D50) of the sediment. **z0** is given in **meters**.

Table 0.1 gives the correspondence between the type of bottom sediment and the macroscopic roughness length.

.. csv-table:: Table 0.1: Bottom sediment types and roughness length
   :header: "Sediment type", "D50 [mm]", "Z0 [m]"
   :widths: 50, 50, 50

   "Gravels, Peebles", 20, 0.050
   "Very dense sands", 2, 0.005
   "Dense sands", 1, 0.0025
   "Medium dense sands", 0.5, 0.00125
   "Loose sands", 0.1, 0.00025
   "Very loose sands", 0.1, 0.00025
   "Hard clay", 0.05, 0.000125
   "Very stiff clay", 0.05, 0.000125
   "Stiff clay", 0.05, 0.000125
   "Firm clay", 0.02, 0.00005
   "Soft clay", 0.01, 0.000025
   "Very soft clay", 0.01, 0.000025
   "No data", 0, 0
..

Endangered species:
-------------------

A local database of maps of large-scale probability of presence for each species is integrated to Site Characterisation module. 

This database including the global geographical information of all species has been built from AquaMaps [Kaschner et al., 2016]. This collaborative project aims at producing computer-generated (and ultimately, expert reviewed) predicted global distribution maps for marine species on a 0.5 x 0.5-degree grid of the oceans. 

Models are constructed from estimates of the environmental tolerance of a given species with respect to depth, salinity, temperature, primary productivity, and its association with sea ice or coastal areas. Maps represent mean annual distributions of species and do not account for changes in species occurrence due to migration or unusual environmental events such as El Niño. They are based on data available through online species databases such as FishBase and SeaLifeBase and species occurrence records from OBIS or GBIF and using an environmental envelope model in conjunction with expert input.

More information is available in deliverable D6.5 “Environmental and Social Acceptance Tools - alpha version” available `here <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D6.5-Environmental-and-Social-Acceptance-Tools-alpha-version>`_.

The variable extracted from this database is the **probability of presence** of 26 endangered species listed in international and European conventions. The unit associated with the variable is **%**.


Extraction
==========

The databases we just mentioned are in `netCDF <https://www.unidata.ucar.edu/software/netcdf/>`_ format. NetCDF (Network Common Data Form) is a set of software libraries and machine-independent data formats that support the creation, access and sharing of array-oriented scientific data. It is also a community standard for sharing scientific data. The Unidata Program Center supports and maintains netCDF programming interfaces for C, C++, Java and Fortran. NetCDF format is self-describing, portable, scalable, appendable, sharable and archivable.

These netCDF files are stored in a folder of the SC module called “Databases” and are referenced through a Python dictionary.

Data extraction scripts refer to this dictionary according to the user's choices to read the files and extract the necessary variables. This extraction is carried out at the points of the site of interest thanks to the geo-referencing (longitudes and latitudes in WGS84) of the databases.

For certain variables such as H0 (bathymetry), a spatial interpolation is also carried out in order to take into account the points in the vicinity by achieving an average weighted by the distance to the point of interest.

