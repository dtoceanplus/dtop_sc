# -*- coding: utf-8 -*-
"""
Created on Thu Jun  6 10:09:26 2019

@author: ykervell
"""

import setuptools

setuptools.setup(
    name="dtop_site",
    version="0.0.3",
    author="Youen Kervella",
    author_email="youen.kervella@france-energies-marines.org",
    description="Site Characterisation module package",
    long_description="DTOceanPlus Project: Site Characterisation module package",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fem-dtocean/dtop-site",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: Microsoft :: Windows :: Windows 10",
    ],
)