all: clean pytest dredd

site-base:
	docker build --tag site-base .

rm-dist:
	rm -rf ./dist

create-dist:
	python setup.py sdist

x-build: rm-dist create-dist

build: site-base x-build

site-deploy:
	docker build --cache-from=site-deploy --tag site-deploy .

site-pytest: site-deploy
	docker build --tag site-pytest .

pytest: site-pytest
	docker run --name site-pytest-cont site-pytest make --file test.makefile pytest
	docker cp site-pytest-cont:/app/report.xml .
	docker container rm site-pytest-cont

pytest-mb: site-deploy
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	site pytest mc sc

shell-pytest: site-pytest
	docker run --tty --interactive site-pytest bash

dredd: site-deploy
	touch ./dredd-hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose-test.yml down
	docker-compose --file docker-compose.yml --file docker-compose-test.yml up \
	--build --abort-on-container-exit --exit-code-from=dredd dredd

clean:
	rm -fr dist
	docker image rm --force site-base site-deploy site-pytest

cypress-run:
	docker-compose --file docker-compose.yml --file docker-compose-test.yml down
	docker-compose --file docker-compose.yml --file docker-compose-test.yml up --detach sc gui nginx e2e-cypress
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 gui:8080
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 sc:5000
	docker exec e2e-cypress npx cypress run
	docker exec e2e-cypress npx nyc report --reporter=text-summary


## !
## Set the required module nickname
MODULE_SHORT_NAME=sc

## !
## Set the required docker image TAG
MODULE_TAG=v1.5.2

## !
# Set the required MODULE_DB_INIT value that defines a necessity to initialize module db or data files
#   1 - reinitialize and repopulate db or data files with initial data
#   0 - reuse the current database or data files created after the previous deployment
# ! make sure that MODULE_DB_INIT = 1 for initial deployment
MODULE_DB_INIT=1
#MODULE_DB_INIT=0

## !
## Update the values of CI_REGISTRY_IMAGE to your module registry
CI_REGISTRY_IMAGE?=registry.gitlab.com/dtoceanplus/dtop_sc

## !
## Set the value to cors urls
ALL_MODULES_NICKNAMES := si sg sc mc ec et ed sk lmo spey rams slc esa cm mm
CORS_URLS := $(foreach module,${ALL_MODULES_NICKNAMES},http://${module}.${DTOP_DOMAIN},https://${module}.${DTOP_DOMAIN},)

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_BASIC_AUTH="${DTOP_BASIC_AUTH}" --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
		--cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--file ./src/dtop_site/GUI/frontend-prod.dockerfile \
		./src/dtop_site
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images
