FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt .
COPY setup.py .
COPY storage/ ./storage/
COPY src/dtop_site/ ./src/dtop_site/
RUN mkdir -p ./databases

RUN apt-get update && \
    apt-get install --yes --no-install-recommends build-essential liblapacke-dev && \
    pip install --requirement requirements.txt && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes build-essential liblapacke-dev

ENV FLASK_APP src.dtop_site.Services

EXPOSE 5000
