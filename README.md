# Context

This project is dedicated to the DTOceanPlus Site Characterisation module.
At this time, it is divided in 2 sections:
- Documents (technical documents, mainly for the DTO+ sharepoint)
- src/dtop_site (source code, divided in BusinessLogic (~backend), Services (~API) and GUI )

# Documents

* Site Characterisation Technical Note V0.1: template from frédéric Pons 
* Site Characterisation Technical Note V0.2: on going 
* Site Characterisation Meshing Tool Technical Note V0.1: document for OCC about Meshing Tool developments

# Before the launching of the SC module
## Cloning of the Gitlab repository 

Clone the gitlab repository of Site Characterisation with the following command: 

```python
git clone https://gitlab.com/fem-dtocean/dtop-site.git
``` 

(you can also use a software like Github desktop). 

## Download of the SC databases 
### Main 

Download the main Site Characterisation database at: https://tecnalia365.sharepoint.com/:u:/r/sites/t.extranet/sp064811/Documentos%20compartidos/DTOceanPlus_ReadWrite/WP5/T5.3/Database/MAIN/SiteCharacterisation_Main-Database.zip?csf=1&web=1&e=Lv9G27

Unzip the file and follow the readme.md. 

### User 

Download all the User Databases at: https://tecnalia365.sharepoint.com/:f:/r/sites/t.extranet/sp064811/Documentos%20compartidos/DTOceanPlus_ReadWrite/WP5/T5.3/Database/USER?csf=1&web=1&e=8XsiWp

Unzip the file and follow the readme.md. 

## Install and run Docker Desktop
Go to https://docs.docker.com/get-docker/ and choose the installer for your platform. Follow the instruction and run Docker.
Open Docker and verify that it is running (on the bottom left part). Open the settings and increase the resources memory to 2.50 GB .

# To launch the SC module with Docker

Open a terminal. Go to the main directory dtop-site and launch Docker:
```python
docker-compose up
```
The graphical user interface will open in your browser and you can then follow the steps of the user cases.


# To launch the SC module (by hand)
Create a virtual environment to isolate the application development:
```python
conda create -n envtest
```
Activate this environment: 
```python
conda activate envtest
```
Install the package geopandas (shapefile manipulation):
```python
conda install -c conda-forge/label/cf201901 geopandas
```
Install the package peakutils (extreme statistics):
```python
conda install -c conda-forge peakutils
```
Install the package psutil (acess to system and process):
```python
conda install psutil
```
Install the package plotly-orca (to plot the results):
```python
conda install -c plotly plotly-orca
```
Install the pip installer:
```python
conda install pip
```
Install the required packages:
```python
pip install flask flask-cors python-dotenv Flask-Babel netCDF4 matplotlib lmoments3 viroconcom plotly==4.8.1 utm xarray networkx -U scikit-learn requests
```
Note that lmoments3 can lead to a versioning problem because of the use of the package scipy.misc.
A workaround is to change the `import scipy.misc` by `import scipy.special` in the init file: `/opt/anaconda3/envs/NEWENV/lib/python3.6/site-packages/lmoments3/__init__.py`

Finally, go to the dtop-site folder and install the package dtop_site:
```python
pip install -e .
```

You can now launch the application (for the first try, you have to download the databases before, see below):
```python
flask.exe run
```
and visualise the result at the adress: http://localhost:5000/

NOTE: you can have a problem in the computation of extreme values. In fact, in some versions of the packages lmoments3, scipy.misc is deprecated (you have to replace by scipy.special).


# src/dtop_site (in progress)

## BusinessLogic:

The figure Business_Logic_archi.png presents the architecture of the SC module's Business Logic.

### new script and versioning

If you want to test a new script in API, you must update your package:

* Local:

Remove your package (generally, in C:\Users\..\AppData\..) and the dist folder, and type the following commands:

```python
python.exe setup.py sdist dbist_wheel # create package
pip install dtop_site  --find-links dist # install package from dist
```


and run Flask to test:
```python
flask.exe run
```

* on test.pypi:


First, edit your setup.py with a new version number.

Then, remove your package (generally, in C:\Users\..\AppData\..) and the dist folder, and type the following commands:

```python
python.exe setup.py sdist bdist_wheel # create package
python.exe -m twine upload --repository-url https://test.pypi.org/legacy/ .\dist\* # upload the new package
pip install --index-url https://test.pypi.org/simple/ --no-deps dtop_site # download this package
```

and run Flask to test:
```python
flask.exe run
```



## Services:

The Services folder contains source code related to gui and api.

## Testing API

First, edit the file index.html in /src/dtop_site/Services/templates.

Then, in a console (Anaconda PowerShell Prompt for example), launch the command
```python
flask.exe run
```

You can visualise the result at the adress: http://localhost:5000/


# Documentation

Documentation for the full suite of tools is available at the temporary link; https://wave-energy-scotland.gitlab.io/dtoceanplus/dtop_documentation/deployment/sc/docs/index.html.
This documentation will be moved to a permanent address upon the final release of the suite of tools. 




